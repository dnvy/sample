package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Kết chuyển.
 */
@Entity
public class AccountTransfer {

    private Integer id; // PK Tài khoản kết chuyển
    private String accountTransferCode; // Mã kết chuyển
    private Integer transferOrder; // Thứ tự kết chuyển
    private String fromAccount; // Từ Tài khoản
    private String toAccount; // Đến Tài khoản
    private Integer transferSide; // Bên kết chuyển: 0: Nợ, 1: Có, 2: Hai bên
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạn thái Theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer setupType; // 0: Kết chuyển chi phí sản xuất; 1: kết chuyển xác định KQKD

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountTransferCode")
    public String getAccountTransferCode() {
        return accountTransferCode;
    }

    public void setAccountTransferCode(String accountTransferCode) {
        this.accountTransferCode = accountTransferCode;
    }

    @Basic
    @Column(name = "TransferOrder")
    public Integer getTransferOrder() {
        return transferOrder;
    }

    public void setTransferOrder(Integer transferOrder) {
        this.transferOrder = transferOrder;
    }

    @Basic
    @Column(name = "FromAccount")
    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    @Basic
    @Column(name = "ToAccount")
    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    @Basic
    @Column(name = "TransferSide")
    public Integer getTransferSide() {
        return transferSide;
    }

    public void setTransferSide(Integer transferSide) {
        this.transferSide = transferSide;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "SetupType")
    public Integer getSetupType() {
        return setupType;
    }

    public void setSetupType(Integer setupType) {
        this.setupType = setupType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountTransfer that = (AccountTransfer) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountTransferCode, that.accountTransferCode) &&
                Objects.equals(transferOrder, that.transferOrder) &&
                Objects.equals(fromAccount, that.fromAccount) &&
                Objects.equals(toAccount, that.toAccount) &&
                Objects.equals(transferSide, that.transferSide) &&
                Objects.equals(description, that.description) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(setupType, that.setupType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountTransferCode, transferOrder, fromAccount, toAccount, transferSide, description, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, setupType);
    }
}
