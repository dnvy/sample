package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class InvType {

    private Integer id;
    private String invTypeCode;
    private String invTypeName;
    private String invoiceTemplatePrefix;
    private Integer invoiceType;
    private Boolean activeStatus;
    private Boolean isSystem;
    private Timestamp modifiedDate;
    private Timestamp createdDate;
    private String createdBy;
    private String modifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "InvTypeCode")
    public String getInvTypeCode() {
        return invTypeCode;
    }

    public void setInvTypeCode(String invTypeCode) {
        this.invTypeCode = invTypeCode;
    }

    @Basic
    @Column(name = "InvTypeName")
    public String getInvTypeName() {
        return invTypeName;
    }

    public void setInvTypeName(String invTypeName) {
        this.invTypeName = invTypeName;
    }

    @Basic
    @Column(name = "InvoiceTemplatePrefix")
    public String getInvoiceTemplatePrefix() {
        return invoiceTemplatePrefix;
    }

    public void setInvoiceTemplatePrefix(String invoiceTemplatePrefix) {
        this.invoiceTemplatePrefix = invoiceTemplatePrefix;
    }

    @Basic
    @Column(name = "InvoiceType")
    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getSystem() {
        return isSystem;
    }

    public void setSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvType invType = (InvType) o;
        return Objects.equals(id, invType.id) &&
                Objects.equals(invTypeCode, invType.invTypeCode) &&
                Objects.equals(invTypeName, invType.invTypeName) &&
                Objects.equals(invoiceTemplatePrefix, invType.invoiceTemplatePrefix) &&
                Objects.equals(invoiceType, invType.invoiceType) &&
                Objects.equals(activeStatus, invType.activeStatus) &&
                Objects.equals(isSystem, invType.isSystem) &&
                Objects.equals(modifiedDate, invType.modifiedDate) &&
                Objects.equals(createdDate, invType.createdDate) &&
                Objects.equals(createdBy, invType.createdBy) &&
                Objects.equals(modifiedBy, invType.modifiedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, invTypeCode, invTypeName, invoiceTemplatePrefix, invoiceType, activeStatus, isSystem, modifiedDate, createdDate, createdBy, modifiedBy);
    }
}
