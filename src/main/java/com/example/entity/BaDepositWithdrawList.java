package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "BADepositWithdrawList")
public class BaDepositWithdrawList {

    private Integer id;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cabaRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cabaPostedDate;

    private Integer refType;
    private String refNoFinance;
    private String refNoManagement;
    private String cabaRefNoManagement;
    private String cabaRefNoFinance;
    private Integer fromBankAccountId;
    private String fromBankAccountName;
    private Boolean isFreightService;
    private Boolean isPostedFinance;
    private Boolean isPostedManagement;
    private Integer toBankAccountId;
    private Integer includeInvoice;
    private Integer reasonTypeId;
    private String toBankAccountName;
    private Integer puInvoiceRefId;
    private Integer accountObjectId;
    private String accountObjectName;
    private String accountObjectAddress;
    private String accountObjectBankAccount;
    private String accountObjectContactIdNumber;
    private String accountObjectBankName;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date accountObjectContactIssueDate;

    private String accountObjectContactIssueBy;
    private String accountObjectContactName;
    private String identificationNumber;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date issueDate;

    private String issueBy;
    private String receiver;
    private Integer branchId;
    private String journalMemo;
    private Integer employeeId;
    private String documentIncluded;
    private String cabaJournalMemo;
    private String cabaDocumentIncluded;
    private Integer bankAccountId;
    private String bankName;
    private Integer paymentTermId;
    private Integer dueTime;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date paymentDate;

    private Integer currencyId;
    private BigDecimal exchangeRate;
    private BigDecimal totalAmountOc;
    private BigDecimal totalAmount;
    private BigDecimal totalImportTaxAmountOc;
    private BigDecimal totalImportTaxAmount;
    private BigDecimal totalVatAmountOc;
    private BigDecimal totalVatAmount;
    private BigDecimal totalDiscountAmountOc;
    private BigDecimal totalDiscountAmount;
    private BigDecimal totalFreightAmount;
    private BigDecimal totalInwardAmount;
    private BigDecimal totalSpecialConsumeTaxAmountOc;
    private BigDecimal totalSpecialConsumeTaxAmount;
    private BigDecimal totalCustomBeforeAmount;
    private Integer displayOnBook;
    private Boolean isPaid;
    private Boolean isPostedCashBookFinance;
    private Boolean isPostedCashBookManagement;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate;

    private Boolean isPostedInventoryBookFinance;
    private Boolean isPostedInventoryBookManagement;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inventoryPostedDate;

    private Integer refOrder;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer baType;
    private String listTableName;
    private String refTypeName;
    private BigDecimal cabaAmountOc;
    private BigDecimal cabaAmount;
    private String invNo;
    private Boolean isInvoiceExported;
    private String payer;
    private Integer supplierId;
    private String supplierName;
    private Integer isOutwardExported;
    private Integer dueDay;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate;

    private BigDecimal totalSaleAmountOc;
    private BigDecimal totalSaleAmount;
    private BigDecimal totalExportTaxAmountOc;
    private BigDecimal totalExportTaxAmount;
    private Integer debtStatus;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inRefOrder;

    private Boolean isCreateFromEbHistory;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "CABARefDate")
    public Date getCabaRefDate() {
        return cabaRefDate;
    }

    public void setCabaRefDate(Date cabaRefDate) {
        this.cabaRefDate = cabaRefDate;
    }

    @Basic
    @Column(name = "CABAPostedDate")
    public Date getCabaPostedDate() {
        return cabaPostedDate;
    }

    public void setCabaPostedDate(Date cabaPostedDate) {
        this.cabaPostedDate = cabaPostedDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "CABARefNoManagement")
    public String getCabaRefNoManagement() {
        return cabaRefNoManagement;
    }

    public void setCabaRefNoManagement(String cabaRefNoManagement) {
        this.cabaRefNoManagement = cabaRefNoManagement;
    }

    @Basic
    @Column(name = "CABARefNoFinance")
    public String getCabaRefNoFinance() {
        return cabaRefNoFinance;
    }

    public void setCabaRefNoFinance(String cabaRefNoFinance) {
        this.cabaRefNoFinance = cabaRefNoFinance;
    }

    @Basic
    @Column(name = "FromBankAccountID")
    public Integer getFromBankAccountId() {
        return fromBankAccountId;
    }

    public void setFromBankAccountId(Integer fromBankAccountId) {
        this.fromBankAccountId = fromBankAccountId;
    }

    @Basic
    @Column(name = "FromBankAccountName")
    public String getFromBankAccountName() {
        return fromBankAccountName;
    }

    public void setFromBankAccountName(String fromBankAccountName) {
        this.fromBankAccountName = fromBankAccountName;
    }

    @Basic
    @Column(name = "IsFreightService")
    public Boolean getFreightService() {
        return isFreightService;
    }

    public void setFreightService(Boolean freightService) {
        isFreightService = freightService;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getPostedFinance() {
        return isPostedFinance;
    }

    public void setPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getPostedManagement() {
        return isPostedManagement;
    }

    public void setPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "ToBankAccountID")
    public Integer getToBankAccountId() {
        return toBankAccountId;
    }

    public void setToBankAccountId(Integer toBankAccountId) {
        this.toBankAccountId = toBankAccountId;
    }

    @Basic
    @Column(name = "IncludeInvoice")
    public Integer getIncludeInvoice() {
        return includeInvoice;
    }

    public void setIncludeInvoice(Integer includeInvoice) {
        this.includeInvoice = includeInvoice;
    }

    @Basic
    @Column(name = "ReasonTypeID")
    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    @Basic
    @Column(name = "ToBankAccountName")
    public String getToBankAccountName() {
        return toBankAccountName;
    }

    public void setToBankAccountName(String toBankAccountName) {
        this.toBankAccountName = toBankAccountName;
    }

    @Basic
    @Column(name = "PUInvoiceRefID")
    public Integer getPuInvoiceRefId() {
        return puInvoiceRefId;
    }

    public void setPuInvoiceRefId(Integer puInvoiceRefId) {
        this.puInvoiceRefId = puInvoiceRefId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectBankAccount")
    public String getAccountObjectBankAccount() {
        return accountObjectBankAccount;
    }

    public void setAccountObjectBankAccount(String accountObjectBankAccount) {
        this.accountObjectBankAccount = accountObjectBankAccount;
    }

    @Basic
    @Column(name = "AccountObjectContactIDNumber")
    public String getAccountObjectContactIdNumber() {
        return accountObjectContactIdNumber;
    }

    public void setAccountObjectContactIdNumber(String accountObjectContactIdNumber) {
        this.accountObjectContactIdNumber = accountObjectContactIdNumber;
    }

    @Basic
    @Column(name = "AccountObjectBankName")
    public String getAccountObjectBankName() {
        return accountObjectBankName;
    }

    public void setAccountObjectBankName(String accountObjectBankName) {
        this.accountObjectBankName = accountObjectBankName;
    }

    @Basic
    @Column(name = "AccountObjectContactIssueDate")
    public Date getAccountObjectContactIssueDate() {
        return accountObjectContactIssueDate;
    }

    public void setAccountObjectContactIssueDate(Date accountObjectContactIssueDate) {
        this.accountObjectContactIssueDate = accountObjectContactIssueDate;
    }

    @Basic
    @Column(name = "AccountObjectContactIssueBy")
    public String getAccountObjectContactIssueBy() {
        return accountObjectContactIssueBy;
    }

    public void setAccountObjectContactIssueBy(String accountObjectContactIssueBy) {
        this.accountObjectContactIssueBy = accountObjectContactIssueBy;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "IdentificationNumber")
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Basic
    @Column(name = "IssueDate")
    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    @Basic
    @Column(name = "IssueBy")
    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

    @Basic
    @Column(name = "Receiver")
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "DocumentIncluded")
    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    @Basic
    @Column(name = "CABAJournalMemo")
    public String getCabaJournalMemo() {
        return cabaJournalMemo;
    }

    public void setCabaJournalMemo(String cabaJournalMemo) {
        this.cabaJournalMemo = cabaJournalMemo;
    }

    @Basic
    @Column(name = "CABADocumentIncluded")
    public String getCabaDocumentIncluded() {
        return cabaDocumentIncluded;
    }

    public void setCabaDocumentIncluded(String cabaDocumentIncluded) {
        this.cabaDocumentIncluded = cabaDocumentIncluded;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "DueTime")
    public Integer getDueTime() {
        return dueTime;
    }

    public void setDueTime(Integer dueTime) {
        this.dueTime = dueTime;
    }

    @Basic
    @Column(name = "PaymentDate")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "TotalImportTaxAmountOC")
    public BigDecimal getTotalImportTaxAmountOc() {
        return totalImportTaxAmountOc;
    }

    public void setTotalImportTaxAmountOc(BigDecimal totalImportTaxAmountOc) {
        this.totalImportTaxAmountOc = totalImportTaxAmountOc;
    }

    @Basic
    @Column(name = "TotalImportTaxAmount")
    public BigDecimal getTotalImportTaxAmount() {
        return totalImportTaxAmount;
    }

    public void setTotalImportTaxAmount(BigDecimal totalImportTaxAmount) {
        this.totalImportTaxAmount = totalImportTaxAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "TotalFreightAmount")
    public BigDecimal getTotalFreightAmount() {
        return totalFreightAmount;
    }

    public void setTotalFreightAmount(BigDecimal totalFreightAmount) {
        this.totalFreightAmount = totalFreightAmount;
    }

    @Basic
    @Column(name = "TotalInwardAmount")
    public BigDecimal getTotalInwardAmount() {
        return totalInwardAmount;
    }

    public void setTotalInwardAmount(BigDecimal totalInwardAmount) {
        this.totalInwardAmount = totalInwardAmount;
    }

    @Basic
    @Column(name = "TotalSpecialConsumeTaxAmountOC")
    public BigDecimal getTotalSpecialConsumeTaxAmountOc() {
        return totalSpecialConsumeTaxAmountOc;
    }

    public void setTotalSpecialConsumeTaxAmountOc(BigDecimal totalSpecialConsumeTaxAmountOc) {
        this.totalSpecialConsumeTaxAmountOc = totalSpecialConsumeTaxAmountOc;
    }

    @Basic
    @Column(name = "TotalSpecialConsumeTaxAmount")
    public BigDecimal getTotalSpecialConsumeTaxAmount() {
        return totalSpecialConsumeTaxAmount;
    }

    public void setTotalSpecialConsumeTaxAmount(BigDecimal totalSpecialConsumeTaxAmount) {
        this.totalSpecialConsumeTaxAmount = totalSpecialConsumeTaxAmount;
    }

    @Basic
    @Column(name = "TotalCustomBeforeAmount")
    public BigDecimal getTotalCustomBeforeAmount() {
        return totalCustomBeforeAmount;
    }

    public void setTotalCustomBeforeAmount(BigDecimal totalCustomBeforeAmount) {
        this.totalCustomBeforeAmount = totalCustomBeforeAmount;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "IsPaid")
    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    @Basic
    @Column(name = "IsPostedCashBookFinance")
    public Boolean getPostedCashBookFinance() {
        return isPostedCashBookFinance;
    }

    public void setPostedCashBookFinance(Boolean postedCashBookFinance) {
        isPostedCashBookFinance = postedCashBookFinance;
    }

    @Basic
    @Column(name = "IsPostedCashBookManagement")
    public Boolean getPostedCashBookManagement() {
        return isPostedCashBookManagement;
    }

    public void setPostedCashBookManagement(Boolean postedCashBookManagement) {
        isPostedCashBookManagement = postedCashBookManagement;
    }

    @Basic
    @Column(name = "CashBookPostedDate")
    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookFinance")
    public Boolean getPostedInventoryBookFinance() {
        return isPostedInventoryBookFinance;
    }

    public void setPostedInventoryBookFinance(Boolean postedInventoryBookFinance) {
        isPostedInventoryBookFinance = postedInventoryBookFinance;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookManagement")
    public Boolean getPostedInventoryBookManagement() {
        return isPostedInventoryBookManagement;
    }

    public void setPostedInventoryBookManagement(Boolean postedInventoryBookManagement) {
        isPostedInventoryBookManagement = postedInventoryBookManagement;
    }

    @Basic
    @Column(name = "InventoryPostedDate")
    public Date getInventoryPostedDate() {
        return inventoryPostedDate;
    }

    public void setInventoryPostedDate(Date inventoryPostedDate) {
        this.inventoryPostedDate = inventoryPostedDate;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "BAType")
    public Integer getBaType() {
        return baType;
    }

    public void setBaType(Integer baType) {
        this.baType = baType;
    }

    @Basic
    @Column(name = "ListTableName")
    public String getListTableName() {
        return listTableName;
    }

    public void setListTableName(String listTableName) {
        this.listTableName = listTableName;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "CABAAmountOC")
    public BigDecimal getCabaAmountOc() {
        return cabaAmountOc;
    }

    public void setCabaAmountOc(BigDecimal cabaAmountOc) {
        this.cabaAmountOc = cabaAmountOc;
    }

    @Basic
    @Column(name = "CABAAmount")
    public BigDecimal getCabaAmount() {
        return cabaAmount;
    }

    public void setCabaAmount(BigDecimal cabaAmount) {
        this.cabaAmount = cabaAmount;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "IsInvoiceExported")
    public Boolean getInvoiceExported() {
        return isInvoiceExported;
    }

    public void setInvoiceExported(Boolean invoiceExported) {
        isInvoiceExported = invoiceExported;
    }

    @Basic
    @Column(name = "Payer")
    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    @Basic
    @Column(name = "SupplierID")
    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    @Basic
    @Column(name = "SupplierName")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Basic
    @Column(name = "IsOutwardExported")
    public Integer getIsOutwardExported() {
        return isOutwardExported;
    }

    public void setIsOutwardExported(Integer isOutwardExported) {
        this.isOutwardExported = isOutwardExported;
    }

    @Basic
    @Column(name = "DueDay")
    public Integer getDueDay() {
        return dueDay;
    }

    public void setDueDay(Integer dueDay) {
        this.dueDay = dueDay;
    }

    @Basic
    @Column(name = "DueDate")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "TotalSaleAmountOC")
    public BigDecimal getTotalSaleAmountOc() {
        return totalSaleAmountOc;
    }

    public void setTotalSaleAmountOc(BigDecimal totalSaleAmountOc) {
        this.totalSaleAmountOc = totalSaleAmountOc;
    }

    @Basic
    @Column(name = "TotalSaleAmount")
    public BigDecimal getTotalSaleAmount() {
        return totalSaleAmount;
    }

    public void setTotalSaleAmount(BigDecimal totalSaleAmount) {
        this.totalSaleAmount = totalSaleAmount;
    }

    @Basic
    @Column(name = "TotalExportTaxAmountOC")
    public BigDecimal getTotalExportTaxAmountOc() {
        return totalExportTaxAmountOc;
    }

    public void setTotalExportTaxAmountOc(BigDecimal totalExportTaxAmountOc) {
        this.totalExportTaxAmountOc = totalExportTaxAmountOc;
    }

    @Basic
    @Column(name = "TotalExportTaxAmount")
    public BigDecimal getTotalExportTaxAmount() {
        return totalExportTaxAmount;
    }

    public void setTotalExportTaxAmount(BigDecimal totalExportTaxAmount) {
        this.totalExportTaxAmount = totalExportTaxAmount;
    }

    @Basic
    @Column(name = "DebtStatus")
    public Integer getDebtStatus() {
        return debtStatus;
    }

    public void setDebtStatus(Integer debtStatus) {
        this.debtStatus = debtStatus;
    }

    @Basic
    @Column(name = "INRefOrder")
    public Date getInRefOrder() {
        return inRefOrder;
    }

    public void setInRefOrder(Date inRefOrder) {
        this.inRefOrder = inRefOrder;
    }

    @Basic
    @Column(name = "IsCreateFromEBHistory")
    public Boolean getCreateFromEbHistory() {
        return isCreateFromEbHistory;
    }

    public void setCreateFromEbHistory(Boolean createFromEbHistory) {
        isCreateFromEbHistory = createFromEbHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaDepositWithdrawList that = (BaDepositWithdrawList) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(cabaRefDate, that.cabaRefDate) &&
                Objects.equals(cabaPostedDate, that.cabaPostedDate) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refNoFinance, that.refNoFinance) &&
                Objects.equals(refNoManagement, that.refNoManagement) &&
                Objects.equals(cabaRefNoManagement, that.cabaRefNoManagement) &&
                Objects.equals(cabaRefNoFinance, that.cabaRefNoFinance) &&
                Objects.equals(fromBankAccountId, that.fromBankAccountId) &&
                Objects.equals(fromBankAccountName, that.fromBankAccountName) &&
                Objects.equals(isFreightService, that.isFreightService) &&
                Objects.equals(isPostedFinance, that.isPostedFinance) &&
                Objects.equals(isPostedManagement, that.isPostedManagement) &&
                Objects.equals(toBankAccountId, that.toBankAccountId) &&
                Objects.equals(includeInvoice, that.includeInvoice) &&
                Objects.equals(reasonTypeId, that.reasonTypeId) &&
                Objects.equals(toBankAccountName, that.toBankAccountName) &&
                Objects.equals(puInvoiceRefId, that.puInvoiceRefId) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectBankAccount, that.accountObjectBankAccount) &&
                Objects.equals(accountObjectContactIdNumber, that.accountObjectContactIdNumber) &&
                Objects.equals(accountObjectBankName, that.accountObjectBankName) &&
                Objects.equals(accountObjectContactIssueDate, that.accountObjectContactIssueDate) &&
                Objects.equals(accountObjectContactIssueBy, that.accountObjectContactIssueBy) &&
                Objects.equals(accountObjectContactName, that.accountObjectContactName) &&
                Objects.equals(identificationNumber, that.identificationNumber) &&
                Objects.equals(issueDate, that.issueDate) &&
                Objects.equals(issueBy, that.issueBy) &&
                Objects.equals(receiver, that.receiver) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(documentIncluded, that.documentIncluded) &&
                Objects.equals(cabaJournalMemo, that.cabaJournalMemo) &&
                Objects.equals(cabaDocumentIncluded, that.cabaDocumentIncluded) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(dueTime, that.dueTime) &&
                Objects.equals(paymentDate, that.paymentDate) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(totalAmountOc, that.totalAmountOc) &&
                Objects.equals(totalAmount, that.totalAmount) &&
                Objects.equals(totalImportTaxAmountOc, that.totalImportTaxAmountOc) &&
                Objects.equals(totalImportTaxAmount, that.totalImportTaxAmount) &&
                Objects.equals(totalVatAmountOc, that.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, that.totalVatAmount) &&
                Objects.equals(totalDiscountAmountOc, that.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, that.totalDiscountAmount) &&
                Objects.equals(totalFreightAmount, that.totalFreightAmount) &&
                Objects.equals(totalInwardAmount, that.totalInwardAmount) &&
                Objects.equals(totalSpecialConsumeTaxAmountOc, that.totalSpecialConsumeTaxAmountOc) &&
                Objects.equals(totalSpecialConsumeTaxAmount, that.totalSpecialConsumeTaxAmount) &&
                Objects.equals(totalCustomBeforeAmount, that.totalCustomBeforeAmount) &&
                Objects.equals(displayOnBook, that.displayOnBook) &&
                Objects.equals(isPaid, that.isPaid) &&
                Objects.equals(isPostedCashBookFinance, that.isPostedCashBookFinance) &&
                Objects.equals(isPostedCashBookManagement, that.isPostedCashBookManagement) &&
                Objects.equals(cashBookPostedDate, that.cashBookPostedDate) &&
                Objects.equals(isPostedInventoryBookFinance, that.isPostedInventoryBookFinance) &&
                Objects.equals(isPostedInventoryBookManagement, that.isPostedInventoryBookManagement) &&
                Objects.equals(inventoryPostedDate, that.inventoryPostedDate) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(baType, that.baType) &&
                Objects.equals(listTableName, that.listTableName) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(cabaAmountOc, that.cabaAmountOc) &&
                Objects.equals(cabaAmount, that.cabaAmount) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(isInvoiceExported, that.isInvoiceExported) &&
                Objects.equals(payer, that.payer) &&
                Objects.equals(supplierId, that.supplierId) &&
                Objects.equals(supplierName, that.supplierName) &&
                Objects.equals(isOutwardExported, that.isOutwardExported) &&
                Objects.equals(dueDay, that.dueDay) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(totalSaleAmountOc, that.totalSaleAmountOc) &&
                Objects.equals(totalSaleAmount, that.totalSaleAmount) &&
                Objects.equals(totalExportTaxAmountOc, that.totalExportTaxAmountOc) &&
                Objects.equals(totalExportTaxAmount, that.totalExportTaxAmount) &&
                Objects.equals(debtStatus, that.debtStatus) &&
                Objects.equals(inRefOrder, that.inRefOrder) &&
                Objects.equals(isCreateFromEbHistory, that.isCreateFromEbHistory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postedDate, refDate, cabaRefDate, cabaPostedDate, refType, refNoFinance, refNoManagement, cabaRefNoManagement, cabaRefNoFinance, fromBankAccountId, fromBankAccountName, isFreightService, isPostedFinance, isPostedManagement, toBankAccountId, includeInvoice, reasonTypeId, toBankAccountName, puInvoiceRefId, accountObjectId, accountObjectName, accountObjectAddress, accountObjectBankAccount, accountObjectContactIdNumber, accountObjectBankName, accountObjectContactIssueDate, accountObjectContactIssueBy, accountObjectContactName, identificationNumber, issueDate, issueBy, receiver, branchId, journalMemo, employeeId, documentIncluded, cabaJournalMemo, cabaDocumentIncluded, bankAccountId, bankName, paymentTermId, dueTime, paymentDate, currencyId, exchangeRate, totalAmountOc, totalAmount, totalImportTaxAmountOc, totalImportTaxAmount, totalVatAmountOc, totalVatAmount, totalDiscountAmountOc, totalDiscountAmount, totalFreightAmount, totalInwardAmount, totalSpecialConsumeTaxAmountOc, totalSpecialConsumeTaxAmount, totalCustomBeforeAmount, displayOnBook, isPaid, isPostedCashBookFinance, isPostedCashBookManagement, cashBookPostedDate, isPostedInventoryBookFinance, isPostedInventoryBookManagement, inventoryPostedDate, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, baType, listTableName, refTypeName, cabaAmountOc, cabaAmount, invNo, isInvoiceExported, payer, supplierId, supplierName, isOutwardExported, dueDay, dueDate, totalSaleAmountOc, totalSaleAmount, totalExportTaxAmountOc, totalExportTaxAmount, debtStatus, inRefOrder, isCreateFromEbHistory);
    }
}
