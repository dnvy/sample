package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Kiểm kê quỹ.
 */
@Entity
@Table(name = "CAAudit")
public class CaAudit {

    private Integer id; // PK
    private Integer branchId; // Mã chi nhánh.
    private String refNo; // Số kiểm kê
    private Integer refType; // NOT NULL. Loại chứng từ (Lấy từ bảng RefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày kiểm kê

    private Timestamp refTime; // Giờ kiểm kê
    private String journalMemo; // Diễn giải

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date auditDate; // NOT NULL. Kiểm kê đến ngày

    private Integer currencyId; // Loại tiền: Mặc định đồng tiền hạch toán
    private BigDecimal totalAuditAmount; // NOT NULL. Tổng số tiền kiểm kê
    private BigDecimal totalBalanceAmount; // NOT NULL. Tổng dư nợ tài khoản theo sổ kế toán tiền mặt
    private String reason; // Lý do
    private String conclusion; // Kết luận
    private Boolean isExecuted; // NOT NULL. Đã xử lý chênh lệch
    private Integer displayOnBook; // NOT NULL. 0 = Kiểm kê trên Sổ tài chinh;1 = Kiểm kê trên Sổ quản trị
    private Integer editVersion; // Phiên bản sửa chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer caReceiptRefId;
    private Integer caPaymentRefId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "RefTime")
    public Timestamp getRefTime() {
        return refTime;
    }

    public void setRefTime(Timestamp refTime) {
        this.refTime = refTime;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "AuditDate")
    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "TotalAuditAmount")
    public BigDecimal getTotalAuditAmount() {
        return totalAuditAmount;
    }

    public void setTotalAuditAmount(BigDecimal totalAuditAmount) {
        this.totalAuditAmount = totalAuditAmount;
    }

    @Basic
    @Column(name = "TotalBalanceAmount")
    public BigDecimal getTotalBalanceAmount() {
        return totalBalanceAmount;
    }

    public void setTotalBalanceAmount(BigDecimal totalBalanceAmount) {
        this.totalBalanceAmount = totalBalanceAmount;
    }

    @Basic
    @Column(name = "Reason")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Basic
    @Column(name = "Conclusion")
    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    @Basic
    @Column(name = "IsExecuted")
    public Boolean getIsExecuted() {
        return isExecuted;
    }

    public void setIsExecuted(Boolean executed) {
        isExecuted = executed;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CAReceiptRefID")
    public Integer getCaReceiptRefId() {
        return caReceiptRefId;
    }

    public void setCaReceiptRefId(Integer caReceiptRefId) {
        this.caReceiptRefId = caReceiptRefId;
    }

    @Basic
    @Column(name = "CAPaymentRefID")
    public Integer getCaPaymentRefId() {
        return caPaymentRefId;
    }

    public void setCaPaymentRefId(Integer caPaymentRefId) {
        this.caPaymentRefId = caPaymentRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaAudit caAudit = (CaAudit) o;
        return Objects.equals(id, caAudit.id) &&
                Objects.equals(branchId, caAudit.branchId) &&
                Objects.equals(refNo, caAudit.refNo) &&
                Objects.equals(refType, caAudit.refType) &&
                Objects.equals(refDate, caAudit.refDate) &&
                Objects.equals(refTime, caAudit.refTime) &&
                Objects.equals(journalMemo, caAudit.journalMemo) &&
                Objects.equals(auditDate, caAudit.auditDate) &&
                Objects.equals(currencyId, caAudit.currencyId) &&
                Objects.equals(totalAuditAmount, caAudit.totalAuditAmount) &&
                Objects.equals(totalBalanceAmount, caAudit.totalBalanceAmount) &&
                Objects.equals(reason, caAudit.reason) &&
                Objects.equals(conclusion, caAudit.conclusion) &&
                Objects.equals(isExecuted, caAudit.isExecuted) &&
                Objects.equals(displayOnBook, caAudit.displayOnBook) &&
                Objects.equals(editVersion, caAudit.editVersion) &&
                Objects.equals(createdDate, caAudit.createdDate) &&
                Objects.equals(createdBy, caAudit.createdBy) &&
                Objects.equals(modifiedDate, caAudit.modifiedDate) &&
                Objects.equals(modifiedBy, caAudit.modifiedBy) &&
                Objects.equals(caReceiptRefId, caAudit.caReceiptRefId) &&
                Objects.equals(caPaymentRefId, caAudit.caPaymentRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, refNo, refType, refDate, refTime, journalMemo, auditDate, currencyId, totalAuditAmount, totalBalanceAmount, reason, conclusion, isExecuted, displayOnBook, editVersion, createdDate, createdBy, modifiedDate, modifiedBy, caReceiptRefId, caPaymentRefId);
    }
}
