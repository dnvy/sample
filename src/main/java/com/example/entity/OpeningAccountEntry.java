package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Số dư tài khoản, Số dư TK ngân hàng, Số dư công nợ KH, NCC, Nhân viên.
 */
@Entity
public class OpeningAccountEntry {

    private Integer id; // PK - Số dư đầu Tài khoản
    private Integer refType; // NOT NULL. Loại chứng từ: số dư ban đầu tài khoản

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày ghi sổ (fix = StartDate-1)

    private String accountNumber; // Tài khoản
    private Integer accountObjectId; // Mã đối tượng
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal debitAmountOc; // NOT NULL. Dự nợ
    private BigDecimal debitAmount; // NOT NULL. Dư nợ quy đổi
    private BigDecimal creditAmountOc; // NOT NULL. Dư có
    private BigDecimal creditAmount; // NOT NULL. Dư có quy đổi
    private Integer bankAccountId; // Tài khoản ngân hàng
    private Integer branchId; // ID của chi nhánh.
    private Integer displayOnBook; // NOT NULL. 0=Sổ tài chinh;1=Sổ quản trị
    private Boolean isPostedCashBook; // Trạng thái ghi sổ quỹ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate; // Ngày ghi sổ quỹ

    private Integer editVersion; // NOT NULL. Phiên bản sửa chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi sổ Sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái ghi sổ Sổ tài chính
    private Boolean isAutoGenerate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "DebitAmountOC")
    public BigDecimal getDebitAmountOc() {
        return debitAmountOc;
    }

    public void setDebitAmountOc(BigDecimal debitAmountOc) {
        this.debitAmountOc = debitAmountOc;
    }

    @Basic
    @Column(name = "DebitAmount")
    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    @Basic
    @Column(name = "CreditAmountOC")
    public BigDecimal getCreditAmountOc() {
        return creditAmountOc;
    }

    public void setCreditAmountOc(BigDecimal creditAmountOc) {
        this.creditAmountOc = creditAmountOc;
    }

    @Basic
    @Column(name = "CreditAmount")
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "IsPostedCashBook")
    public Boolean getIsPostedCashBook() {
        return isPostedCashBook;
    }

    public void setIsPostedCashBook(Boolean postedCashBook) {
        isPostedCashBook = postedCashBook;
    }

    @Basic
    @Column(name = "CashBookPostedDate")
    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsAutoGenerate")
    public Boolean getIsAutoGenerate() {
        return isAutoGenerate;
    }

    public void setIsAutoGenerate(Boolean autoGenerate) {
        isAutoGenerate = autoGenerate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpeningAccountEntry that = (OpeningAccountEntry) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(debitAmountOc, that.debitAmountOc) &&
                Objects.equals(debitAmount, that.debitAmount) &&
                Objects.equals(creditAmountOc, that.creditAmountOc) &&
                Objects.equals(creditAmount, that.creditAmount) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(displayOnBook, that.displayOnBook) &&
                Objects.equals(isPostedCashBook, that.isPostedCashBook) &&
                Objects.equals(cashBookPostedDate, that.cashBookPostedDate) &&
                Objects.equals(editVersion, that.editVersion) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(isPostedManagement, that.isPostedManagement) &&
                Objects.equals(isPostedFinance, that.isPostedFinance) &&
                Objects.equals(isAutoGenerate, that.isAutoGenerate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, postedDate, accountNumber, accountObjectId, currencyId, exchangeRate, debitAmountOc, debitAmount, creditAmountOc, creditAmount, bankAccountId, branchId, displayOnBook, isPostedCashBook, cashBookPostedDate, editVersion, createdDate, createdBy, modifiedDate, modifiedBy, isPostedManagement, isPostedFinance, isAutoGenerate);
    }
}
