package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng master của chứng từ thu tiền gửi
 * Là Master của các bảng detail sau:
 *  -BADepositeDetail
 *  -BADepositeDetailSale
 *  -BADepositeDetailFixedAsset
 */
@Entity
@Table(name = "BADeposit")
public class BaDeposit {

    private Integer id; // NOT NULL. PK

    /**
     * 1500 = Thu tiền gửi
     * 1501 = Thu hoàn thuế
     * 1502 = Thu tiền gửi từ khách hàng
     * 1503 = Thu tiền gửi từ khách hàng hàng loạt
     */
    private Integer refType; // NOT NULL. Loại chứng từ (Lấy từ bảng SYSRefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // NOT NULL. Ngày hạch toán


    private String refNoFinance; // Số chứng từ sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái (Ghi sổ/Chưa ghi sổ)
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi vào sổ quản trị
    private Integer accountObjectId; // Mã người nộp tiền
    private String accountObjectName; // Tên người nộp tiền/Kho bạc
    private String accountObjectAddress; // Địa chỉ người nộp tiền
    private Integer bankAccountId; // Tài khoản ngân hàng
    private String bankName; // Tên ngân hàng
    private Integer reasonTypeId; // Lý do thu
    private String journalMemo; // Diễn giải Lý do thu
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Tổng tiền hàng
    private BigDecimal totalAmount; // NOT NULL. Tổng tiền hàng quy đổi
    private Integer employeeId; // Nhân viên
    private Integer branchId; // Mã chi nhánh
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer editVersion; // Phiên bản sửa chứng từ
    private Integer refOrder; // NOT NULL. Số thứ tự chứng từ nhập vào database
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Boolean isCreateFromEbHistory; // Có trước khi sử dụng phần mềm.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "ReasonTypeID")
    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "IsCreateFromEBHistory")
    public Boolean getIsCreateFromEbHistory() {
        return isCreateFromEbHistory;
    }

    public void setIsCreateFromEbHistory(Boolean createFromEbHistory) {
        isCreateFromEbHistory = createFromEbHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaDeposit baDeposit = (BaDeposit) o;
        return Objects.equals(id, baDeposit.id) &&
                Objects.equals(refType, baDeposit.refType) &&
                Objects.equals(refDate, baDeposit.refDate) &&
                Objects.equals(postedDate, baDeposit.postedDate) &&
                Objects.equals(refNoFinance, baDeposit.refNoFinance) &&
                Objects.equals(refNoManagement, baDeposit.refNoManagement) &&
                Objects.equals(isPostedFinance, baDeposit.isPostedFinance) &&
                Objects.equals(isPostedManagement, baDeposit.isPostedManagement) &&
                Objects.equals(accountObjectId, baDeposit.accountObjectId) &&
                Objects.equals(accountObjectName, baDeposit.accountObjectName) &&
                Objects.equals(accountObjectAddress, baDeposit.accountObjectAddress) &&
                Objects.equals(bankAccountId, baDeposit.bankAccountId) &&
                Objects.equals(bankName, baDeposit.bankName) &&
                Objects.equals(reasonTypeId, baDeposit.reasonTypeId) &&
                Objects.equals(journalMemo, baDeposit.journalMemo) &&
                Objects.equals(currencyId, baDeposit.currencyId) &&
                Objects.equals(exchangeRate, baDeposit.exchangeRate) &&
                Objects.equals(totalAmountOc, baDeposit.totalAmountOc) &&
                Objects.equals(totalAmount, baDeposit.totalAmount) &&
                Objects.equals(employeeId, baDeposit.employeeId) &&
                Objects.equals(branchId, baDeposit.branchId) &&
                Objects.equals(displayOnBook, baDeposit.displayOnBook) &&
                Objects.equals(editVersion, baDeposit.editVersion) &&
                Objects.equals(refOrder, baDeposit.refOrder) &&
                Objects.equals(createdDate, baDeposit.createdDate) &&
                Objects.equals(createdBy, baDeposit.createdBy) &&
                Objects.equals(modifiedDate, baDeposit.modifiedDate) &&
                Objects.equals(modifiedBy, baDeposit.modifiedBy) &&
                Objects.equals(customField1, baDeposit.customField1) &&
                Objects.equals(customField2, baDeposit.customField2) &&
                Objects.equals(customField3, baDeposit.customField3) &&
                Objects.equals(customField4, baDeposit.customField4) &&
                Objects.equals(customField5, baDeposit.customField5) &&
                Objects.equals(customField6, baDeposit.customField6) &&
                Objects.equals(customField7, baDeposit.customField7) &&
                Objects.equals(customField8, baDeposit.customField8) &&
                Objects.equals(customField9, baDeposit.customField9) &&
                Objects.equals(customField10, baDeposit.customField10) &&
                Objects.equals(isCreateFromEbHistory, baDeposit.isCreateFromEbHistory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, refDate, postedDate, refNoFinance, refNoManagement, isPostedFinance, isPostedManagement, accountObjectId, accountObjectName, accountObjectAddress, bankAccountId, bankName, reasonTypeId, journalMemo, currencyId, exchangeRate, totalAmountOc, totalAmount, employeeId, branchId, displayOnBook, editVersion, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, isCreateFromEbHistory);
    }
}
