package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * Khi add Phiếu nhập kho, thì ghi vào bảng này
 */
@Entity
@Table(name = "INInwardOutwardList")
public class InInwardOutwardList {

    private Integer id;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cabaRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date caRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cabaPostedDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date caPostedDate;


    private Integer refType;
    private String invTemplateNo;
    private String refNoFinance;
    private String invSeries;
    private String refNoManagement;
    private String cabaRefNoManagement;
    private String caRefNoFinance;
    private String invNo;
    private String cabaRefNoFinance;
    private String caRefNoManagement;
    private Boolean isPostedFinance;
    private Boolean isPostedManagement;
    private Boolean includeInvoice;
    private Integer saInvoiceRefId;
    private Integer puInvoiceRefId;
    private Integer accountObjectId;
    private String orderNo;
    private String accountObjectName;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date orderDate;

    private String accountObjectAddress;
    private String contactName;
    private String receiverAddress;
    private Integer saReturnRefId;
    private String accountObjectBankAccount;
    private Integer unitPriceMethod;
    private String accountObjectBankName;
    private String outDocumentIncluded;
    private String accountObjectContactName;
    private String payer;
    private String identificationNumber;
    private String payerAddress;
    private Integer transporterId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date issueDate;

    private String payReason;
    private String transporterName;
    private String caDocumentIncluded;
    private String contractCode;
    private String issueBy;
    private Boolean isInvoiceExported;
    private String receiver;
    private String transport;
    private Integer fromStockId;
    private String journalMemo;
    private Integer toStockId;
    private String documentIncluded;
    private Integer employeeId;
    private Integer branchId;
    private String cabaJournalMemo;
    private String cabaDocumentIncluded;
    private Integer bankAccountId;
    private String bankName;
    private Integer paymentTermId;
    private Integer dueTime;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date paymentDate;

    private Integer revenueStatus;
    private String currencyId;
    private BigDecimal exchangeRate;
    private BigDecimal totalAmountOc;
    private BigDecimal totalAmount;
    private BigDecimal totalImportTaxAmountOc;
    private BigDecimal totalImportTaxAmount;
    private BigDecimal totalVatAmountOc;
    private BigDecimal totalVatAmount;
    private BigDecimal totalDiscountAmountOc;
    private BigDecimal totalDiscountAmount;
    private BigDecimal totalFreightAmount;
    private BigDecimal totalInwardAmount;
    private BigDecimal totalSpecialConsumeTaxAmountOc;
    private BigDecimal totalSpecialConsumeTaxAmount;
    private BigDecimal totalCustomBeforeAmount;
    private Integer displayOnBook;
    private Boolean isPaid;
    private Boolean isPostedCashBookFinance;
    private Boolean isPostedCashBookManagement;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate;

    private Boolean isPostedInventoryBookFinance;
    private Boolean isPostedInventoryBookManagement;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inventoryPostedDate;

    private Integer refOrder;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer inType;
    private String listTableName;
    private String refTypeName;
    private String revenueStatusName;
    private String caJournalMemo;
    private BigDecimal totalAmountFinance;
    private BigDecimal totalAmountManagement;
    private BigDecimal cabaAmountOc;
    private BigDecimal cabaAmount;
    private Integer outwardDependentRefId;
    private Integer assemblyRefId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inRefOrder;

    private Boolean isSaleWithOutward;
    private Boolean isCreatedSaReturnLastYear;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "CABARefDate")
    public Date getCabaRefDate() {
        return cabaRefDate;
    }

    public void setCabaRefDate(Date cabaRefDate) {
        this.cabaRefDate = cabaRefDate;
    }

    @Basic
    @Column(name = "CARefDate")
    public Date getCaRefDate() {
        return caRefDate;
    }

    public void setCaRefDate(Date caRefDate) {
        this.caRefDate = caRefDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "CABAPostedDate")
    public Date getCabaPostedDate() {
        return cabaPostedDate;
    }

    public void setCabaPostedDate(Date cabaPostedDate) {
        this.cabaPostedDate = cabaPostedDate;
    }

    @Basic
    @Column(name = "CAPostedDate")
    public Date getCaPostedDate() {
        return caPostedDate;
    }

    public void setCaPostedDate(Date caPostedDate) {
        this.caPostedDate = caPostedDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "InvTemplateNo")
    public String getInvTemplateNo() {
        return invTemplateNo;
    }

    public void setInvTemplateNo(String invTemplateNo) {
        this.invTemplateNo = invTemplateNo;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "CABARefNoManagement")
    public String getCabaRefNoManagement() {
        return cabaRefNoManagement;
    }

    public void setCabaRefNoManagement(String cabaRefNoManagement) {
        this.cabaRefNoManagement = cabaRefNoManagement;
    }

    @Basic
    @Column(name = "CARefNoFinance")
    public String getCaRefNoFinance() {
        return caRefNoFinance;
    }

    public void setCaRefNoFinance(String caRefNoFinance) {
        this.caRefNoFinance = caRefNoFinance;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "CABARefNoFinance")
    public String getCabaRefNoFinance() {
        return cabaRefNoFinance;
    }

    public void setCabaRefNoFinance(String cabaRefNoFinance) {
        this.cabaRefNoFinance = cabaRefNoFinance;
    }

    @Basic
    @Column(name = "CARefNoManagement")
    public String getCaRefNoManagement() {
        return caRefNoManagement;
    }

    public void setCaRefNoManagement(String caRefNoManagement) {
        this.caRefNoManagement = caRefNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "IncludeInvoice")
    public Boolean getIncludeInvoice() {
        return includeInvoice;
    }

    public void setIncludeInvoice(Boolean includeInvoice) {
        this.includeInvoice = includeInvoice;
    }

    @Basic
    @Column(name = "SAInvoiceRefID")
    public Integer getSaInvoiceRefId() {
        return saInvoiceRefId;
    }

    public void setSaInvoiceRefId(Integer saInvoiceRefId) {
        this.saInvoiceRefId = saInvoiceRefId;
    }

    @Basic
    @Column(name = "PUInvoiceRefID")
    public Integer getPuInvoiceRefId() {
        return puInvoiceRefId;
    }

    public void setPuInvoiceRefId(Integer puInvoiceRefId) {
        this.puInvoiceRefId = puInvoiceRefId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "OrderNo")
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "OrderDate")
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "ContactName")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "ReceiverAddress")
    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    @Basic
    @Column(name = "SAReturnRefID")
    public Integer getSaReturnRefId() {
        return saReturnRefId;
    }

    public void setSaReturnRefId(Integer saReturnRefId) {
        this.saReturnRefId = saReturnRefId;
    }

    @Basic
    @Column(name = "AccountObjectBankAccount")
    public String getAccountObjectBankAccount() {
        return accountObjectBankAccount;
    }

    public void setAccountObjectBankAccount(String accountObjectBankAccount) {
        this.accountObjectBankAccount = accountObjectBankAccount;
    }

    @Basic
    @Column(name = "UnitPriceMethod")
    public Integer getUnitPriceMethod() {
        return unitPriceMethod;
    }

    public void setUnitPriceMethod(Integer unitPriceMethod) {
        this.unitPriceMethod = unitPriceMethod;
    }

    @Basic
    @Column(name = "AccountObjectBankName")
    public String getAccountObjectBankName() {
        return accountObjectBankName;
    }

    public void setAccountObjectBankName(String accountObjectBankName) {
        this.accountObjectBankName = accountObjectBankName;
    }

    @Basic
    @Column(name = "OutDocumentIncluded")
    public String getOutDocumentIncluded() {
        return outDocumentIncluded;
    }

    public void setOutDocumentIncluded(String outDocumentIncluded) {
        this.outDocumentIncluded = outDocumentIncluded;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "Payer")
    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    @Basic
    @Column(name = "IdentificationNumber")
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Basic
    @Column(name = "PayerAddress")
    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }

    @Basic
    @Column(name = "TransporterID")
    public Integer getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(Integer transporterId) {
        this.transporterId = transporterId;
    }

    @Basic
    @Column(name = "IssueDate")
    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    @Basic
    @Column(name = "PayReason")
    public String getPayReason() {
        return payReason;
    }

    public void setPayReason(String payReason) {
        this.payReason = payReason;
    }

    @Basic
    @Column(name = "TransporterName")
    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    @Basic
    @Column(name = "CADocumentIncluded")
    public String getCaDocumentIncluded() {
        return caDocumentIncluded;
    }

    public void setCaDocumentIncluded(String caDocumentIncluded) {
        this.caDocumentIncluded = caDocumentIncluded;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "IssueBy")
    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

    @Basic
    @Column(name = "IsInvoiceExported")
    public Boolean getIsInvoiceExported() {
        return isInvoiceExported;
    }

    public void setIsInvoiceExported(Boolean invoiceExported) {
        isInvoiceExported = invoiceExported;
    }

    @Basic
    @Column(name = "Receiver")
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Basic
    @Column(name = "Transport")
    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    @Basic
    @Column(name = "FromStockID")
    public Integer getFromStockId() {
        return fromStockId;
    }

    public void setFromStockId(Integer fromStockId) {
        this.fromStockId = fromStockId;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "ToStockID")
    public Integer getToStockId() {
        return toStockId;
    }

    public void setToStockId(Integer toStockId) {
        this.toStockId = toStockId;
    }

    @Basic
    @Column(name = "DocumentIncluded")
    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "CABAJournalMemo")
    public String getCabaJournalMemo() {
        return cabaJournalMemo;
    }

    public void setCabaJournalMemo(String cabaJournalMemo) {
        this.cabaJournalMemo = cabaJournalMemo;
    }

    @Basic
    @Column(name = "CABADocumentIncluded")
    public String getCabaDocumentIncluded() {
        return cabaDocumentIncluded;
    }

    public void setCabaDocumentIncluded(String cabaDocumentIncluded) {
        this.cabaDocumentIncluded = cabaDocumentIncluded;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "DueTime")
    public Integer getDueTime() {
        return dueTime;
    }

    public void setDueTime(Integer dueTime) {
        this.dueTime = dueTime;
    }

    @Basic
    @Column(name = "PaymentDate")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "RevenueStatus")
    public Integer getRevenueStatus() {
        return revenueStatus;
    }

    public void setRevenueStatus(Integer revenueStatus) {
        this.revenueStatus = revenueStatus;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "TotalImportTaxAmountOC")
    public BigDecimal getTotalImportTaxAmountOc() {
        return totalImportTaxAmountOc;
    }

    public void setTotalImportTaxAmountOc(BigDecimal totalImportTaxAmountOc) {
        this.totalImportTaxAmountOc = totalImportTaxAmountOc;
    }

    @Basic
    @Column(name = "TotalImportTaxAmount")
    public BigDecimal getTotalImportTaxAmount() {
        return totalImportTaxAmount;
    }

    public void setTotalImportTaxAmount(BigDecimal totalImportTaxAmount) {
        this.totalImportTaxAmount = totalImportTaxAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "TotalFreightAmount")
    public BigDecimal getTotalFreightAmount() {
        return totalFreightAmount;
    }

    public void setTotalFreightAmount(BigDecimal totalFreightAmount) {
        this.totalFreightAmount = totalFreightAmount;
    }

    @Basic
    @Column(name = "TotalInwardAmount")
    public BigDecimal getTotalInwardAmount() {
        return totalInwardAmount;
    }

    public void setTotalInwardAmount(BigDecimal totalInwardAmount) {
        this.totalInwardAmount = totalInwardAmount;
    }

    @Basic
    @Column(name = "TotalSpecialConsumeTaxAmountOC")
    public BigDecimal getTotalSpecialConsumeTaxAmountOc() {
        return totalSpecialConsumeTaxAmountOc;
    }

    public void setTotalSpecialConsumeTaxAmountOc(BigDecimal totalSpecialConsumeTaxAmountOc) {
        this.totalSpecialConsumeTaxAmountOc = totalSpecialConsumeTaxAmountOc;
    }

    @Basic
    @Column(name = "TotalSpecialConsumeTaxAmount")
    public BigDecimal getTotalSpecialConsumeTaxAmount() {
        return totalSpecialConsumeTaxAmount;
    }

    public void setTotalSpecialConsumeTaxAmount(BigDecimal totalSpecialConsumeTaxAmount) {
        this.totalSpecialConsumeTaxAmount = totalSpecialConsumeTaxAmount;
    }

    @Basic
    @Column(name = "TotalCustomBeforeAmount")
    public BigDecimal getTotalCustomBeforeAmount() {
        return totalCustomBeforeAmount;
    }

    public void setTotalCustomBeforeAmount(BigDecimal totalCustomBeforeAmount) {
        this.totalCustomBeforeAmount = totalCustomBeforeAmount;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "IsPaid")
    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean paid) {
        isPaid = paid;
    }

    @Basic
    @Column(name = "IsPostedCashBookFinance")
    public Boolean getIsPostedCashBookFinance() {
        return isPostedCashBookFinance;
    }

    public void setIsPostedCashBookFinance(Boolean postedCashBookFinance) {
        isPostedCashBookFinance = postedCashBookFinance;
    }

    @Basic
    @Column(name = "IsPostedCashBookManagement")
    public Boolean getIsPostedCashBookManagement() {
        return isPostedCashBookManagement;
    }

    public void setIsPostedCashBookManagement(Boolean postedCashBookManagement) {
        isPostedCashBookManagement = postedCashBookManagement;
    }

    @Basic
    @Column(name = "CashBookPostedDate")
    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookFinance")
    public Boolean getIsPostedInventoryBookFinance() {
        return isPostedInventoryBookFinance;
    }

    public void setIsPostedInventoryBookFinance(Boolean postedInventoryBookFinance) {
        isPostedInventoryBookFinance = postedInventoryBookFinance;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookManagement")
    public Boolean getIsPostedInventoryBookManagement() {
        return isPostedInventoryBookManagement;
    }

    public void setIsPostedInventoryBookManagement(Boolean postedInventoryBookManagement) {
        isPostedInventoryBookManagement = postedInventoryBookManagement;
    }

    @Basic
    @Column(name = "InventoryPostedDate")
    public Date getInventoryPostedDate() {
        return inventoryPostedDate;
    }

    public void setInventoryPostedDate(Date inventoryPostedDate) {
        this.inventoryPostedDate = inventoryPostedDate;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "INType")
    public Integer getInType() {
        return inType;
    }

    public void setInType(Integer inType) {
        this.inType = inType;
    }

    @Basic
    @Column(name = "ListTableName")
    public String getListTableName() {
        return listTableName;
    }

    public void setListTableName(String listTableName) {
        this.listTableName = listTableName;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "RevenueStatusName")
    public String getRevenueStatusName() {
        return revenueStatusName;
    }

    public void setRevenueStatusName(String revenueStatusName) {
        this.revenueStatusName = revenueStatusName;
    }

    @Basic
    @Column(name = "CAJournalMemo")
    public String getCaJournalMemo() {
        return caJournalMemo;
    }

    public void setCaJournalMemo(String caJournalMemo) {
        this.caJournalMemo = caJournalMemo;
    }

    @Basic
    @Column(name = "TotalAmountFinance")
    public BigDecimal getTotalAmountFinance() {
        return totalAmountFinance;
    }

    public void setTotalAmountFinance(BigDecimal totalAmountFinance) {
        this.totalAmountFinance = totalAmountFinance;
    }

    @Basic
    @Column(name = "TotalAmountManagement")
    public BigDecimal getTotalAmountManagement() {
        return totalAmountManagement;
    }

    public void setTotalAmountManagement(BigDecimal totalAmountManagement) {
        this.totalAmountManagement = totalAmountManagement;
    }

    @Basic
    @Column(name = "CABAAmountOC")
    public BigDecimal getCabaAmountOc() {
        return cabaAmountOc;
    }

    public void setCabaAmountOc(BigDecimal cabaAmountOc) {
        this.cabaAmountOc = cabaAmountOc;
    }

    @Basic
    @Column(name = "CABAAmount")
    public BigDecimal getCabaAmount() {
        return cabaAmount;
    }

    public void setCabaAmount(BigDecimal cabaAmount) {
        this.cabaAmount = cabaAmount;
    }

    @Basic
    @Column(name = "OutwardDependentRefID")
    public Integer getOutwardDependentRefId() {
        return outwardDependentRefId;
    }

    public void setOutwardDependentRefId(Integer outwardDependentRefId) {
        this.outwardDependentRefId = outwardDependentRefId;
    }

    @Basic
    @Column(name = "AssemblyRefID")
    public Integer getAssemblyRefId() {
        return assemblyRefId;
    }

    public void setAssemblyRefId(Integer assemblyRefId) {
        this.assemblyRefId = assemblyRefId;
    }

    @Basic
    @Column(name = "INRefOrder")
    public Date getInRefOrder() {
        return inRefOrder;
    }

    public void setInRefOrder(Date inRefOrder) {
        this.inRefOrder = inRefOrder;
    }

    @Basic
    @Column(name = "IsSaleWithOutward")
    public Boolean getIsSaleWithOutward() {
        return isSaleWithOutward;
    }

    public void setIsSaleWithOutward(Boolean saleWithOutward) {
        isSaleWithOutward = saleWithOutward;
    }

    @Basic
    @Column(name = "IsCreatedSAReturnLastYear")
    public Boolean getIsCreatedSaReturnLastYear() {
        return isCreatedSaReturnLastYear;
    }

    public void setIsCreatedSaReturnLastYear(Boolean createdSaReturnLastYear) {
        isCreatedSaReturnLastYear = createdSaReturnLastYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InInwardOutwardList that = (InInwardOutwardList) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(cabaRefDate, that.cabaRefDate) &&
                Objects.equals(caRefDate, that.caRefDate) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(cabaPostedDate, that.cabaPostedDate) &&
                Objects.equals(caPostedDate, that.caPostedDate) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(invTemplateNo, that.invTemplateNo) &&
                Objects.equals(refNoFinance, that.refNoFinance) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(refNoManagement, that.refNoManagement) &&
                Objects.equals(cabaRefNoManagement, that.cabaRefNoManagement) &&
                Objects.equals(caRefNoFinance, that.caRefNoFinance) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(cabaRefNoFinance, that.cabaRefNoFinance) &&
                Objects.equals(caRefNoManagement, that.caRefNoManagement) &&
                Objects.equals(isPostedFinance, that.isPostedFinance) &&
                Objects.equals(isPostedManagement, that.isPostedManagement) &&
                Objects.equals(includeInvoice, that.includeInvoice) &&
                Objects.equals(saInvoiceRefId, that.saInvoiceRefId) &&
                Objects.equals(puInvoiceRefId, that.puInvoiceRefId) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(orderNo, that.orderNo) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(orderDate, that.orderDate) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(contactName, that.contactName) &&
                Objects.equals(receiverAddress, that.receiverAddress) &&
                Objects.equals(saReturnRefId, that.saReturnRefId) &&
                Objects.equals(accountObjectBankAccount, that.accountObjectBankAccount) &&
                Objects.equals(unitPriceMethod, that.unitPriceMethod) &&
                Objects.equals(accountObjectBankName, that.accountObjectBankName) &&
                Objects.equals(outDocumentIncluded, that.outDocumentIncluded) &&
                Objects.equals(accountObjectContactName, that.accountObjectContactName) &&
                Objects.equals(payer, that.payer) &&
                Objects.equals(identificationNumber, that.identificationNumber) &&
                Objects.equals(payerAddress, that.payerAddress) &&
                Objects.equals(transporterId, that.transporterId) &&
                Objects.equals(issueDate, that.issueDate) &&
                Objects.equals(payReason, that.payReason) &&
                Objects.equals(transporterName, that.transporterName) &&
                Objects.equals(caDocumentIncluded, that.caDocumentIncluded) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(issueBy, that.issueBy) &&
                Objects.equals(isInvoiceExported, that.isInvoiceExported) &&
                Objects.equals(receiver, that.receiver) &&
                Objects.equals(transport, that.transport) &&
                Objects.equals(fromStockId, that.fromStockId) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(toStockId, that.toStockId) &&
                Objects.equals(documentIncluded, that.documentIncluded) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(cabaJournalMemo, that.cabaJournalMemo) &&
                Objects.equals(cabaDocumentIncluded, that.cabaDocumentIncluded) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(dueTime, that.dueTime) &&
                Objects.equals(paymentDate, that.paymentDate) &&
                Objects.equals(revenueStatus, that.revenueStatus) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(totalAmountOc, that.totalAmountOc) &&
                Objects.equals(totalAmount, that.totalAmount) &&
                Objects.equals(totalImportTaxAmountOc, that.totalImportTaxAmountOc) &&
                Objects.equals(totalImportTaxAmount, that.totalImportTaxAmount) &&
                Objects.equals(totalVatAmountOc, that.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, that.totalVatAmount) &&
                Objects.equals(totalDiscountAmountOc, that.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, that.totalDiscountAmount) &&
                Objects.equals(totalFreightAmount, that.totalFreightAmount) &&
                Objects.equals(totalInwardAmount, that.totalInwardAmount) &&
                Objects.equals(totalSpecialConsumeTaxAmountOc, that.totalSpecialConsumeTaxAmountOc) &&
                Objects.equals(totalSpecialConsumeTaxAmount, that.totalSpecialConsumeTaxAmount) &&
                Objects.equals(totalCustomBeforeAmount, that.totalCustomBeforeAmount) &&
                Objects.equals(displayOnBook, that.displayOnBook) &&
                Objects.equals(isPaid, that.isPaid) &&
                Objects.equals(isPostedCashBookFinance, that.isPostedCashBookFinance) &&
                Objects.equals(isPostedCashBookManagement, that.isPostedCashBookManagement) &&
                Objects.equals(cashBookPostedDate, that.cashBookPostedDate) &&
                Objects.equals(isPostedInventoryBookFinance, that.isPostedInventoryBookFinance) &&
                Objects.equals(isPostedInventoryBookManagement, that.isPostedInventoryBookManagement) &&
                Objects.equals(inventoryPostedDate, that.inventoryPostedDate) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(inType, that.inType) &&
                Objects.equals(listTableName, that.listTableName) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(revenueStatusName, that.revenueStatusName) &&
                Objects.equals(caJournalMemo, that.caJournalMemo) &&
                Objects.equals(totalAmountFinance, that.totalAmountFinance) &&
                Objects.equals(totalAmountManagement, that.totalAmountManagement) &&
                Objects.equals(cabaAmountOc, that.cabaAmountOc) &&
                Objects.equals(cabaAmount, that.cabaAmount) &&
                Objects.equals(outwardDependentRefId, that.outwardDependentRefId) &&
                Objects.equals(assemblyRefId, that.assemblyRefId) &&
                Objects.equals(inRefOrder, that.inRefOrder) &&
                Objects.equals(isSaleWithOutward, that.isSaleWithOutward) &&
                Objects.equals(isCreatedSaReturnLastYear, that.isCreatedSaReturnLastYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refDate, cabaRefDate, caRefDate, postedDate, cabaPostedDate, caPostedDate, refType, invTemplateNo, refNoFinance, invSeries, refNoManagement, cabaRefNoManagement, caRefNoFinance, invNo, cabaRefNoFinance, caRefNoManagement, isPostedFinance, isPostedManagement, includeInvoice, saInvoiceRefId, puInvoiceRefId, accountObjectId, orderNo, accountObjectName, orderDate, accountObjectAddress, contactName, receiverAddress, saReturnRefId, accountObjectBankAccount, unitPriceMethod, accountObjectBankName, outDocumentIncluded, accountObjectContactName, payer, identificationNumber, payerAddress, transporterId, issueDate, payReason, transporterName, caDocumentIncluded, contractCode, issueBy, isInvoiceExported, receiver, transport, fromStockId, journalMemo, toStockId, documentIncluded, employeeId, branchId, cabaJournalMemo, cabaDocumentIncluded, bankAccountId, bankName, paymentTermId, dueTime, paymentDate, revenueStatus, currencyId, exchangeRate, totalAmountOc, totalAmount, totalImportTaxAmountOc, totalImportTaxAmount, totalVatAmountOc, totalVatAmount, totalDiscountAmountOc, totalDiscountAmount, totalFreightAmount, totalInwardAmount, totalSpecialConsumeTaxAmountOc, totalSpecialConsumeTaxAmount, totalCustomBeforeAmount, displayOnBook, isPaid, isPostedCashBookFinance, isPostedCashBookManagement, cashBookPostedDate, isPostedInventoryBookFinance, isPostedInventoryBookManagement, inventoryPostedDate, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, inType, listTableName, refTypeName, revenueStatusName, caJournalMemo, totalAmountFinance, totalAmountManagement, cabaAmountOc, cabaAmount, outwardDependentRefId, assemblyRefId, inRefOrder, isSaleWithOutward, isCreatedSaReturnLastYear);
    }
}
