package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Loại công trình.
 */
@Entity
public class ProjectWorkCategory {

    private Integer id; // PK
    private String projectWorkCategoryCode; // NOT NULL. Mã loại công trình dự án
    private String projectWorkCategoryName; // NOT NULL. Tên loại công trình dự án
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String description; // Mô tả tóm tắt

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ProjectWorkCategoryCode")
    public String getProjectWorkCategoryCode() {
        return projectWorkCategoryCode;
    }

    public void setProjectWorkCategoryCode(String projectWorkCategoryCode) {
        this.projectWorkCategoryCode = projectWorkCategoryCode;
    }

    @Basic
    @Column(name = "ProjectWorkCategoryName")
    public String getProjectWorkCategoryName() {
        return projectWorkCategoryName;
    }

    public void setProjectWorkCategoryName(String projectWorkCategoryName) {
        this.projectWorkCategoryName = projectWorkCategoryName;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectWorkCategory that = (ProjectWorkCategory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(projectWorkCategoryCode, that.projectWorkCategoryCode) &&
                Objects.equals(projectWorkCategoryName, that.projectWorkCategoryName) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, projectWorkCategoryCode, projectWorkCategoryName, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, description);
    }
}
