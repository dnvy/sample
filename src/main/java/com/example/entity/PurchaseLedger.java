package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Bảng Ledger mua hàng.
 */
@Entity
public class PurchaseLedger {

    private Integer id; // PK-Sổ mua hàng
    private Integer refDetailId; // RefDetailID của chứng từ gốc
    private Integer refId; // RefID của chứng từ gốc
    private Integer branchId; // Chi nhánh

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày hạch toán

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày chứng từ

    private Integer refType; // Loại chứng từ (lấy từ bảng RefType)
    private String refNo; // Số chứng từ Sổ quản trị
    private String journalMemo; // Diễn giải master
    private Integer inventoryItemId; // Mã hàng
    private String description; // Diễn giả
    private Integer stockId; // Kho
    private String debitAccount; // TK Nợ
    private String creditAccount; // Tk Có
    private Integer unitId; // Đơn vị tính
    private BigDecimal unitPrice; // Đơn giá quy đổi
    private BigDecimal purchaseQuantity; // Số lượng mua
    private BigDecimal purchaseAmountOc; // Giá trị mua
    private BigDecimal purchaseAmount; // Giá trị mua Quy đổi
    private BigDecimal discountRate; // Tỉ lệ chiết khấu
    private BigDecimal discountAmountOc; // Tiền chiêt khấu
    private BigDecimal discountAmount; // Tiền chiêt khấu quy đổi
    private BigDecimal vatRate; // Thuế suất GTGT
    private BigDecimal vatAmount; // Tiền thuế GTGT quy đổi
    private BigDecimal vatAmountOc; // Tiền thuế nguyên tệ
    private String vatAccount; // TK thuế GTGT
    private BigDecimal returnQuantity; // Số lượng trả lại
    private BigDecimal returnAmountOc; // Giá trị trả lại
    private BigDecimal returnAmount; // Giá trị trả lại Quy đổi
    private BigDecimal reduceAmountOc; // Giá trị giảm
    private BigDecimal reduceAmount; // Giá trị giảm Quy đổi

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate; // Ngày hóa đơn

    private String invSeries; // Ký hiệu hóa đơn
    private String invNo; // Số hóa đơn
    private String currencyId; // Số hóa đơn
    private BigDecimal exchangeRate; // Tỉ giá hối đoái
    private Integer mainUnitId; // Đơn vị chính
    private BigDecimal mainUnitPrice; // Đơn giá theo đơn vị chính
    private BigDecimal mainConvertRate; // Tỷ lệ chuyển đổi ra đơn vị chính
    private BigDecimal mainQuantity; // Số lượng theo đơn vị chính
    private String exchangeRateOperator; // Toán tử quy đổi *=nhân;/=chia
    private Boolean isPostToManagementBook; // Trạng thái ghi vào vào sổ quản trị
    private Integer accountObjectId; // Đối tượng
    private Integer employeeId; // Nhân viên
    private String accountObjectName; // Tên đối tượng
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectTaxCode; // Mã số thuế
    private Integer puOrderRefId; // ID Đơn mua hàng
    private Integer orderId; // ID đơn hàng
    private Integer jobId; // Công việc
    private Integer contractId; // Hợp đồng
    private Integer listItemId; // Mã thống kê
    private Integer expenseItemId; // Khoản mục chi phí
    private Integer organizationUnitId; // ID đơn vị

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiryDate; // Ngày hết hạn

    private String lotNo; // Số lô

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date paymentDate; // Ngày thanh toán

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate; // Hạn thanh toán

    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private Integer refOrder; // Thứ tự các chứng từ nhập trước, nhập sau
    private Integer paymentTermId; // Điều khoản chiết khấu/thanh toán
    private Integer puVoucherRefId; // RefID của chứng từ Mua hàng
    private Integer puVoucherRefDetailId; // RefID của chứng từ Mua hàng
    private String inventoryItemCode; // Mã vật tư, hàng hóa
    private String inventoryItemName; // Tên vật tư, hàng hóa
    private String stockCode; // Mã kho
    private String stockName; // Tên kho
    private String accountObjectCode; // Mã đối tượng
    private String employeeCode; // Mã nhân viên
    private String employeeName; // Tên nhân viên
    private Integer projectWorkId; // Công trình/Dự án
    private String contractCode; // Mã hợp đồng
    private String contractName; // Tên hợp đồng
    private String paymentTermCode; // Mã điều khoản chiết khấu
    private String paymentTermName; // Tên điều khoản chiết khấu
    private Boolean isUpdateRedundant; // Có cần cập nhật dữ liệu từ các bảng dư thừa sang không, 0: không, 1: có. Sau khi cập nhật xong thì thiết lập thông tin này về =0
    private String accountObjectNameDi; // Tên đối tượng (Lấy từ danh mục)
    private String refTypeName;
    private BigDecimal returnMainQuantity;
    private Integer invRefId;
    private BigDecimal unitPriceOc;
    private BigDecimal mainUnitPriceOc;
    private Integer includeInvoice;
    private BigDecimal importChargeAmount;
    private BigDecimal freightAmount;
    private String accountObjectAddressOther;
    private String accountObjectIdentificationNumberOther;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefDetailID")
    public Integer getRefDetailId() {
        return refDetailId;
    }

    public void setRefDetailId(Integer refDetailId) {
        this.refDetailId = refDetailId;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "StockID")
    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "PurchaseQuantity")
    public BigDecimal getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    @Basic
    @Column(name = "PurchaseAmountOC")
    public BigDecimal getPurchaseAmountOc() {
        return purchaseAmountOc;
    }

    public void setPurchaseAmountOc(BigDecimal purchaseAmountOc) {
        this.purchaseAmountOc = purchaseAmountOc;
    }

    @Basic
    @Column(name = "PurchaseAmount")
    public BigDecimal getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(BigDecimal purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    @Basic
    @Column(name = "DiscountRate")
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @Basic
    @Column(name = "DiscountAmountOC")
    public BigDecimal getDiscountAmountOc() {
        return discountAmountOc;
    }

    public void setDiscountAmountOc(BigDecimal discountAmountOc) {
        this.discountAmountOc = discountAmountOc;
    }

    @Basic
    @Column(name = "DiscountAmount")
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "VATAccount")
    public String getVatAccount() {
        return vatAccount;
    }

    public void setVatAccount(String vatAccount) {
        this.vatAccount = vatAccount;
    }

    @Basic
    @Column(name = "ReturnQuantity")
    public BigDecimal getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(BigDecimal returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    @Basic
    @Column(name = "ReturnAmountOC")
    public BigDecimal getReturnAmountOc() {
        return returnAmountOc;
    }

    public void setReturnAmountOc(BigDecimal returnAmountOc) {
        this.returnAmountOc = returnAmountOc;
    }

    @Basic
    @Column(name = "ReturnAmount")
    public BigDecimal getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(BigDecimal returnAmount) {
        this.returnAmount = returnAmount;
    }

    @Basic
    @Column(name = "ReduceAmountOC")
    public BigDecimal getReduceAmountOc() {
        return reduceAmountOc;
    }

    public void setReduceAmountOc(BigDecimal reduceAmountOc) {
        this.reduceAmountOc = reduceAmountOc;
    }

    @Basic
    @Column(name = "ReduceAmount")
    public BigDecimal getReduceAmount() {
        return reduceAmount;
    }

    public void setReduceAmount(BigDecimal reduceAmount) {
        this.reduceAmount = reduceAmount;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "IsPostToManagementBook")
    public Boolean getPostToManagementBook() {
        return isPostToManagementBook;
    }

    public void setPostToManagementBook(Boolean postToManagementBook) {
        isPostToManagementBook = postToManagementBook;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "PUOrderRefID")
    public Integer getPuOrderRefId() {
        return puOrderRefId;
    }

    public void setPuOrderRefId(Integer puOrderRefId) {
        this.puOrderRefId = puOrderRefId;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ExpiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "LotNo")
    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    @Basic
    @Column(name = "PaymentDate")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "DueDate")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "PUVoucherRefID")
    public Integer getPuVoucherRefId() {
        return puVoucherRefId;
    }

    public void setPuVoucherRefId(Integer puVoucherRefId) {
        this.puVoucherRefId = puVoucherRefId;
    }

    @Basic
    @Column(name = "PUVoucherRefDetailID")
    public Integer getPuVoucherRefDetailId() {
        return puVoucherRefDetailId;
    }

    public void setPuVoucherRefDetailId(Integer puVoucherRefDetailId) {
        this.puVoucherRefDetailId = puVoucherRefDetailId;
    }

    @Basic
    @Column(name = "InventoryItemCode")
    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    @Basic
    @Column(name = "InventoryItemName")
    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    @Basic
    @Column(name = "StockCode")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @Basic
    @Column(name = "StockName")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @Basic
    @Column(name = "AccountObjectCode")
    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    @Basic
    @Column(name = "EmployeeCode")
    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Basic
    @Column(name = "EmployeeName")
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "ContractName")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "PaymentTermCode")
    public String getPaymentTermCode() {
        return paymentTermCode;
    }

    public void setPaymentTermCode(String paymentTermCode) {
        this.paymentTermCode = paymentTermCode;
    }

    @Basic
    @Column(name = "PaymentTermName")
    public String getPaymentTermName() {
        return paymentTermName;
    }

    public void setPaymentTermName(String paymentTermName) {
        this.paymentTermName = paymentTermName;
    }

    @Basic
    @Column(name = "IsUpdateRedundant")
    public Boolean getUpdateRedundant() {
        return isUpdateRedundant;
    }

    public void setUpdateRedundant(Boolean updateRedundant) {
        isUpdateRedundant = updateRedundant;
    }

    @Basic
    @Column(name = "AccountObjectNameDI")
    public String getAccountObjectNameDi() {
        return accountObjectNameDi;
    }

    public void setAccountObjectNameDi(String accountObjectNameDi) {
        this.accountObjectNameDi = accountObjectNameDi;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "ReturnMainQuantity")
    public BigDecimal getReturnMainQuantity() {
        return returnMainQuantity;
    }

    public void setReturnMainQuantity(BigDecimal returnMainQuantity) {
        this.returnMainQuantity = returnMainQuantity;
    }

    @Basic
    @Column(name = "InvRefID")
    public Integer getInvRefId() {
        return invRefId;
    }

    public void setInvRefId(Integer invRefId) {
        this.invRefId = invRefId;
    }

    @Basic
    @Column(name = "UnitPriceOC")
    public BigDecimal getUnitPriceOc() {
        return unitPriceOc;
    }

    public void setUnitPriceOc(BigDecimal unitPriceOc) {
        this.unitPriceOc = unitPriceOc;
    }

    @Basic
    @Column(name = "MainUnitPriceOC")
    public BigDecimal getMainUnitPriceOc() {
        return mainUnitPriceOc;
    }

    public void setMainUnitPriceOc(BigDecimal mainUnitPriceOc) {
        this.mainUnitPriceOc = mainUnitPriceOc;
    }

    @Basic
    @Column(name = "IncludeInvoice")
    public Integer getIncludeInvoice() {
        return includeInvoice;
    }

    public void setIncludeInvoice(Integer includeInvoice) {
        this.includeInvoice = includeInvoice;
    }

    @Basic
    @Column(name = "ImportChargeAmount")
    public BigDecimal getImportChargeAmount() {
        return importChargeAmount;
    }

    public void setImportChargeAmount(BigDecimal importChargeAmount) {
        this.importChargeAmount = importChargeAmount;
    }

    @Basic
    @Column(name = "FreightAmount")
    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(BigDecimal freightAmount) {
        this.freightAmount = freightAmount;
    }

    @Basic
    @Column(name = "AccountObjectAddressOther")
    public String getAccountObjectAddressOther() {
        return accountObjectAddressOther;
    }

    public void setAccountObjectAddressOther(String accountObjectAddressOther) {
        this.accountObjectAddressOther = accountObjectAddressOther;
    }

    @Basic
    @Column(name = "AccountObjectIdentificationNumberOther")
    public String getAccountObjectIdentificationNumberOther() {
        return accountObjectIdentificationNumberOther;
    }

    public void setAccountObjectIdentificationNumberOther(String accountObjectIdentificationNumberOther) {
        this.accountObjectIdentificationNumberOther = accountObjectIdentificationNumberOther;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseLedger that = (PurchaseLedger) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refDetailId, that.refDetailId) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refNo, that.refNo) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(stockId, that.stockId) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(purchaseQuantity, that.purchaseQuantity) &&
                Objects.equals(purchaseAmountOc, that.purchaseAmountOc) &&
                Objects.equals(purchaseAmount, that.purchaseAmount) &&
                Objects.equals(discountRate, that.discountRate) &&
                Objects.equals(discountAmountOc, that.discountAmountOc) &&
                Objects.equals(discountAmount, that.discountAmount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(vatAccount, that.vatAccount) &&
                Objects.equals(returnQuantity, that.returnQuantity) &&
                Objects.equals(returnAmountOc, that.returnAmountOc) &&
                Objects.equals(returnAmount, that.returnAmount) &&
                Objects.equals(reduceAmountOc, that.reduceAmountOc) &&
                Objects.equals(reduceAmount, that.reduceAmount) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(isPostToManagementBook, that.isPostToManagementBook) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, that.accountObjectTaxCode) &&
                Objects.equals(puOrderRefId, that.puOrderRefId) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(expiryDate, that.expiryDate) &&
                Objects.equals(lotNo, that.lotNo) &&
                Objects.equals(paymentDate, that.paymentDate) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(puVoucherRefId, that.puVoucherRefId) &&
                Objects.equals(puVoucherRefDetailId, that.puVoucherRefDetailId) &&
                Objects.equals(inventoryItemCode, that.inventoryItemCode) &&
                Objects.equals(inventoryItemName, that.inventoryItemName) &&
                Objects.equals(stockCode, that.stockCode) &&
                Objects.equals(stockName, that.stockName) &&
                Objects.equals(accountObjectCode, that.accountObjectCode) &&
                Objects.equals(employeeCode, that.employeeCode) &&
                Objects.equals(employeeName, that.employeeName) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(contractName, that.contractName) &&
                Objects.equals(paymentTermCode, that.paymentTermCode) &&
                Objects.equals(paymentTermName, that.paymentTermName) &&
                Objects.equals(isUpdateRedundant, that.isUpdateRedundant) &&
                Objects.equals(accountObjectNameDi, that.accountObjectNameDi) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(returnMainQuantity, that.returnMainQuantity) &&
                Objects.equals(invRefId, that.invRefId) &&
                Objects.equals(unitPriceOc, that.unitPriceOc) &&
                Objects.equals(mainUnitPriceOc, that.mainUnitPriceOc) &&
                Objects.equals(includeInvoice, that.includeInvoice) &&
                Objects.equals(importChargeAmount, that.importChargeAmount) &&
                Objects.equals(freightAmount, that.freightAmount) &&
                Objects.equals(accountObjectAddressOther, that.accountObjectAddressOther) &&
                Objects.equals(accountObjectIdentificationNumberOther, that.accountObjectIdentificationNumberOther);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refDetailId, refId, branchId, postedDate, refDate, refType, refNo, journalMemo, inventoryItemId, description, stockId, debitAccount, creditAccount, unitId, unitPrice, purchaseQuantity, purchaseAmountOc, purchaseAmount, discountRate, discountAmountOc, discountAmount, vatRate, vatAmount, vatAmountOc, vatAccount, returnQuantity, returnAmountOc, returnAmount, reduceAmountOc, reduceAmount, invDate, invSeries, invNo, currencyId, exchangeRate, mainUnitId, mainUnitPrice, mainConvertRate, mainQuantity, exchangeRateOperator, isPostToManagementBook, accountObjectId, employeeId, accountObjectName, accountObjectAddress, accountObjectTaxCode, puOrderRefId, orderId, jobId, contractId, listItemId, expenseItemId, organizationUnitId, expiryDate, lotNo, paymentDate, dueDate, sortOrder, refOrder, paymentTermId, puVoucherRefId, puVoucherRefDetailId, inventoryItemCode, inventoryItemName, stockCode, stockName, accountObjectCode, employeeCode, employeeName, projectWorkId, contractCode, contractName, paymentTermCode, paymentTermName, isUpdateRedundant, accountObjectNameDi, refTypeName, returnMainQuantity, invRefId, unitPriceOc, mainUnitPriceOc, includeInvoice, importChargeAmount, freightAmount, accountObjectAddressOther, accountObjectIdentificationNumberOther);
    }
}
