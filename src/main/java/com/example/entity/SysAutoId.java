package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng lưu cấu hình tự tăng của Số chứng từ.
 * Ví dụ, khi vào màn hình thêm Ủy nhiệm chi, số tự fill vào là UNC016,
 * điều đó có nghĩa là
 *
 * branchId = Đơn vị đang trong phiên làm việc.
 * prefixString = UNC
 * suffix = ""
 * lengthOfValue = 3
 * maxExistValue = 15
 */
@Entity
@Table(name = "SYSAutoID")
public class SysAutoId {

    private Integer id; // PK.
    private Integer refTypeCategory; // FK
    private String refTypeCategoryName; // Tên loại chứng từ
    private String prefixString; // Tiền tố
    private Integer maxExistValue; // Giá trị
    private Integer lengthOfValue; // Độ dài
    private String suffix; // Hậu tố
    private Integer branchId; // Chi nhánh. Các thiết lập mặc định mang đi thì BranchID = NULL
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer displayOnBook; // 0=Sổ tài chính; 1=Sổ quản trị; 2=Tự tăng trên cả 2 sổ

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefTypeCategory")
    public Integer getRefTypeCategory() {
        return refTypeCategory;
    }

    public void setRefTypeCategory(Integer refTypeCategory) {
        this.refTypeCategory = refTypeCategory;
    }

    @Basic
    @Column(name = "RefTypeCategoryName")
    public String getRefTypeCategoryName() {
        return refTypeCategoryName;
    }

    public void setRefTypeCategoryName(String refTypeCategoryName) {
        this.refTypeCategoryName = refTypeCategoryName;
    }

    @Basic
    @Column(name = "PrefixString")
    public String getPrefixString() {
        return prefixString;
    }

    public void setPrefixString(String prefixString) {
        this.prefixString = prefixString;
    }

    @Basic
    @Column(name = "MaxExistValue")
    public Integer getMaxExistValue() {
        return maxExistValue;
    }

    public void setMaxExistValue(Integer maxExistValue) {
        this.maxExistValue = maxExistValue;
    }

    @Basic
    @Column(name = "LengthOfValue")
    public Integer getLengthOfValue() {
        return lengthOfValue;
    }

    public void setLengthOfValue(Integer lengthOfValue) {
        this.lengthOfValue = lengthOfValue;
    }

    @Basic
    @Column(name = "Suffix")
    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysAutoId sysAutoId = (SysAutoId) o;
        return Objects.equals(id, sysAutoId.id) &&
                Objects.equals(refTypeCategory, sysAutoId.refTypeCategory) &&
                Objects.equals(refTypeCategoryName, sysAutoId.refTypeCategoryName) &&
                Objects.equals(prefixString, sysAutoId.prefixString) &&
                Objects.equals(maxExistValue, sysAutoId.maxExistValue) &&
                Objects.equals(lengthOfValue, sysAutoId.lengthOfValue) &&
                Objects.equals(suffix, sysAutoId.suffix) &&
                Objects.equals(branchId, sysAutoId.branchId) &&
                Objects.equals(createdDate, sysAutoId.createdDate) &&
                Objects.equals(createdBy, sysAutoId.createdBy) &&
                Objects.equals(modifiedDate, sysAutoId.modifiedDate) &&
                Objects.equals(modifiedBy, sysAutoId.modifiedBy) &&
                Objects.equals(displayOnBook, sysAutoId.displayOnBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refTypeCategory, refTypeCategoryName, prefixString, maxExistValue, lengthOfValue, suffix, branchId, createdDate, createdBy, modifiedDate, modifiedBy, displayOnBook);
    }
}
