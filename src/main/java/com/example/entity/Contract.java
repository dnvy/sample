package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Hợp đồng bán. Bảng hợp đồng
 */
@Entity
public class Contract {

    private Integer id; // NOT NULL. PK
    private Integer branchId; // Chi nhánh
    private Boolean isProject; // NOT NULL. True: Là dự án; Flase: Là hợp đồng
    private Integer projectId; // Cho biết Hợp đồng thuộc dự án nào
    private String contractCode; // NOT NULL. Số hợp đồng bán/mua

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date signDate; // Ngày ký (*)

    private String contractSubject; // Trích yếu hợp đồng bán/mua
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá
    private BigDecimal contractAmountOc; // NOT NULL. Giá trị hợp đồng/dự án
    private BigDecimal contractAmount; // NOT NULL. Giá trị hợp đồng/dự án Quy đổi
    private BigDecimal closeAmountOc; // NOT NULL. Giá trị thanh lý
    private BigDecimal closeAmount; // NOT NULL. Giá trị thanh lý Quy đổi

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date paymentDate; // Hạn thanh toán

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliveryDate; // Hạn giao hàng


    private Boolean isArisedBeforeUseSoftware; // NOT NULL. Là hợp đồng phát sinh trước khi sử dụng phần mềm
    private BigDecimal expenseAmountFinance; // NOT NULL. Số đã chi
    private BigDecimal receiptAmountOcFinance; // NOT NULL. Số đã thu
    private BigDecimal receiptAmountFinance; // NOT NULL. Số đã thu Quy đổi
    private BigDecimal invoiceAmountOcFinance; // NOT NULL. Giá trị đã xuất hóa đơn
    private BigDecimal invoiceAmountFinance; // NOT NULL. Giá trị đã xuất hóa đơn Quy đổi
    private Integer accountObjectId; // ID là khách hàng (cá nhân hay tổ chức) trong hợp đồng bán
    private String accountObjectAddress; // Địa chỉ khách hàng
    private String accountObjectContactName; // Người liên hệ của khách hàng
    private Integer organizationUnitId; // Đơn vị thực hiện
    private Integer employeeId; // Người thực hiện

    // 0 = Chưa thực hiện. 1 = Đang thực hiện. 2 = Đã thanh lý. 3 = Đã hủy bỏ.
    private Integer contractStatusId; // Tình trạng hợp đồng bán/mua

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date closeDate; // Ngày thanh lý/Hủy bỏ

    private String closeReason; // Lý do thanh lý, hủy bỏ
    private Integer revenueStatus; // Tình trạng ghi nhận doanh số. 0=Chưa ghi doanh số: 1=Đã ghi doanh số
    private String otherTerms; // Điều khoản khác
    private Boolean isCalculatedCost; // NOT NULL. Check tính giá thành
    private Boolean isInvoiced; // NOT NULL. Đã xuất hóa đơn

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date revenueDate; // Ngày ghi nhận doanh số

    private BigDecimal totalExpenseExpectAmount; // NOT NULL. Dự kiến chi
    private BigDecimal totalExpensedAmount; // NOT NULL. Thực chi
    private BigDecimal balanceExpenseAmountFinance; // NOT NULL. Số còn phải chi=Dự kiến chi-thực chi
    private BigDecimal totalReceiptedAmount; // NOT NULL. Thực thu
    private BigDecimal balanceReceiptAmountFinance; // NOT NULL. Số còn phải thu
    private BigDecimal profitAndLossExpectAmountFinance; // NOT NULL. Dự kiến lãi lỗ
    private String createdBy; // Người tạo
    private Timestamp createdDate; // Ngày tạo
    private String modifiedBy; // Người sửa cuối
    private Timestamp modifiedDate; // Ngày sửa cuối
    private Integer refType; // NOT NULL. Loại chứng từ
    private Integer saOrderId;
    private Boolean isParent;
    private BigDecimal totalInvoiceAmountFinance; // NOT NULL. Tổng giá trị đã xuất hóa đơn Sổ Tài Chính
    private BigDecimal totalInvoiceAmountManagement; // NOT NULL. Tổng giá trị đã xuất hóa đơn Sổ Quản Trị
    private BigDecimal accumSaleAmountFinance; // NOT NULL.
    private BigDecimal accumCostAmountFinance; // NOT NULL.
    private BigDecimal accumOtherAmountFinance; // NOT NULL.
    private BigDecimal expenseAmountManagement; // NOT NULL.
    private BigDecimal receiptAmountOcManagement; // NOT NULL.
    private BigDecimal receiptAmountManagement; // NOT NULL.
    private BigDecimal invoiceAmountOcManagement; // NOT NULL.
    private BigDecimal invoiceAmountManagement; // NOT NULL.
    private BigDecimal accumSaleAmountManagement; // NOT NULL.
    private BigDecimal accumCostAmountManagement; // NOT NULL.
    private BigDecimal accumOtherAmountManagement; // NOT NULL.
    private BigDecimal totalInvoiceAmountOcFinance; // NOT NULL.
    private BigDecimal totalInvoiceAmountOcManagement; // NOT NULL.
    private BigDecimal balanceReceiptAmountManagement; // NOT NULL.
    private BigDecimal balanceExpenseAmountManagement; // NOT NULL.
    private BigDecimal profitAndLossExpectAmountManagement; // NOT NULL.
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private String accountObjectName; // Tên khách hàng

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "IsProject")
    public Boolean getIsProject() {
        return isProject;
    }

    public void setIsProject(Boolean project) {
        isProject = project;
    }

    @Basic
    @Column(name = "ProjectID")
    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "SignDate")
    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    @Basic
    @Column(name = "ContractSubject")
    public String getContractSubject() {
        return contractSubject;
    }

    public void setContractSubject(String contractSubject) {
        this.contractSubject = contractSubject;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "ContractAmountOC")
    public BigDecimal getContractAmountOc() {
        return contractAmountOc;
    }

    public void setContractAmountOc(BigDecimal contractAmountOc) {
        this.contractAmountOc = contractAmountOc;
    }

    @Basic
    @Column(name = "ContractAmount")
    public BigDecimal getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }

    @Basic
    @Column(name = "CloseAmountOC")
    public BigDecimal getCloseAmountOc() {
        return closeAmountOc;
    }

    public void setCloseAmountOc(BigDecimal closeAmountOc) {
        this.closeAmountOc = closeAmountOc;
    }

    @Basic
    @Column(name = "CloseAmount")
    public BigDecimal getCloseAmount() {
        return closeAmount;
    }

    public void setCloseAmount(BigDecimal closeAmount) {
        this.closeAmount = closeAmount;
    }

    @Basic
    @Column(name = "PaymentDate")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "DeliveryDate")
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @Basic
    @Column(name = "IsArisedBeforeUseSoftware")
    public Boolean getIsArisedBeforeUseSoftware() {
        return isArisedBeforeUseSoftware;
    }

    public void setIsArisedBeforeUseSoftware(Boolean arisedBeforeUseSoftware) {
        isArisedBeforeUseSoftware = arisedBeforeUseSoftware;
    }

    @Basic
    @Column(name = "ExpenseAmountFinance")
    public BigDecimal getExpenseAmountFinance() {
        return expenseAmountFinance;
    }

    public void setExpenseAmountFinance(BigDecimal expenseAmountFinance) {
        this.expenseAmountFinance = expenseAmountFinance;
    }

    @Basic
    @Column(name = "ReceiptAmountOCFinance")
    public BigDecimal getReceiptAmountOcFinance() {
        return receiptAmountOcFinance;
    }

    public void setReceiptAmountOcFinance(BigDecimal receiptAmountOcFinance) {
        this.receiptAmountOcFinance = receiptAmountOcFinance;
    }

    @Basic
    @Column(name = "ReceiptAmountFinance")
    public BigDecimal getReceiptAmountFinance() {
        return receiptAmountFinance;
    }

    public void setReceiptAmountFinance(BigDecimal receiptAmountFinance) {
        this.receiptAmountFinance = receiptAmountFinance;
    }

    @Basic
    @Column(name = "InvoiceAmountOCFinance")
    public BigDecimal getInvoiceAmountOcFinance() {
        return invoiceAmountOcFinance;
    }

    public void setInvoiceAmountOcFinance(BigDecimal invoiceAmountOcFinance) {
        this.invoiceAmountOcFinance = invoiceAmountOcFinance;
    }

    @Basic
    @Column(name = "InvoiceAmountFinance")
    public BigDecimal getInvoiceAmountFinance() {
        return invoiceAmountFinance;
    }

    public void setInvoiceAmountFinance(BigDecimal invoiceAmountFinance) {
        this.invoiceAmountFinance = invoiceAmountFinance;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "ContractStatusID")
    public Integer getContractStatusId() {
        return contractStatusId;
    }

    public void setContractStatusId(Integer contractStatusId) {
        this.contractStatusId = contractStatusId;
    }

    @Basic
    @Column(name = "CloseDate")
    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    @Basic
    @Column(name = "CloseReason")
    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    @Basic
    @Column(name = "RevenueStatus")
    public Integer getRevenueStatus() {
        return revenueStatus;
    }

    public void setRevenueStatus(Integer revenueStatus) {
        this.revenueStatus = revenueStatus;
    }

    @Basic
    @Column(name = "OtherTerms")
    public String getOtherTerms() {
        return otherTerms;
    }

    public void setOtherTerms(String otherTerms) {
        this.otherTerms = otherTerms;
    }

    @Basic
    @Column(name = "IsCalculatedCost")
    public Boolean getIsCalculatedCost() {
        return isCalculatedCost;
    }

    public void setIsCalculatedCost(Boolean calculatedCost) {
        isCalculatedCost = calculatedCost;
    }

    @Basic
    @Column(name = "IsInvoiced")
    public Boolean getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Boolean invoiced) {
        isInvoiced = invoiced;
    }

    @Basic
    @Column(name = "RevenueDate")
    public Date getRevenueDate() {
        return revenueDate;
    }

    public void setRevenueDate(Date revenueDate) {
        this.revenueDate = revenueDate;
    }

    @Basic
    @Column(name = "TotalExpenseExpectAmount")
    public BigDecimal getTotalExpenseExpectAmount() {
        return totalExpenseExpectAmount;
    }

    public void setTotalExpenseExpectAmount(BigDecimal totalExpenseExpectAmount) {
        this.totalExpenseExpectAmount = totalExpenseExpectAmount;
    }

    @Basic
    @Column(name = "TotalExpensedAmount")
    public BigDecimal getTotalExpensedAmount() {
        return totalExpensedAmount;
    }

    public void setTotalExpensedAmount(BigDecimal totalExpensedAmount) {
        this.totalExpensedAmount = totalExpensedAmount;
    }

    @Basic
    @Column(name = "BalanceExpenseAmountFinance")
    public BigDecimal getBalanceExpenseAmountFinance() {
        return balanceExpenseAmountFinance;
    }

    public void setBalanceExpenseAmountFinance(BigDecimal balanceExpenseAmountFinance) {
        this.balanceExpenseAmountFinance = balanceExpenseAmountFinance;
    }

    @Basic
    @Column(name = "TotalReceiptedAmount")
    public BigDecimal getTotalReceiptedAmount() {
        return totalReceiptedAmount;
    }

    public void setTotalReceiptedAmount(BigDecimal totalReceiptedAmount) {
        this.totalReceiptedAmount = totalReceiptedAmount;
    }

    @Basic
    @Column(name = "BalanceReceiptAmountFinance")
    public BigDecimal getBalanceReceiptAmountFinance() {
        return balanceReceiptAmountFinance;
    }

    public void setBalanceReceiptAmountFinance(BigDecimal balanceReceiptAmountFinance) {
        this.balanceReceiptAmountFinance = balanceReceiptAmountFinance;
    }

    @Basic
    @Column(name = "ProfitAndLossExpectAmountFinance")
    public BigDecimal getProfitAndLossExpectAmountFinance() {
        return profitAndLossExpectAmountFinance;
    }

    public void setProfitAndLossExpectAmountFinance(BigDecimal profitAndLossExpectAmountFinance) {
        this.profitAndLossExpectAmountFinance = profitAndLossExpectAmountFinance;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "SAOrderID")
    public Integer getSaOrderId() {
        return saOrderId;
    }

    public void setSaOrderId(Integer saOrderId) {
        this.saOrderId = saOrderId;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "TotalInvoiceAmountFinance")
    public BigDecimal getTotalInvoiceAmountFinance() {
        return totalInvoiceAmountFinance;
    }

    public void setTotalInvoiceAmountFinance(BigDecimal totalInvoiceAmountFinance) {
        this.totalInvoiceAmountFinance = totalInvoiceAmountFinance;
    }

    @Basic
    @Column(name = "TotalInvoiceAmountManagement")
    public BigDecimal getTotalInvoiceAmountManagement() {
        return totalInvoiceAmountManagement;
    }

    public void setTotalInvoiceAmountManagement(BigDecimal totalInvoiceAmountManagement) {
        this.totalInvoiceAmountManagement = totalInvoiceAmountManagement;
    }

    @Basic
    @Column(name = "AccumSaleAmountFinance")
    public BigDecimal getAccumSaleAmountFinance() {
        return accumSaleAmountFinance;
    }

    public void setAccumSaleAmountFinance(BigDecimal accumSaleAmountFinance) {
        this.accumSaleAmountFinance = accumSaleAmountFinance;
    }

    @Basic
    @Column(name = "AccumCostAmountFinance")
    public BigDecimal getAccumCostAmountFinance() {
        return accumCostAmountFinance;
    }

    public void setAccumCostAmountFinance(BigDecimal accumCostAmountFinance) {
        this.accumCostAmountFinance = accumCostAmountFinance;
    }

    @Basic
    @Column(name = "AccumOtherAmountFinance")
    public BigDecimal getAccumOtherAmountFinance() {
        return accumOtherAmountFinance;
    }

    public void setAccumOtherAmountFinance(BigDecimal accumOtherAmountFinance) {
        this.accumOtherAmountFinance = accumOtherAmountFinance;
    }

    @Basic
    @Column(name = "ExpenseAmountManagement")
    public BigDecimal getExpenseAmountManagement() {
        return expenseAmountManagement;
    }

    public void setExpenseAmountManagement(BigDecimal expenseAmountManagement) {
        this.expenseAmountManagement = expenseAmountManagement;
    }

    @Basic
    @Column(name = "ReceiptAmountOCManagement")
    public BigDecimal getReceiptAmountOcManagement() {
        return receiptAmountOcManagement;
    }

    public void setReceiptAmountOcManagement(BigDecimal receiptAmountOcManagement) {
        this.receiptAmountOcManagement = receiptAmountOcManagement;
    }

    @Basic
    @Column(name = "ReceiptAmountManagement")
    public BigDecimal getReceiptAmountManagement() {
        return receiptAmountManagement;
    }

    public void setReceiptAmountManagement(BigDecimal receiptAmountManagement) {
        this.receiptAmountManagement = receiptAmountManagement;
    }

    @Basic
    @Column(name = "InvoiceAmountOCManagement")
    public BigDecimal getInvoiceAmountOcManagement() {
        return invoiceAmountOcManagement;
    }

    public void setInvoiceAmountOcManagement(BigDecimal invoiceAmountOcManagement) {
        this.invoiceAmountOcManagement = invoiceAmountOcManagement;
    }

    @Basic
    @Column(name = "InvoiceAmountManagement")
    public BigDecimal getInvoiceAmountManagement() {
        return invoiceAmountManagement;
    }

    public void setInvoiceAmountManagement(BigDecimal invoiceAmountManagement) {
        this.invoiceAmountManagement = invoiceAmountManagement;
    }

    @Basic
    @Column(name = "AccumSaleAmountManagement")
    public BigDecimal getAccumSaleAmountManagement() {
        return accumSaleAmountManagement;
    }

    public void setAccumSaleAmountManagement(BigDecimal accumSaleAmountManagement) {
        this.accumSaleAmountManagement = accumSaleAmountManagement;
    }

    @Basic
    @Column(name = "AccumCostAmountManagement")
    public BigDecimal getAccumCostAmountManagement() {
        return accumCostAmountManagement;
    }

    public void setAccumCostAmountManagement(BigDecimal accumCostAmountManagement) {
        this.accumCostAmountManagement = accumCostAmountManagement;
    }

    @Basic
    @Column(name = "AccumOtherAmountManagement")
    public BigDecimal getAccumOtherAmountManagement() {
        return accumOtherAmountManagement;
    }

    public void setAccumOtherAmountManagement(BigDecimal accumOtherAmountManagement) {
        this.accumOtherAmountManagement = accumOtherAmountManagement;
    }

    @Basic
    @Column(name = "TotalInvoiceAmountOCFinance")
    public BigDecimal getTotalInvoiceAmountOcFinance() {
        return totalInvoiceAmountOcFinance;
    }

    public void setTotalInvoiceAmountOcFinance(BigDecimal totalInvoiceAmountOcFinance) {
        this.totalInvoiceAmountOcFinance = totalInvoiceAmountOcFinance;
    }

    @Basic
    @Column(name = "TotalInvoiceAmountOCManagement")
    public BigDecimal getTotalInvoiceAmountOcManagement() {
        return totalInvoiceAmountOcManagement;
    }

    public void setTotalInvoiceAmountOcManagement(BigDecimal totalInvoiceAmountOcManagement) {
        this.totalInvoiceAmountOcManagement = totalInvoiceAmountOcManagement;
    }

    @Basic
    @Column(name = "BalanceReceiptAmountManagement")
    public BigDecimal getBalanceReceiptAmountManagement() {
        return balanceReceiptAmountManagement;
    }

    public void setBalanceReceiptAmountManagement(BigDecimal balanceReceiptAmountManagement) {
        this.balanceReceiptAmountManagement = balanceReceiptAmountManagement;
    }

    @Basic
    @Column(name = "BalanceExpenseAmountManagement")
    public BigDecimal getBalanceExpenseAmountManagement() {
        return balanceExpenseAmountManagement;
    }

    public void setBalanceExpenseAmountManagement(BigDecimal balanceExpenseAmountManagement) {
        this.balanceExpenseAmountManagement = balanceExpenseAmountManagement;
    }

    @Basic
    @Column(name = "ProfitAndLossExpectAmountManagement")
    public BigDecimal getProfitAndLossExpectAmountManagement() {
        return profitAndLossExpectAmountManagement;
    }

    public void setProfitAndLossExpectAmountManagement(BigDecimal profitAndLossExpectAmountManagement) {
        this.profitAndLossExpectAmountManagement = profitAndLossExpectAmountManagement;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contract contract = (Contract) o;
        return Objects.equals(id, contract.id) &&
                Objects.equals(branchId, contract.branchId) &&
                Objects.equals(isProject, contract.isProject) &&
                Objects.equals(projectId, contract.projectId) &&
                Objects.equals(contractCode, contract.contractCode) &&
                Objects.equals(signDate, contract.signDate) &&
                Objects.equals(contractSubject, contract.contractSubject) &&
                Objects.equals(currencyId, contract.currencyId) &&
                Objects.equals(exchangeRate, contract.exchangeRate) &&
                Objects.equals(contractAmountOc, contract.contractAmountOc) &&
                Objects.equals(contractAmount, contract.contractAmount) &&
                Objects.equals(closeAmountOc, contract.closeAmountOc) &&
                Objects.equals(closeAmount, contract.closeAmount) &&
                Objects.equals(paymentDate, contract.paymentDate) &&
                Objects.equals(deliveryDate, contract.deliveryDate) &&
                Objects.equals(isArisedBeforeUseSoftware, contract.isArisedBeforeUseSoftware) &&
                Objects.equals(expenseAmountFinance, contract.expenseAmountFinance) &&
                Objects.equals(receiptAmountOcFinance, contract.receiptAmountOcFinance) &&
                Objects.equals(receiptAmountFinance, contract.receiptAmountFinance) &&
                Objects.equals(invoiceAmountOcFinance, contract.invoiceAmountOcFinance) &&
                Objects.equals(invoiceAmountFinance, contract.invoiceAmountFinance) &&
                Objects.equals(accountObjectId, contract.accountObjectId) &&
                Objects.equals(accountObjectAddress, contract.accountObjectAddress) &&
                Objects.equals(accountObjectContactName, contract.accountObjectContactName) &&
                Objects.equals(organizationUnitId, contract.organizationUnitId) &&
                Objects.equals(employeeId, contract.employeeId) &&
                Objects.equals(contractStatusId, contract.contractStatusId) &&
                Objects.equals(closeDate, contract.closeDate) &&
                Objects.equals(closeReason, contract.closeReason) &&
                Objects.equals(revenueStatus, contract.revenueStatus) &&
                Objects.equals(otherTerms, contract.otherTerms) &&
                Objects.equals(isCalculatedCost, contract.isCalculatedCost) &&
                Objects.equals(isInvoiced, contract.isInvoiced) &&
                Objects.equals(revenueDate, contract.revenueDate) &&
                Objects.equals(totalExpenseExpectAmount, contract.totalExpenseExpectAmount) &&
                Objects.equals(totalExpensedAmount, contract.totalExpensedAmount) &&
                Objects.equals(balanceExpenseAmountFinance, contract.balanceExpenseAmountFinance) &&
                Objects.equals(totalReceiptedAmount, contract.totalReceiptedAmount) &&
                Objects.equals(balanceReceiptAmountFinance, contract.balanceReceiptAmountFinance) &&
                Objects.equals(profitAndLossExpectAmountFinance, contract.profitAndLossExpectAmountFinance) &&
                Objects.equals(createdBy, contract.createdBy) &&
                Objects.equals(createdDate, contract.createdDate) &&
                Objects.equals(modifiedBy, contract.modifiedBy) &&
                Objects.equals(modifiedDate, contract.modifiedDate) &&
                Objects.equals(refType, contract.refType) &&
                Objects.equals(saOrderId, contract.saOrderId) &&
                Objects.equals(isParent, contract.isParent) &&
                Objects.equals(totalInvoiceAmountFinance, contract.totalInvoiceAmountFinance) &&
                Objects.equals(totalInvoiceAmountManagement, contract.totalInvoiceAmountManagement) &&
                Objects.equals(accumSaleAmountFinance, contract.accumSaleAmountFinance) &&
                Objects.equals(accumCostAmountFinance, contract.accumCostAmountFinance) &&
                Objects.equals(accumOtherAmountFinance, contract.accumOtherAmountFinance) &&
                Objects.equals(expenseAmountManagement, contract.expenseAmountManagement) &&
                Objects.equals(receiptAmountOcManagement, contract.receiptAmountOcManagement) &&
                Objects.equals(receiptAmountManagement, contract.receiptAmountManagement) &&
                Objects.equals(invoiceAmountOcManagement, contract.invoiceAmountOcManagement) &&
                Objects.equals(invoiceAmountManagement, contract.invoiceAmountManagement) &&
                Objects.equals(accumSaleAmountManagement, contract.accumSaleAmountManagement) &&
                Objects.equals(accumCostAmountManagement, contract.accumCostAmountManagement) &&
                Objects.equals(accumOtherAmountManagement, contract.accumOtherAmountManagement) &&
                Objects.equals(totalInvoiceAmountOcFinance, contract.totalInvoiceAmountOcFinance) &&
                Objects.equals(totalInvoiceAmountOcManagement, contract.totalInvoiceAmountOcManagement) &&
                Objects.equals(balanceReceiptAmountManagement, contract.balanceReceiptAmountManagement) &&
                Objects.equals(balanceExpenseAmountManagement, contract.balanceExpenseAmountManagement) &&
                Objects.equals(profitAndLossExpectAmountManagement, contract.profitAndLossExpectAmountManagement) &&
                Objects.equals(customField1, contract.customField1) &&
                Objects.equals(customField2, contract.customField2) &&
                Objects.equals(customField3, contract.customField3) &&
                Objects.equals(customField4, contract.customField4) &&
                Objects.equals(customField5, contract.customField5) &&
                Objects.equals(customField6, contract.customField6) &&
                Objects.equals(customField7, contract.customField7) &&
                Objects.equals(customField8, contract.customField8) &&
                Objects.equals(customField9, contract.customField9) &&
                Objects.equals(customField10, contract.customField10) &&
                Objects.equals(accountObjectName, contract.accountObjectName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, isProject, projectId, contractCode, signDate, contractSubject, currencyId, exchangeRate, contractAmountOc, contractAmount, closeAmountOc, closeAmount, paymentDate, deliveryDate, isArisedBeforeUseSoftware, expenseAmountFinance, receiptAmountOcFinance, receiptAmountFinance, invoiceAmountOcFinance, invoiceAmountFinance, accountObjectId, accountObjectAddress, accountObjectContactName, organizationUnitId, employeeId, contractStatusId, closeDate, closeReason, revenueStatus, otherTerms, isCalculatedCost, isInvoiced, revenueDate, totalExpenseExpectAmount, totalExpensedAmount, balanceExpenseAmountFinance, totalReceiptedAmount, balanceReceiptAmountFinance, profitAndLossExpectAmountFinance, createdBy, createdDate, modifiedBy, modifiedDate, refType, saOrderId, isParent, totalInvoiceAmountFinance, totalInvoiceAmountManagement, accumSaleAmountFinance, accumCostAmountFinance, accumOtherAmountFinance, expenseAmountManagement, receiptAmountOcManagement, receiptAmountManagement, invoiceAmountOcManagement, invoiceAmountManagement, accumSaleAmountManagement, accumCostAmountManagement, accumOtherAmountManagement, totalInvoiceAmountOcFinance, totalInvoiceAmountOcManagement, balanceReceiptAmountManagement, balanceExpenseAmountManagement, profitAndLossExpectAmountManagement, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, accountObjectName);
    }
}
