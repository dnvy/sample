package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Loại Lý do giao dịch Tiền mặt - Ngân hàng.
 */
@Entity
@Table(name = "CABAReasonType")
public class CabaReasonType {

    private Integer id;
    private Integer refType;
    private String reasonTypeName;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "ReasonTypeName")
    public String getReasonTypeName() {
        return reasonTypeName;
    }

    public void setReasonTypeName(String reasonTypeName) {
        this.reasonTypeName = reasonTypeName;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CabaReasonType that = (CabaReasonType) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(reasonTypeName, that.reasonTypeName) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, reasonTypeName, description);
    }
}
