package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Thông tin chi tiết về Đơn vị, phòng ban.
 */
@Entity
public class OrganizationUnitInfo {

    private Integer id;
    private Integer organizationUnitId;
    private String itemCode;
    private String itemName;
    private String description;
    private Boolean isBold;
    private Boolean isItalic;
    private Integer sortOrder;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ItemCode")
    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    @Basic
    @Column(name = "ItemName")
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "IsBold")
    public Boolean getIsBold() {
        return isBold;
    }

    public void setIsBold(Boolean bold) {
        isBold = bold;
    }

    @Basic
    @Column(name = "IsItalic")
    public Boolean getIsItalic() {
        return isItalic;
    }

    public void setIsItalic(Boolean italic) {
        isItalic = italic;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationUnitInfo that = (OrganizationUnitInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(itemCode, that.itemCode) &&
                Objects.equals(itemName, that.itemName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(isBold, that.isBold) &&
                Objects.equals(isItalic, that.isItalic) &&
                Objects.equals(sortOrder, that.sortOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, organizationUnitId, itemCode, itemName, description, isBold, isItalic, sortOrder);
    }
}
