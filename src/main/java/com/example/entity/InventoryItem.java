package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Danh mục Vật tư, hàng hóa. Xem http://localhost:8080/inventory_items
 */
@Entity
public class InventoryItem {

    private Integer id; // NOT NULL
    private Integer branchId;
    private String barCode;
    private String inventoryItemCode; // NOT NULL. Mã
    private String inventoryItemName; // Tên
    private Integer inventoryItemType; // Tính chất: 0 = Là Vật tư hàng hóa; 1 = Là thành phẩm; 2 = Là dịch vụ; 3 = Chỉ là diễn giải
    private String inventoryItemCategoryList; // Nhóm vật tư hàng hóa. Lưu VyCodeID của các nhóm VTHH, phân tách nhau bởi dấu ;
    private String inventoryItemCategoryCode; // Mã nhóm vật tư hàng hóa. Lưu Mã của các nhóm VTHH, phân tách nhau bởi dấu ;
    private String description; // Mô tả. Mô tả thêm thông tin hàng hóa, quy cách
    private Integer unitId; // Đơn vị tính chính
    private String guarantyPeriod; // Thời hạn bảo hành
    private BigDecimal minimumStock; // NOT NULL. Số lượng tồn tối thiểu
    private String inventoryItemSource; // Nguồn gốc. Nguồn gốc hàng hóa.
    private Integer defaultStockId;     // Kho ngầm định.
    private String inventoryAccount; // Tài khoản kho
    private String cogsAccount;         // Tài khoản chi phí (giá vốn)
    private String saleAccount; // Tài khoản doanh thu
    private BigDecimal purchaseDiscountRate; // NOT NULL.
    private BigDecimal unitPrice; // NOT NULL. Đơn giá mua gần nhất
    private BigDecimal salePrice1; // NOT NULL. Đơn giá bán 1
    private BigDecimal salePrice2; // NOT NULL. Đơn giá bán 2
    private BigDecimal salePrice3; // NOT NULL. Đơn giá bán 3
    private BigDecimal fixedSalePrice; // NOT NULL. Đơn giá bán cố định
    private boolean isUnitPriceAfterTax; // NOT NULL.
    private BigDecimal taxRate; // Thuế suất thuế GTGT (%). Không chịu thuế = -1.
    private BigDecimal importTaxRate;
    private BigDecimal exportTaxRate;
    private Integer inventoryCategorySpecialTaxId; // Nhóm hàng hóa chịu thuế Tiêu thụ đặc biệt
    private boolean isSystem;
    private boolean activeStatus;
    private Boolean isSaleDiscount;
    private Integer discountType;
    private Boolean isFollowSerialNumber;
    private Integer costMethod;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String inventoryItemCategoryName;
    private String specificity; // Đặc tính
    private String purchaseDescription; // Diễn giải khi mua
    private String saleDescription; // Diễn giải khi bán
    private Integer taCareerGroupId; // ID nhóm ngành nghề.
    private String image;   // Hình ảnh sản phẩm
    private BigDecimal fixedUnitPrice; // NOT NULL. Đơn giá mua cố định
    private String frontEndFormula; // Công thức tính số lượng trên chứng từ bán hàng do NSD nhập.
    private String backEndFormula; // Công thức tính số lượng trên chứng từ bán hàng chuyển từ công thức NSD ra để dùng tính toán trên chứng từ bán hàng.
    private Integer baseOnFormula; // Lưu Value của Enum Dựa trên cách tính khi thiết lập công thức tính số lượng. Cho phép NSD không nhập liệu.
    private String discountAccount;
    private String saleOffAccount; // Tài khoản giảm giá.
    private String returnAccount; // Tài khoản trả lại

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "BarCode")
    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Basic
    @Column(name = "InventoryItemCode")
    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    @Basic
    @Column(name = "InventoryItemName")
    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    @Basic
    @Column(name = "InventoryItemType")
    public Integer getInventoryItemType() {
        return inventoryItemType;
    }

    public void setInventoryItemType(Integer inventoryItemType) {
        this.inventoryItemType = inventoryItemType;
    }

    @Basic
    @Column(name = "InventoryItemCategoryList")
    public String getInventoryItemCategoryList() {
        return inventoryItemCategoryList;
    }

    public void setInventoryItemCategoryList(String inventoryItemCategoryList) {
        this.inventoryItemCategoryList = inventoryItemCategoryList;
    }

    @Basic
    @Column(name = "InventoryItemCategoryCode")
    public String getInventoryItemCategoryCode() {
        return inventoryItemCategoryCode;
    }

    public void setInventoryItemCategoryCode(String inventoryItemCategoryCode) {
        this.inventoryItemCategoryCode = inventoryItemCategoryCode;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "GuarantyPeriod")
    public String getGuarantyPeriod() {
        return guarantyPeriod;
    }

    public void setGuarantyPeriod(String guarantyPeriod) {
        this.guarantyPeriod = guarantyPeriod;
    }

    @Basic
    @Column(name = "MinimumStock")
    public BigDecimal getMinimumStock() {
        return minimumStock;
    }

    public void setMinimumStock(BigDecimal minimumStock) {
        this.minimumStock = minimumStock;
    }

    @Basic
    @Column(name = "InventoryItemSource")
    public String getInventoryItemSource() {
        return inventoryItemSource;
    }

    public void setInventoryItemSource(String inventoryItemSource) {
        this.inventoryItemSource = inventoryItemSource;
    }

    @Basic
    @Column(name = "DefaultStockID")
    public Integer getDefaultStockId() {
        return defaultStockId;
    }

    public void setDefaultStockId(Integer defaultStockId) {
        this.defaultStockId = defaultStockId;
    }

    @Basic
    @Column(name = "InventoryAccount")
    public String getInventoryAccount() {
        return inventoryAccount;
    }

    public void setInventoryAccount(String inventoryAccount) {
        this.inventoryAccount = inventoryAccount;
    }

    @Basic
    @Column(name = "COGSAccount")
    public String getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(String cogsAccount) {
        this.cogsAccount = cogsAccount;
    }

    @Basic
    @Column(name = "SaleAccount")
    public String getSaleAccount() {
        return saleAccount;
    }

    public void setSaleAccount(String saleAccount) {
        this.saleAccount = saleAccount;
    }

    @Basic
    @Column(name = "PurchaseDiscountRate")
    public BigDecimal getPurchaseDiscountRate() {
        return purchaseDiscountRate;
    }

    public void setPurchaseDiscountRate(BigDecimal purchaseDiscountRate) {
        this.purchaseDiscountRate = purchaseDiscountRate;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "SalePrice1")
    public BigDecimal getSalePrice1() {
        return salePrice1;
    }

    public void setSalePrice1(BigDecimal salePrice1) {
        this.salePrice1 = salePrice1;
    }

    @Basic
    @Column(name = "SalePrice2")
    public BigDecimal getSalePrice2() {
        return salePrice2;
    }

    public void setSalePrice2(BigDecimal salePrice2) {
        this.salePrice2 = salePrice2;
    }

    @Basic
    @Column(name = "SalePrice3")
    public BigDecimal getSalePrice3() {
        return salePrice3;
    }

    public void setSalePrice3(BigDecimal salePrice3) {
        this.salePrice3 = salePrice3;
    }

    @Basic
    @Column(name = "FixedSalePrice")
    public BigDecimal getFixedSalePrice() {
        return fixedSalePrice;
    }

    public void setFixedSalePrice(BigDecimal fixedSalePrice) {
        this.fixedSalePrice = fixedSalePrice;
    }

    @Basic
    @Column(name = "IsUnitPriceAfterTax")
    public boolean getIsUnitPriceAfterTax() {
        return isUnitPriceAfterTax;
    }

    public void setIsUnitPriceAfterTax(boolean unitPriceAfterTax) {
        isUnitPriceAfterTax = unitPriceAfterTax;
    }

    @Basic
    @Column(name = "TaxRate")
    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    @Basic
    @Column(name = "ImportTaxRate")
    public BigDecimal getImportTaxRate() {
        return importTaxRate;
    }

    public void setImportTaxRate(BigDecimal importTaxRate) {
        this.importTaxRate = importTaxRate;
    }

    @Basic
    @Column(name = "ExportTaxRate")
    public BigDecimal getExportTaxRate() {
        return exportTaxRate;
    }

    public void setExportTaxRate(BigDecimal exportTaxRate) {
        this.exportTaxRate = exportTaxRate;
    }

    @Basic
    @Column(name = "InventoryCategorySpecialTaxID")
    public Integer getInventoryCategorySpecialTaxId() {
        return inventoryCategorySpecialTaxId;
    }

    public void setInventoryCategorySpecialTaxId(Integer inventoryCategorySpecialTaxId) {
        this.inventoryCategorySpecialTaxId = inventoryCategorySpecialTaxId;
    }

    @Basic
    @Column(name = "IsSystem")
    public boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public boolean isActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "IsSaleDiscount")
    public Boolean getIsSaleDiscount() {
        return isSaleDiscount;
    }

    public void setIsSaleDiscount(Boolean saleDiscount) {
        isSaleDiscount = saleDiscount;
    }

    @Basic
    @Column(name = "DiscountType")
    public Integer getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    @Basic
    @Column(name = "IsFollowSerialNumber")
    public Boolean getIsFollowSerialNumber() {
        return isFollowSerialNumber;
    }

    public void setIsFollowSerialNumber(Boolean followSerialNumber) {
        isFollowSerialNumber = followSerialNumber;
    }

    @Basic
    @Column(name = "CostMethod")
    public Integer getCostMethod() {
        return costMethod;
    }

    public void setCostMethod(Integer costMethod) {
        this.costMethod = costMethod;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "InventoryItemCategoryName")
    public String getInventoryItemCategoryName() {
        return inventoryItemCategoryName;
    }

    public void setInventoryItemCategoryName(String inventoryItemCategoryName) {
        this.inventoryItemCategoryName = inventoryItemCategoryName;
    }

    @Basic
    @Column(name = "Specificity")
    public String getSpecificity() {
        return specificity;
    }

    public void setSpecificity(String specificity) {
        this.specificity = specificity;
    }

    @Basic
    @Column(name = "PurchaseDescription")
    public String getPurchaseDescription() {
        return purchaseDescription;
    }

    public void setPurchaseDescription(String purchaseDescription) {
        this.purchaseDescription = purchaseDescription;
    }

    @Basic
    @Column(name = "SaleDescription")
    public String getSaleDescription() {
        return saleDescription;
    }

    public void setSaleDescription(String saleDescription) {
        this.saleDescription = saleDescription;
    }

    @Basic
    @Column(name = "TACareerGroupID")
    public Integer getTaCareerGroupId() {
        return taCareerGroupId;
    }

    public void setTaCareerGroupId(Integer taCareerGroupId) {
        this.taCareerGroupId = taCareerGroupId;
    }

    @Basic
    @Column(name = "Image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "FixedUnitPrice")
    public BigDecimal getFixedUnitPrice() {
        return fixedUnitPrice;
    }

    public void setFixedUnitPrice(BigDecimal fixedUnitPrice) {
        this.fixedUnitPrice = fixedUnitPrice;
    }

    @Basic
    @Column(name = "FrontEndFormula")
    public String getFrontEndFormula() {
        return frontEndFormula;
    }

    public void setFrontEndFormula(String frontEndFormula) {
        this.frontEndFormula = frontEndFormula;
    }

    @Basic
    @Column(name = "BackEndFormula")
    public String getBackEndFormula() {
        return backEndFormula;
    }

    public void setBackEndFormula(String backEndFormula) {
        this.backEndFormula = backEndFormula;
    }

    @Basic
    @Column(name = "BaseOnFormula")
    public Integer getBaseOnFormula() {
        return baseOnFormula;
    }

    public void setBaseOnFormula(Integer baseOnFormula) {
        this.baseOnFormula = baseOnFormula;
    }

    @Basic
    @Column(name = "DiscountAccount")
    public String getDiscountAccount() {
        return discountAccount;
    }

    public void setDiscountAccount(String discountAccount) {
        this.discountAccount = discountAccount;
    }

    @Basic
    @Column(name = "SaleOffAccount")
    public String getSaleOffAccount() {
        return saleOffAccount;
    }

    public void setSaleOffAccount(String saleOffAccount) {
        this.saleOffAccount = saleOffAccount;
    }

    @Basic
    @Column(name = "ReturnAccount")
    public String getReturnAccount() {
        return returnAccount;
    }

    public void setReturnAccount(String returnAccount) {
        this.returnAccount = returnAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryItem that = (InventoryItem) o;
        return id == that.id &&
                isUnitPriceAfterTax == that.isUnitPriceAfterTax &&
                isSystem == that.isSystem &&
                activeStatus == that.activeStatus &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(barCode, that.barCode) &&
                Objects.equals(inventoryItemCode, that.inventoryItemCode) &&
                Objects.equals(inventoryItemName, that.inventoryItemName) &&
                Objects.equals(inventoryItemType, that.inventoryItemType) &&
                Objects.equals(inventoryItemCategoryList, that.inventoryItemCategoryList) &&
                Objects.equals(inventoryItemCategoryCode, that.inventoryItemCategoryCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(guarantyPeriod, that.guarantyPeriod) &&
                Objects.equals(minimumStock, that.minimumStock) &&
                Objects.equals(inventoryItemSource, that.inventoryItemSource) &&
                Objects.equals(defaultStockId, that.defaultStockId) &&
                Objects.equals(inventoryAccount, that.inventoryAccount) &&
                Objects.equals(cogsAccount, that.cogsAccount) &&
                Objects.equals(saleAccount, that.saleAccount) &&
                Objects.equals(purchaseDiscountRate, that.purchaseDiscountRate) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(salePrice1, that.salePrice1) &&
                Objects.equals(salePrice2, that.salePrice2) &&
                Objects.equals(salePrice3, that.salePrice3) &&
                Objects.equals(fixedSalePrice, that.fixedSalePrice) &&
                Objects.equals(taxRate, that.taxRate) &&
                Objects.equals(importTaxRate, that.importTaxRate) &&
                Objects.equals(exportTaxRate, that.exportTaxRate) &&
                Objects.equals(inventoryCategorySpecialTaxId, that.inventoryCategorySpecialTaxId) &&
                Objects.equals(isSaleDiscount, that.isSaleDiscount) &&
                Objects.equals(discountType, that.discountType) &&
                Objects.equals(isFollowSerialNumber, that.isFollowSerialNumber) &&
                Objects.equals(costMethod, that.costMethod) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(inventoryItemCategoryName, that.inventoryItemCategoryName) &&
                Objects.equals(specificity, that.specificity) &&
                Objects.equals(purchaseDescription, that.purchaseDescription) &&
                Objects.equals(saleDescription, that.saleDescription) &&
                Objects.equals(taCareerGroupId, that.taCareerGroupId) &&
                Objects.equals(image, that.image) &&
                Objects.equals(fixedUnitPrice, that.fixedUnitPrice) &&
                Objects.equals(frontEndFormula, that.frontEndFormula) &&
                Objects.equals(backEndFormula, that.backEndFormula) &&
                Objects.equals(baseOnFormula, that.baseOnFormula) &&
                Objects.equals(discountAccount, that.discountAccount) &&
                Objects.equals(saleOffAccount, that.saleOffAccount) &&
                Objects.equals(returnAccount, that.returnAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, barCode, inventoryItemCode, inventoryItemName, inventoryItemType, inventoryItemCategoryList, inventoryItemCategoryCode, description, unitId, guarantyPeriod, minimumStock, inventoryItemSource, defaultStockId, inventoryAccount, cogsAccount, saleAccount, purchaseDiscountRate, unitPrice, salePrice1, salePrice2, salePrice3, fixedSalePrice, isUnitPriceAfterTax, taxRate, importTaxRate, exportTaxRate, inventoryCategorySpecialTaxId, isSystem, activeStatus, isSaleDiscount, discountType, isFollowSerialNumber, costMethod, createdDate, createdBy, modifiedDate, modifiedBy, inventoryItemCategoryName, specificity, purchaseDescription, saleDescription, taCareerGroupId, image, fixedUnitPrice, frontEndFormula, backEndFormula, baseOnFormula, discountAccount, saleOffAccount, returnAccount);
    }
}
