package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Bảng chi tiết hàng hóa, đi theo hợp đồng.
 */
@Entity
public class ContractDetailInventoryItem {

    private Integer id;
    private Integer contractId; // Số hợp đồng
    private Integer inventoryItemId; // Mã hàng hóa, dịch vụ, sản phẩm
    private String description; // Mô tả
    private Integer unitId; // Đơn vị tính
    private BigDecimal quantity; // Số lượng
    private BigDecimal quantityDeliveredSa;
    private BigDecimal unitPrice; // Đơn giá
    private BigDecimal unitPriceAfterTax; // Đơn giá đã có thuế
    private BigDecimal amountOc; // Số tiền (đơn vị tiền tệ đang sử dụng)
    private BigDecimal amount; // Số tiền quy đổi
    private BigDecimal vatRate; // Thuế GTGT
    private BigDecimal vatAmountOc; // Số tiền thuế GTGT (theo loại tiền tệ đang sử dụng)
    private BigDecimal vatAmount; // Số tiền thuế GTGT quy đổi ra VND
    private BigDecimal discountRate; // Tỷ lệ chiết khấu
    private BigDecimal discountAmountOc; // Số tiền chiết khấu (theo loại tiền tệ đang sử dụng)
    private BigDecimal discountAmount; // Số tiền chiết khấu (quy về VND)
    private BigDecimal totalAmountOc; // Tổng số tiền (theo loại tiền tệ đang sử dụng)
    private BigDecimal totalAmount; // Tổng số tiền (Quy về VND)
    private Integer mainUnitId; // Đơn vị tính chính
    private BigDecimal mainUnitPrice; // Đơn giá đơn vị tính chính
    private BigDecimal mainConvertRate;
    private BigDecimal mainQuantity;
    private String exchangeRateOperator;
    private Integer sortOrder; // Thứ tự sắp xếp
    private BigDecimal quantityDeliveredIn;
    private BigDecimal quantityDeliveredSaLastYear; // Số đã giao năm trước (Tính theo Bán hàng)
    private BigDecimal quantityDeliveredInLastYear; // Số đã giao năm trước (Tính theo Phiếu xuất)
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer saOrderId;
    private Integer saOrderRefDetailId;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "Quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "QuantityDeliveredSA")
    public BigDecimal getQuantityDeliveredSa() {
        return quantityDeliveredSa;
    }

    public void setQuantityDeliveredSa(BigDecimal quantityDeliveredSa) {
        this.quantityDeliveredSa = quantityDeliveredSa;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "UnitPriceAfterTax")
    public BigDecimal getUnitPriceAfterTax() {
        return unitPriceAfterTax;
    }

    public void setUnitPriceAfterTax(BigDecimal unitPriceAfterTax) {
        this.unitPriceAfterTax = unitPriceAfterTax;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "DiscountRate")
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @Basic
    @Column(name = "DiscountAmountOC")
    public BigDecimal getDiscountAmountOc() {
        return discountAmountOc;
    }

    public void setDiscountAmountOc(BigDecimal discountAmountOc) {
        this.discountAmountOc = discountAmountOc;
    }

    @Basic
    @Column(name = "DiscountAmount")
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "QuantityDeliveredIN")
    public BigDecimal getQuantityDeliveredIn() {
        return quantityDeliveredIn;
    }

    public void setQuantityDeliveredIn(BigDecimal quantityDeliveredIn) {
        this.quantityDeliveredIn = quantityDeliveredIn;
    }

    @Basic
    @Column(name = "QuantityDeliveredSALastYear")
    public BigDecimal getQuantityDeliveredSaLastYear() {
        return quantityDeliveredSaLastYear;
    }

    public void setQuantityDeliveredSaLastYear(BigDecimal quantityDeliveredSaLastYear) {
        this.quantityDeliveredSaLastYear = quantityDeliveredSaLastYear;
    }

    @Basic
    @Column(name = "QuantityDeliveredINLastYear")
    public BigDecimal getQuantityDeliveredInLastYear() {
        return quantityDeliveredInLastYear;
    }

    public void setQuantityDeliveredInLastYear(BigDecimal quantityDeliveredInLastYear) {
        this.quantityDeliveredInLastYear = quantityDeliveredInLastYear;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "SAOrderID")
    public Integer getSaOrderId() {
        return saOrderId;
    }

    public void setSaOrderId(Integer saOrderId) {
        this.saOrderId = saOrderId;
    }

    @Basic
    @Column(name = "SAOrderRefDetailID")
    public Integer getSaOrderRefDetailId() {
        return saOrderRefDetailId;
    }

    public void setSaOrderRefDetailId(Integer saOrderRefDetailId) {
        this.saOrderRefDetailId = saOrderRefDetailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractDetailInventoryItem that = (ContractDetailInventoryItem) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(quantityDeliveredSa, that.quantityDeliveredSa) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(unitPriceAfterTax, that.unitPriceAfterTax) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(discountRate, that.discountRate) &&
                Objects.equals(discountAmountOc, that.discountAmountOc) &&
                Objects.equals(discountAmount, that.discountAmount) &&
                Objects.equals(totalAmountOc, that.totalAmountOc) &&
                Objects.equals(totalAmount, that.totalAmount) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(quantityDeliveredIn, that.quantityDeliveredIn) &&
                Objects.equals(quantityDeliveredSaLastYear, that.quantityDeliveredSaLastYear) &&
                Objects.equals(quantityDeliveredInLastYear, that.quantityDeliveredInLastYear) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(saOrderId, that.saOrderId) &&
                Objects.equals(saOrderRefDetailId, that.saOrderRefDetailId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contractId, inventoryItemId, description, unitId, quantity, quantityDeliveredSa, unitPrice, unitPriceAfterTax, amountOc, amount, vatRate, vatAmountOc, vatAmount, discountRate, discountAmountOc, discountAmount, totalAmountOc, totalAmount, mainUnitId, mainUnitPrice, mainConvertRate, mainQuantity, exchangeRateOperator, sortOrder, quantityDeliveredIn, quantityDeliveredSaLastYear, quantityDeliveredInLastYear, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, saOrderId, saOrderRefDetailId);
    }
}
