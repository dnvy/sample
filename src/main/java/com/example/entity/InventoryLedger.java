package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Sổ chi tiết vật tư (không cần dùng InventoryBalance).
 */
@Entity
public class InventoryLedger {

    private Integer id; // PK - Sổ chi tiết vật tư
    private Integer refId; // RefID của chứng từ gốc
    private Integer refDetailId; // RefDetailID của chúng từ gốc
    private Integer refType; // Loại chứng từ
    private String refNo; // Số chứng từ Sổ tài chính
    private Timestamp refDate; // Ngày chứng từ
    private Timestamp postedDate; // Ngày hạch toán
    private String accountNumber; // TK Nợ
    private String correspondingAccountNumber; // TK Có. Tài khoản đối ứng.
    private Integer stockId; // Kho
    private Integer inventoryItemId; // Hàng hóa, Vật tư, dịch vụ
    private Integer unitId; // Đơn vị tính
    private BigDecimal unitPrice; // Đơn giá (Sổ tài chính)
    private BigDecimal inwardQuantity; // Số lượng nhập kho
    private BigDecimal outwardQuantity; // Số lượng xuất kho
    private BigDecimal inwardAmount; // Giá trị nhập kho (Sổ tài chính)
    private BigDecimal outwardAmount; // Giá trị xuât kho (Sổ tài chính)
    private BigDecimal inwardQuantityBalance; // Số lượng tồn trên chứng từ nhập kho (dùng cho phương pháp NTXT và đích danh)
    private BigDecimal inwardAmountBalance; // Giá trị tồn trên chứng từ nhập kho (dùng cho phương pháp NTXT và đích danh) (Sổ tài chính)
    private String journalMemo; // Diễn giải master
    private String description; // Diễn giải detail

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiryDate; // Ngày hết hạn

    private String lotNo; // Số lô
    private Integer branchId; // Chi nhánh
    private Integer mainUnitId; // ĐVT
    private BigDecimal mainUnitPrice; // Đơn giá
    private BigDecimal mainInwardQuantity; // Số lượng nhập kho
    private BigDecimal mainOutwardQuantity; // Số lượng xuất kho
    private BigDecimal mainConvertRate; // Tỷ lệ chuyển đổi ra đơn vị chính
    private String exchangeRateOperator; // Toán tử quy đổi *=nhân;/=chia
    private Boolean isPromotion; // 1=Là hàng khuyến mại;0=không phải là hàng khuyến mại
    private Boolean isPostToManagementBook; // Trạng thái ghi vào vào sổ quản trị
    private Integer sortOrder; // Dùng để sắp xếp các chứng từ nào insert vào trước sau chứng từ nào Insert vào sau
    private Integer refOrder; // Thứ tự của chứng từ
    private Integer confrontingRefId; // Số chứng từ nhập đối trừ (Áp dụng cho PP đích danh)
    private Integer confrontingRefDetailId; // Số chứng từ nhập đối trừ (Áp dụng cho PP đích danh)
    private Boolean isUnUpdateOutwardPrice; // Ko cập nhật giá xuất (Áp dụng cho bán đại lý, mua trả lại, giảm giá, điều chỉnh giảm GT, ...)
    private String stockCode; // Mã kho
    private String stockName; // Tên kho
    private String inventoryItemCode; // Mã vật tư, hàng hóa
    private String inventoryItemName; // Tên vật tư, hàng hóa
    private String accountName; // Tên tài khoản
    private Integer orderId; // ID Đơn hàng
    private Integer listItemId; // Mã thống kê
    private Integer organizationUnitId; // ID đơn vị
    private Integer contractId; // Hợp đồng
    private String contractCode; // Mã hợp đồng
    private String contractName; // Tên hợp đồng
    private Integer projectWorkId; // Công trình/Dự án
    private Integer jobId; // Công việc
    private Integer expenseItemId; // Khoản mục chi phí
    private Integer employeeId; // Nhân viên
    private String employeeCode; // Mã nhân viên
    private String employeeName; // Tên nhân viên
    private String contactName; // Người nhận tiền/Người chi tiền
    private Integer accountObjectId; // Đối tượng hạch toán kế toán
    private String accountObjectCode; // Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private String accountObjectAddress; // Địa chỉ
    private Integer productionOrderRefId; // RefID của lệnh sản xuất
    private Boolean isUpdateRedundant; // Có cần cập nhật dữ liệu từ các bảng dư thừa sang không, 0: không, 1: có. Sau khi cập nhật xong thì thiết lập thông tin này về =0
    private String accountObjectNameDi; // Tên đối tượng ( lấy từ danh mục)
    private String refTypeName;
    private Integer unUpdateOutwardPriceType;
    private Integer inOutWardType;
    private Integer unitPriceMethod;
    private Integer outwardRefId;
    private Integer outwardRefDetailId;
    private Integer assemblyRefId;
    private Integer productionId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inRefOrder;

    private String currencyId;
    private BigDecimal exchangeRate;
    private Boolean isInward;
    private Integer inventoryResaleTypeId;
    private String refNoFinance;
    private String refNoManagement;
    private Integer puContractId;
    private String puContractCode;
    private String puContractName;
    private BigDecimal fifoAccumInwardQuantityMainUnitAfterRow;
    private BigDecimal fifoAccumOutwardQuantityMainUnitAfterRow;
    private Integer fifoInventoryLedgerIDfirstInwardForAllocate;
    private BigDecimal fifoAccumInwardAmountMainUnitAfterRow;
    private BigDecimal fifoAccumOutwardAmountMainUnitAfterRow;
    private Timestamp fifoAccumLastUpdate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "RefDetailID")
    public Integer getRefDetailId() {
        return refDetailId;
    }

    public void setRefDetailId(Integer refDetailId) {
        this.refDetailId = refDetailId;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "RefDate")
    public Timestamp getRefDate() {
        return refDate;
    }

    public void setRefDate(Timestamp refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Timestamp getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Timestamp postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "CorrespondingAccountNumber")
    public String getCorrespondingAccountNumber() {
        return correspondingAccountNumber;
    }

    public void setCorrespondingAccountNumber(String correspondingAccountNumber) {
        this.correspondingAccountNumber = correspondingAccountNumber;
    }

    @Basic
    @Column(name = "StockID")
    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "InwardQuantity")
    public BigDecimal getInwardQuantity() {
        return inwardQuantity;
    }

    public void setInwardQuantity(BigDecimal inwardQuantity) {
        this.inwardQuantity = inwardQuantity;
    }

    @Basic
    @Column(name = "OutwardQuantity")
    public BigDecimal getOutwardQuantity() {
        return outwardQuantity;
    }

    public void setOutwardQuantity(BigDecimal outwardQuantity) {
        this.outwardQuantity = outwardQuantity;
    }

    @Basic
    @Column(name = "InwardAmount")
    public BigDecimal getInwardAmount() {
        return inwardAmount;
    }

    public void setInwardAmount(BigDecimal inwardAmount) {
        this.inwardAmount = inwardAmount;
    }

    @Basic
    @Column(name = "OutwardAmount")
    public BigDecimal getOutwardAmount() {
        return outwardAmount;
    }

    public void setOutwardAmount(BigDecimal outwardAmount) {
        this.outwardAmount = outwardAmount;
    }

    @Basic
    @Column(name = "InwardQuantityBalance")
    public BigDecimal getInwardQuantityBalance() {
        return inwardQuantityBalance;
    }

    public void setInwardQuantityBalance(BigDecimal inwardQuantityBalance) {
        this.inwardQuantityBalance = inwardQuantityBalance;
    }

    @Basic
    @Column(name = "InwardAmountBalance")
    public BigDecimal getInwardAmountBalance() {
        return inwardAmountBalance;
    }

    public void setInwardAmountBalance(BigDecimal inwardAmountBalance) {
        this.inwardAmountBalance = inwardAmountBalance;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ExpiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "LotNo")
    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainInwardQuantity")
    public BigDecimal getMainInwardQuantity() {
        return mainInwardQuantity;
    }

    public void setMainInwardQuantity(BigDecimal mainInwardQuantity) {
        this.mainInwardQuantity = mainInwardQuantity;
    }

    @Basic
    @Column(name = "MainOutwardQuantity")
    public BigDecimal getMainOutwardQuantity() {
        return mainOutwardQuantity;
    }

    public void setMainOutwardQuantity(BigDecimal mainOutwardQuantity) {
        this.mainOutwardQuantity = mainOutwardQuantity;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "IsPromotion")
    public Boolean getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Boolean promotion) {
        isPromotion = promotion;
    }

    @Basic
    @Column(name = "IsPostToManagementBook")
    public Boolean getIsPostToManagementBook() {
        return isPostToManagementBook;
    }

    public void setIsPostToManagementBook(Boolean postToManagementBook) {
        isPostToManagementBook = postToManagementBook;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "ConfrontingRefID")
    public Integer getConfrontingRefId() {
        return confrontingRefId;
    }

    public void setConfrontingRefId(Integer confrontingRefId) {
        this.confrontingRefId = confrontingRefId;
    }

    @Basic
    @Column(name = "ConfrontingRefDetailID")
    public Integer getConfrontingRefDetailId() {
        return confrontingRefDetailId;
    }

    public void setConfrontingRefDetailId(Integer confrontingRefDetailId) {
        this.confrontingRefDetailId = confrontingRefDetailId;
    }

    @Basic
    @Column(name = "IsUnUpdateOutwardPrice")
    public Boolean getIsUnUpdateOutwardPrice() {
        return isUnUpdateOutwardPrice;
    }

    public void setIsUnUpdateOutwardPrice(Boolean unUpdateOutwardPrice) {
        isUnUpdateOutwardPrice = unUpdateOutwardPrice;
    }

    @Basic
    @Column(name = "StockCode")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @Basic
    @Column(name = "StockName")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @Basic
    @Column(name = "InventoryItemCode")
    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    @Basic
    @Column(name = "InventoryItemName")
    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    @Basic
    @Column(name = "AccountName")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "ContractName")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "EmployeeCode")
    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Basic
    @Column(name = "EmployeeName")
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "ContactName")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectCode")
    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "ProductionOrderRefID")
    public Integer getProductionOrderRefId() {
        return productionOrderRefId;
    }

    public void setProductionOrderRefId(Integer productionOrderRefId) {
        this.productionOrderRefId = productionOrderRefId;
    }

    @Basic
    @Column(name = "IsUpdateRedundant")
    public Boolean getIsUpdateRedundant() {
        return isUpdateRedundant;
    }

    public void setIsUpdateRedundant(Boolean updateRedundant) {
        isUpdateRedundant = updateRedundant;
    }

    @Basic
    @Column(name = "AccountObjectNameDI")
    public String getAccountObjectNameDi() {
        return accountObjectNameDi;
    }

    public void setAccountObjectNameDi(String accountObjectNameDi) {
        this.accountObjectNameDi = accountObjectNameDi;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "UnUpdateOutwardPriceType")
    public Integer getUnUpdateOutwardPriceType() {
        return unUpdateOutwardPriceType;
    }

    public void setUnUpdateOutwardPriceType(Integer unUpdateOutwardPriceType) {
        this.unUpdateOutwardPriceType = unUpdateOutwardPriceType;
    }

    @Basic
    @Column(name = "InOutWardType")
    public Integer getInOutWardType() {
        return inOutWardType;
    }

    public void setInOutWardType(Integer inOutWardType) {
        this.inOutWardType = inOutWardType;
    }

    @Basic
    @Column(name = "UnitPriceMethod")
    public Integer getUnitPriceMethod() {
        return unitPriceMethod;
    }

    public void setUnitPriceMethod(Integer unitPriceMethod) {
        this.unitPriceMethod = unitPriceMethod;
    }

    @Basic
    @Column(name = "OutwardRefID")
    public Integer getOutwardRefId() {
        return outwardRefId;
    }

    public void setOutwardRefId(Integer outwardRefId) {
        this.outwardRefId = outwardRefId;
    }

    @Basic
    @Column(name = "OutwardRefDetailID")
    public Integer getOutwardRefDetailId() {
        return outwardRefDetailId;
    }

    public void setOutwardRefDetailId(Integer outwardRefDetailId) {
        this.outwardRefDetailId = outwardRefDetailId;
    }

    @Basic
    @Column(name = "AssemblyRefID")
    public Integer getAssemblyRefId() {
        return assemblyRefId;
    }

    public void setAssemblyRefId(Integer assemblyRefId) {
        this.assemblyRefId = assemblyRefId;
    }

    @Basic
    @Column(name = "ProductionID")
    public Integer getProductionId() {
        return productionId;
    }

    public void setProductionId(Integer productionId) {
        this.productionId = productionId;
    }

    @Basic
    @Column(name = "INRefOrder")
    public Date getInRefOrder() {
        return inRefOrder;
    }

    public void setInRefOrder(Date inRefOrder) {
        this.inRefOrder = inRefOrder;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "IsInward")
    public Boolean getIsInward() {
        return isInward;
    }

    public void setIsInward(Boolean inward) {
        isInward = inward;
    }

    @Basic
    @Column(name = "InventoryResaleTypeID")
    public Integer getInventoryResaleTypeId() {
        return inventoryResaleTypeId;
    }

    public void setInventoryResaleTypeId(Integer inventoryResaleTypeId) {
        this.inventoryResaleTypeId = inventoryResaleTypeId;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "PUContractCode")
    public String getPuContractCode() {
        return puContractCode;
    }

    public void setPuContractCode(String puContractCode) {
        this.puContractCode = puContractCode;
    }

    @Basic
    @Column(name = "PUContractName")
    public String getPuContractName() {
        return puContractName;
    }

    public void setPuContractName(String puContractName) {
        this.puContractName = puContractName;
    }

    @Basic
    @Column(name = "FIFOAccumInwardQuantityMainUnitAfterRow")
    public BigDecimal getFifoAccumInwardQuantityMainUnitAfterRow() {
        return fifoAccumInwardQuantityMainUnitAfterRow;
    }

    public void setFifoAccumInwardQuantityMainUnitAfterRow(BigDecimal fifoAccumInwardQuantityMainUnitAfterRow) {
        this.fifoAccumInwardQuantityMainUnitAfterRow = fifoAccumInwardQuantityMainUnitAfterRow;
    }

    @Basic
    @Column(name = "FIFOAccumOutwardQuantityMainUnitAfterRow")
    public BigDecimal getFifoAccumOutwardQuantityMainUnitAfterRow() {
        return fifoAccumOutwardQuantityMainUnitAfterRow;
    }

    public void setFifoAccumOutwardQuantityMainUnitAfterRow(BigDecimal fifoAccumOutwardQuantityMainUnitAfterRow) {
        this.fifoAccumOutwardQuantityMainUnitAfterRow = fifoAccumOutwardQuantityMainUnitAfterRow;
    }

    @Basic
    @Column(name = "FIFOInventoryLedgerIDfirstInwardForAllocate")
    public Integer getFifoInventoryLedgerIDfirstInwardForAllocate() {
        return fifoInventoryLedgerIDfirstInwardForAllocate;
    }

    public void setFifoInventoryLedgerIDfirstInwardForAllocate(Integer fifoInventoryLedgerIDfirstInwardForAllocate) {
        this.fifoInventoryLedgerIDfirstInwardForAllocate = fifoInventoryLedgerIDfirstInwardForAllocate;
    }

    @Basic
    @Column(name = "FIFOAccumInwardAmountMainUnitAfterRow")
    public BigDecimal getFifoAccumInwardAmountMainUnitAfterRow() {
        return fifoAccumInwardAmountMainUnitAfterRow;
    }

    public void setFifoAccumInwardAmountMainUnitAfterRow(BigDecimal fifoAccumInwardAmountMainUnitAfterRow) {
        this.fifoAccumInwardAmountMainUnitAfterRow = fifoAccumInwardAmountMainUnitAfterRow;
    }

    @Basic
    @Column(name = "FIFOAccumOutwardAmountMainUnitAfterRow")
    public BigDecimal getFifoAccumOutwardAmountMainUnitAfterRow() {
        return fifoAccumOutwardAmountMainUnitAfterRow;
    }

    public void setFifoAccumOutwardAmountMainUnitAfterRow(BigDecimal fifoAccumOutwardAmountMainUnitAfterRow) {
        this.fifoAccumOutwardAmountMainUnitAfterRow = fifoAccumOutwardAmountMainUnitAfterRow;
    }

    @Basic
    @Column(name = "FIFOAccumLastUpdate")
    public Timestamp getFifoAccumLastUpdate() {
        return fifoAccumLastUpdate;
    }

    public void setFifoAccumLastUpdate(Timestamp fifoAccumLastUpdate) {
        this.fifoAccumLastUpdate = fifoAccumLastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryLedger that = (InventoryLedger) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(refDetailId, that.refDetailId) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refNo, that.refNo) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(correspondingAccountNumber, that.correspondingAccountNumber) &&
                Objects.equals(stockId, that.stockId) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(inwardQuantity, that.inwardQuantity) &&
                Objects.equals(outwardQuantity, that.outwardQuantity) &&
                Objects.equals(inwardAmount, that.inwardAmount) &&
                Objects.equals(outwardAmount, that.outwardAmount) &&
                Objects.equals(inwardQuantityBalance, that.inwardQuantityBalance) &&
                Objects.equals(inwardAmountBalance, that.inwardAmountBalance) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(expiryDate, that.expiryDate) &&
                Objects.equals(lotNo, that.lotNo) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainInwardQuantity, that.mainInwardQuantity) &&
                Objects.equals(mainOutwardQuantity, that.mainOutwardQuantity) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(isPromotion, that.isPromotion) &&
                Objects.equals(isPostToManagementBook, that.isPostToManagementBook) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(confrontingRefId, that.confrontingRefId) &&
                Objects.equals(confrontingRefDetailId, that.confrontingRefDetailId) &&
                Objects.equals(isUnUpdateOutwardPrice, that.isUnUpdateOutwardPrice) &&
                Objects.equals(stockCode, that.stockCode) &&
                Objects.equals(stockName, that.stockName) &&
                Objects.equals(inventoryItemCode, that.inventoryItemCode) &&
                Objects.equals(inventoryItemName, that.inventoryItemName) &&
                Objects.equals(accountName, that.accountName) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(contractName, that.contractName) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(employeeCode, that.employeeCode) &&
                Objects.equals(employeeName, that.employeeName) &&
                Objects.equals(contactName, that.contactName) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectCode, that.accountObjectCode) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(productionOrderRefId, that.productionOrderRefId) &&
                Objects.equals(isUpdateRedundant, that.isUpdateRedundant) &&
                Objects.equals(accountObjectNameDi, that.accountObjectNameDi) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(unUpdateOutwardPriceType, that.unUpdateOutwardPriceType) &&
                Objects.equals(inOutWardType, that.inOutWardType) &&
                Objects.equals(unitPriceMethod, that.unitPriceMethod) &&
                Objects.equals(outwardRefId, that.outwardRefId) &&
                Objects.equals(outwardRefDetailId, that.outwardRefDetailId) &&
                Objects.equals(assemblyRefId, that.assemblyRefId) &&
                Objects.equals(productionId, that.productionId) &&
                Objects.equals(inRefOrder, that.inRefOrder) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(isInward, that.isInward) &&
                Objects.equals(inventoryResaleTypeId, that.inventoryResaleTypeId) &&
                Objects.equals(refNoFinance, that.refNoFinance) &&
                Objects.equals(refNoManagement, that.refNoManagement) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(puContractCode, that.puContractCode) &&
                Objects.equals(puContractName, that.puContractName) &&
                Objects.equals(fifoAccumInwardQuantityMainUnitAfterRow, that.fifoAccumInwardQuantityMainUnitAfterRow) &&
                Objects.equals(fifoAccumOutwardQuantityMainUnitAfterRow, that.fifoAccumOutwardQuantityMainUnitAfterRow) &&
                Objects.equals(fifoInventoryLedgerIDfirstInwardForAllocate, that.fifoInventoryLedgerIDfirstInwardForAllocate) &&
                Objects.equals(fifoAccumInwardAmountMainUnitAfterRow, that.fifoAccumInwardAmountMainUnitAfterRow) &&
                Objects.equals(fifoAccumOutwardAmountMainUnitAfterRow, that.fifoAccumOutwardAmountMainUnitAfterRow) &&
                Objects.equals(fifoAccumLastUpdate, that.fifoAccumLastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, refDetailId, refType, refNo, refDate, postedDate, accountNumber, correspondingAccountNumber, stockId, inventoryItemId, unitId, unitPrice, inwardQuantity, outwardQuantity, inwardAmount, outwardAmount, inwardQuantityBalance, inwardAmountBalance, journalMemo, description, expiryDate, lotNo, branchId, mainUnitId, mainUnitPrice, mainInwardQuantity, mainOutwardQuantity, mainConvertRate, exchangeRateOperator, isPromotion, isPostToManagementBook, sortOrder, refOrder, confrontingRefId, confrontingRefDetailId, isUnUpdateOutwardPrice, stockCode, stockName, inventoryItemCode, inventoryItemName, accountName, orderId, listItemId, organizationUnitId, contractId, contractCode, contractName, projectWorkId, jobId, expenseItemId, employeeId, employeeCode, employeeName, contactName, accountObjectId, accountObjectCode, accountObjectName, accountObjectAddress, productionOrderRefId, isUpdateRedundant, accountObjectNameDi, refTypeName, unUpdateOutwardPriceType, inOutWardType, unitPriceMethod, outwardRefId, outwardRefDetailId, assemblyRefId, productionId, inRefOrder, currencyId, exchangeRate, isInward, inventoryResaleTypeId, refNoFinance, refNoManagement, puContractId, puContractCode, puContractName, fifoAccumInwardQuantityMainUnitAfterRow, fifoAccumOutwardQuantityMainUnitAfterRow, fifoInventoryLedgerIDfirstInwardForAllocate, fifoAccumInwardAmountMainUnitAfterRow, fifoAccumOutwardAmountMainUnitAfterRow, fifoAccumLastUpdate);
    }
}
