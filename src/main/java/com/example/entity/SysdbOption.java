package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Menu [Hệ thống] \ [Tùy chọn]
 * Là option của [Tab 8. Hiển thị các nghiệp vụ].
 */
@Entity
@Table(name = "SYSDBOption")
public class SysdbOption {

    private Integer id; // not null. PK - ID của tùy chọn
    private Integer userId; // ID của người dùng
    private String optionId; // not null. Mã của tùy chọn
    private Integer branchId; // ID chi nhánh
    private String optionValue; // Giá trị của tùy chọn đó
    private Integer valueType; // Kiểu giá trị (Thiết lập theo EnumDataType trên chương trình)
    private Boolean isDefault; // Là tùy chọn mặc định mang đi. Khi thêm mới Tùy chọn theo chi nhánh, theo User thì copy từ Tùy chọn này
    private String description; // Diễn giải: option này để làm gì, có value range nào.
    private Boolean lockStatus; // Khóa
    private Boolean isGlobalOption; // not null. Là tùy chọn chung toàn hệ thống
    private Boolean isUserOption; // not null. Là tùy chọn chung toàn hệ thống
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isBranchOption; // not null. Là tùy chọn theo chi nhánh

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "UserID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "OptionID")
    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "OptionValue")
    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    @Basic
    @Column(name = "ValueType")
    public Integer getValueType() {
        return valueType;
    }

    public void setValueType(Integer valueType) {
        this.valueType = valueType;
    }

    @Basic
    @Column(name = "IsDefault")
    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "LockStatus")
    public Boolean getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Boolean lockStatus) {
        this.lockStatus = lockStatus;
    }

    @Basic
    @Column(name = "IsGlobalOption")
    public Boolean getGlobalOption() {
        return isGlobalOption;
    }

    public void setGlobalOption(Boolean globalOption) {
        isGlobalOption = globalOption;
    }

    @Basic
    @Column(name = "IsUserOption")
    public Boolean getUserOption() {
        return isUserOption;
    }

    public void setUserOption(Boolean userOption) {
        isUserOption = userOption;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "IsBranchOption")
    public Boolean getBranchOption() {
        return isBranchOption;
    }

    public void setBranchOption(Boolean branchOption) {
        isBranchOption = branchOption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysdbOption that = (SysdbOption) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(optionId, that.optionId) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(optionValue, that.optionValue) &&
                Objects.equals(valueType, that.valueType) &&
                Objects.equals(isDefault, that.isDefault) &&
                Objects.equals(description, that.description) &&
                Objects.equals(lockStatus, that.lockStatus) &&
                Objects.equals(isGlobalOption, that.isGlobalOption) &&
                Objects.equals(isUserOption, that.isUserOption) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(isBranchOption, that.isBranchOption);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, optionId, branchId, optionValue, valueType, isDefault, description, lockStatus, isGlobalOption, isUserOption, createdDate, createdBy, modifiedDate, modifiedBy, isBranchOption);
    }
}
