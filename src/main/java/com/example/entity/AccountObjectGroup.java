package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Nhóm Đối tượng Kế toán.
 * https://stackoverflow.com/questions/15372654/uniqueconstraint-and-columnunique-true-in-hibernate-annotation
 */
@Entity
public class AccountObjectGroup {

    private Integer id; // PK

    @NotNull(message = "Mã Nhóm KH, NCC, NV không được để trống")
    @NotEmpty(message = "Mã Nhóm KH, NCC, NV không được để trống")
    private String accountObjectGroupCode; // Mã nhóm khách hàng, nhà cung cấp

    @NotNull(message = "Tên Nhóm KH, NCC, NV không được để trống")
    private String accountObjectGroupName; // Tên nhóm khách hàng, nhà cung cấp

    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent;
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạng thái theo dõi
    private Boolean isSystem; // Thuộc hệ thống
    private Integer sortOrder;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountObjectGroupCode")
    public String getAccountObjectGroupCode() {
        return accountObjectGroupCode;
    }

    public void setAccountObjectGroupCode(String accountObjectGroupCode) {
        this.accountObjectGroupCode = accountObjectGroupCode;
    }

    @Basic
    @Column(name = "AccountObjectGroupName")
    public String getAccountObjectGroupName() {
        return accountObjectGroupName;
    }

    public void setAccountObjectGroupName(String accountObjectGroupName) {
        this.accountObjectGroupName = accountObjectGroupName;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountObjectGroup that = (AccountObjectGroup) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountObjectGroupCode, that.accountObjectGroupCode) &&
                Objects.equals(accountObjectGroupName, that.accountObjectGroupName) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(vyCodeId, that.vyCodeId) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(isParent, that.isParent) &&
                Objects.equals(description, that.description) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(isSystem, that.isSystem) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(sortVyCodeId, that.sortVyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountObjectGroupCode, accountObjectGroupName, parentId, vyCodeId, grade, isParent, description, activeStatus, isSystem, sortOrder, createdDate, createdBy, modifiedDate, modifiedBy, sortVyCodeId);
    }

}
