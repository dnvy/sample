package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "BAWithdrawDetailImportVAT")
public class BaWithdrawDetailImportVat {

    private Integer id;
    private Integer refId;
    private Integer voucherRefId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date voucherRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date voucherPostedDate;

    private String voucherRefNoFinance;
    private String voucherRefNoManagement;
    private Integer voucherRefType;
    private String debitAccount;
    private String creditAccount;
    private String deductionDebitAccount;
    private BigDecimal payableAmount;
    private BigDecimal amount;
    private BigDecimal remainningAmount;
    private Integer sortOrder;
    private Integer voucherRefDetailId;
    private String invNo;
    private String description;
    private BigDecimal vatRate;
    private BigDecimal vatAmount;
    private BigDecimal vatAmountOc;
    private BigDecimal turnOverAmount;
    private String invTemplateNo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate;

    private String invSeries;
    private Integer accountObjectId;
    private String accountObjectName;
    private String accountObjectAddress;
    private String accountObjectTaxCode;
    private Integer purchasePurposeId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "VoucherRefID")
    public Integer getVoucherRefId() {
        return voucherRefId;
    }

    public void setVoucherRefId(Integer voucherRefId) {
        this.voucherRefId = voucherRefId;
    }

    @Basic
    @Column(name = "VoucherRefDate")
    public Date getVoucherRefDate() {
        return voucherRefDate;
    }

    public void setVoucherRefDate(Date voucherRefDate) {
        this.voucherRefDate = voucherRefDate;
    }

    @Basic
    @Column(name = "VoucherPostedDate")
    public Date getVoucherPostedDate() {
        return voucherPostedDate;
    }

    public void setVoucherPostedDate(Date voucherPostedDate) {
        this.voucherPostedDate = voucherPostedDate;
    }

    @Basic
    @Column(name = "VoucherRefNoFinance")
    public String getVoucherRefNoFinance() {
        return voucherRefNoFinance;
    }

    public void setVoucherRefNoFinance(String voucherRefNoFinance) {
        this.voucherRefNoFinance = voucherRefNoFinance;
    }

    @Basic
    @Column(name = "VoucherRefNoManagement")
    public String getVoucherRefNoManagement() {
        return voucherRefNoManagement;
    }

    public void setVoucherRefNoManagement(String voucherRefNoManagement) {
        this.voucherRefNoManagement = voucherRefNoManagement;
    }

    @Basic
    @Column(name = "VoucherRefType")
    public Integer getVoucherRefType() {
        return voucherRefType;
    }

    public void setVoucherRefType(Integer voucherRefType) {
        this.voucherRefType = voucherRefType;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "DeductionDebitAccount")
    public String getDeductionDebitAccount() {
        return deductionDebitAccount;
    }

    public void setDeductionDebitAccount(String deductionDebitAccount) {
        this.deductionDebitAccount = deductionDebitAccount;
    }

    @Basic
    @Column(name = "PayableAmount")
    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "RemainningAmount")
    public BigDecimal getRemainningAmount() {
        return remainningAmount;
    }

    public void setRemainningAmount(BigDecimal remainningAmount) {
        this.remainningAmount = remainningAmount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "VoucherRefDetailID")
    public Integer getVoucherRefDetailId() {
        return voucherRefDetailId;
    }

    public void setVoucherRefDetailId(Integer voucherRefDetailId) {
        this.voucherRefDetailId = voucherRefDetailId;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "TurnOverAmount")
    public BigDecimal getTurnOverAmount() {
        return turnOverAmount;
    }

    public void setTurnOverAmount(BigDecimal turnOverAmount) {
        this.turnOverAmount = turnOverAmount;
    }

    @Basic
    @Column(name = "InvTemplateNo")
    public String getInvTemplateNo() {
        return invTemplateNo;
    }

    public void setInvTemplateNo(String invTemplateNo) {
        this.invTemplateNo = invTemplateNo;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "PurchasePurposeID")
    public Integer getPurchasePurposeId() {
        return purchasePurposeId;
    }

    public void setPurchasePurposeId(Integer purchasePurposeId) {
        this.purchasePurposeId = purchasePurposeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaWithdrawDetailImportVat that = (BaWithdrawDetailImportVat) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(voucherRefId, that.voucherRefId) &&
                Objects.equals(voucherRefDate, that.voucherRefDate) &&
                Objects.equals(voucherPostedDate, that.voucherPostedDate) &&
                Objects.equals(voucherRefNoFinance, that.voucherRefNoFinance) &&
                Objects.equals(voucherRefNoManagement, that.voucherRefNoManagement) &&
                Objects.equals(voucherRefType, that.voucherRefType) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(deductionDebitAccount, that.deductionDebitAccount) &&
                Objects.equals(payableAmount, that.payableAmount) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(remainningAmount, that.remainningAmount) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(voucherRefDetailId, that.voucherRefDetailId) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(turnOverAmount, that.turnOverAmount) &&
                Objects.equals(invTemplateNo, that.invTemplateNo) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, that.accountObjectTaxCode) &&
                Objects.equals(purchasePurposeId, that.purchasePurposeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, voucherRefId, voucherRefDate, voucherPostedDate, voucherRefNoFinance, voucherRefNoManagement, voucherRefType, debitAccount, creditAccount, deductionDebitAccount, payableAmount, amount, remainningAmount, sortOrder, voucherRefDetailId, invNo, description, vatRate, vatAmount, vatAmountOc, turnOverAmount, invTemplateNo, invDate, invSeries, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, purchasePurposeId);
    }
}
