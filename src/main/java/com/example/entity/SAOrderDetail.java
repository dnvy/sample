package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "SAOrderDetail")
public class SAOrderDetail {

    private Integer id; // PK
    private Integer refId; // FK, lấy từ PK của table SAOrder
    private Integer inventoryItemId; // Id của Hàng hóa, dịch vụ
    private String description; // Mô tả
    private Integer unitId; // Id đơn vị
    private BigDecimal quantity; // Số lượng
    private BigDecimal quantityDeliveredSa; // Số lượng đã giao.
    private BigDecimal quantityDeliveredIn; // Số lượng đã giao năm trước.
    private BigDecimal unitPrice; // Đơn giá
    private BigDecimal unitPriceAfterTax; // Đơn giá sau thuế
    private BigDecimal amountOc; // Giá trị (nguyên tệ)
    private BigDecimal amount; // Giá trị (quy về VND)
    private BigDecimal discountRate; // Tỷ lệ chiết khấu
    private BigDecimal discountAmountOc; // Số tiền chiết khấu (nguyên tệ)
    private BigDecimal discountAmount; // Số tiền chiết khấu
    private BigDecimal vatRate; // Thuế suất VAT
    private BigDecimal vatAmountOc; // Tiền thuế VAT (nguyên tệ)
    private BigDecimal vatAmount; // Tiền thuế VAT
    private BigDecimal mainQuantity; // Số lượng chính
    private Integer mainUnitId; // Đơn vị tính chính
    private BigDecimal mainUnitPrice; // Đơn giá chính
    private BigDecimal mainConvertRate; // Tỷ lệ chuyển đổi chính
    private String exchangeRateOperator; // Toán tử Tỷ giá hối đoái
    private Integer sortOrder; // Thứ tự sắp xếp
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer saQuoteRefDetailId; // Tham chiếu Báo giá
    private Integer contractId; // Số hợp đồng
    private Integer projectWorkId; // Mã Công trình/dự án/ vụ việc
    private Integer expenseItemId; // ID của khoản mục chi phí
    private Integer organizationUnitId; // Id đơn vị
    private Integer jobId; // Id đối tượng tập hợp chi phí
    private Integer listItemId; // Mã thống kê
    private Integer stockId; // Mã kho
    private String lotNo; // Số lô

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiryDate; // Ngày hết hạn. Hạn sử dụng

    private String specificity; // quy cách
    private Boolean isPromotion; // Là hàng khuyến mại
    private BigDecimal quantityDeliveredInLastYear; // Số đã giao năm trước (Tính theo Bán hàng)
    private BigDecimal quantityDeliveredSaLastYear; // Số đã giao năm trước (Tính theo Phiếu xuất)
    private BigDecimal mainQuantityDeliveredSa;
    private BigDecimal mainQuantityDeliveredSaLastYear; // Số lượng đã giao năm trước
    private BigDecimal mainQuantityDeliveredIn;
    private BigDecimal mainQuantityDeliveredInLastYear; // Giá trị thực hiện năm trước
    private String guarantyPeriod; // Thời gian bảo hành
    private BigDecimal lastYearDeliveredAmountOc; // Giá trị đã giao năm trước (nguyên tệ)
    private BigDecimal lastYearDeliveredAmount; // Giá trị đã giao năm trước (VND)
    private BigDecimal lastYearDeliveredBeforeTaxAmountOc;
    private BigDecimal lastYearDeliveredBeforeTaxAmount;
    private BigDecimal panelLengthQuantity; // Chiều dài
    private BigDecimal panelWidthQuantity; // Chiều rộng
    private BigDecimal panelHeightQuantity; // Chiều cao
    private BigDecimal panelRadiusQuantity; // Bán kính
    private BigDecimal panelQuantity; // Lượng

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "Quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "QuantityDeliveredSA")
    public BigDecimal getQuantityDeliveredSa() {
        return quantityDeliveredSa;
    }

    public void setQuantityDeliveredSa(BigDecimal quantityDeliveredSa) {
        this.quantityDeliveredSa = quantityDeliveredSa;
    }

    @Basic
    @Column(name = "QuantityDeliveredIN")
    public BigDecimal getQuantityDeliveredIn() {
        return quantityDeliveredIn;
    }

    public void setQuantityDeliveredIn(BigDecimal quantityDeliveredIn) {
        this.quantityDeliveredIn = quantityDeliveredIn;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "UnitPriceAfterTax")
    public BigDecimal getUnitPriceAfterTax() {
        return unitPriceAfterTax;
    }

    public void setUnitPriceAfterTax(BigDecimal unitPriceAfterTax) {
        this.unitPriceAfterTax = unitPriceAfterTax;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "DiscountRate")
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @Basic
    @Column(name = "DiscountAmountOC")
    public BigDecimal getDiscountAmountOc() {
        return discountAmountOc;
    }

    public void setDiscountAmountOc(BigDecimal discountAmountOc) {
        this.discountAmountOc = discountAmountOc;
    }

    @Basic
    @Column(name = "DiscountAmount")
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "SAQuoteRefDetailID")
    public Integer getSaQuoteRefDetailId() {
        return saQuoteRefDetailId;
    }

    public void setSaQuoteRefDetailId(Integer saQuoteRefDetailId) {
        this.saQuoteRefDetailId = saQuoteRefDetailId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "StockID")
    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    @Basic
    @Column(name = "LotNo")
    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    @Basic
    @Column(name = "ExpiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "Specificity")
    public String getSpecificity() {
        return specificity;
    }

    public void setSpecificity(String specificity) {
        this.specificity = specificity;
    }

    @Basic
    @Column(name = "IsPromotion")
    public Boolean getPromotion() {
        return isPromotion;
    }

    public void setPromotion(Boolean promotion) {
        isPromotion = promotion;
    }

    @Basic
    @Column(name = "QuantityDeliveredINLastYear")
    public BigDecimal getQuantityDeliveredInLastYear() {
        return quantityDeliveredInLastYear;
    }

    public void setQuantityDeliveredInLastYear(BigDecimal quantityDeliveredInLastYear) {
        this.quantityDeliveredInLastYear = quantityDeliveredInLastYear;
    }

    @Basic
    @Column(name = "QuantityDeliveredSALastYear")
    public BigDecimal getQuantityDeliveredSaLastYear() {
        return quantityDeliveredSaLastYear;
    }

    public void setQuantityDeliveredSaLastYear(BigDecimal quantityDeliveredSaLastYear) {
        this.quantityDeliveredSaLastYear = quantityDeliveredSaLastYear;
    }

    @Basic
    @Column(name = "MainQuantityDeliveredSA")
    public BigDecimal getMainQuantityDeliveredSa() {
        return mainQuantityDeliveredSa;
    }

    public void setMainQuantityDeliveredSa(BigDecimal mainQuantityDeliveredSa) {
        this.mainQuantityDeliveredSa = mainQuantityDeliveredSa;
    }

    @Basic
    @Column(name = "MainQuantityDeliveredSALastYear")
    public BigDecimal getMainQuantityDeliveredSaLastYear() {
        return mainQuantityDeliveredSaLastYear;
    }

    public void setMainQuantityDeliveredSaLastYear(BigDecimal mainQuantityDeliveredSaLastYear) {
        this.mainQuantityDeliveredSaLastYear = mainQuantityDeliveredSaLastYear;
    }

    @Basic
    @Column(name = "MainQuantityDeliveredIN")
    public BigDecimal getMainQuantityDeliveredIn() {
        return mainQuantityDeliveredIn;
    }

    public void setMainQuantityDeliveredIn(BigDecimal mainQuantityDeliveredIn) {
        this.mainQuantityDeliveredIn = mainQuantityDeliveredIn;
    }

    @Basic
    @Column(name = "MainQuantityDeliveredINLastYear")
    public BigDecimal getMainQuantityDeliveredInLastYear() {
        return mainQuantityDeliveredInLastYear;
    }

    public void setMainQuantityDeliveredInLastYear(BigDecimal mainQuantityDeliveredInLastYear) {
        this.mainQuantityDeliveredInLastYear = mainQuantityDeliveredInLastYear;
    }

    @Basic
    @Column(name = "GuarantyPeriod")
    public String getGuarantyPeriod() {
        return guarantyPeriod;
    }

    public void setGuarantyPeriod(String guarantyPeriod) {
        this.guarantyPeriod = guarantyPeriod;
    }

    @Basic
    @Column(name = "LastYearDeliveredAmountOC")
    public BigDecimal getLastYearDeliveredAmountOc() {
        return lastYearDeliveredAmountOc;
    }

    public void setLastYearDeliveredAmountOc(BigDecimal lastYearDeliveredAmountOc) {
        this.lastYearDeliveredAmountOc = lastYearDeliveredAmountOc;
    }

    @Basic
    @Column(name = "LastYearDeliveredAmount")
    public BigDecimal getLastYearDeliveredAmount() {
        return lastYearDeliveredAmount;
    }

    public void setLastYearDeliveredAmount(BigDecimal lastYearDeliveredAmount) {
        this.lastYearDeliveredAmount = lastYearDeliveredAmount;
    }

    @Basic
    @Column(name = "LastYearDeliveredBeforeTaxAmountOC")
    public BigDecimal getLastYearDeliveredBeforeTaxAmountOc() {
        return lastYearDeliveredBeforeTaxAmountOc;
    }

    public void setLastYearDeliveredBeforeTaxAmountOc(BigDecimal lastYearDeliveredBeforeTaxAmountOc) {
        this.lastYearDeliveredBeforeTaxAmountOc = lastYearDeliveredBeforeTaxAmountOc;
    }

    @Basic
    @Column(name = "LastYearDeliveredBeforeTaxAmount")
    public BigDecimal getLastYearDeliveredBeforeTaxAmount() {
        return lastYearDeliveredBeforeTaxAmount;
    }

    public void setLastYearDeliveredBeforeTaxAmount(BigDecimal lastYearDeliveredBeforeTaxAmount) {
        this.lastYearDeliveredBeforeTaxAmount = lastYearDeliveredBeforeTaxAmount;
    }

    @Basic
    @Column(name = "PanelLengthQuantity")
    public BigDecimal getPanelLengthQuantity() {
        return panelLengthQuantity;
    }

    public void setPanelLengthQuantity(BigDecimal panelLengthQuantity) {
        this.panelLengthQuantity = panelLengthQuantity;
    }

    @Basic
    @Column(name = "PanelWidthQuantity")
    public BigDecimal getPanelWidthQuantity() {
        return panelWidthQuantity;
    }

    public void setPanelWidthQuantity(BigDecimal panelWidthQuantity) {
        this.panelWidthQuantity = panelWidthQuantity;
    }

    @Basic
    @Column(name = "PanelHeightQuantity")
    public BigDecimal getPanelHeightQuantity() {
        return panelHeightQuantity;
    }

    public void setPanelHeightQuantity(BigDecimal panelHeightQuantity) {
        this.panelHeightQuantity = panelHeightQuantity;
    }

    @Basic
    @Column(name = "PanelRadiusQuantity")
    public BigDecimal getPanelRadiusQuantity() {
        return panelRadiusQuantity;
    }

    public void setPanelRadiusQuantity(BigDecimal panelRadiusQuantity) {
        this.panelRadiusQuantity = panelRadiusQuantity;
    }

    @Basic
    @Column(name = "PanelQuantity")
    public BigDecimal getPanelQuantity() {
        return panelQuantity;
    }

    public void setPanelQuantity(BigDecimal panelQuantity) {
        this.panelQuantity = panelQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SAOrderDetail that = (SAOrderDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(quantityDeliveredSa, that.quantityDeliveredSa) &&
                Objects.equals(quantityDeliveredIn, that.quantityDeliveredIn) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(unitPriceAfterTax, that.unitPriceAfterTax) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(discountRate, that.discountRate) &&
                Objects.equals(discountAmountOc, that.discountAmountOc) &&
                Objects.equals(discountAmount, that.discountAmount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(saQuoteRefDetailId, that.saQuoteRefDetailId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(stockId, that.stockId) &&
                Objects.equals(lotNo, that.lotNo) &&
                Objects.equals(expiryDate, that.expiryDate) &&
                Objects.equals(specificity, that.specificity) &&
                Objects.equals(isPromotion, that.isPromotion) &&
                Objects.equals(quantityDeliveredInLastYear, that.quantityDeliveredInLastYear) &&
                Objects.equals(quantityDeliveredSaLastYear, that.quantityDeliveredSaLastYear) &&
                Objects.equals(mainQuantityDeliveredSa, that.mainQuantityDeliveredSa) &&
                Objects.equals(mainQuantityDeliveredSaLastYear, that.mainQuantityDeliveredSaLastYear) &&
                Objects.equals(mainQuantityDeliveredIn, that.mainQuantityDeliveredIn) &&
                Objects.equals(mainQuantityDeliveredInLastYear, that.mainQuantityDeliveredInLastYear) &&
                Objects.equals(guarantyPeriod, that.guarantyPeriod) &&
                Objects.equals(lastYearDeliveredAmountOc, that.lastYearDeliveredAmountOc) &&
                Objects.equals(lastYearDeliveredAmount, that.lastYearDeliveredAmount) &&
                Objects.equals(lastYearDeliveredBeforeTaxAmountOc, that.lastYearDeliveredBeforeTaxAmountOc) &&
                Objects.equals(lastYearDeliveredBeforeTaxAmount, that.lastYearDeliveredBeforeTaxAmount) &&
                Objects.equals(panelLengthQuantity, that.panelLengthQuantity) &&
                Objects.equals(panelWidthQuantity, that.panelWidthQuantity) &&
                Objects.equals(panelHeightQuantity, that.panelHeightQuantity) &&
                Objects.equals(panelRadiusQuantity, that.panelRadiusQuantity) &&
                Objects.equals(panelQuantity, that.panelQuantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, inventoryItemId, description, unitId, quantity, quantityDeliveredSa, quantityDeliveredIn, unitPrice, unitPriceAfterTax, amountOc, amount, discountRate, discountAmountOc, discountAmount, vatRate, vatAmountOc, vatAmount, mainQuantity, mainUnitId, mainUnitPrice, mainConvertRate, exchangeRateOperator, sortOrder, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, saQuoteRefDetailId, contractId, projectWorkId, expenseItemId, organizationUnitId, jobId, listItemId, stockId, lotNo, expiryDate, specificity, isPromotion, quantityDeliveredInLastYear, quantityDeliveredSaLastYear, mainQuantityDeliveredSa, mainQuantityDeliveredSaLastYear, mainQuantityDeliveredIn, mainQuantityDeliveredInLastYear, guarantyPeriod, lastYearDeliveredAmountOc, lastYearDeliveredAmount, lastYearDeliveredBeforeTaxAmountOc, lastYearDeliveredBeforeTaxAmount, panelLengthQuantity, panelWidthQuantity, panelHeightQuantity, panelRadiusQuantity, panelQuantity);
    }
}
