package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Danh sách đối chiếu ngân hàng.
 */
@Entity
@Table(name = "BAReconcile")
public class BaReconcile {

    private Integer id; // NOT NULL. PK
    private Integer bankAccountId; // NOT NULL. TK ngân hàng
    private Integer currencyId; // Loại tiền
    private BigDecimal closingAmountInBank; // NOT NULL. Số dư cuối kỳ tại ngân hàng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date fromDate; // NOT NULL. Từ ngày

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date toDate; // NOT NULL. Đến ngày

    private Integer sortOrder; // NOT NULL. Thứ tự cất vào Database.
    private Boolean isTemporary; // NOT NULL. Là dữ liệu cất tạm
    private Integer branchId; // Công ty/ chi nhánh
    private Integer displayOnBook; // Sổ Kế toán tài chính, Sổ kế toán quản trị, hay cả 2 sổ?

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ClosingAmounInBank")
    public BigDecimal getClosingAmountInBank() {
        return closingAmountInBank;
    }

    public void setClosingAmountInBank(BigDecimal closingAmountInBank) {
        this.closingAmountInBank = closingAmountInBank;
    }

    @Basic
    @Column(name = "FromDate")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "ToDate")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "IsTemporary")
    public Boolean getTemporary() {
        return isTemporary;
    }

    public void setTemporary(Boolean temporary) {
        isTemporary = temporary;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaReconcile that = (BaReconcile) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(closingAmountInBank, that.closingAmountInBank) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(isTemporary, that.isTemporary) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(displayOnBook, that.displayOnBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bankAccountId, currencyId, closingAmountInBank, fromDate, toDate, sortOrder, isTemporary, branchId, displayOnBook);
    }
}
