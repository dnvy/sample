package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "SYSRefTypeCategory")
public class SysRefTypeCategory {

    private Integer refTypeCategory;
    private String refTypeCategoryName;
    private String defaultDebitAccountId;
    private String defaultCreditAccountId;
    private Boolean useRebuildRefNo;
    private Integer sortOrder;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RefTypeCategory")
    public Integer getRefTypeCategory() {
        return refTypeCategory;
    }

    public void setRefTypeCategory(Integer refTypeCategory) {
        this.refTypeCategory = refTypeCategory;
    }

    @Basic
    @Column(name = "RefTypeCategoryName")
    public String getRefTypeCategoryName() {
        return refTypeCategoryName;
    }

    public void setRefTypeCategoryName(String refTypeCategoryName) {
        this.refTypeCategoryName = refTypeCategoryName;
    }

    @Basic
    @Column(name = "DefaultDebitAccountID")
    public String getDefaultDebitAccountId() {
        return defaultDebitAccountId;
    }

    public void setDefaultDebitAccountId(String defaultDebitAccountId) {
        this.defaultDebitAccountId = defaultDebitAccountId;
    }

    @Basic
    @Column(name = "DefaultCreditAccountID")
    public String getDefaultCreditAccountId() {
        return defaultCreditAccountId;
    }

    public void setDefaultCreditAccountId(String defaultCreditAccountId) {
        this.defaultCreditAccountId = defaultCreditAccountId;
    }

    @Basic
    @Column(name = "UseRebuildRefNo")
    public Boolean getUseRebuildRefNo() {
        return useRebuildRefNo;
    }

    public void setUseRebuildRefNo(Boolean useRebuildRefNo) {
        this.useRebuildRefNo = useRebuildRefNo;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysRefTypeCategory that = (SysRefTypeCategory) o;
        return Objects.equals(refTypeCategory, that.refTypeCategory) &&
                Objects.equals(refTypeCategoryName, that.refTypeCategoryName) &&
                Objects.equals(defaultDebitAccountId, that.defaultDebitAccountId) &&
                Objects.equals(defaultCreditAccountId, that.defaultCreditAccountId) &&
                Objects.equals(useRebuildRefNo, that.useRebuildRefNo) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(refTypeCategory, refTypeCategoryName, defaultDebitAccountId, defaultCreditAccountId, useRebuildRefNo, sortOrder, description);
    }
}
