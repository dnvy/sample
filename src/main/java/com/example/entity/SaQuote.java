package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng báo giá.
 */
@Entity
@Table(name = "SAQuote")
public class SaQuote {

    private Integer id; // NOT NULL. PK
    private String refNo; // NOT NULL. Số báo giá

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày báo giá

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date effectiveDate; // Ngày hiệu lực

    private Integer refType; // NOT NULL. Loại chứng từ
    private Integer branchId; // Chi nhánh
    private Integer accountObjectId; // ID khách hàng là tổ chức hoặc cá nhân
    private String accountObjectName; // Tên khách hàng
    private String accountObjectAddress; // Địa chỉ khách hàng
    private String accountObjectTaxCode; // Mã số thuế
    private String accountObjectContactName; // Người liên hệ
    private String journalMemo; // Ghi chú
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // NOT NULL. Tỉ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Tổng tiền hàng
    private BigDecimal totalAmount; // NOT NULL. Tổng tiền hàng quy đổi
    private BigDecimal totalDiscountAmountOc; // NOT NULL. Tổng tiền chiết khấu
    private BigDecimal totalDiscountAmount; // NOT NULL. Tổng tiền chiết khấu quy đổi
    private BigDecimal totalVatAmountOc; // NOT NULL. Tổng tiền thuế GTGT
    private BigDecimal totalVatAmount; // NOT NULL. Tổng tiền thuế GTGT quy đổi
    private Integer editVersion;
    private Timestamp modifiedDate;
    private String createdBy;
    private Timestamp createdDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer employeeId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "EffectiveDate")
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaQuote saQuote = (SaQuote) o;
        return Objects.equals(id, saQuote.id) &&
                Objects.equals(refNo, saQuote.refNo) &&
                Objects.equals(refDate, saQuote.refDate) &&
                Objects.equals(effectiveDate, saQuote.effectiveDate) &&
                Objects.equals(refType, saQuote.refType) &&
                Objects.equals(branchId, saQuote.branchId) &&
                Objects.equals(accountObjectId, saQuote.accountObjectId) &&
                Objects.equals(accountObjectName, saQuote.accountObjectName) &&
                Objects.equals(accountObjectAddress, saQuote.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, saQuote.accountObjectTaxCode) &&
                Objects.equals(accountObjectContactName, saQuote.accountObjectContactName) &&
                Objects.equals(journalMemo, saQuote.journalMemo) &&
                Objects.equals(currencyId, saQuote.currencyId) &&
                Objects.equals(exchangeRate, saQuote.exchangeRate) &&
                Objects.equals(totalAmountOc, saQuote.totalAmountOc) &&
                Objects.equals(totalAmount, saQuote.totalAmount) &&
                Objects.equals(totalDiscountAmountOc, saQuote.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, saQuote.totalDiscountAmount) &&
                Objects.equals(totalVatAmountOc, saQuote.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, saQuote.totalVatAmount) &&
                Objects.equals(editVersion, saQuote.editVersion) &&
                Objects.equals(modifiedDate, saQuote.modifiedDate) &&
                Objects.equals(createdBy, saQuote.createdBy) &&
                Objects.equals(createdDate, saQuote.createdDate) &&
                Objects.equals(modifiedBy, saQuote.modifiedBy) &&
                Objects.equals(customField1, saQuote.customField1) &&
                Objects.equals(customField2, saQuote.customField2) &&
                Objects.equals(customField3, saQuote.customField3) &&
                Objects.equals(customField4, saQuote.customField4) &&
                Objects.equals(customField5, saQuote.customField5) &&
                Objects.equals(customField6, saQuote.customField6) &&
                Objects.equals(customField7, saQuote.customField7) &&
                Objects.equals(customField8, saQuote.customField8) &&
                Objects.equals(customField9, saQuote.customField9) &&
                Objects.equals(customField10, saQuote.customField10) &&
                Objects.equals(employeeId, saQuote.employeeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refNo, refDate, effectiveDate, refType, branchId, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, accountObjectContactName, journalMemo, currencyId, exchangeRate, totalAmountOc, totalAmount, totalDiscountAmountOc, totalDiscountAmount, totalVatAmountOc, totalVatAmount, editVersion, modifiedDate, createdBy, createdDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, employeeId);
    }
}
