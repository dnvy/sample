package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Danh mục tài khoản.
 */
@Entity
public class Account {

    private Integer id; // NOT NULL. PK tài khoản
    private String accountNumber; // NOT NULL. Số hiệu Tài khoản
    private String accountName; // NOT NULL. Tên Tài khoản
    private String accountNameEnglish; // Tên Tài khoản bằng Tiếng Anh
    private String description; // Diễn giải
    private Integer parentId; // Tài khoản tổng hợp
    private String vyCodeId;
    private Integer grade; // Cấp bậc
    private Boolean isParent; // NOT NULL. Là Tài khoản tổng hợp (Tài khoản có 3 ký tự)
    private Integer accountCategoryKind; // NOT NULL. Tính chất tài khoản: 0: Dư nợ; 1: Dư có; 2: Lưỡng tính
    private Boolean isPostableInForeignCurrency; // NOT NULL. Có hạch toán ngoại tệ
    private Boolean detailByAccountObject; // NOT NULL. Chi tiết theo đối tượng
    private Integer accountObjectType; // Loại đối tượng: 0 - Nhà cung cấp, 1- Khách hàng, 2- Nhân viên
    private Boolean detailByBankAccount; // NOT NULL. Chi tiết theo tài khoản ngân hàng
    private Boolean detailByJob; // NOT NULL. Chi tiết theo đối tượng tập hợp chi phí
    private Integer detailByJobKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByProjectWork; // NOT NULL. Chi tiết theo công trình vụ việc
    private Integer detailByProjectWorkKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByOrder; // NOT NULL. Chi tiết theo đơn hàng
    private Integer detailByOrderKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByContract; // NOT NULL. Chi tiết theo hợp đồng
    private Integer detailByContractKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByExpenseItem; // NOT NULL. Chi tiết theo Khoản mục CP
    private Integer detailByExpenseItemKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByDepartment; // NOT NULL. Chi tiết theo đơn vị
    private Integer detailByDepartmentKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean detailByListItem; // NOT NULL. Chi tiết theo mã thống kê
    private Integer detailByListItemKind; // 0 = Chỉ cảnh báo; 1 = Bắt buộc nhập
    private Boolean activeStatus; // NOT NULL. Trạng thái Theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.
    private Boolean detailByPuContract; // NOT NULL.
    private Integer detailByPuContractKind;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "AccountName")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "AccountNameEnglish")
    public String getAccountNameEnglish() {
        return accountNameEnglish;
    }

    public void setAccountNameEnglish(String accountNameEnglish) {
        this.accountNameEnglish = accountNameEnglish;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "AccountCategoryKind")
    public Integer getAccountCategoryKind() {
        return accountCategoryKind;
    }

    public void setAccountCategoryKind(Integer accountCategoryKind) {
        this.accountCategoryKind = accountCategoryKind;
    }

    @Basic
    @Column(name = "IsPostableInForeignCurrency")
    public Boolean getIsPostableInForeignCurrency() {
        return isPostableInForeignCurrency;
    }

    public void setIsPostableInForeignCurrency(Boolean postableInForeignCurrency) {
        isPostableInForeignCurrency = postableInForeignCurrency;
    }

    @Basic
    @Column(name = "DetailByAccountObject")
    public Boolean getDetailByAccountObject() {
        return detailByAccountObject;
    }

    public void setDetailByAccountObject(Boolean detailByAccountObject) {
        this.detailByAccountObject = detailByAccountObject;
    }

    @Basic
    @Column(name = "AccountObjectType")
    public Integer getAccountObjectType() {
        return accountObjectType;
    }

    public void setAccountObjectType(Integer accountObjectType) {
        this.accountObjectType = accountObjectType;
    }

    @Basic
    @Column(name = "DetailByBankAccount")
    public Boolean getDetailByBankAccount() {
        return detailByBankAccount;
    }

    public void setDetailByBankAccount(Boolean detailByBankAccount) {
        this.detailByBankAccount = detailByBankAccount;
    }

    @Basic
    @Column(name = "DetailByJob")
    public Boolean getDetailByJob() {
        return detailByJob;
    }

    public void setDetailByJob(Boolean detailByJob) {
        this.detailByJob = detailByJob;
    }

    @Basic
    @Column(name = "DetailByJobKind")
    public Integer getDetailByJobKind() {
        return detailByJobKind;
    }

    public void setDetailByJobKind(Integer detailByJobKind) {
        this.detailByJobKind = detailByJobKind;
    }

    @Basic
    @Column(name = "DetailByProjectWork")
    public Boolean getDetailByProjectWork() {
        return detailByProjectWork;
    }

    public void setDetailByProjectWork(Boolean detailByProjectWork) {
        this.detailByProjectWork = detailByProjectWork;
    }

    @Basic
    @Column(name = "DetailByProjectWorkKind")
    public Integer getDetailByProjectWorkKind() {
        return detailByProjectWorkKind;
    }

    public void setDetailByProjectWorkKind(Integer detailByProjectWorkKind) {
        this.detailByProjectWorkKind = detailByProjectWorkKind;
    }

    @Basic
    @Column(name = "DetailByOrder")
    public Boolean getDetailByOrder() {
        return detailByOrder;
    }

    public void setDetailByOrder(Boolean detailByOrder) {
        this.detailByOrder = detailByOrder;
    }

    @Basic
    @Column(name = "DetailByOrderKind")
    public Integer getDetailByOrderKind() {
        return detailByOrderKind;
    }

    public void setDetailByOrderKind(Integer detailByOrderKind) {
        this.detailByOrderKind = detailByOrderKind;
    }

    @Basic
    @Column(name = "DetailByContract")
    public Boolean getDetailByContract() {
        return detailByContract;
    }

    public void setDetailByContract(Boolean detailByContract) {
        this.detailByContract = detailByContract;
    }

    @Basic
    @Column(name = "DetailByContractKind")
    public Integer getDetailByContractKind() {
        return detailByContractKind;
    }

    public void setDetailByContractKind(Integer detailByContractKind) {
        this.detailByContractKind = detailByContractKind;
    }

    @Basic
    @Column(name = "DetailByExpenseItem")
    public Boolean getDetailByExpenseItem() {
        return detailByExpenseItem;
    }

    public void setDetailByExpenseItem(Boolean detailByExpenseItem) {
        this.detailByExpenseItem = detailByExpenseItem;
    }

    @Basic
    @Column(name = "DetailByExpenseItemKind")
    public Integer getDetailByExpenseItemKind() {
        return detailByExpenseItemKind;
    }

    public void setDetailByExpenseItemKind(Integer detailByExpenseItemKind) {
        this.detailByExpenseItemKind = detailByExpenseItemKind;
    }

    @Basic
    @Column(name = "DetailByDepartment")
    public Boolean getDetailByDepartment() {
        return detailByDepartment;
    }

    public void setDetailByDepartment(Boolean detailByDepartment) {
        this.detailByDepartment = detailByDepartment;
    }

    @Basic
    @Column(name = "DetailByDepartmentKind")
    public Integer getDetailByDepartmentKind() {
        return detailByDepartmentKind;
    }

    public void setDetailByDepartmentKind(Integer detailByDepartmentKind) {
        this.detailByDepartmentKind = detailByDepartmentKind;
    }

    @Basic
    @Column(name = "DetailByListItem")
    public Boolean getDetailByListItem() {
        return detailByListItem;
    }

    public void setDetailByListItem(Boolean detailByListItem) {
        this.detailByListItem = detailByListItem;
    }

    @Basic
    @Column(name = "DetailByListItemKind")
    public Integer getDetailByListItemKind() {
        return detailByListItemKind;
    }

    public void setDetailByListItemKind(Integer detailByListItemKind) {
        this.detailByListItemKind = detailByListItemKind;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Basic
    @Column(name = "DetailByPUContract")
    public Boolean getDetailByPuContract() {
        return detailByPuContract;
    }

    public void setDetailByPuContract(Boolean detailByPuContract) {
        this.detailByPuContract = detailByPuContract;
    }

    @Basic
    @Column(name = "DetailByPUContractKind")
    public Integer getDetailByPuContractKind() {
        return detailByPuContractKind;
    }

    public void setDetailByPuContractKind(Integer detailByPuContractKind) {
        this.detailByPuContractKind = detailByPuContractKind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(accountNumber, account.accountNumber) &&
                Objects.equals(accountName, account.accountName) &&
                Objects.equals(accountNameEnglish, account.accountNameEnglish) &&
                Objects.equals(description, account.description) &&
                Objects.equals(parentId, account.parentId) &&
                Objects.equals(vyCodeId, account.vyCodeId) &&
                Objects.equals(grade, account.grade) &&
                Objects.equals(isParent, account.isParent) &&
                Objects.equals(accountCategoryKind, account.accountCategoryKind) &&
                Objects.equals(isPostableInForeignCurrency, account.isPostableInForeignCurrency) &&
                Objects.equals(detailByAccountObject, account.detailByAccountObject) &&
                Objects.equals(accountObjectType, account.accountObjectType) &&
                Objects.equals(detailByBankAccount, account.detailByBankAccount) &&
                Objects.equals(detailByJob, account.detailByJob) &&
                Objects.equals(detailByJobKind, account.detailByJobKind) &&
                Objects.equals(detailByProjectWork, account.detailByProjectWork) &&
                Objects.equals(detailByProjectWorkKind, account.detailByProjectWorkKind) &&
                Objects.equals(detailByOrder, account.detailByOrder) &&
                Objects.equals(detailByOrderKind, account.detailByOrderKind) &&
                Objects.equals(detailByContract, account.detailByContract) &&
                Objects.equals(detailByContractKind, account.detailByContractKind) &&
                Objects.equals(detailByExpenseItem, account.detailByExpenseItem) &&
                Objects.equals(detailByExpenseItemKind, account.detailByExpenseItemKind) &&
                Objects.equals(detailByDepartment, account.detailByDepartment) &&
                Objects.equals(detailByDepartmentKind, account.detailByDepartmentKind) &&
                Objects.equals(detailByListItem, account.detailByListItem) &&
                Objects.equals(detailByListItemKind, account.detailByListItemKind) &&
                Objects.equals(activeStatus, account.activeStatus) &&
                Objects.equals(createdDate, account.createdDate) &&
                Objects.equals(createdBy, account.createdBy) &&
                Objects.equals(modifiedDate, account.modifiedDate) &&
                Objects.equals(modifiedBy, account.modifiedBy) &&
                Objects.equals(sortVyCodeId, account.sortVyCodeId) &&
                Objects.equals(detailByPuContract, account.detailByPuContract) &&
                Objects.equals(detailByPuContractKind, account.detailByPuContractKind);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNumber, accountName, accountNameEnglish, description, parentId, vyCodeId, grade, isParent, accountCategoryKind, isPostableInForeignCurrency, detailByAccountObject, accountObjectType, detailByBankAccount, detailByJob, detailByJobKind, detailByProjectWork, detailByProjectWorkKind, detailByOrder, detailByOrderKind, detailByContract, detailByContractKind, detailByExpenseItem, detailByExpenseItemKind, detailByDepartment, detailByDepartmentKind, detailByListItem, detailByListItemKind, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, sortVyCodeId, detailByPuContract, detailByPuContractKind);
    }
}
