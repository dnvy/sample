package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Bảng Detail của phiếu nhập kho và xuất kho.
 */
@Entity
@Table(name = "INInwardDetail")
public class InInwardDetail {

    private Integer id; // PK
    private Integer refId; // FK
    private Integer inventoryItemId; // Mã hàng/Mã vật tư
    private String description; // Diễn giải
    private Integer stockId; // Mã kho
    private String debitAccount; // Tài khoản Nợ
    private String creditAccount; // Tài khoản Có
    private Integer unitId; // Đơn vị tính(bộ, chiếc, mét...)
    private BigDecimal quantity; // Số lượng
    private BigDecimal unitPriceFinance; // Đơn giá (Sổ tài chính)
    private BigDecimal amountFinance; // Số tiền (Sổ tài chính)
    private BigDecimal unitPriceManagement; // Đơn giá (Sổ quản trị)
    private BigDecimal amountManagement; // Số tiền (Sổ quản trị)
    private String lotNo; // Số lô

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiryDate; // Ngày hết hạn

    private Integer productionOrderRefId; // RefID của lệnh sản xuất
    private Integer outwardRefId; // RefID của phiếu xuất kho (dùng cho Nhập kho từ hàng bán trả lại Lấy từ giá xuất bán)
    private Integer outwardRefDetailId; // RefDetailID của phiếu xuất kho (dùng cho Nhập kho từ hàng bán trả lại Lấy từ giá xuất bán)
    private Integer jobId; // Đối tượng tập hợp chi phí
    private Integer projectWorkId; // Công trình/Dự án
    private Integer orderId; // Đơn hàng
    private Integer contractId; // Hợp đồng
    private Integer expenseItemId; // Khoản mục CP
    private Integer organizationUnitId; // Đơn vị
    private Boolean unResonableCost; // Chi phí không hợp lý
    private Integer listItemId; // Mã thống kê
    private Integer mainUnitId; // Đơn vị tính chính
    private BigDecimal mainUnitPriceFinance; // Đơn giá theo đơn vị chính (Sổ tài chính)
    private BigDecimal mainUnitPriceManagement; // Đơn giá theo đơn vị chính (Sổ quản trị)
    private BigDecimal mainConvertRate; // Tỷ lệ chuyển đổi ra đơn vị chính
    private BigDecimal mainQuantity; // Số lượng theo đơn vị chính
    private String exchangeRateOperator; // Toán tử quy đổi *=nhân;/=chia
    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Boolean isPromotion; // Là hàng khuyến mại
    private Integer productionId;
    private Integer confrontingRefId;
    private Integer confrontingRefDetailId;
    private Integer inventoryResaleTypeId;
    private Integer puContractId;
    private Integer inAssemblyRefId;
    private Integer inAssemblyRefDetailId;
    private BigDecimal amountFinanceOc;
    private BigDecimal amountManagementOc;
    private Integer accountObjectId;
    private String accountObjectName;
    private Integer saOrderRefDetailId;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "StockID")
    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "Quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "UnitPriceFinance")
    public BigDecimal getUnitPriceFinance() {
        return unitPriceFinance;
    }

    public void setUnitPriceFinance(BigDecimal unitPriceFinance) {
        this.unitPriceFinance = unitPriceFinance;
    }

    @Basic
    @Column(name = "AmountFinance")
    public BigDecimal getAmountFinance() {
        return amountFinance;
    }

    public void setAmountFinance(BigDecimal amountFinance) {
        this.amountFinance = amountFinance;
    }

    @Basic
    @Column(name = "UnitPriceManagement")
    public BigDecimal getUnitPriceManagement() {
        return unitPriceManagement;
    }

    public void setUnitPriceManagement(BigDecimal unitPriceManagement) {
        this.unitPriceManagement = unitPriceManagement;
    }

    @Basic
    @Column(name = "AmountManagement")
    public BigDecimal getAmountManagement() {
        return amountManagement;
    }

    public void setAmountManagement(BigDecimal amountManagement) {
        this.amountManagement = amountManagement;
    }

    @Basic
    @Column(name = "LotNo")
    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    @Basic
    @Column(name = "ExpiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "ProductionOrderRefID")
    public Integer getProductionOrderRefId() {
        return productionOrderRefId;
    }

    public void setProductionOrderRefId(Integer productionOrderRefId) {
        this.productionOrderRefId = productionOrderRefId;
    }

    @Basic
    @Column(name = "OutwardRefID")
    public Integer getOutwardRefId() {
        return outwardRefId;
    }

    public void setOutwardRefId(Integer outwardRefId) {
        this.outwardRefId = outwardRefId;
    }

    @Basic
    @Column(name = "OutwardRefDetailID")
    public Integer getOutwardRefDetailId() {
        return outwardRefDetailId;
    }

    public void setOutwardRefDetailId(Integer outwardRefDetailId) {
        this.outwardRefDetailId = outwardRefDetailId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "UnResonableCost")
    public Boolean getUnResonableCost() {
        return unResonableCost;
    }

    public void setUnResonableCost(Boolean unResonableCost) {
        this.unResonableCost = unResonableCost;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPriceFinance")
    public BigDecimal getMainUnitPriceFinance() {
        return mainUnitPriceFinance;
    }

    public void setMainUnitPriceFinance(BigDecimal mainUnitPriceFinance) {
        this.mainUnitPriceFinance = mainUnitPriceFinance;
    }

    @Basic
    @Column(name = "MainUnitPriceManagement")
    public BigDecimal getMainUnitPriceManagement() {
        return mainUnitPriceManagement;
    }

    public void setMainUnitPriceManagement(BigDecimal mainUnitPriceManagement) {
        this.mainUnitPriceManagement = mainUnitPriceManagement;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "IsPromotion")
    public Boolean getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Boolean promotion) {
        isPromotion = promotion;
    }

    @Basic
    @Column(name = "ProductionID")
    public Integer getProductionId() {
        return productionId;
    }

    public void setProductionId(Integer productionId) {
        this.productionId = productionId;
    }

    @Basic
    @Column(name = "ConfrontingRefID")
    public Integer getConfrontingRefId() {
        return confrontingRefId;
    }

    public void setConfrontingRefId(Integer confrontingRefId) {
        this.confrontingRefId = confrontingRefId;
    }

    @Basic
    @Column(name = "ConfrontingRefDetailID")
    public Integer getConfrontingRefDetailId() {
        return confrontingRefDetailId;
    }

    public void setConfrontingRefDetailId(Integer confrontingRefDetailId) {
        this.confrontingRefDetailId = confrontingRefDetailId;
    }

    @Basic
    @Column(name = "InventoryResaleTypeID")
    public Integer getInventoryResaleTypeId() {
        return inventoryResaleTypeId;
    }

    public void setInventoryResaleTypeId(Integer inventoryResaleTypeId) {
        this.inventoryResaleTypeId = inventoryResaleTypeId;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "INAssemblyRefID")
    public Integer getInAssemblyRefId() {
        return inAssemblyRefId;
    }

    public void setInAssemblyRefId(Integer inAssemblyRefId) {
        this.inAssemblyRefId = inAssemblyRefId;
    }

    @Basic
    @Column(name = "INAssemblyRefDetailID")
    public Integer getInAssemblyRefDetailId() {
        return inAssemblyRefDetailId;
    }

    public void setInAssemblyRefDetailId(Integer inAssemblyRefDetailId) {
        this.inAssemblyRefDetailId = inAssemblyRefDetailId;
    }

    @Basic
    @Column(name = "AmountFinanceOC")
    public BigDecimal getAmountFinanceOc() {
        return amountFinanceOc;
    }

    public void setAmountFinanceOc(BigDecimal amountFinanceOc) {
        this.amountFinanceOc = amountFinanceOc;
    }

    @Basic
    @Column(name = "AmountManagementOC")
    public BigDecimal getAmountManagementOc() {
        return amountManagementOc;
    }

    public void setAmountManagementOc(BigDecimal amountManagementOc) {
        this.amountManagementOc = amountManagementOc;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "SAOrderRefDetailID")
    public Integer getSaOrderRefDetailId() {
        return saOrderRefDetailId;
    }

    public void setSaOrderRefDetailId(Integer saOrderRefDetailId) {
        this.saOrderRefDetailId = saOrderRefDetailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InInwardDetail that = (InInwardDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(stockId, that.stockId) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(unitPriceFinance, that.unitPriceFinance) &&
                Objects.equals(amountFinance, that.amountFinance) &&
                Objects.equals(unitPriceManagement, that.unitPriceManagement) &&
                Objects.equals(amountManagement, that.amountManagement) &&
                Objects.equals(lotNo, that.lotNo) &&
                Objects.equals(expiryDate, that.expiryDate) &&
                Objects.equals(productionOrderRefId, that.productionOrderRefId) &&
                Objects.equals(outwardRefId, that.outwardRefId) &&
                Objects.equals(outwardRefDetailId, that.outwardRefDetailId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(unResonableCost, that.unResonableCost) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPriceFinance, that.mainUnitPriceFinance) &&
                Objects.equals(mainUnitPriceManagement, that.mainUnitPriceManagement) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(isPromotion, that.isPromotion) &&
                Objects.equals(productionId, that.productionId) &&
                Objects.equals(confrontingRefId, that.confrontingRefId) &&
                Objects.equals(confrontingRefDetailId, that.confrontingRefDetailId) &&
                Objects.equals(inventoryResaleTypeId, that.inventoryResaleTypeId) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(inAssemblyRefId, that.inAssemblyRefId) &&
                Objects.equals(inAssemblyRefDetailId, that.inAssemblyRefDetailId) &&
                Objects.equals(amountFinanceOc, that.amountFinanceOc) &&
                Objects.equals(amountManagementOc, that.amountManagementOc) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(saOrderRefDetailId, that.saOrderRefDetailId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, inventoryItemId, description, stockId, debitAccount, creditAccount, unitId, quantity, unitPriceFinance, amountFinance, unitPriceManagement, amountManagement, lotNo, expiryDate, productionOrderRefId, outwardRefId, outwardRefDetailId, jobId, projectWorkId, orderId, contractId, expenseItemId, organizationUnitId, unResonableCost, listItemId, mainUnitId, mainUnitPriceFinance, mainUnitPriceManagement, mainConvertRate, mainQuantity, exchangeRateOperator, sortOrder, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, isPromotion, productionId, confrontingRefId, confrontingRefDetailId, inventoryResaleTypeId, puContractId, inAssemblyRefId, inAssemblyRefDetailId, amountFinanceOc, amountManagementOc, accountObjectId, accountObjectName, saOrderRefDetailId);
    }
}
