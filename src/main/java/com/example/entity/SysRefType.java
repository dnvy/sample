package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng hệ thống, dành cho hạch toán.
 */
@Entity
@Table(name = "SYSRefType")
public class SysRefType {

    private Integer refType; // PK - Mã loại chứng từ
    private String refTypeName; // Tên loại chứng từ
    private Integer refTypeCategory; // Nhóm chứng từ. Các chu cùng nhóm thì dùng chung AutoID và cùng hiển thị trên 1 danh sách chứng từ.
    private String masterTableName; // Tên bảng Master của chứng từ ứng với RefType (phục vụ phần PostVoucher)
    private String detailTableName; // Tên bảng Detail của chứng từ ứng với RefType (phục vụ phần PostVoucher)
    private Boolean postable; // Loại chứng từ này có chức năng ghi sổ (post)
    private Boolean searchable; // Loại chứng từ này cho xuất hiện trong chức năng "Tìm kiếm chứng từ"
    private Integer sortOrder; // Thứ tự sắp xếp
    private String subSystem; // Phân hệ
    private Integer postType; // Xác định ứng với Reftype nay thi có post vào bảng GeneralLedger không: 0 : có Post vào bảng GL, 1 : không post vào bảng GL
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isShowOnAccountDefault; // Trường này dùng để chỉ các loại RefType Nào được show lên trên danh mục tài khoản ngầm định
    private String description; // Diễn giải
    private Boolean isReference;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "RefTypeCategory")
    public Integer getRefTypeCategory() {
        return refTypeCategory;
    }

    public void setRefTypeCategory(Integer refTypeCategory) {
        this.refTypeCategory = refTypeCategory;
    }

    @Basic
    @Column(name = "MasterTableName")
    public String getMasterTableName() {
        return masterTableName;
    }

    public void setMasterTableName(String masterTableName) {
        this.masterTableName = masterTableName;
    }

    @Basic
    @Column(name = "DetailTableName")
    public String getDetailTableName() {
        return detailTableName;
    }

    public void setDetailTableName(String detailTableName) {
        this.detailTableName = detailTableName;
    }

    @Basic
    @Column(name = "Postable")
    public Boolean getPostable() {
        return postable;
    }

    public void setPostable(Boolean postable) {
        this.postable = postable;
    }

    @Basic
    @Column(name = "Searchable")
    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "SubSystem")
    public String getSubSystem() {
        return subSystem;
    }

    public void setSubSystem(String subSystem) {
        this.subSystem = subSystem;
    }

    @Basic
    @Column(name = "PostType")
    public Integer getPostType() {
        return postType;
    }

    public void setPostType(Integer postType) {
        this.postType = postType;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "IsShowOnAccountDefault")
    public Boolean getIsShowOnAccountDefault() {
        return isShowOnAccountDefault;
    }

    public void setIsShowOnAccountDefault(Boolean showOnAccountDefault) {
        isShowOnAccountDefault = showOnAccountDefault;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "IsReference")
    public Boolean getIsReference() {
        return isReference;
    }

    public void setIsReference(Boolean reference) {
        isReference = reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysRefType that = (SysRefType) o;
        return Objects.equals(refType, that.refType) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(refTypeCategory, that.refTypeCategory) &&
                Objects.equals(masterTableName, that.masterTableName) &&
                Objects.equals(detailTableName, that.detailTableName) &&
                Objects.equals(postable, that.postable) &&
                Objects.equals(searchable, that.searchable) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(subSystem, that.subSystem) &&
                Objects.equals(postType, that.postType) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(isShowOnAccountDefault, that.isShowOnAccountDefault) &&
                Objects.equals(description, that.description) &&
                Objects.equals(isReference, that.isReference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(refType, refTypeName, refTypeCategory, masterTableName, detailTableName, postable, searchable, sortOrder, subSystem, postType, createdDate, createdBy, modifiedDate, modifiedBy, isShowOnAccountDefault, description, isReference);
    }
}
