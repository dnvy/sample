package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Chứng từ nghiệp vụ khác, Hạch toán chi phí lương, Xử lý chênh lệch tỷ giá.
 */
@Entity
@Table(name = "GLVoucher")
public class GlVoucher {

    private Integer id; // PK. NOT NULL.
    private Integer displayOnBook; // NOT NULL. Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer refType; // NOT NULL. Loại chứng từ (lấy từ bảng RefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // NOT NULL. Ngày hạch toán

    private String refNoFinance; // Số chứng từ Sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái ghi sổ Sổ tài chính
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi sổ Sổ quản trị
    private String journalMemo; // Diễn giải/Lý do
    private BigDecimal totalAmountOc; // NOT NULL. Tổng số tiền
    private BigDecimal totalAmount; // NOT NULL. Tổng số tiền quy đổi
    private Integer deptStatus; // Tình trạng đòi nợ: (0=nợ bình thường;1=Nợ khó đòi;2=Nợ không thể đòi)
    private Integer editVersion;
    private Integer branchId; // Mã chi nhánh
    private String currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỉ giá hối đoái
    private Integer accountObjectId; // ID đối tượng
    private String accountObjectName; // Tên đối tượng
    private BigDecimal outputAmount; // Số thuế GTGT đầu ra dùng để hiển thị trên chứng từ khấu trừ thuế
    private BigDecimal deductionAmount; // Số thuế GTGT đầu vào được khấu trừ
    private String creditAccount; // TK Có/Tài khoản xử lý lãi khi đánh giá ngoại tệ cuối năm
    private String debitAccount; // TK Nợ/Tài khoản xử lý lỗ khi đánh giá ngoại tệ cuối năm
    private Integer voucherMonth; // Tháng tính tỷ giá xuất quỹ
    private Integer voucherYear; // Năm tính tỷ giá xuất quỹ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer refOrder;
    private Integer paralellRefId;
    private Boolean receiptType;
    private Integer employeeId;
    private Timestamp crossEntryDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate; // Hạn thanh toán

    private BigDecimal deductionAmountLastPeriod; // Thuế GTGT đầu vào được khấu trừ từ kỳ trước chuyển sang
    private BigDecimal deductionAmountThisPeriod; // Thuế GTGT đầu vào được khấu trừ của kỳ này
    private Boolean isOnceSettlementAdvance; // NOT NULL.
    private BigDecimal advancedAmount; // NOT NULL.
    private BigDecimal advancedAmountOc; // NOT NULL.
    private Integer periodTypeVatDeduction;
    private Boolean isExecuted; // NOT NULL.
    private BigDecimal diffAmount; // NOT NULL.
    private BigDecimal diffAmountOc; // NOT NULL.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "DeptStatus")
    public Integer getDeptStatus() {
        return deptStatus;
    }

    public void setDeptStatus(Integer deptStatus) {
        this.deptStatus = deptStatus;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "OutputAmount")
    public BigDecimal getOutputAmount() {
        return outputAmount;
    }

    public void setOutputAmount(BigDecimal outputAmount) {
        this.outputAmount = outputAmount;
    }

    @Basic
    @Column(name = "DeductionAmount")
    public BigDecimal getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(BigDecimal deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "VoucherMonth")
    public Integer getVoucherMonth() {
        return voucherMonth;
    }

    public void setVoucherMonth(Integer voucherMonth) {
        this.voucherMonth = voucherMonth;
    }

    @Basic
    @Column(name = "VoucherYear")
    public Integer getVoucherYear() {
        return voucherYear;
    }

    public void setVoucherYear(Integer voucherYear) {
        this.voucherYear = voucherYear;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "ParalellRefID")
    public Integer getParalellRefId() {
        return paralellRefId;
    }

    public void setParalellRefId(Integer paralellRefId) {
        this.paralellRefId = paralellRefId;
    }

    @Basic
    @Column(name = "ReceiptType")
    public Boolean getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(Boolean receiptType) {
        this.receiptType = receiptType;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "CrossEntryDate")
    public Timestamp getCrossEntryDate() {
        return crossEntryDate;
    }

    public void setCrossEntryDate(Timestamp crossEntryDate) {
        this.crossEntryDate = crossEntryDate;
    }

    @Basic
    @Column(name = "DueDate")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "DeductionAmountLastPeriod")
    public BigDecimal getDeductionAmountLastPeriod() {
        return deductionAmountLastPeriod;
    }

    public void setDeductionAmountLastPeriod(BigDecimal deductionAmountLastPeriod) {
        this.deductionAmountLastPeriod = deductionAmountLastPeriod;
    }

    @Basic
    @Column(name = "DeductionAmountThisPeriod")
    public BigDecimal getDeductionAmountThisPeriod() {
        return deductionAmountThisPeriod;
    }

    public void setDeductionAmountThisPeriod(BigDecimal deductionAmountThisPeriod) {
        this.deductionAmountThisPeriod = deductionAmountThisPeriod;
    }

    @Basic
    @Column(name = "IsOnceSettlementAdvance")
    public Boolean getIsOnceSettlementAdvance() {
        return isOnceSettlementAdvance;
    }

    public void setIsOnceSettlementAdvance(Boolean onceSettlementAdvance) {
        isOnceSettlementAdvance = onceSettlementAdvance;
    }

    @Basic
    @Column(name = "AdvancedAmount")
    public BigDecimal getAdvancedAmount() {
        return advancedAmount;
    }

    public void setAdvancedAmount(BigDecimal advancedAmount) {
        this.advancedAmount = advancedAmount;
    }

    @Basic
    @Column(name = "AdvancedAmountOC")
    public BigDecimal getAdvancedAmountOc() {
        return advancedAmountOc;
    }

    public void setAdvancedAmountOc(BigDecimal advancedAmountOc) {
        this.advancedAmountOc = advancedAmountOc;
    }

    @Basic
    @Column(name = "PeriodTypeVATDeduction")
    public Integer getPeriodTypeVatDeduction() {
        return periodTypeVatDeduction;
    }

    public void setPeriodTypeVatDeduction(Integer periodTypeVatDeduction) {
        this.periodTypeVatDeduction = periodTypeVatDeduction;
    }

    @Basic
    @Column(name = "IsExecuted")
    public Boolean getIsExecuted() {
        return isExecuted;
    }

    public void setIsExecuted(Boolean executed) {
        isExecuted = executed;
    }

    @Basic
    @Column(name = "DiffAmount")
    public BigDecimal getDiffAmount() {
        return diffAmount;
    }

    public void setDiffAmount(BigDecimal diffAmount) {
        this.diffAmount = diffAmount;
    }

    @Basic
    @Column(name = "DiffAmountOC")
    public BigDecimal getDiffAmountOc() {
        return diffAmountOc;
    }

    public void setDiffAmountOc(BigDecimal diffAmountOc) {
        this.diffAmountOc = diffAmountOc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GlVoucher glVoucher = (GlVoucher) o;
        return Objects.equals(id, glVoucher.id) &&
                Objects.equals(displayOnBook, glVoucher.displayOnBook) &&
                Objects.equals(refType, glVoucher.refType) &&
                Objects.equals(refDate, glVoucher.refDate) &&
                Objects.equals(postedDate, glVoucher.postedDate) &&
                Objects.equals(refNoFinance, glVoucher.refNoFinance) &&
                Objects.equals(refNoManagement, glVoucher.refNoManagement) &&
                Objects.equals(isPostedFinance, glVoucher.isPostedFinance) &&
                Objects.equals(isPostedManagement, glVoucher.isPostedManagement) &&
                Objects.equals(journalMemo, glVoucher.journalMemo) &&
                Objects.equals(totalAmountOc, glVoucher.totalAmountOc) &&
                Objects.equals(totalAmount, glVoucher.totalAmount) &&
                Objects.equals(deptStatus, glVoucher.deptStatus) &&
                Objects.equals(editVersion, glVoucher.editVersion) &&
                Objects.equals(branchId, glVoucher.branchId) &&
                Objects.equals(currencyId, glVoucher.currencyId) &&
                Objects.equals(exchangeRate, glVoucher.exchangeRate) &&
                Objects.equals(accountObjectId, glVoucher.accountObjectId) &&
                Objects.equals(accountObjectName, glVoucher.accountObjectName) &&
                Objects.equals(outputAmount, glVoucher.outputAmount) &&
                Objects.equals(deductionAmount, glVoucher.deductionAmount) &&
                Objects.equals(creditAccount, glVoucher.creditAccount) &&
                Objects.equals(debitAccount, glVoucher.debitAccount) &&
                Objects.equals(voucherMonth, glVoucher.voucherMonth) &&
                Objects.equals(voucherYear, glVoucher.voucherYear) &&
                Objects.equals(createdDate, glVoucher.createdDate) &&
                Objects.equals(createdBy, glVoucher.createdBy) &&
                Objects.equals(modifiedDate, glVoucher.modifiedDate) &&
                Objects.equals(modifiedBy, glVoucher.modifiedBy) &&
                Objects.equals(customField1, glVoucher.customField1) &&
                Objects.equals(customField2, glVoucher.customField2) &&
                Objects.equals(customField3, glVoucher.customField3) &&
                Objects.equals(customField4, glVoucher.customField4) &&
                Objects.equals(customField5, glVoucher.customField5) &&
                Objects.equals(customField6, glVoucher.customField6) &&
                Objects.equals(customField7, glVoucher.customField7) &&
                Objects.equals(customField8, glVoucher.customField8) &&
                Objects.equals(customField9, glVoucher.customField9) &&
                Objects.equals(customField10, glVoucher.customField10) &&
                Objects.equals(refOrder, glVoucher.refOrder) &&
                Objects.equals(paralellRefId, glVoucher.paralellRefId) &&
                Objects.equals(receiptType, glVoucher.receiptType) &&
                Objects.equals(employeeId, glVoucher.employeeId) &&
                Objects.equals(crossEntryDate, glVoucher.crossEntryDate) &&
                Objects.equals(dueDate, glVoucher.dueDate) &&
                Objects.equals(deductionAmountLastPeriod, glVoucher.deductionAmountLastPeriod) &&
                Objects.equals(deductionAmountThisPeriod, glVoucher.deductionAmountThisPeriod) &&
                Objects.equals(isOnceSettlementAdvance, glVoucher.isOnceSettlementAdvance) &&
                Objects.equals(advancedAmount, glVoucher.advancedAmount) &&
                Objects.equals(advancedAmountOc, glVoucher.advancedAmountOc) &&
                Objects.equals(periodTypeVatDeduction, glVoucher.periodTypeVatDeduction) &&
                Objects.equals(isExecuted, glVoucher.isExecuted) &&
                Objects.equals(diffAmount, glVoucher.diffAmount) &&
                Objects.equals(diffAmountOc, glVoucher.diffAmountOc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayOnBook, refType, refDate, postedDate, refNoFinance, refNoManagement, isPostedFinance, isPostedManagement, journalMemo, totalAmountOc, totalAmount, deptStatus, editVersion, branchId, currencyId, exchangeRate, accountObjectId, accountObjectName, outputAmount, deductionAmount, creditAccount, debitAccount, voucherMonth, voucherYear, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, refOrder, paralellRefId, receiptType, employeeId, crossEntryDate, dueDate, deductionAmountLastPeriod, deductionAmountThisPeriod, isOnceSettlementAdvance, advancedAmount, advancedAmountOc, periodTypeVatDeduction, isExecuted, diffAmount, diffAmountOc);
    }
}
