package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Đối tượng Kế toán. Khách hàng, nhà cung cấp, nhân viên.
 *
 */
@Entity
public class AccountObject {

    private Integer id; // PK Đối tượng
    private String accountObjectCode; // not_null Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private Integer gender; // Giới tính 0 = Nam; 1 = Nữ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthDate; // Ngày sinh

    private String birthPlace; // Nơi sinh
    private BigDecimal agreementSalary; //Lương thỏa thuận
    private BigDecimal salaryCoefficient; // Hệ số lương
    private Integer numberOfDependent; // Số người phụ thuộc
    private BigDecimal insuranceSalary; // Lương đóng bảo hiểm
    private String bankAccount; // Tài khoản ngân hàng (Là số TK cá nhân nếu là nhân viên)
    private String bankName; // Tên ngân hàng
    private String address; // Địa chỉ của đối tượng
    private String accountObjectGroupList; // Lưu VyCodeID của nhóm KH, NCC được chọn, cách nhau bởi dấu ;
    private String accountObjectGroupListCode; // Lưu AccountObjectGroupCode của các nhóm KH, NCC được chọn, cách nhau bởi dấu ;
    private String companyTaxCode; // Mã số thuế
    private String tel; // Số điện thoại cố định
    private String mobile; // Số điện thoại di động
    private String fax; // Fax
    private String emailAddress; // Địa chỉ Email của tổ chức
    private String website; // Website
    private Integer paymentTermId; // Điều khoản thanh toán
    private BigDecimal maxDebtAmount; // Số nợ tối đa
    private Integer dueTime; // Hạn nợ ( Số ngày)
    private String identificationNumber; // Số CMTND của người liên hệ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date issueDate; // Ngày cấp CMTND người liên hệ

    private String issueBy; // Nơi cấp CMTND người liên hệ
    private String country; // Quốc gia
    private String provinceOrCity; // Tỉnh/ Thành phố
    private String district; // Quận/ Huyện
    private String wardOrCommune; // Phường/Xã
    private String prefix; // Xưng hô
    private String contactName; // Tên người liên hệ
    private String contactTitle; // Chức vụ người liên hệ (Nếu đối tượng là nhân viên thì đây chính là chức vụ mặc định của nhân viên đó)
    private String contactMobile; // Số điện thoại di động của người liên hệ
    private String otherContactMobile; // Số điện thoại di động khác của người liên hệ
    private String contactFixedTel; // Điện thoại cố định người liên hệ
    private String contactEmail; // Email người liên hệ
    private String contactAddress; // Địa chỉ người liên hệ
    private Boolean isVendor; // Là nhà cung cấp
    private Boolean isCustomer; // Là khách hàng
    private Boolean isEmployee; // Là cán bộ nhân viên
    private Integer accountObjectType; // 0 = là tổ chức; 1 = Là cá nhân
    private Boolean activeStatus; // Trạng thái theo dõi
    private Integer organizationUnitId; // Đơn vị của nhân viên
    private Integer branchId; // Chi nhánh
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private BigDecimal receiptableDebtAmount;
    private String shippingAddress;
    private String accountObjectGroupListName;
    private Integer employeeId;
    private String description;
    private String bankBranchName; // Chi nhánh tài khoản ngân hàng
    private String bankProvinceOrCity; // Tỉnh/Thành phố nơi mở tài khoản ngân hàng
    private String legalRepresentative;
    private String eInvoiceContactName;
    private String eInvoiceContactEmail;
    private String eInvoiceContactAddress;
    private String eInvoiceContactMobile;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountObjectCode")
    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "Gender")
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "BirthDate")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @Column(name = "BirthPlace")
    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Basic
    @Column(name = "AgreementSalary")
    public BigDecimal getAgreementSalary() {
        return agreementSalary;
    }

    public void setAgreementSalary(BigDecimal agreementSalary) {
        this.agreementSalary = agreementSalary;
    }

    @Basic
    @Column(name = "SalaryCoefficient")
    public BigDecimal getSalaryCoefficient() {
        return salaryCoefficient;
    }

    public void setSalaryCoefficient(BigDecimal salaryCoefficient) {
        this.salaryCoefficient = salaryCoefficient;
    }

    @Basic
    @Column(name = "NumberOfDependent")
    public Integer getNumberOfDependent() {
        return numberOfDependent;
    }

    public void setNumberOfDependent(Integer numberOfDependent) {
        this.numberOfDependent = numberOfDependent;
    }

    @Basic
    @Column(name = "InsuranceSalary")
    public BigDecimal getInsuranceSalary() {
        return insuranceSalary;
    }

    public void setInsuranceSalary(BigDecimal insuranceSalary) {
        this.insuranceSalary = insuranceSalary;
    }

    @Basic
    @Column(name = "BankAccount")
    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "AccountObjectGroupList")
    public String getAccountObjectGroupList() {
        return accountObjectGroupList;
    }

    public void setAccountObjectGroupList(String accountObjectGroupList) {
        this.accountObjectGroupList = accountObjectGroupList;
    }

    @Basic
    @Column(name = "AccountObjectGroupListCode")
    public String getAccountObjectGroupListCode() {
        return accountObjectGroupListCode;
    }

    public void setAccountObjectGroupListCode(String accountObjectGroupListCode) {
        this.accountObjectGroupListCode = accountObjectGroupListCode;
    }

    @Basic
    @Column(name = "CompanyTaxCode")
    public String getCompanyTaxCode() {
        return companyTaxCode;
    }

    public void setCompanyTaxCode(String companyTaxCode) {
        this.companyTaxCode = companyTaxCode;
    }

    @Basic
    @Column(name = "Tel")
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "Mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "Fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Basic
    @Column(name = "EmailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Basic
    @Column(name = "Website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "MaxDebtAmount")
    public BigDecimal getMaxDebtAmount() {
        return maxDebtAmount;
    }

    public void setMaxDebtAmount(BigDecimal maxDebtAmount) {
        this.maxDebtAmount = maxDebtAmount;
    }

    @Basic
    @Column(name = "DueTime")
    public Integer getDueTime() {
        return dueTime;
    }

    public void setDueTime(Integer dueTime) {
        this.dueTime = dueTime;
    }

    @Basic
    @Column(name = "IdentificationNumber")
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Basic
    @Column(name = "IssueDate")
    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    @Basic
    @Column(name = "IssueBy")
    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

    @Basic
    @Column(name = "Country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "ProvinceOrCity")
    public String getProvinceOrCity() {
        return provinceOrCity;
    }

    public void setProvinceOrCity(String provinceOrCity) {
        this.provinceOrCity = provinceOrCity;
    }

    @Basic
    @Column(name = "District")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "WardOrCommune")
    public String getWardOrCommune() {
        return wardOrCommune;
    }

    public void setWardOrCommune(String wardOrCommune) {
        this.wardOrCommune = wardOrCommune;
    }

    @Basic
    @Column(name = "Prefix")
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Basic
    @Column(name = "ContactName")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "ContactTitle")
    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    @Basic
    @Column(name = "ContactMobile")
    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    @Basic
    @Column(name = "OtherContactMobile")
    public String getOtherContactMobile() {
        return otherContactMobile;
    }

    public void setOtherContactMobile(String otherContactMobile) {
        this.otherContactMobile = otherContactMobile;
    }

    @Basic
    @Column(name = "ContactFixedTel")
    public String getContactFixedTel() {
        return contactFixedTel;
    }

    public void setContactFixedTel(String contactFixedTel) {
        this.contactFixedTel = contactFixedTel;
    }

    @Basic
    @Column(name = "ContactEmail")
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Basic
    @Column(name = "ContactAddress")
    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @Basic
    @Column(name = "IsVendor")
    public Boolean getIsVendor() {
        return isVendor;
    }

    public void setIsVendor(Boolean vendor) {
        isVendor = vendor;
    }

    @Basic
    @Column(name = "IsCustomer")
    public Boolean getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(Boolean customer) {
        isCustomer = customer;
    }

    @Basic
    @Column(name = "IsEmployee")
    public Boolean getIsEmployee() {
        return isEmployee;
    }

    public void setIsEmployee(Boolean employee) {
        isEmployee = employee;
    }

    @Basic
    @Column(name = "AccountObjectType")
    public Integer getAccountObjectType() {
        return accountObjectType;
    }

    public void setAccountObjectType(Integer accountObjectType) {
        this.accountObjectType = accountObjectType;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "ReceiptableDebtAmount")
    public BigDecimal getReceiptableDebtAmount() {
        return receiptableDebtAmount;
    }

    public void setReceiptableDebtAmount(BigDecimal receiptableDebtAmount) {
        this.receiptableDebtAmount = receiptableDebtAmount;
    }

    @Basic
    @Column(name = "ShippingAddress")
    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Basic
    @Column(name = "AccountObjectGroupListName")
    public String getAccountObjectGroupListName() {
        return accountObjectGroupListName;
    }

    public void setAccountObjectGroupListName(String accountObjectGroupListName) {
        this.accountObjectGroupListName = accountObjectGroupListName;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "BankBranchName")
    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    @Basic
    @Column(name = "BankProvinceOrCity")
    public String getBankProvinceOrCity() {
        return bankProvinceOrCity;
    }

    public void setBankProvinceOrCity(String bankProvinceOrCity) {
        this.bankProvinceOrCity = bankProvinceOrCity;
    }

    @Basic
    @Column(name = "LegalRepresentative")
    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    @Basic
    @Column(name = "EInvoiceContactName")
    public String geteInvoiceContactName() {
        return eInvoiceContactName;
    }

    public void seteInvoiceContactName(String eInvoiceContactName) {
        this.eInvoiceContactName = eInvoiceContactName;
    }

    @Basic
    @Column(name = "EInvoiceContactEmail")
    public String geteInvoiceContactEmail() {
        return eInvoiceContactEmail;
    }

    public void seteInvoiceContactEmail(String eInvoiceContactEmail) {
        this.eInvoiceContactEmail = eInvoiceContactEmail;
    }

    @Basic
    @Column(name = "EInvoiceContactAddress")
    public String geteInvoiceContactAddress() {
        return eInvoiceContactAddress;
    }

    public void seteInvoiceContactAddress(String eInvoiceContactAddress) {
        this.eInvoiceContactAddress = eInvoiceContactAddress;
    }

    @Basic
    @Column(name = "EInvoiceContactMobile")
    public String geteInvoiceContactMobile() {
        return eInvoiceContactMobile;
    }

    public void seteInvoiceContactMobile(String eInvoiceContactMobile) {
        this.eInvoiceContactMobile = eInvoiceContactMobile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountObject that = (AccountObject) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountObjectCode, that.accountObjectCode) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(birthPlace, that.birthPlace) &&
                Objects.equals(agreementSalary, that.agreementSalary) &&
                Objects.equals(salaryCoefficient, that.salaryCoefficient) &&
                Objects.equals(numberOfDependent, that.numberOfDependent) &&
                Objects.equals(insuranceSalary, that.insuranceSalary) &&
                Objects.equals(bankAccount, that.bankAccount) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(address, that.address) &&
                Objects.equals(accountObjectGroupList, that.accountObjectGroupList) &&
                Objects.equals(accountObjectGroupListCode, that.accountObjectGroupListCode) &&
                Objects.equals(companyTaxCode, that.companyTaxCode) &&
                Objects.equals(tel, that.tel) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(fax, that.fax) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(website, that.website) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(maxDebtAmount, that.maxDebtAmount) &&
                Objects.equals(dueTime, that.dueTime) &&
                Objects.equals(identificationNumber, that.identificationNumber) &&
                Objects.equals(issueDate, that.issueDate) &&
                Objects.equals(issueBy, that.issueBy) &&
                Objects.equals(country, that.country) &&
                Objects.equals(provinceOrCity, that.provinceOrCity) &&
                Objects.equals(district, that.district) &&
                Objects.equals(wardOrCommune, that.wardOrCommune) &&
                Objects.equals(prefix, that.prefix) &&
                Objects.equals(contactName, that.contactName) &&
                Objects.equals(contactTitle, that.contactTitle) &&
                Objects.equals(contactMobile, that.contactMobile) &&
                Objects.equals(otherContactMobile, that.otherContactMobile) &&
                Objects.equals(contactFixedTel, that.contactFixedTel) &&
                Objects.equals(contactEmail, that.contactEmail) &&
                Objects.equals(contactAddress, that.contactAddress) &&
                Objects.equals(isVendor, that.isVendor) &&
                Objects.equals(isCustomer, that.isCustomer) &&
                Objects.equals(isEmployee, that.isEmployee) &&
                Objects.equals(accountObjectType, that.accountObjectType) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(receiptableDebtAmount, that.receiptableDebtAmount) &&
                Objects.equals(shippingAddress, that.shippingAddress) &&
                Objects.equals(accountObjectGroupListName, that.accountObjectGroupListName) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(bankBranchName, that.bankBranchName) &&
                Objects.equals(bankProvinceOrCity, that.bankProvinceOrCity) &&
                Objects.equals(legalRepresentative, that.legalRepresentative) &&
                Objects.equals(eInvoiceContactName, that.eInvoiceContactName) &&
                Objects.equals(eInvoiceContactEmail, that.eInvoiceContactEmail) &&
                Objects.equals(eInvoiceContactAddress, that.eInvoiceContactAddress) &&
                Objects.equals(eInvoiceContactMobile, that.eInvoiceContactMobile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountObjectCode, accountObjectName, gender, birthDate, birthPlace, agreementSalary, salaryCoefficient, numberOfDependent, insuranceSalary, bankAccount, bankName, address, accountObjectGroupList, accountObjectGroupListCode, companyTaxCode, tel, mobile, fax, emailAddress, website, paymentTermId, maxDebtAmount, dueTime, identificationNumber, issueDate, issueBy, country, provinceOrCity, district, wardOrCommune, prefix, contactName, contactTitle, contactMobile, otherContactMobile, contactFixedTel, contactEmail, contactAddress, isVendor, isCustomer, isEmployee, accountObjectType, activeStatus, organizationUnitId, branchId, createdDate, createdBy, modifiedDate, modifiedBy, receiptableDebtAmount, shippingAddress, accountObjectGroupListName, employeeId, description, bankBranchName, bankProvinceOrCity, legalRepresentative, eInvoiceContactName, eInvoiceContactEmail, eInvoiceContactAddress, eInvoiceContactMobile);
    }
}
