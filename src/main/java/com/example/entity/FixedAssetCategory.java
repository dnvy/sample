package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Danh mục Tài sản cố định. http://localhost:8080/fixed_asset_categories
 */
@Entity
public class FixedAssetCategory {

    private Integer id; // NOT NULL. PK Loại Tài sản cố định
    private Integer parentId; // Id của Loại TSCĐ cha
    private String vyCodeId;
    private Boolean isParent; // NOT NULL.
    private Integer grade; // Cấp bậc
    private String fixedAssetCategoryCode; // Mã loại TSCĐ
    private String fixedAssetCategoryName; // Tên loại TSCĐ
    private String orgPriceAccount; // Tài khoản nguyên giá
    private String depreciationAccount; // Tài khoản khấu hao
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "FixedAssetCategoryCode")
    public String getFixedAssetCategoryCode() {
        return fixedAssetCategoryCode;
    }

    public void setFixedAssetCategoryCode(String fixedAssetCategoryCode) {
        this.fixedAssetCategoryCode = fixedAssetCategoryCode;
    }

    @Basic
    @Column(name = "FixedAssetCategoryName")
    public String getFixedAssetCategoryName() {
        return fixedAssetCategoryName;
    }

    public void setFixedAssetCategoryName(String fixedAssetCategoryName) {
        this.fixedAssetCategoryName = fixedAssetCategoryName;
    }

    @Basic
    @Column(name = "OrgPriceAccount")
    public String getOrgPriceAccount() {
        return orgPriceAccount;
    }

    public void setOrgPriceAccount(String orgPriceAccount) {
        this.orgPriceAccount = orgPriceAccount;
    }

    @Basic
    @Column(name = "DepreciationAccount")
    public String getDepreciationAccount() {
        return depreciationAccount;
    }

    public void setDepreciationAccount(String depreciationAccount) {
        this.depreciationAccount = depreciationAccount;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FixedAssetCategory that = (FixedAssetCategory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(vyCodeId, that.vyCodeId) &&
                Objects.equals(isParent, that.isParent) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(fixedAssetCategoryCode, that.fixedAssetCategoryCode) &&
                Objects.equals(fixedAssetCategoryName, that.fixedAssetCategoryName) &&
                Objects.equals(orgPriceAccount, that.orgPriceAccount) &&
                Objects.equals(depreciationAccount, that.depreciationAccount) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(sortVyCodeId, that.sortVyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, parentId, vyCodeId, isParent, grade, fixedAssetCategoryCode, fixedAssetCategoryName, orgPriceAccount, depreciationAccount, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, sortVyCodeId);
    }
}
