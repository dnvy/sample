package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Chi tiết báo giá.
 */
@Entity
@Table(name = "SAQuoteDetail")
public class SaQuoteDetail {

    private Integer id; // PK.
    private Integer inventoryItemId; // Mã sản phẩm
    private Integer refId; // FK
    private String description; // Mô tả hàng hóa
    private BigDecimal quantity; // Số lượng
    private BigDecimal unitPrice; // Đơn giá
    private BigDecimal amountOc; // Thành tiền
    private BigDecimal amount; // Thành tiền quy đổi
    private BigDecimal discountRate; // Tỉ lệ chiết khấu
    private BigDecimal discountAmountOc; // Tiền chiết khấu
    private BigDecimal discountAmount; // Tiền chiết khấu quy đổi
    private BigDecimal vatRate; // Thuế suất
    private BigDecimal vatAmountOc; // Tiền thuế
    private BigDecimal vatAmount; // Tiền thuế quy đổi
    private Integer sortOrder;
    private Integer unitId; // Đơn vị tính
    private BigDecimal unitPriceAfterTax; // Đơn giá sau thuế
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer orderId; // Đơn hàng
    private Integer contractId; // Hợp đồng
    private Integer projectWorkId; // Công trình/Hạng mục/Vụ việc
    private Integer expenseItemId;
    private Integer organizationUnitId;
    private Integer jobId;
    private Integer listItemId;
    private String specificity;
    private String exchangeRateOperator; // Toán tử quy đổi *=nhân;/=chia
    private BigDecimal mainQuantity; // Số lượng theo đơn vị chính
    private Integer mainUnitId; // Đơn vị tính chính
    private BigDecimal mainUnitPrice; // Đơn giá theo đơn vị chính
    private BigDecimal mainConvertRate; // Tỷ lệ chuyển đổi ra đơn vị chính
    private Boolean isPromotion;
    private String guarantyPeriod;
    private BigDecimal panelLengthQuantity;
    private BigDecimal panelWidthQuantity;
    private BigDecimal panelHeightQuantity;
    private BigDecimal panelRadiusQuantity;
    private BigDecimal panelQuantity;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "DiscountRate")
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @Basic
    @Column(name = "DiscountAmountOC")
    public BigDecimal getDiscountAmountOc() {
        return discountAmountOc;
    }

    public void setDiscountAmountOc(BigDecimal discountAmountOc) {
        this.discountAmountOc = discountAmountOc;
    }

    @Basic
    @Column(name = "DiscountAmount")
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "UnitPriceAfterTax")
    public BigDecimal getUnitPriceAfterTax() {
        return unitPriceAfterTax;
    }

    public void setUnitPriceAfterTax(BigDecimal unitPriceAfterTax) {
        this.unitPriceAfterTax = unitPriceAfterTax;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "Specificity")
    public String getSpecificity() {
        return specificity;
    }

    public void setSpecificity(String specificity) {
        this.specificity = specificity;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "IsPromotion")
    public Boolean getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Boolean promotion) {
        isPromotion = promotion;
    }

    @Basic
    @Column(name = "GuarantyPeriod")
    public String getGuarantyPeriod() {
        return guarantyPeriod;
    }

    public void setGuarantyPeriod(String guarantyPeriod) {
        this.guarantyPeriod = guarantyPeriod;
    }

    @Basic
    @Column(name = "PanelLengthQuantity")
    public BigDecimal getPanelLengthQuantity() {
        return panelLengthQuantity;
    }

    public void setPanelLengthQuantity(BigDecimal panelLengthQuantity) {
        this.panelLengthQuantity = panelLengthQuantity;
    }

    @Basic
    @Column(name = "PanelWidthQuantity")
    public BigDecimal getPanelWidthQuantity() {
        return panelWidthQuantity;
    }

    public void setPanelWidthQuantity(BigDecimal panelWidthQuantity) {
        this.panelWidthQuantity = panelWidthQuantity;
    }

    @Basic
    @Column(name = "PanelHeightQuantity")
    public BigDecimal getPanelHeightQuantity() {
        return panelHeightQuantity;
    }

    public void setPanelHeightQuantity(BigDecimal panelHeightQuantity) {
        this.panelHeightQuantity = panelHeightQuantity;
    }

    @Basic
    @Column(name = "PanelRadiusQuantity")
    public BigDecimal getPanelRadiusQuantity() {
        return panelRadiusQuantity;
    }

    public void setPanelRadiusQuantity(BigDecimal panelRadiusQuantity) {
        this.panelRadiusQuantity = panelRadiusQuantity;
    }

    @Basic
    @Column(name = "PanelQuantity")
    public BigDecimal getPanelQuantity() {
        return panelQuantity;
    }

    public void setPanelQuantity(BigDecimal panelQuantity) {
        this.panelQuantity = panelQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaQuoteDetail that = (SaQuoteDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(discountRate, that.discountRate) &&
                Objects.equals(discountAmountOc, that.discountAmountOc) &&
                Objects.equals(discountAmount, that.discountAmount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(unitPriceAfterTax, that.unitPriceAfterTax) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(specificity, that.specificity) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(isPromotion, that.isPromotion) &&
                Objects.equals(guarantyPeriod, that.guarantyPeriod) &&
                Objects.equals(panelLengthQuantity, that.panelLengthQuantity) &&
                Objects.equals(panelWidthQuantity, that.panelWidthQuantity) &&
                Objects.equals(panelHeightQuantity, that.panelHeightQuantity) &&
                Objects.equals(panelRadiusQuantity, that.panelRadiusQuantity) &&
                Objects.equals(panelQuantity, that.panelQuantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, inventoryItemId, refId, description, quantity, unitPrice, amountOc, amount, discountRate, discountAmountOc, discountAmount, vatRate, vatAmountOc, vatAmount, sortOrder, unitId, unitPriceAfterTax, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, orderId, contractId, projectWorkId, expenseItemId, organizationUnitId, jobId, listItemId, specificity, exchangeRateOperator, mainQuantity, mainUnitId, mainUnitPrice, mainConvertRate, isPromotion, guarantyPeriod, panelLengthQuantity, panelWidthQuantity, panelHeightQuantity, panelRadiusQuantity, panelQuantity);
    }
}
