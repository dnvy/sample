package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Ký hiệu chấm công. Xem http://localhost:8080/timesheet_signs
 */
@Entity
public class TimeSheetSign {

    private Integer id; // PK
    private String timeSheetSignCode; // Ký hiệu chấm công
    private String timeSheetSignName; // Diễn giải
    private BigDecimal salaryRate; // Tỷ lệ hưởng lương
    private Boolean activeStatus; // Trạng thái theo dõi
    private Boolean isDefault; // Là ký hiệu mặc định
    private Boolean isSystem; // Là ký hiệu mặc định của hệ thống
    private Boolean isHalfDay;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TimeSheetSignCode")
    public String getTimeSheetSignCode() {
        return timeSheetSignCode;
    }

    public void setTimeSheetSignCode(String timeSheetSignCode) {
        this.timeSheetSignCode = timeSheetSignCode;
    }

    @Basic
    @Column(name = "TimeSheetSignName")
    public String getTimeSheetSignName() {
        return timeSheetSignName;
    }

    public void setTimeSheetSignName(String timeSheetSignName) {
        this.timeSheetSignName = timeSheetSignName;
    }

    @Basic
    @Column(name = "SalaryRate")
    public BigDecimal getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryRate(BigDecimal salaryRate) {
        this.salaryRate = salaryRate;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "IsDefault")
    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "IsHalfDay")
    public Boolean getIsHalfDay() {
        return isHalfDay;
    }

    public void setIsHalfDay(Boolean halfDay) {
        isHalfDay = halfDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSheetSign that = (TimeSheetSign) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(timeSheetSignCode, that.timeSheetSignCode) &&
                Objects.equals(timeSheetSignName, that.timeSheetSignName) &&
                Objects.equals(salaryRate, that.salaryRate) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(isDefault, that.isDefault) &&
                Objects.equals(isSystem, that.isSystem) &&
                Objects.equals(isHalfDay, that.isHalfDay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timeSheetSignCode, timeSheetSignName, salaryRate, activeStatus, isDefault, isSystem, isHalfDay);
    }
}
