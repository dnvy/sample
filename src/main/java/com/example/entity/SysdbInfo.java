package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Thông tin Hệ thống kế toán. Xem http://localhost:8080/accounting_info
 */
@Entity
@Table(name = "SYSDBInfo")
public class SysdbInfo {

    private Integer id;
    private String application;
    private String version;
    private String mvc;
    private String createdBy;
    private Timestamp createdDate;
    private Timestamp closedDate;
    private String description;
    private Boolean demoData;
    private Timestamp demoDate;
    private Integer particularity;
    private Integer mobileDatabaseId;
    private Boolean isFirstTimeSyncLedger;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Application")
    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Basic
    @Column(name = "Version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "MVC")
    public String getMvc() {
        return mvc;
    }

    public void setMvc(String mvc) {
        this.mvc = mvc;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "ClosedDate")
    public Timestamp getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Timestamp closedDate) {
        this.closedDate = closedDate;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "DemoData")
    public Boolean getDemoData() {
        return demoData;
    }

    public void setDemoData(Boolean demoData) {
        this.demoData = demoData;
    }

    @Basic
    @Column(name = "DemoDate")
    public Timestamp getDemoDate() {
        return demoDate;
    }

    public void setDemoDate(Timestamp demoDate) {
        this.demoDate = demoDate;
    }

    @Basic
    @Column(name = "Particularity")
    public Integer getParticularity() {
        return particularity;
    }

    public void setParticularity(Integer particularity) {
        this.particularity = particularity;
    }

    @Basic
    @Column(name = "MobileDatabaseID")
    public Integer getMobileDatabaseId() {
        return mobileDatabaseId;
    }

    public void setMobileDatabaseId(Integer mobileDatabaseId) {
        this.mobileDatabaseId = mobileDatabaseId;
    }

    @Basic
    @Column(name = "IsFirstTimeSyncLedger")
    public Boolean getIsFirstTimeSyncLedger() {
        return isFirstTimeSyncLedger;
    }

    public void setIsFirstTimeSyncLedger(Boolean firstTimeSyncLedger) {
        isFirstTimeSyncLedger = firstTimeSyncLedger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysdbInfo sysdbInfo = (SysdbInfo) o;
        return Objects.equals(id, sysdbInfo.id) &&
                Objects.equals(application, sysdbInfo.application) &&
                Objects.equals(version, sysdbInfo.version) &&
                Objects.equals(mvc, sysdbInfo.mvc) &&
                Objects.equals(createdBy, sysdbInfo.createdBy) &&
                Objects.equals(createdDate, sysdbInfo.createdDate) &&
                Objects.equals(closedDate, sysdbInfo.closedDate) &&
                Objects.equals(description, sysdbInfo.description) &&
                Objects.equals(demoData, sysdbInfo.demoData) &&
                Objects.equals(demoDate, sysdbInfo.demoDate) &&
                Objects.equals(particularity, sysdbInfo.particularity) &&
                Objects.equals(mobileDatabaseId, sysdbInfo.mobileDatabaseId) &&
                Objects.equals(isFirstTimeSyncLedger, sysdbInfo.isFirstTimeSyncLedger);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, application, version, mvc, createdBy, createdDate, closedDate, description, demoData, demoDate, particularity, mobileDatabaseId, isFirstTimeSyncLedger);
    }
}
