package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Đơn mua hàng.
 */
@Entity
@Table(name = "PUOrder")
public class PuOrder {

    private Integer id; // PK
    private Integer branchId; // Mã chi nhánh
    private Date refDate; // Ngày đơn hàng
    private Integer refType; // Loại chứng từ (lấy từ bảng RefType)
    private String refNo; // Số đơn hàng
    private Integer status; // Tình trạng đơn hàng: 0: Chưa gửi; 1: Đã gửi; 2: Đang thực hiện; 3: Hoàn thành 4: Đã hủy bỏ
    private Integer accountObjectId; // Mã nhà cung cấp/khách hàng/cán bộ
    private String accountObjectName; // Tên nhà cung cấp/khách hàng/cán bộ
    private String accountObjectAddress; // Địa chỉ NCC
    private String accountObjectTaxCode; // Mã số thuế NCC
    private String journalMemo; // Diễn giải/Lý do
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỉ giá hối đoái
    private Date receiveDate; // Ngày nhận hàng
    private String receiveAddress; // Địa chỉ nhận hàng
    private Integer paymentTermId; // Điều khoản thanh toán
    private Integer dueDay; // Số ngày được nợ
    private String otherTerm; // Điều khoản khác
    private Integer employeeId; // Nhân viên
    private BigDecimal totalAmountOc; // Tổng tiền
    private BigDecimal totalAmount; // Tổng tiền quy đổi
    private BigDecimal totalVatAmountOc; // Tổng tiền thuế
    private BigDecimal totalVatAmount; // Tổng tiền thuế Quy đổi
    private BigDecimal totalDiscountAmountOc; // Tổng tiền chiết khấu
    private BigDecimal totalDiscountAmount; // Tổng tiền chiết khấu quy đổi
    private Integer editVersion; // Số thứ tự nhập chứng từ
    private Integer refOrder;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "Status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "ReceiveDate")
    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    @Basic
    @Column(name = "ReceiveAddress")
    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "DueDay")
    public Integer getDueDay() {
        return dueDay;
    }

    public void setDueDay(Integer dueDay) {
        this.dueDay = dueDay;
    }

    @Basic
    @Column(name = "OtherTerm")
    public String getOtherTerm() {
        return otherTerm;
    }

    public void setOtherTerm(String otherTerm) {
        this.otherTerm = otherTerm;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PuOrder puOrder = (PuOrder) o;
        return Objects.equals(id, puOrder.id) &&
                Objects.equals(branchId, puOrder.branchId) &&
                Objects.equals(refDate, puOrder.refDate) &&
                Objects.equals(refType, puOrder.refType) &&
                Objects.equals(refNo, puOrder.refNo) &&
                Objects.equals(status, puOrder.status) &&
                Objects.equals(accountObjectId, puOrder.accountObjectId) &&
                Objects.equals(accountObjectName, puOrder.accountObjectName) &&
                Objects.equals(accountObjectAddress, puOrder.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, puOrder.accountObjectTaxCode) &&
                Objects.equals(journalMemo, puOrder.journalMemo) &&
                Objects.equals(currencyId, puOrder.currencyId) &&
                Objects.equals(exchangeRate, puOrder.exchangeRate) &&
                Objects.equals(receiveDate, puOrder.receiveDate) &&
                Objects.equals(receiveAddress, puOrder.receiveAddress) &&
                Objects.equals(paymentTermId, puOrder.paymentTermId) &&
                Objects.equals(dueDay, puOrder.dueDay) &&
                Objects.equals(otherTerm, puOrder.otherTerm) &&
                Objects.equals(employeeId, puOrder.employeeId) &&
                Objects.equals(totalAmountOc, puOrder.totalAmountOc) &&
                Objects.equals(totalAmount, puOrder.totalAmount) &&
                Objects.equals(totalVatAmountOc, puOrder.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, puOrder.totalVatAmount) &&
                Objects.equals(totalDiscountAmountOc, puOrder.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, puOrder.totalDiscountAmount) &&
                Objects.equals(editVersion, puOrder.editVersion) &&
                Objects.equals(refOrder, puOrder.refOrder) &&
                Objects.equals(createdDate, puOrder.createdDate) &&
                Objects.equals(createdBy, puOrder.createdBy) &&
                Objects.equals(modifiedDate, puOrder.modifiedDate) &&
                Objects.equals(modifiedBy, puOrder.modifiedBy) &&
                Objects.equals(customField1, puOrder.customField1) &&
                Objects.equals(customField2, puOrder.customField2) &&
                Objects.equals(customField3, puOrder.customField3) &&
                Objects.equals(customField4, puOrder.customField4) &&
                Objects.equals(customField5, puOrder.customField5) &&
                Objects.equals(customField6, puOrder.customField6) &&
                Objects.equals(customField7, puOrder.customField7) &&
                Objects.equals(customField8, puOrder.customField8) &&
                Objects.equals(customField9, puOrder.customField9) &&
                Objects.equals(customField10, puOrder.customField10);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, refDate, refType, refNo, status, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, journalMemo, currencyId, exchangeRate, receiveDate, receiveAddress, paymentTermId, dueDay, otherTerm, employeeId, totalAmountOc, totalAmount, totalVatAmountOc, totalVatAmount, totalDiscountAmountOc, totalDiscountAmount, editVersion, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10);
    }
}
