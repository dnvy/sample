package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Danh mục mã thống kê.
 */
@Entity
public class ListItem {

    private Integer id; // PK
    private String listItemCode; // NOT NULL. Mã thông kê
    private String listItemName; // NOT NULL. Tên mã thông kê
    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent; // NOT NULL.
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ListItemCode")
    public String getListItemCode() {
        return listItemCode;
    }

    public void setListItemCode(String listItemCode) {
        this.listItemCode = listItemCode;
    }

    @Basic
    @Column(name = "ListItemName")
    public String getListItemName() {
        return listItemName;
    }

    public void setListItemName(String listItemName) {
        this.listItemName = listItemName;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListItem listItem = (ListItem) o;
        return Objects.equals(id, listItem.id) &&
                Objects.equals(listItemCode, listItem.listItemCode) &&
                Objects.equals(listItemName, listItem.listItemName) &&
                Objects.equals(parentId, listItem.parentId) &&
                Objects.equals(vyCodeId, listItem.vyCodeId) &&
                Objects.equals(grade, listItem.grade) &&
                Objects.equals(isParent, listItem.isParent) &&
                Objects.equals(description, listItem.description) &&
                Objects.equals(activeStatus, listItem.activeStatus) &&
                Objects.equals(createdDate, listItem.createdDate) &&
                Objects.equals(createdBy, listItem.createdBy) &&
                Objects.equals(modifiedDate, listItem.modifiedDate) &&
                Objects.equals(modifiedBy, listItem.modifiedBy) &&
                Objects.equals(sortVyCodeId, listItem.sortVyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, listItemCode, listItemName, parentId, vyCodeId, grade, isParent, description, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, sortVyCodeId);
    }
}
