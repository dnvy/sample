package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Danh sách đợt thu nợ.
 */
@Entity
public class DebtList {

    private Integer id; // NOT NULL. PK
    private Integer branchId; // NOT NULL. Chi nhánh
    private String debtListName; // NOT NULL. Tên đợt thu nợ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date fromDate; // NOT NULL. Từ ngày

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date toDate; // NOT NULL. Đến ngày

    private BigDecimal totalReceiptableAmount; // NOT NULL. Tổng công nợ
    private BigDecimal targetPercent; // NOT NULL. Mục tiêu thu được (%)
    private BigDecimal targetAmount; // NOT NULL. Mục tiêu thu được (Số tiền)
    private String description; // Diễn giải
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DebtListName")
    public String getDebtListName() {
        return debtListName;
    }

    public void setDebtListName(String debtListName) {
        this.debtListName = debtListName;
    }

    @Basic
    @Column(name = "FromDate")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "ToDate")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Basic
    @Column(name = "TotalReceiptableAmount")
    public BigDecimal getTotalReceiptableAmount() {
        return totalReceiptableAmount;
    }

    public void setTotalReceiptableAmount(BigDecimal totalReceiptableAmount) {
        this.totalReceiptableAmount = totalReceiptableAmount;
    }

    @Basic
    @Column(name = "TargetPercent")
    public BigDecimal getTargetPercent() {
        return targetPercent;
    }

    public void setTargetPercent(BigDecimal targetPercent) {
        this.targetPercent = targetPercent;
    }

    @Basic
    @Column(name = "TargetAmount")
    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DebtList debtList = (DebtList) o;
        return Objects.equals(id, debtList.id) &&
                Objects.equals(branchId, debtList.branchId) &&
                Objects.equals(debtListName, debtList.debtListName) &&
                Objects.equals(fromDate, debtList.fromDate) &&
                Objects.equals(toDate, debtList.toDate) &&
                Objects.equals(totalReceiptableAmount, debtList.totalReceiptableAmount) &&
                Objects.equals(targetPercent, debtList.targetPercent) &&
                Objects.equals(targetAmount, debtList.targetAmount) &&
                Objects.equals(description, debtList.description) &&
                Objects.equals(createdDate, debtList.createdDate) &&
                Objects.equals(createdBy, debtList.createdBy) &&
                Objects.equals(modifiedDate, debtList.modifiedDate) &&
                Objects.equals(modifiedBy, debtList.modifiedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, debtListName, fromDate, toDate, totalReceiptableAmount, targetPercent, targetAmount, description, createdDate, createdBy, modifiedDate, modifiedBy);
    }
}
