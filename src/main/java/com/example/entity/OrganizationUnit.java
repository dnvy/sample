package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Đơn vị tổ chức, phòng ban. Xem http://localhost:8080/organization_units
 */
@Entity
public class OrganizationUnit {

    private Integer id; // PK của bảng
    private Integer branchId; // Chi nhánh
    private String organizationUnitCode; // NOT NULL. Mã đơn vị
    private String organizationUnitName; // NOT NULL. Tên đơn vị
    private Boolean isSystem; // NOT NULL. Thuộc hệ thống
    private String vyCodeId; //
    private Integer grade; // Cấp tổ chức
    private Integer parentId; // ID đơn vị cha
    private Boolean isParent; // NOT NULL. Là đơn vị cha
    private String address; // Địa chỉ

    /**
     * Cấp tổ chức:
     * 1 - Tổng công ty;
     * 2 - Chi nhánh;
     * 3 - VP/TT;
     * 4 - Phòng ban;
     * 5- Phân xưởng;
     * 6 - Nhóm/Tổ,hội
     */
    private Integer organizationUnitTypeId; // NOT NULL.

    private String businessRegistrationNumber; // Số đăng ký kinh doanh

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date businessRegistrationNumberIssuedDate; // Ngày cấp

    private String businessRegistrationNumberIssuedPlace; // Nơi cấp
    private Boolean isDependent; // 0 = Hạch toán độc lập; 1 = Hạch toán  phụ thuộc
    private Boolean isPrivateVatDeclaration; // NOT NULL. Kê khai thuế giá trị gia tăng riêng
    private String costAccount; // Tài khoản chi phí lương
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi.
    private String companyTaxCode; // Mã số thuế
    private String companyTel; // Số điện thoại
    private String companyFax; // Fax
    private String companyEmail; // Email
    private String companyWebsite; // Website
    private Integer companyBankAccountId; // Tài khoản ngân hàng
    private String companyOwnerName; // Tên đơn vị chủ quản
    private String companyOwnerTaxCode; // Mã số thuế đơn vị chủ quản
    private String directorTitle; // Tiêu đề người ký là giám đốc
    private String directorName; // Tên giám đốc
    private String chiefOfAccountingTitle; // Tiêu đề người ký là Kế toán trưởng
    private String chiefOfAccountingName; // Tên Kế toán trưởng
    private String storeKeeperTitle; // Tiêu đề người ký là Thủ kho
    private String storeKeeperName; // Tên thủ kho
    private String cashierTitle; // Tiêu đề người ký là Thủ quỹ
    private String cashierName; // Tên Thủ quỹ
    private String reporterTitle; // Tiêu đề người ký là Người lập biểu
    private String reporterName; // Tên Người lập biểu
    private Boolean isPrintSigner; // NOT NULL. In tên người ký lên chứng từ, báo cáo
    private Boolean isGetReporterNameByUserLogIn; // NOT NULL. Lấy tên người lập biểu theo tên người đăng nhập. not null
    private String createdBy;
    private Timestamp createdDate;
    private String modifiedBy;
    private Timestamp modifiedDate;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.
    private String companyDistrict; // Quận/Huyện
    private String companyCity; // Tỉnh/TP

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "OrganizationUnitCode")
    public String getOrganizationUnitCode() {
        return organizationUnitCode;
    }

    public void setOrganizationUnitCode(String organizationUnitCode) {
        this.organizationUnitCode = organizationUnitCode;
    }

    @Basic
    @Column(name = "OrganizationUnitName")
    public String getOrganizationUnitName() {
        return organizationUnitName;
    }

    public void setOrganizationUnitName(String organizationUnitName) {
        this.organizationUnitName = organizationUnitName;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "OrganizationUnitTypeID")
    public Integer getOrganizationUnitTypeId() {
        return organizationUnitTypeId;
    }

    public void setOrganizationUnitTypeId(Integer organizationUnitTypeId) {
        this.organizationUnitTypeId = organizationUnitTypeId;
    }

    @Basic
    @Column(name = "BusinessRegistrationNumber")
    public String getBusinessRegistrationNumber() {
        return businessRegistrationNumber;
    }

    public void setBusinessRegistrationNumber(String businessRegistrationNumber) {
        this.businessRegistrationNumber = businessRegistrationNumber;
    }

    @Basic
    @Column(name = "BusinessRegistrationNumberIssuedDate")
    public Date getBusinessRegistrationNumberIssuedDate() {
        return businessRegistrationNumberIssuedDate;
    }

    public void setBusinessRegistrationNumberIssuedDate(Date businessRegistrationNumberIssuedDate) {
        this.businessRegistrationNumberIssuedDate = businessRegistrationNumberIssuedDate;
    }

    @Basic
    @Column(name = "BusinessRegistrationNumberIssuedPlace")
    public String getBusinessRegistrationNumberIssuedPlace() {
        return businessRegistrationNumberIssuedPlace;
    }

    public void setBusinessRegistrationNumberIssuedPlace(String businessRegistrationNumberIssuedPlace) {
        this.businessRegistrationNumberIssuedPlace = businessRegistrationNumberIssuedPlace;
    }

    @Basic
    @Column(name = "IsDependent")
    public Boolean getIsDependent() {
        return isDependent;
    }

    public void setIsDependent(Boolean dependent) {
        isDependent = dependent;
    }

    @Basic
    @Column(name = "IsPrivateVATDeclaration")
    public Boolean getIsPrivateVatDeclaration() {
        return isPrivateVatDeclaration;
    }

    public void setIsPrivateVatDeclaration(Boolean privateVatDeclaration) {
        isPrivateVatDeclaration = privateVatDeclaration;
    }

    @Basic
    @Column(name = "CostAccount")
    public String getCostAccount() {
        return costAccount;
    }

    public void setCostAccount(String costAccount) {
        this.costAccount = costAccount;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CompanyTaxCode")
    public String getCompanyTaxCode() {
        return companyTaxCode;
    }

    public void setCompanyTaxCode(String companyTaxCode) {
        this.companyTaxCode = companyTaxCode;
    }

    @Basic
    @Column(name = "CompanyTel")
    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    @Basic
    @Column(name = "CompanyFax")
    public String getCompanyFax() {
        return companyFax;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    @Basic
    @Column(name = "CompanyEmail")
    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    @Basic
    @Column(name = "CompanyWebsite")
    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    @Basic
    @Column(name = "CompanyBankAccountID")
    public Integer getCompanyBankAccountId() {
        return companyBankAccountId;
    }

    public void setCompanyBankAccountId(Integer companyBankAccountId) {
        this.companyBankAccountId = companyBankAccountId;
    }

    @Basic
    @Column(name = "CompanyOwnerName")
    public String getCompanyOwnerName() {
        return companyOwnerName;
    }

    public void setCompanyOwnerName(String companyOwnerName) {
        this.companyOwnerName = companyOwnerName;
    }

    @Basic
    @Column(name = "CompanyOwnerTaxCode")
    public String getCompanyOwnerTaxCode() {
        return companyOwnerTaxCode;
    }

    public void setCompanyOwnerTaxCode(String companyOwnerTaxCode) {
        this.companyOwnerTaxCode = companyOwnerTaxCode;
    }

    @Basic
    @Column(name = "DirectorTitle")
    public String getDirectorTitle() {
        return directorTitle;
    }

    public void setDirectorTitle(String directorTitle) {
        this.directorTitle = directorTitle;
    }

    @Basic
    @Column(name = "DirectorName")
    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    @Basic
    @Column(name = "ChiefOfAccountingTitle")
    public String getChiefOfAccountingTitle() {
        return chiefOfAccountingTitle;
    }

    public void setChiefOfAccountingTitle(String chiefOfAccountingTitle) {
        this.chiefOfAccountingTitle = chiefOfAccountingTitle;
    }

    @Basic
    @Column(name = "ChiefOfAccountingName")
    public String getChiefOfAccountingName() {
        return chiefOfAccountingName;
    }

    public void setChiefOfAccountingName(String chiefOfAccountingName) {
        this.chiefOfAccountingName = chiefOfAccountingName;
    }

    @Basic
    @Column(name = "StoreKeeperTitle")
    public String getStoreKeeperTitle() {
        return storeKeeperTitle;
    }

    public void setStoreKeeperTitle(String storeKeeperTitle) {
        this.storeKeeperTitle = storeKeeperTitle;
    }

    @Basic
    @Column(name = "StoreKeeperName")
    public String getStoreKeeperName() {
        return storeKeeperName;
    }

    public void setStoreKeeperName(String storeKeeperName) {
        this.storeKeeperName = storeKeeperName;
    }

    @Basic
    @Column(name = "CashierTitle")
    public String getCashierTitle() {
        return cashierTitle;
    }

    public void setCashierTitle(String cashierTitle) {
        this.cashierTitle = cashierTitle;
    }

    @Basic
    @Column(name = "CashierName")
    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    @Basic
    @Column(name = "ReporterTitle")
    public String getReporterTitle() {
        return reporterTitle;
    }

    public void setReporterTitle(String reporterTitle) {
        this.reporterTitle = reporterTitle;
    }

    @Basic
    @Column(name = "ReporterName")
    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    @Basic
    @Column(name = "IsPrintSigner")
    public Boolean getIsPrintSigner() {
        return isPrintSigner;
    }

    public void setIsPrintSigner(Boolean printSigner) {
        isPrintSigner = printSigner;
    }

    @Basic
    @Column(name = "IsGetReporterNameByUserLogIn")
    public Boolean getIsGetReporterNameByUserLogIn() {
        return isGetReporterNameByUserLogIn;
    }

    public void setIsGetReporterNameByUserLogIn(Boolean getReporterNameByUserLogIn) {
        isGetReporterNameByUserLogIn = getReporterNameByUserLogIn;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Basic
    @Column(name = "CompanyDistrict")
    public String getCompanyDistrict() {
        return companyDistrict;
    }

    public void setCompanyDistrict(String companyDistrict) {
        this.companyDistrict = companyDistrict;
    }

    @Basic
    @Column(name = "CompanyCity")
    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationUnit that = (OrganizationUnit) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(organizationUnitCode, that.organizationUnitCode) &&
                Objects.equals(organizationUnitName, that.organizationUnitName) &&
                Objects.equals(isSystem, that.isSystem) &&
                Objects.equals(vyCodeId, that.vyCodeId) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(isParent, that.isParent) &&
                Objects.equals(address, that.address) &&
                Objects.equals(organizationUnitTypeId, that.organizationUnitTypeId) &&
                Objects.equals(businessRegistrationNumber, that.businessRegistrationNumber) &&
                Objects.equals(businessRegistrationNumberIssuedDate, that.businessRegistrationNumberIssuedDate) &&
                Objects.equals(businessRegistrationNumberIssuedPlace, that.businessRegistrationNumberIssuedPlace) &&
                Objects.equals(isDependent, that.isDependent) &&
                Objects.equals(isPrivateVatDeclaration, that.isPrivateVatDeclaration) &&
                Objects.equals(costAccount, that.costAccount) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(companyTaxCode, that.companyTaxCode) &&
                Objects.equals(companyTel, that.companyTel) &&
                Objects.equals(companyFax, that.companyFax) &&
                Objects.equals(companyEmail, that.companyEmail) &&
                Objects.equals(companyWebsite, that.companyWebsite) &&
                Objects.equals(companyBankAccountId, that.companyBankAccountId) &&
                Objects.equals(companyOwnerName, that.companyOwnerName) &&
                Objects.equals(companyOwnerTaxCode, that.companyOwnerTaxCode) &&
                Objects.equals(directorTitle, that.directorTitle) &&
                Objects.equals(directorName, that.directorName) &&
                Objects.equals(chiefOfAccountingTitle, that.chiefOfAccountingTitle) &&
                Objects.equals(chiefOfAccountingName, that.chiefOfAccountingName) &&
                Objects.equals(storeKeeperTitle, that.storeKeeperTitle) &&
                Objects.equals(storeKeeperName, that.storeKeeperName) &&
                Objects.equals(cashierTitle, that.cashierTitle) &&
                Objects.equals(cashierName, that.cashierName) &&
                Objects.equals(reporterTitle, that.reporterTitle) &&
                Objects.equals(reporterName, that.reporterName) &&
                Objects.equals(isPrintSigner, that.isPrintSigner) &&
                Objects.equals(isGetReporterNameByUserLogIn, that.isGetReporterNameByUserLogIn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(sortVyCodeId, that.sortVyCodeId) &&
                Objects.equals(companyDistrict, that.companyDistrict) &&
                Objects.equals(companyCity, that.companyCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, organizationUnitCode, organizationUnitName, isSystem, vyCodeId, grade, parentId, isParent, address, organizationUnitTypeId, businessRegistrationNumber, businessRegistrationNumberIssuedDate, businessRegistrationNumberIssuedPlace, isDependent, isPrivateVatDeclaration, costAccount, activeStatus, companyTaxCode, companyTel, companyFax, companyEmail, companyWebsite, companyBankAccountId, companyOwnerName, companyOwnerTaxCode, directorTitle, directorName, chiefOfAccountingTitle, chiefOfAccountingName, storeKeeperTitle, storeKeeperName, cashierTitle, cashierName, reporterTitle, reporterName, isPrintSigner, isGetReporterNameByUserLogIn, createdBy, createdDate, modifiedBy, modifiedDate, sortVyCodeId, companyDistrict, companyCity);
    }
}
