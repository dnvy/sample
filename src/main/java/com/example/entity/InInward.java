package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng Master của phiếu nhập kho.
 */
@Entity
@Table(name = "INInward")
public class InInward {

    private Integer id; // PK
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer refType; // Loại chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày phiếu nhập

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày hạch toán

    private String refNoFinance; // Số chứng từ Sổ tài chính
    private String refNoManagement; // Số chứng từ Sổ quản trị
    private Boolean isPostedFinance; // Trạng thái ghi sổ Sổ tài chính
    private Boolean isPostedManagement; // Trạng thái ghi sổ Sổ quản trị
    private Integer accountObjectId; // Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private String contactName; // Người giao hàng
    private String journalMemo; // Diễn giải
    private String documentIncluded; // Tài liệu kèm theo trên phiếu thu
    private Integer employeeId; // Nhân viên
    private BigDecimal totalAmountFinance; // Tổng thành tiền Sổ tài chính
    private Integer branchId; // ID của chi nhánh.
    private Integer saReturnRefId; // RefID của chứng từ Bán trả lại
    private Integer unitPriceMethod; // Phương thức nhập/tính đơn giá nhập. 0 = Lấy từ giá xuất bán; 1 = Nhập đơn giá bằng tay
    private Boolean isPostedInventoryBookFinance; // Trạng thái ghi sổ thủ kho (Sổ tài chính)
    private Boolean isPostedInventoryBookManagement; // Trạng thái ghi sổ thủ kho (Sổ quản trị)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inventoryPostedDate; // Ngày ghi sổ kho

    private Integer editVersion; // Phiên bản sửa chứng từ
    private Integer refOrder; // Số thứ tự nhập chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private BigDecimal totalAmountManagement; // Tổng thành tiền Sổ quản trị
    private Integer outwardDependentRefId;
    private Integer assemblyRefId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inRefOrder;

    private Boolean isReturnWithInward;
    private Integer currencyId;
    private BigDecimal exchangeRate;
    private Boolean isCreatedSaReturnLastYear; // Đã lập bán trả lại năm trước

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "ContactName")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "DocumentIncluded")
    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "TotalAmountFinance")
    public BigDecimal getTotalAmountFinance() {
        return totalAmountFinance;
    }

    public void setTotalAmountFinance(BigDecimal totalAmountFinance) {
        this.totalAmountFinance = totalAmountFinance;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "SAReturnRefID")
    public Integer getSaReturnRefId() {
        return saReturnRefId;
    }

    public void setSaReturnRefId(Integer saReturnRefId) {
        this.saReturnRefId = saReturnRefId;
    }

    @Basic
    @Column(name = "UnitPriceMethod")
    public Integer getUnitPriceMethod() {
        return unitPriceMethod;
    }

    public void setUnitPriceMethod(Integer unitPriceMethod) {
        this.unitPriceMethod = unitPriceMethod;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookFinance")
    public Boolean getIsPostedInventoryBookFinance() {
        return isPostedInventoryBookFinance;
    }

    public void setIsPostedInventoryBookFinance(Boolean postedInventoryBookFinance) {
        isPostedInventoryBookFinance = postedInventoryBookFinance;
    }

    @Basic
    @Column(name = "IsPostedInventoryBookManagement")
    public Boolean getIsPostedInventoryBookManagement() {
        return isPostedInventoryBookManagement;
    }

    public void setIsPostedInventoryBookManagement(Boolean postedInventoryBookManagement) {
        isPostedInventoryBookManagement = postedInventoryBookManagement;
    }

    @Basic
    @Column(name = "InventoryPostedDate")
    public Date getInventoryPostedDate() {
        return inventoryPostedDate;
    }

    public void setInventoryPostedDate(Date inventoryPostedDate) {
        this.inventoryPostedDate = inventoryPostedDate;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "TotalAmountManagement")
    public BigDecimal getTotalAmountManagement() {
        return totalAmountManagement;
    }

    public void setTotalAmountManagement(BigDecimal totalAmountManagement) {
        this.totalAmountManagement = totalAmountManagement;
    }

    @Basic
    @Column(name = "OutwardDependentRefID")
    public Integer getOutwardDependentRefId() {
        return outwardDependentRefId;
    }

    public void setOutwardDependentRefId(Integer outwardDependentRefId) {
        this.outwardDependentRefId = outwardDependentRefId;
    }

    @Basic
    @Column(name = "AssemblyRefID")
    public Integer getAssemblyRefId() {
        return assemblyRefId;
    }

    public void setAssemblyRefId(Integer assemblyRefId) {
        this.assemblyRefId = assemblyRefId;
    }

    @Basic
    @Column(name = "INRefOrder")
    public Date getInRefOrder() {
        return inRefOrder;
    }

    public void setInRefOrder(Date inRefOrder) {
        this.inRefOrder = inRefOrder;
    }

    @Basic
    @Column(name = "IsReturnWithInward")
    public Boolean getIsReturnWithInward() {
        return isReturnWithInward;
    }

    public void setIsReturnWithInward(Boolean returnWithInward) {
        isReturnWithInward = returnWithInward;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "IsCreatedSAReturnLastYear")
    public Boolean getIsCreatedSaReturnLastYear() {
        return isCreatedSaReturnLastYear;
    }

    public void setIsCreatedSaReturnLastYear(Boolean createdSaReturnLastYear) {
        isCreatedSaReturnLastYear = createdSaReturnLastYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InInward inInward = (InInward) o;
        return Objects.equals(id, inInward.id) &&
                Objects.equals(displayOnBook, inInward.displayOnBook) &&
                Objects.equals(refType, inInward.refType) &&
                Objects.equals(refDate, inInward.refDate) &&
                Objects.equals(postedDate, inInward.postedDate) &&
                Objects.equals(refNoFinance, inInward.refNoFinance) &&
                Objects.equals(refNoManagement, inInward.refNoManagement) &&
                Objects.equals(isPostedFinance, inInward.isPostedFinance) &&
                Objects.equals(isPostedManagement, inInward.isPostedManagement) &&
                Objects.equals(accountObjectId, inInward.accountObjectId) &&
                Objects.equals(accountObjectName, inInward.accountObjectName) &&
                Objects.equals(contactName, inInward.contactName) &&
                Objects.equals(journalMemo, inInward.journalMemo) &&
                Objects.equals(documentIncluded, inInward.documentIncluded) &&
                Objects.equals(employeeId, inInward.employeeId) &&
                Objects.equals(totalAmountFinance, inInward.totalAmountFinance) &&
                Objects.equals(branchId, inInward.branchId) &&
                Objects.equals(saReturnRefId, inInward.saReturnRefId) &&
                Objects.equals(unitPriceMethod, inInward.unitPriceMethod) &&
                Objects.equals(isPostedInventoryBookFinance, inInward.isPostedInventoryBookFinance) &&
                Objects.equals(isPostedInventoryBookManagement, inInward.isPostedInventoryBookManagement) &&
                Objects.equals(inventoryPostedDate, inInward.inventoryPostedDate) &&
                Objects.equals(editVersion, inInward.editVersion) &&
                Objects.equals(refOrder, inInward.refOrder) &&
                Objects.equals(createdDate, inInward.createdDate) &&
                Objects.equals(createdBy, inInward.createdBy) &&
                Objects.equals(modifiedDate, inInward.modifiedDate) &&
                Objects.equals(modifiedBy, inInward.modifiedBy) &&
                Objects.equals(customField1, inInward.customField1) &&
                Objects.equals(customField2, inInward.customField2) &&
                Objects.equals(customField3, inInward.customField3) &&
                Objects.equals(customField4, inInward.customField4) &&
                Objects.equals(customField5, inInward.customField5) &&
                Objects.equals(customField6, inInward.customField6) &&
                Objects.equals(customField7, inInward.customField7) &&
                Objects.equals(customField8, inInward.customField8) &&
                Objects.equals(customField9, inInward.customField9) &&
                Objects.equals(customField10, inInward.customField10) &&
                Objects.equals(totalAmountManagement, inInward.totalAmountManagement) &&
                Objects.equals(outwardDependentRefId, inInward.outwardDependentRefId) &&
                Objects.equals(assemblyRefId, inInward.assemblyRefId) &&
                Objects.equals(inRefOrder, inInward.inRefOrder) &&
                Objects.equals(isReturnWithInward, inInward.isReturnWithInward) &&
                Objects.equals(currencyId, inInward.currencyId) &&
                Objects.equals(exchangeRate, inInward.exchangeRate) &&
                Objects.equals(isCreatedSaReturnLastYear, inInward.isCreatedSaReturnLastYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayOnBook, refType, refDate, postedDate, refNoFinance, refNoManagement, isPostedFinance, isPostedManagement, accountObjectId, accountObjectName, contactName, journalMemo, documentIncluded, employeeId, totalAmountFinance, branchId, saReturnRefId, unitPriceMethod, isPostedInventoryBookFinance, isPostedInventoryBookManagement, inventoryPostedDate, editVersion, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, totalAmountManagement, outwardDependentRefId, assemblyRefId, inRefOrder, isReturnWithInward, currencyId, exchangeRate, isCreatedSaReturnLastYear);
    }
}
