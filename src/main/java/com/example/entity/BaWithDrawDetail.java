package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Bảng Detail của chứng từ chi tiền gửi. Bao gồm các loại chi sau:
 * - Chi tiền gửi thông thường
 * - Chi tiền gửi trả nhà cung cấp
 * - Tiền đang chuyển đi
 */
@Entity
public class BaWithDrawDetail {

    private Integer id; // not null
    private Integer refId; // not null. FK
    private String description; // Diễn giải
    private String debitAccount; // Tài khoản nợ
    private String creditAccount; // Tài khoản Có
    private BigDecimal amountOc; // not null. Số tiền
    private BigDecimal amount; // not null. Số tiền quy đổi
    private Integer budgetItemId; // Mục chi
    private Integer projectWorkId; // Công trình/vụ việc
    private Integer jobId; // ĐT tập hợp chi phí
    private Integer orderId; // Đơn hàng
    private Integer contractId; // Hợp đồng bán
    private Integer listItemId; // Mã thống kê
    private Integer organizationUnitId; // Đơn vị
    private Integer expenseItemId; // Khoản mục chi phí
    private Integer debtAgreementId; // Khế ước vay
    private Boolean reasonableCost; // not null. Chi phí có/không hợp lý
    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer accountObjectId; // ID đối tượng
    private Integer puContractId; // Hợp đồng mua
    private BigDecimal cashOutAmountFinance; // not null. Quy đổi theo Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutDiffAmountFinance; // not null. Chênh lệch Tỷ giá xuất quỹ sổ tài chính
    private String cashOutDiffAccountNumberFinance; // Tài khoản xử lý chênh lệch Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutAmountManagement; // not null. Quy đổi theo Tỷ giá xuất quỹ sổ quản trị
    private BigDecimal cashOutDiffAmountManagement; // not null. Chênh lệch Tỷ giá xuất quỹ sổ quản trị
    private String cashOutDiffAccountNumberManagement; // Tài khoản xử lý chênh lệch Tỷ giá xuất quỹ sổ quản trị
    private BigDecimal cashOutExchangeRateFinance; // not null. Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutExchangeRateManagement; // not null. Tỷ giá xuất quỹ sổ tài chính
    private Integer businessType; // Nghiệp vụ
    private Integer puOrderRefId; // Đơn mua hàng

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "BudgetItemID")
    public Integer getBudgetItemId() {
        return budgetItemId;
    }

    public void setBudgetItemId(Integer budgetItemId) {
        this.budgetItemId = budgetItemId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "DebtAgreementID")
    public Integer getDebtAgreementId() {
        return debtAgreementId;
    }

    public void setDebtAgreementId(Integer debtAgreementId) {
        this.debtAgreementId = debtAgreementId;
    }

    @Basic
    @Column(name = "ReasonableCost")
    public Boolean getReasonableCost() {
        return reasonableCost;
    }

    public void setReasonableCost(Boolean reasonableCost) {
        this.reasonableCost = reasonableCost;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "CashOutAmountFinance")
    public BigDecimal getCashOutAmountFinance() {
        return cashOutAmountFinance;
    }

    public void setCashOutAmountFinance(BigDecimal cashOutAmountFinance) {
        this.cashOutAmountFinance = cashOutAmountFinance;
    }

    @Basic
    @Column(name = "CashOutDiffAmountFinance")
    public BigDecimal getCashOutDiffAmountFinance() {
        return cashOutDiffAmountFinance;
    }

    public void setCashOutDiffAmountFinance(BigDecimal cashOutDiffAmountFinance) {
        this.cashOutDiffAmountFinance = cashOutDiffAmountFinance;
    }

    @Basic
    @Column(name = "CashOutDiffAccountNumberFinance")
    public String getCashOutDiffAccountNumberFinance() {
        return cashOutDiffAccountNumberFinance;
    }

    public void setCashOutDiffAccountNumberFinance(String cashOutDiffAccountNumberFinance) {
        this.cashOutDiffAccountNumberFinance = cashOutDiffAccountNumberFinance;
    }

    @Basic
    @Column(name = "CashOutAmountManagement")
    public BigDecimal getCashOutAmountManagement() {
        return cashOutAmountManagement;
    }

    public void setCashOutAmountManagement(BigDecimal cashOutAmountManagement) {
        this.cashOutAmountManagement = cashOutAmountManagement;
    }

    @Basic
    @Column(name = "CashOutDiffAmountManagement")
    public BigDecimal getCashOutDiffAmountManagement() {
        return cashOutDiffAmountManagement;
    }

    public void setCashOutDiffAmountManagement(BigDecimal cashOutDiffAmountManagement) {
        this.cashOutDiffAmountManagement = cashOutDiffAmountManagement;
    }

    @Basic
    @Column(name = "CashOutDiffAccountNumberManagement")
    public String getCashOutDiffAccountNumberManagement() {
        return cashOutDiffAccountNumberManagement;
    }

    public void setCashOutDiffAccountNumberManagement(String cashOutDiffAccountNumberManagement) {
        this.cashOutDiffAccountNumberManagement = cashOutDiffAccountNumberManagement;
    }

    @Basic
    @Column(name = "CashOutExchangeRateFinance")
    public BigDecimal getCashOutExchangeRateFinance() {
        return cashOutExchangeRateFinance;
    }

    public void setCashOutExchangeRateFinance(BigDecimal cashOutExchangeRateFinance) {
        this.cashOutExchangeRateFinance = cashOutExchangeRateFinance;
    }

    @Basic
    @Column(name = "CashOutExchangeRateManagement")
    public BigDecimal getCashOutExchangeRateManagement() {
        return cashOutExchangeRateManagement;
    }

    public void setCashOutExchangeRateManagement(BigDecimal cashOutExchangeRateManagement) {
        this.cashOutExchangeRateManagement = cashOutExchangeRateManagement;
    }

    @Basic
    @Column(name = "BusinessType")
    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    @Basic
    @Column(name = "PUOrderRefID")
    public Integer getPuOrderRefId() {
        return puOrderRefId;
    }

    public void setPuOrderRefId(Integer puOrderRefId) {
        this.puOrderRefId = puOrderRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaWithDrawDetail that = (BaWithDrawDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(budgetItemId, that.budgetItemId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(debtAgreementId, that.debtAgreementId) &&
                Objects.equals(reasonableCost, that.reasonableCost) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(cashOutAmountFinance, that.cashOutAmountFinance) &&
                Objects.equals(cashOutDiffAmountFinance, that.cashOutDiffAmountFinance) &&
                Objects.equals(cashOutDiffAccountNumberFinance, that.cashOutDiffAccountNumberFinance) &&
                Objects.equals(cashOutAmountManagement, that.cashOutAmountManagement) &&
                Objects.equals(cashOutDiffAmountManagement, that.cashOutDiffAmountManagement) &&
                Objects.equals(cashOutDiffAccountNumberManagement, that.cashOutDiffAccountNumberManagement) &&
                Objects.equals(cashOutExchangeRateFinance, that.cashOutExchangeRateFinance) &&
                Objects.equals(cashOutExchangeRateManagement, that.cashOutExchangeRateManagement) &&
                Objects.equals(businessType, that.businessType) &&
                Objects.equals(puOrderRefId, that.puOrderRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, description, debitAccount, creditAccount, amountOc, amount, budgetItemId, projectWorkId, jobId, orderId, contractId, listItemId, organizationUnitId, expenseItemId, debtAgreementId, reasonableCost, sortOrder, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, accountObjectId, puContractId, cashOutAmountFinance, cashOutDiffAmountFinance, cashOutDiffAccountNumberFinance, cashOutAmountManagement, cashOutDiffAmountManagement, cashOutDiffAccountNumberManagement, cashOutExchangeRateFinance, cashOutExchangeRateManagement, businessType, puOrderRefId);
    }
}
