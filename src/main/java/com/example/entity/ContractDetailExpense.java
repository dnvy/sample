package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Dự kiến chi (Chi tiết hợp đồng bán).
 */
@Entity
public class ContractDetailExpense {

    private Integer id; // PK
    private Integer contractId; // FK
    private String description; // Diễn giải
    private BigDecimal rate; // Tỷ lệ %
    private BigDecimal amount; // Số tiền

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expenseDate; // Ngày dự kiến chi

    private Integer expenseItemId; // FK: Khoản mục chi phí
    private Integer organizationUnitId; // Đơn vị
    private Integer sortOrder; // Thứ tự sắp xếp dòng

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Rate")
    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "ExpenseDate")
    public Date getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(Date expenseDate) {
        this.expenseDate = expenseDate;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractDetailExpense that = (ContractDetailExpense) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(rate, that.rate) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(expenseDate, that.expenseDate) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(sortOrder, that.sortOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contractId, description, rate, amount, expenseDate, expenseItemId, organizationUnitId, sortOrder);
    }
}
