package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * BaWithDraw là bảng master của chứng từ chi tiền gửi. Là Master của các bảng detail sau
 *
 * - BAWithDrawDetail
 * - BAWithDrawDetailPurchase
 * - BAWithDrawDetaillFixedAsset
 */
@Entity
@Table(name = "BAWithdraw")
public class BaWithDraw {

    private Integer id; // NOT NULL. PK
    private Integer refType; // NOT NULL. Loại chứng từ (Lấy từ bảng RefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // NOT NULL. Ngày hạch toán

    private String refNoFinance; // Số chứng từ Sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái (Ghi sổ/Chưa ghi sổ)
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi vào sổ quản trị
    private Integer bankAccountId; // Tài khoản ngân hàng
    private String bankName; // Tên ngân hàng đơn vị trả tiền
    private Integer reasonTypeId; // Nội dung thanh toán
    private String journalMemo; // Diễn giải Nội dung thanh toán
    private Integer accountObjectId; // Mã đơn vị nhận tiền
    private String accountObjectName; // Têm đơn vị nhận tiền (Tên đối tượng/Kho bạc nhà nước)
    private String accountObjectAddress; // Địa chỉ đơn vị nhận tiền
    private String accountObjectBankAccount; // Tài khoản đơn vị nhận tiền
    private String accountObjectBankName; // Tên ngân hàng đơn vị nhận tiền
    private String accountObjectContactName; // Người lĩnh tiền
    private String accountObjectContactIdNumber; // Số CMTND của Người lĩnh tiền

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date accountObjectContactIssueDate; // Ngày cấp CMTND Người lĩnh tiền

    private String accountObjectContactIssueBy; // Nơi cấp CMTND Người lĩnh tiền
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Số tiền
    private BigDecimal totalAmount; // NOT NULL. Số tiền= Tổng tiền từ Detail tương ứng với ID (Sum(Amount)) (Quy đổi)
    private Integer branchId; // Mã chi nhánh
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer editVersion; // Ghi nhận phiên bản sửa chứng từ
    private Integer refOrder; // NOT NULL. Số thứ tự chứng từ nhập vào database
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer employeeId;
    private Boolean isImportVat; // NOT NULL.
    private Boolean isSpecialVat; // NOT NULL.
    private Boolean isEnvironmentVat; // NOT NULL.
    private Boolean isVat; // NOT NULL.
    private Boolean isCreateFromEbHistory;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "ReasonTypeID")
    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectBankAccount")
    public String getAccountObjectBankAccount() {
        return accountObjectBankAccount;
    }

    public void setAccountObjectBankAccount(String accountObjectBankAccount) {
        this.accountObjectBankAccount = accountObjectBankAccount;
    }

    @Basic
    @Column(name = "AccountObjectBankName")
    public String getAccountObjectBankName() {
        return accountObjectBankName;
    }

    public void setAccountObjectBankName(String accountObjectBankName) {
        this.accountObjectBankName = accountObjectBankName;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "AccountObjectContactIDNumber")
    public String getAccountObjectContactIdNumber() {
        return accountObjectContactIdNumber;
    }

    public void setAccountObjectContactIdNumber(String accountObjectContactIdNumber) {
        this.accountObjectContactIdNumber = accountObjectContactIdNumber;
    }

    @Basic
    @Column(name = "AccountObjectContactIssueDate")
    public Date getAccountObjectContactIssueDate() {
        return accountObjectContactIssueDate;
    }

    public void setAccountObjectContactIssueDate(Date accountObjectContactIssueDate) {
        this.accountObjectContactIssueDate = accountObjectContactIssueDate;
    }

    @Basic
    @Column(name = "AccountObjectContactIssueBy")
    public String getAccountObjectContactIssueBy() {
        return accountObjectContactIssueBy;
    }

    public void setAccountObjectContactIssueBy(String accountObjectContactIssueBy) {
        this.accountObjectContactIssueBy = accountObjectContactIssueBy;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "IsImportVAT")
    public Boolean getIsImportVat() {
        return isImportVat;
    }

    public void setIsImportVat(Boolean importVat) {
        isImportVat = importVat;
    }

    @Basic
    @Column(name = "IsSpecialVAT")
    public Boolean getIsSpecialVat() {
        return isSpecialVat;
    }

    public void setIsSpecialVat(Boolean specialVat) {
        isSpecialVat = specialVat;
    }

    @Basic
    @Column(name = "IsEnvironmentVAT")
    public Boolean getIsEnvironmentVat() {
        return isEnvironmentVat;
    }

    public void setIsEnvironmentVat(Boolean environmentVat) {
        isEnvironmentVat = environmentVat;
    }

    @Basic
    @Column(name = "IsVAT")
    public Boolean getIsVat() {
        return isVat;
    }

    public void setIsVat(Boolean vat) {
        isVat = vat;
    }

    @Basic
    @Column(name = "IsCreateFromEBHistory")
    public Boolean getIsCreateFromEbHistory() {
        return isCreateFromEbHistory;
    }

    public void setIsCreateFromEbHistory(Boolean createFromEbHistory) {
        isCreateFromEbHistory = createFromEbHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaWithDraw that = (BaWithDraw) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(refNoFinance, that.refNoFinance) &&
                Objects.equals(refNoManagement, that.refNoManagement) &&
                Objects.equals(isPostedFinance, that.isPostedFinance) &&
                Objects.equals(isPostedManagement, that.isPostedManagement) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(reasonTypeId, that.reasonTypeId) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectBankAccount, that.accountObjectBankAccount) &&
                Objects.equals(accountObjectBankName, that.accountObjectBankName) &&
                Objects.equals(accountObjectContactName, that.accountObjectContactName) &&
                Objects.equals(accountObjectContactIdNumber, that.accountObjectContactIdNumber) &&
                Objects.equals(accountObjectContactIssueDate, that.accountObjectContactIssueDate) &&
                Objects.equals(accountObjectContactIssueBy, that.accountObjectContactIssueBy) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(totalAmountOc, that.totalAmountOc) &&
                Objects.equals(totalAmount, that.totalAmount) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(displayOnBook, that.displayOnBook) &&
                Objects.equals(editVersion, that.editVersion) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(isImportVat, that.isImportVat) &&
                Objects.equals(isSpecialVat, that.isSpecialVat) &&
                Objects.equals(isEnvironmentVat, that.isEnvironmentVat) &&
                Objects.equals(isVat, that.isVat) &&
                Objects.equals(isCreateFromEbHistory, that.isCreateFromEbHistory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, refDate, postedDate, refNoFinance, refNoManagement, isPostedFinance, isPostedManagement, bankAccountId, bankName, reasonTypeId, journalMemo, accountObjectId, accountObjectName, accountObjectAddress, accountObjectBankAccount, accountObjectBankName, accountObjectContactName, accountObjectContactIdNumber, accountObjectContactIssueDate, accountObjectContactIssueBy, currencyId, exchangeRate, totalAmountOc, totalAmount, branchId, displayOnBook, editVersion, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, employeeId, isImportVat, isSpecialVat, isEnvironmentVat, isVat, isCreateFromEbHistory);
    }
}
