package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "CAReceipt")
public class CAReceipt {

    private Integer id; // NOT NULL. PK
    private Integer refType; // NOT NULL. Loại chứng từ (lấy từ bảng RefType). Từ màn hình Phiếu thu, refType = 1010.

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày phiếu thu

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // NOT NULL. Ngày hạch toán

    private String refNoFinance; // Số chứng từ sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái ghi sổ Sổ tài chính
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi sổ Sổ quản trị
    private Integer accountObjectId; // NOT NULL. Mã đối tượng (Người nộp, khách hàng)
    private String accountObjectName; // Tên đối tượng
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectContactName; // Người nộp
    private Integer reasonTypeId; // Lý do thu
    private String journalMemo; // Diễn giải Lý do nộp
    private String documentIncluded; // Kèm theo
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Tổng tiền
    private BigDecimal totalAmount; // NOT NULL. Tổng tiền quy đổi
    private Integer branchId; // ID của chi nhánh.
    private Integer employeeId; // Nhân viên (đại diện mua, bán hàng)
    private Integer displayOnBook; // NOT NULL. Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Boolean isPostedCashBookFinance; // Trạng thái ghi sổ quỹ (Sổ tài chính)
    private Boolean isPostedCashBookManagement; // Trạng thái ghi sổ quỹ (Sổ quản trị)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate; // Ngày ghi sổ quỹ

    private Integer editVersion; // Phiên bản sửa chứng từ
    private Integer refOrder; // NOT NULL. Số thứ tự chứng từ nhập vào database
    private Timestamp createdDate; // Ngày lập chứng từ
    private String createdBy; // Người lập chứng từ
    private Timestamp modifiedDate; // Ngày sửa chứng từ lần cuối
    private String modifiedBy; // Người sửa chứng từ lần cuối
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer glVoucherRefId; // Như vậy, phải ghi vào table GLVoucher trước khi lưu vào CAReceipt.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "RefNoFinance")
    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    @Basic
    @Column(name = "RefNoManagement")
    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    @Basic
    @Column(name = "IsPostedFinance")
    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    @Basic
    @Column(name = "IsPostedManagement")
    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "ReasonTypeID")
    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "DocumentIncluded")
    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "IsPostedCashBookFinance")
    public Boolean getIsPostedCashBookFinance() {
        return isPostedCashBookFinance;
    }

    public void setIsPostedCashBookFinance(Boolean postedCashBookFinance) {
        isPostedCashBookFinance = postedCashBookFinance;
    }

    @Basic
    @Column(name = "IsPostedCashBookManagement")
    public Boolean getIsPostedCashBookManagement() {
        return isPostedCashBookManagement;
    }

    public void setIsPostedCashBookManagement(Boolean postedCashBookManagement) {
        isPostedCashBookManagement = postedCashBookManagement;
    }

    @Basic
    @Column(name = "CashBookPostedDate")
    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "GLVoucherRefID")
    public Integer getGlVoucherRefId() {
        return glVoucherRefId;
    }

    public void setGlVoucherRefId(Integer glVoucherRefId) {
        this.glVoucherRefId = glVoucherRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CAReceipt caReceipt = (CAReceipt) o;
        return Objects.equals(id, caReceipt.id) &&
                Objects.equals(refType, caReceipt.refType) &&
                Objects.equals(refDate, caReceipt.refDate) &&
                Objects.equals(postedDate, caReceipt.postedDate) &&
                Objects.equals(refNoFinance, caReceipt.refNoFinance) &&
                Objects.equals(refNoManagement, caReceipt.refNoManagement) &&
                Objects.equals(isPostedFinance, caReceipt.isPostedFinance) &&
                Objects.equals(isPostedManagement, caReceipt.isPostedManagement) &&
                Objects.equals(accountObjectId, caReceipt.accountObjectId) &&
                Objects.equals(accountObjectName, caReceipt.accountObjectName) &&
                Objects.equals(accountObjectAddress, caReceipt.accountObjectAddress) &&
                Objects.equals(accountObjectContactName, caReceipt.accountObjectContactName) &&
                Objects.equals(reasonTypeId, caReceipt.reasonTypeId) &&
                Objects.equals(journalMemo, caReceipt.journalMemo) &&
                Objects.equals(documentIncluded, caReceipt.documentIncluded) &&
                Objects.equals(currencyId, caReceipt.currencyId) &&
                Objects.equals(exchangeRate, caReceipt.exchangeRate) &&
                Objects.equals(totalAmountOc, caReceipt.totalAmountOc) &&
                Objects.equals(totalAmount, caReceipt.totalAmount) &&
                Objects.equals(branchId, caReceipt.branchId) &&
                Objects.equals(employeeId, caReceipt.employeeId) &&
                Objects.equals(displayOnBook, caReceipt.displayOnBook) &&
                Objects.equals(isPostedCashBookFinance, caReceipt.isPostedCashBookFinance) &&
                Objects.equals(isPostedCashBookManagement, caReceipt.isPostedCashBookManagement) &&
                Objects.equals(cashBookPostedDate, caReceipt.cashBookPostedDate) &&
                Objects.equals(editVersion, caReceipt.editVersion) &&
                Objects.equals(refOrder, caReceipt.refOrder) &&
                Objects.equals(createdDate, caReceipt.createdDate) &&
                Objects.equals(createdBy, caReceipt.createdBy) &&
                Objects.equals(modifiedDate, caReceipt.modifiedDate) &&
                Objects.equals(modifiedBy, caReceipt.modifiedBy) &&
                Objects.equals(customField1, caReceipt.customField1) &&
                Objects.equals(customField2, caReceipt.customField2) &&
                Objects.equals(customField3, caReceipt.customField3) &&
                Objects.equals(customField4, caReceipt.customField4) &&
                Objects.equals(customField5, caReceipt.customField5) &&
                Objects.equals(customField6, caReceipt.customField6) &&
                Objects.equals(customField7, caReceipt.customField7) &&
                Objects.equals(customField8, caReceipt.customField8) &&
                Objects.equals(customField9, caReceipt.customField9) &&
                Objects.equals(customField10, caReceipt.customField10) &&
                Objects.equals(glVoucherRefId, caReceipt.glVoucherRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, refDate, postedDate, refNoFinance, refNoManagement, isPostedFinance, isPostedManagement, accountObjectId, accountObjectName, accountObjectAddress, accountObjectContactName, reasonTypeId, journalMemo, documentIncluded, currencyId, exchangeRate, totalAmountOc, totalAmount, branchId, employeeId, displayOnBook, isPostedCashBookFinance, isPostedCashBookManagement, cashBookPostedDate, editVersion, refOrder, createdDate, createdBy, modifiedDate, modifiedBy, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, glVoucherRefId);
    }
}
