package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * (Bảng chi tiết)Chứng từ nghiệp vụ khác, Hạch toán chi phí lương.
 */
@Entity
public class GlVoucherDetail {

    private Integer id; // PK
    private Integer refId; // NOT NULL. FK
    private String description; // Diễn giải
    private String debitAccount; // TK Nợ
    private String creditAccount; // TK Có
    private BigDecimal amountOc; // NOT NULL. Số tiền
    private BigDecimal amount; // NOT NULL. Số tiền quy đổi/Số chênh lệch trên Tỷ giá xuất quỹ
    private BigDecimal vatRate; // % thuế GTGT
    private BigDecimal vatAmount; // NOT NULL. Tiền thuế GTGT quy đổi
    private String vatAccount; // TK thuế GTGT
    private Integer bankAccountId; // Tài khoản ngân hàng
    private Integer jobId; // Đối tượng THCP
    private Integer puContractId; // Hợp đồng mua
    private Integer contractId; // Hợp đồng
    private Integer accountObjectId; // Đối tượng Nợ
    private Integer creditAccountObjectId; // Đối tượng Có
    private Integer listItemId; // Mã thống kê
    private Integer organizationUnitId; // Đơn vị
    private Integer expenseItemId; // Khoản mục chi phí
    private Integer projectWorkId; // Công trình/Dự án
    private Integer orderId; // Đơn hàng
    private Boolean unResonableCost; // NOT NULL. Chi phí không hợp lý
    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private Integer cashOutCurrencyId; // Loại tiền tỷ giá xuất quỹ
    private BigDecimal cashOutAmountOc; // Số tiền đã xuất quỹ
    private BigDecimal cashOutAmountOrg; // Số tiền Quy đổi đã xuất quỹ (theo chứng từ gốc)
    private BigDecimal cashOutExchangeRate; // Tỷ giá xuất quỹ bình quân
    private BigDecimal cashOutAmount; // Số tiền Quy đổi theo tỷ giá xuất quỹ
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer budgetItemId; // Mục chi/thu
    private BigDecimal vatAmountOc; // NOT NULL. Tiền thuế GTGT
    private Boolean isListOnTaxDeclaration; // NOT NULL. Kê lên tờ khai
    private String invTemplateNo;
    private String invSeries;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate;

    private String invNo;
    private Integer purchasePurposeId;
    private Integer taxAccountObjectId;
    private String taxAccountObjectName;
    private String taxAccountObjectTaxCode;
    private String taxAccountObjectAddress;
    private String vatDescription;
    private Integer employeeId;
    private BigDecimal cashOutAmountFinance; // NOT NULL. Quy đổi theo Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutDiffAmountFinance; // NOT NULL. Chênh lệch Tỷ giá xuất quỹ sổ tài chính
    private String cashOutDiffAccountNumberFinance; // Tài khoản xử lý chênh lệch Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutAmountManagement; // NOT NULL. Quy đổi theo Tỷ giá xuất quỹ sổ quản trị
    private BigDecimal cashOutDiffAmountManagement; // NOT NULL. Chênh lệch Tỷ giá xuất quỹ sổ quản trị
    private String cashOutDiffAccountNumberManagement; // Tài khoản xử lý chênh lệch Tỷ giá xuất quỹ sổ quản trị
    private BigDecimal cashOutExchangeRateFinance; // NOT NULL. Tỷ giá xuất quỹ sổ tài chính
    private BigDecimal cashOutExchangeRateManagement; // NOT NULL. Tỷ giá xuất quỹ sổ quản trị
    private String debitBankAccount; // Số TK ngân hàng của đối tượng Nợ
    private String debitBankName; // Tên ngân hàng của đối tượng Nợ
    private BigDecimal lastExchangeRate; // NOT NULL.
    private Boolean notIncludeInvoice; // NOT NULL. Không có hóa đơn
    private Integer businessType;
    private Integer puOrderRefId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "VATAccount")
    public String getVatAccount() {
        return vatAccount;
    }

    public void setVatAccount(String vatAccount) {
        this.vatAccount = vatAccount;
    }

    @Basic
    @Column(name = "BankAccountID")
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "CreditAccountObjectID")
    public Integer getCreditAccountObjectId() {
        return creditAccountObjectId;
    }

    public void setCreditAccountObjectId(Integer creditAccountObjectId) {
        this.creditAccountObjectId = creditAccountObjectId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "UnResonableCost")
    public Boolean getUnResonableCost() {
        return unResonableCost;
    }

    public void setUnResonableCost(Boolean unResonableCost) {
        this.unResonableCost = unResonableCost;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CashOutCurrencyID")
    public Integer getCashOutCurrencyId() {
        return cashOutCurrencyId;
    }

    public void setCashOutCurrencyId(Integer cashOutCurrencyId) {
        this.cashOutCurrencyId = cashOutCurrencyId;
    }

    @Basic
    @Column(name = "CashOutAmountOC")
    public BigDecimal getCashOutAmountOc() {
        return cashOutAmountOc;
    }

    public void setCashOutAmountOc(BigDecimal cashOutAmountOc) {
        this.cashOutAmountOc = cashOutAmountOc;
    }

    @Basic
    @Column(name = "CashOutAmountOrg")
    public BigDecimal getCashOutAmountOrg() {
        return cashOutAmountOrg;
    }

    public void setCashOutAmountOrg(BigDecimal cashOutAmountOrg) {
        this.cashOutAmountOrg = cashOutAmountOrg;
    }

    @Basic
    @Column(name = "CashOutExchangeRate")
    public BigDecimal getCashOutExchangeRate() {
        return cashOutExchangeRate;
    }

    public void setCashOutExchangeRate(BigDecimal cashOutExchangeRate) {
        this.cashOutExchangeRate = cashOutExchangeRate;
    }

    @Basic
    @Column(name = "CashOutAmount")
    public BigDecimal getCashOutAmount() {
        return cashOutAmount;
    }

    public void setCashOutAmount(BigDecimal cashOutAmount) {
        this.cashOutAmount = cashOutAmount;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "BudgetItemID")
    public Integer getBudgetItemId() {
        return budgetItemId;
    }

    public void setBudgetItemId(Integer budgetItemId) {
        this.budgetItemId = budgetItemId;
    }

    @Basic
    @Column(name = "VATAmountOC")
    public BigDecimal getVatAmountOc() {
        return vatAmountOc;
    }

    public void setVatAmountOc(BigDecimal vatAmountOc) {
        this.vatAmountOc = vatAmountOc;
    }

    @Basic
    @Column(name = "IsListOnTaxDeclaration")
    public Boolean getIsListOnTaxDeclaration() {
        return isListOnTaxDeclaration;
    }

    public void setIsListOnTaxDeclaration(Boolean listOnTaxDeclaration) {
        isListOnTaxDeclaration = listOnTaxDeclaration;
    }

    @Basic
    @Column(name = "InvTemplateNo")
    public String getInvTemplateNo() {
        return invTemplateNo;
    }

    public void setInvTemplateNo(String invTemplateNo) {
        this.invTemplateNo = invTemplateNo;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "PurchasePurposeID")
    public Integer getPurchasePurposeId() {
        return purchasePurposeId;
    }

    public void setPurchasePurposeId(Integer purchasePurposeId) {
        this.purchasePurposeId = purchasePurposeId;
    }

    @Basic
    @Column(name = "TaxAccountObjectID")
    public Integer getTaxAccountObjectId() {
        return taxAccountObjectId;
    }

    public void setTaxAccountObjectId(Integer taxAccountObjectId) {
        this.taxAccountObjectId = taxAccountObjectId;
    }

    @Basic
    @Column(name = "TaxAccountObjectName")
    public String getTaxAccountObjectName() {
        return taxAccountObjectName;
    }

    public void setTaxAccountObjectName(String taxAccountObjectName) {
        this.taxAccountObjectName = taxAccountObjectName;
    }

    @Basic
    @Column(name = "TaxAccountObjectTaxCode")
    public String getTaxAccountObjectTaxCode() {
        return taxAccountObjectTaxCode;
    }

    public void setTaxAccountObjectTaxCode(String taxAccountObjectTaxCode) {
        this.taxAccountObjectTaxCode = taxAccountObjectTaxCode;
    }

    @Basic
    @Column(name = "TaxAccountObjectAddress")
    public String getTaxAccountObjectAddress() {
        return taxAccountObjectAddress;
    }

    public void setTaxAccountObjectAddress(String taxAccountObjectAddress) {
        this.taxAccountObjectAddress = taxAccountObjectAddress;
    }

    @Basic
    @Column(name = "VATDescription")
    public String getVatDescription() {
        return vatDescription;
    }

    public void setVatDescription(String vatDescription) {
        this.vatDescription = vatDescription;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "CashOutAmountFinance")
    public BigDecimal getCashOutAmountFinance() {
        return cashOutAmountFinance;
    }

    public void setCashOutAmountFinance(BigDecimal cashOutAmountFinance) {
        this.cashOutAmountFinance = cashOutAmountFinance;
    }

    @Basic
    @Column(name = "CashOutDiffAmountFinance")
    public BigDecimal getCashOutDiffAmountFinance() {
        return cashOutDiffAmountFinance;
    }

    public void setCashOutDiffAmountFinance(BigDecimal cashOutDiffAmountFinance) {
        this.cashOutDiffAmountFinance = cashOutDiffAmountFinance;
    }

    @Basic
    @Column(name = "CashOutDiffAccountNumberFinance")
    public String getCashOutDiffAccountNumberFinance() {
        return cashOutDiffAccountNumberFinance;
    }

    public void setCashOutDiffAccountNumberFinance(String cashOutDiffAccountNumberFinance) {
        this.cashOutDiffAccountNumberFinance = cashOutDiffAccountNumberFinance;
    }

    @Basic
    @Column(name = "CashOutAmountManagement")
    public BigDecimal getCashOutAmountManagement() {
        return cashOutAmountManagement;
    }

    public void setCashOutAmountManagement(BigDecimal cashOutAmountManagement) {
        this.cashOutAmountManagement = cashOutAmountManagement;
    }

    @Basic
    @Column(name = "CashOutDiffAmountManagement")
    public BigDecimal getCashOutDiffAmountManagement() {
        return cashOutDiffAmountManagement;
    }

    public void setCashOutDiffAmountManagement(BigDecimal cashOutDiffAmountManagement) {
        this.cashOutDiffAmountManagement = cashOutDiffAmountManagement;
    }

    @Basic
    @Column(name = "CashOutDiffAccountNumberManagement")
    public String getCashOutDiffAccountNumberManagement() {
        return cashOutDiffAccountNumberManagement;
    }

    public void setCashOutDiffAccountNumberManagement(String cashOutDiffAccountNumberManagement) {
        this.cashOutDiffAccountNumberManagement = cashOutDiffAccountNumberManagement;
    }

    @Basic
    @Column(name = "CashOutExchangeRateFinance")
    public BigDecimal getCashOutExchangeRateFinance() {
        return cashOutExchangeRateFinance;
    }

    public void setCashOutExchangeRateFinance(BigDecimal cashOutExchangeRateFinance) {
        this.cashOutExchangeRateFinance = cashOutExchangeRateFinance;
    }

    @Basic
    @Column(name = "CashOutExchangeRateManagement")
    public BigDecimal getCashOutExchangeRateManagement() {
        return cashOutExchangeRateManagement;
    }

    public void setCashOutExchangeRateManagement(BigDecimal cashOutExchangeRateManagement) {
        this.cashOutExchangeRateManagement = cashOutExchangeRateManagement;
    }

    @Basic
    @Column(name = "DebitBankAccount")
    public String getDebitBankAccount() {
        return debitBankAccount;
    }

    public void setDebitBankAccount(String debitBankAccount) {
        this.debitBankAccount = debitBankAccount;
    }

    @Basic
    @Column(name = "DebitBankName")
    public String getDebitBankName() {
        return debitBankName;
    }

    public void setDebitBankName(String debitBankName) {
        this.debitBankName = debitBankName;
    }

    @Basic
    @Column(name = "LastExchangeRate")
    public BigDecimal getLastExchangeRate() {
        return lastExchangeRate;
    }

    public void setLastExchangeRate(BigDecimal lastExchangeRate) {
        this.lastExchangeRate = lastExchangeRate;
    }

    @Basic
    @Column(name = "NotIncludeInvoice")
    public Boolean getNotIncludeInvoice() {
        return notIncludeInvoice;
    }

    public void setNotIncludeInvoice(Boolean notIncludeInvoice) {
        this.notIncludeInvoice = notIncludeInvoice;
    }

    @Basic
    @Column(name = "BusinessType")
    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    @Basic
    @Column(name = "PUOrderRefID")
    public Integer getPuOrderRefId() {
        return puOrderRefId;
    }

    public void setPuOrderRefId(Integer puOrderRefId) {
        this.puOrderRefId = puOrderRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GlVoucherDetail that = (GlVoucherDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(vatAccount, that.vatAccount) &&
                Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(creditAccountObjectId, that.creditAccountObjectId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(unResonableCost, that.unResonableCost) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(cashOutCurrencyId, that.cashOutCurrencyId) &&
                Objects.equals(cashOutAmountOc, that.cashOutAmountOc) &&
                Objects.equals(cashOutAmountOrg, that.cashOutAmountOrg) &&
                Objects.equals(cashOutExchangeRate, that.cashOutExchangeRate) &&
                Objects.equals(cashOutAmount, that.cashOutAmount) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10) &&
                Objects.equals(budgetItemId, that.budgetItemId) &&
                Objects.equals(vatAmountOc, that.vatAmountOc) &&
                Objects.equals(isListOnTaxDeclaration, that.isListOnTaxDeclaration) &&
                Objects.equals(invTemplateNo, that.invTemplateNo) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(purchasePurposeId, that.purchasePurposeId) &&
                Objects.equals(taxAccountObjectId, that.taxAccountObjectId) &&
                Objects.equals(taxAccountObjectName, that.taxAccountObjectName) &&
                Objects.equals(taxAccountObjectTaxCode, that.taxAccountObjectTaxCode) &&
                Objects.equals(taxAccountObjectAddress, that.taxAccountObjectAddress) &&
                Objects.equals(vatDescription, that.vatDescription) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(cashOutAmountFinance, that.cashOutAmountFinance) &&
                Objects.equals(cashOutDiffAmountFinance, that.cashOutDiffAmountFinance) &&
                Objects.equals(cashOutDiffAccountNumberFinance, that.cashOutDiffAccountNumberFinance) &&
                Objects.equals(cashOutAmountManagement, that.cashOutAmountManagement) &&
                Objects.equals(cashOutDiffAmountManagement, that.cashOutDiffAmountManagement) &&
                Objects.equals(cashOutDiffAccountNumberManagement, that.cashOutDiffAccountNumberManagement) &&
                Objects.equals(cashOutExchangeRateFinance, that.cashOutExchangeRateFinance) &&
                Objects.equals(cashOutExchangeRateManagement, that.cashOutExchangeRateManagement) &&
                Objects.equals(debitBankAccount, that.debitBankAccount) &&
                Objects.equals(debitBankName, that.debitBankName) &&
                Objects.equals(lastExchangeRate, that.lastExchangeRate) &&
                Objects.equals(notIncludeInvoice, that.notIncludeInvoice) &&
                Objects.equals(businessType, that.businessType) &&
                Objects.equals(puOrderRefId, that.puOrderRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, description, debitAccount, creditAccount, amountOc, amount, vatRate, vatAmount, vatAccount, bankAccountId, jobId, puContractId, contractId, accountObjectId, creditAccountObjectId, listItemId, organizationUnitId, expenseItemId, projectWorkId, orderId, unResonableCost, sortOrder, cashOutCurrencyId, cashOutAmountOc, cashOutAmountOrg, cashOutExchangeRate, cashOutAmount, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, budgetItemId, vatAmountOc, isListOnTaxDeclaration, invTemplateNo, invSeries, invDate, invNo, purchasePurposeId, taxAccountObjectId, taxAccountObjectName, taxAccountObjectTaxCode, taxAccountObjectAddress, vatDescription, employeeId, cashOutAmountFinance, cashOutDiffAmountFinance, cashOutDiffAccountNumberFinance, cashOutAmountManagement, cashOutDiffAmountManagement, cashOutDiffAccountNumberManagement, cashOutExchangeRateFinance, cashOutExchangeRateManagement, debitBankAccount, debitBankName, lastExchangeRate, notIncludeInvoice, businessType, puOrderRefId);
    }
}
