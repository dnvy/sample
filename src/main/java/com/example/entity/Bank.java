package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Ngân hàng. http://localhost:8080/banks
 */
@Entity
public class Bank {

    private Integer id; // NOT NULL.PK
    private String bankCode; // NOT NULL. Tên viết tắt
    private String bankName; // Chi nhánh. Ví dụ, chi nhánh: Hà Nội
    private String bankNameEnglish;
    private String address;
    private String description; // Diễn giải
    private String icon; // Logo
    private Boolean activeStatus; // Trạng thái theo dõi
    private String createdBy;
    private Timestamp createdDate;
    private String modifiedBy;
    private Timestamp modifiedDate;
    private String eBankCode;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BankCode")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "BankNameEnglish")
    public String getBankNameEnglish() {
        return bankNameEnglish;
    }

    public void setBankNameEnglish(String bankNameEnglish) {
        this.bankNameEnglish = bankNameEnglish;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Icon")
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "EBankCode")
    public String geteBankCode() {
        return eBankCode;
    }

    public void seteBankCode(String eBankCode) {
        this.eBankCode = eBankCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return Objects.equals(id, bank.id) &&
                Objects.equals(bankCode, bank.bankCode) &&
                Objects.equals(bankName, bank.bankName) &&
                Objects.equals(bankNameEnglish, bank.bankNameEnglish) &&
                Objects.equals(address, bank.address) &&
                Objects.equals(description, bank.description) &&
                Objects.equals(icon, bank.icon) &&
                Objects.equals(activeStatus, bank.activeStatus) &&
                Objects.equals(createdBy, bank.createdBy) &&
                Objects.equals(createdDate, bank.createdDate) &&
                Objects.equals(modifiedBy, bank.modifiedBy) &&
                Objects.equals(modifiedDate, bank.modifiedDate) &&
                Objects.equals(eBankCode, bank.eBankCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bankCode, bankName, bankNameEnglish, address, description, icon, activeStatus, createdBy, createdDate, modifiedBy, modifiedDate, eBankCode);
    }
}
