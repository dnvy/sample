package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Loại của Đơn vị, phòng, ban.
 */
@Entity
public class OrganizationUnitType {

    private Integer id;
    private String organizationUnitTypeName;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "OrganizationUnitTypeName")
    public String getOrganizationUnitTypeName() {
        return organizationUnitTypeName;
    }

    public void setOrganizationUnitTypeName(String organizationUnitTypeName) {
        this.organizationUnitTypeName = organizationUnitTypeName;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationUnitType that = (OrganizationUnitType) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(organizationUnitTypeName, that.organizationUnitTypeName) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, organizationUnitTypeName, description);
    }
}
