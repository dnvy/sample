package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Tài khoản ngầm định.
 */
@Entity
public class AccountDefault {

    private Integer id; // Mã tài khoản ngầm định
    private Integer refType; // FK - Mã loại chứng từ . FK table VoucherType
    private String refTypeName; // Tên loại chứng từ
    private Integer voucherType; // Loại thu, chi trên Thu, chi tiền mặt, tiền gửi
    private String columnName; // Tên cột tài khoản. VD: DebitAccount, ImportTaxAccount, ...
    private String columnCaption; // Caption của tên cột. VD: Tài khoản nợ, Tài khoản thuế nhập khẩu
    private String filterCondition; // Điều kiện lọc TK. VD: 152; 153; 156

    // FK
    private String defaultValue; // TK ngầm định. VD: 1561

    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "VoucherType")
    public Integer getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(Integer voucherType) {
        this.voucherType = voucherType;
    }

    @Basic
    @Column(name = "ColumnName")
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Basic
    @Column(name = "ColumnCaption")
    public String getColumnCaption() {
        return columnCaption;
    }

    public void setColumnCaption(String columnCaption) {
        this.columnCaption = columnCaption;
    }

    @Basic
    @Column(name = "FilterCondition")
    public String getFilterCondition() {
        return filterCondition;
    }

    public void setFilterCondition(String filterCondition) {
        this.filterCondition = filterCondition;
    }

    @Basic
    @Column(name = "DefaultValue")
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDefault that = (AccountDefault) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(voucherType, that.voucherType) &&
                Objects.equals(columnName, that.columnName) &&
                Objects.equals(columnCaption, that.columnCaption) &&
                Objects.equals(filterCondition, that.filterCondition) &&
                Objects.equals(defaultValue, that.defaultValue) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refType, refTypeName, voucherType, columnName, columnCaption, filterCondition, defaultValue, sortOrder, createdDate, createdBy, modifiedDate, modifiedBy);
    }
}
