package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "GLVoucherCrossEntryDetail")
public class GlVoucherCrossEntryDetail {

    private Integer id;
    private Integer mappingCrossId;
    private Integer glVoucherRefId;
    private Integer crossType;
    private Boolean receiptType;
    private String accountNumber;
    private Integer currencyId;
    private Integer accountObjectId;
    private Integer payRefId;
    private Boolean isPayVoucherPosted;
    private Integer payRefType;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date payRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date payPostedDate;

    private String payRefNo;
    private String payDescription;
    private BigDecimal totalPayableAmountOc;
    private BigDecimal totalPayableAmount;
    private BigDecimal payableAmountOc;
    private BigDecimal payableAmount;
    private BigDecimal payAmountOc;
    private BigDecimal payAmount;
    private Integer debtRefId;
    private Boolean isDebtVoucherPosted;
    private Integer debtRefType;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date debtRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date debtPostedDate;

    private String debtRefNo;
    private String debtInvNo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date debtDueDate;

    private String debtDescription;
    private BigDecimal totalDebtableAmountOc;
    private BigDecimal totalDebtableAmount;
    private BigDecimal debtableAmountOc;
    private BigDecimal debtableAmount;
    private BigDecimal debtAmountOc;
    private BigDecimal debtAmount;
    private BigDecimal discountRate;
    private BigDecimal discountAmountOc;
    private BigDecimal discountAmount;
    private String discountAccount;
    private Integer paymentTermId;
    private BigDecimal exchangeDiffAmount;
    private Integer sortOrder;
    private Integer branchId;
    private Integer displayOnBook;
    private BigDecimal debtExchangeRate;
    private BigDecimal lastExchangeRate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date debtInvDate;

    private Integer debtEmployeeId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MappingCrossID")
    public Integer getMappingCrossId() {
        return mappingCrossId;
    }

    public void setMappingCrossId(Integer mappingCrossId) {
        this.mappingCrossId = mappingCrossId;
    }

    @Basic
    @Column(name = "GLVoucherRefID")
    public Integer getGlVoucherRefId() {
        return glVoucherRefId;
    }

    public void setGlVoucherRefId(Integer glVoucherRefId) {
        this.glVoucherRefId = glVoucherRefId;
    }

    @Basic
    @Column(name = "CrossType")
    public Integer getCrossType() {
        return crossType;
    }

    public void setCrossType(Integer crossType) {
        this.crossType = crossType;
    }

    @Basic
    @Column(name = "ReceiptType")
    public Boolean getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(Boolean receiptType) {
        this.receiptType = receiptType;
    }

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "PayRefID")
    public Integer getPayRefId() {
        return payRefId;
    }

    public void setPayRefId(Integer payRefId) {
        this.payRefId = payRefId;
    }

    @Basic
    @Column(name = "IsPayVoucherPosted")
    public Boolean getIsPayVoucherPosted() {
        return isPayVoucherPosted;
    }

    public void setIsPayVoucherPosted(Boolean payVoucherPosted) {
        isPayVoucherPosted = payVoucherPosted;
    }

    @Basic
    @Column(name = "PayRefType")
    public Integer getPayRefType() {
        return payRefType;
    }

    public void setPayRefType(Integer payRefType) {
        this.payRefType = payRefType;
    }

    @Basic
    @Column(name = "PayRefDate")
    public Date getPayRefDate() {
        return payRefDate;
    }

    public void setPayRefDate(Date payRefDate) {
        this.payRefDate = payRefDate;
    }

    @Basic
    @Column(name = "PayPostedDate")
    public Date getPayPostedDate() {
        return payPostedDate;
    }

    public void setPayPostedDate(Date payPostedDate) {
        this.payPostedDate = payPostedDate;
    }

    @Basic
    @Column(name = "PayRefNo")
    public String getPayRefNo() {
        return payRefNo;
    }

    public void setPayRefNo(String payRefNo) {
        this.payRefNo = payRefNo;
    }

    @Basic
    @Column(name = "PayDescription")
    public String getPayDescription() {
        return payDescription;
    }

    public void setPayDescription(String payDescription) {
        this.payDescription = payDescription;
    }

    @Basic
    @Column(name = "TotalPayableAmountOC")
    public BigDecimal getTotalPayableAmountOc() {
        return totalPayableAmountOc;
    }

    public void setTotalPayableAmountOc(BigDecimal totalPayableAmountOc) {
        this.totalPayableAmountOc = totalPayableAmountOc;
    }

    @Basic
    @Column(name = "TotalPayableAmount")
    public BigDecimal getTotalPayableAmount() {
        return totalPayableAmount;
    }

    public void setTotalPayableAmount(BigDecimal totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

    @Basic
    @Column(name = "PayableAmountOC")
    public BigDecimal getPayableAmountOc() {
        return payableAmountOc;
    }

    public void setPayableAmountOc(BigDecimal payableAmountOc) {
        this.payableAmountOc = payableAmountOc;
    }

    @Basic
    @Column(name = "PayableAmount")
    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    @Basic
    @Column(name = "PayAmountOC")
    public BigDecimal getPayAmountOc() {
        return payAmountOc;
    }

    public void setPayAmountOc(BigDecimal payAmountOc) {
        this.payAmountOc = payAmountOc;
    }

    @Basic
    @Column(name = "PayAmount")
    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    @Basic
    @Column(name = "DebtRefID")
    public Integer getDebtRefId() {
        return debtRefId;
    }

    public void setDebtRefId(Integer debtRefId) {
        this.debtRefId = debtRefId;
    }

    @Basic
    @Column(name = "IsDebtVoucherPosted")
    public Boolean getIsDebtVoucherPosted() {
        return isDebtVoucherPosted;
    }

    public void setIsDebtVoucherPosted(Boolean debtVoucherPosted) {
        isDebtVoucherPosted = debtVoucherPosted;
    }

    @Basic
    @Column(name = "DebtRefType")
    public Integer getDebtRefType() {
        return debtRefType;
    }

    public void setDebtRefType(Integer debtRefType) {
        this.debtRefType = debtRefType;
    }

    @Basic
    @Column(name = "DebtRefDate")
    public Date getDebtRefDate() {
        return debtRefDate;
    }

    public void setDebtRefDate(Date debtRefDate) {
        this.debtRefDate = debtRefDate;
    }

    @Basic
    @Column(name = "DebtPostedDate")
    public Date getDebtPostedDate() {
        return debtPostedDate;
    }

    public void setDebtPostedDate(Date debtPostedDate) {
        this.debtPostedDate = debtPostedDate;
    }

    @Basic
    @Column(name = "DebtRefNo")
    public String getDebtRefNo() {
        return debtRefNo;
    }

    public void setDebtRefNo(String debtRefNo) {
        this.debtRefNo = debtRefNo;
    }

    @Basic
    @Column(name = "DebtInvNo")
    public String getDebtInvNo() {
        return debtInvNo;
    }

    public void setDebtInvNo(String debtInvNo) {
        this.debtInvNo = debtInvNo;
    }

    @Basic
    @Column(name = "DebtDueDate")
    public Date getDebtDueDate() {
        return debtDueDate;
    }

    public void setDebtDueDate(Date debtDueDate) {
        this.debtDueDate = debtDueDate;
    }

    @Basic
    @Column(name = "DebtDescription")
    public String getDebtDescription() {
        return debtDescription;
    }

    public void setDebtDescription(String debtDescription) {
        this.debtDescription = debtDescription;
    }

    @Basic
    @Column(name = "TotalDebtableAmountOC")
    public BigDecimal getTotalDebtableAmountOc() {
        return totalDebtableAmountOc;
    }

    public void setTotalDebtableAmountOc(BigDecimal totalDebtableAmountOc) {
        this.totalDebtableAmountOc = totalDebtableAmountOc;
    }

    @Basic
    @Column(name = "TotalDebtableAmount")
    public BigDecimal getTotalDebtableAmount() {
        return totalDebtableAmount;
    }

    public void setTotalDebtableAmount(BigDecimal totalDebtableAmount) {
        this.totalDebtableAmount = totalDebtableAmount;
    }

    @Basic
    @Column(name = "DebtableAmountOC")
    public BigDecimal getDebtableAmountOc() {
        return debtableAmountOc;
    }

    public void setDebtableAmountOc(BigDecimal debtableAmountOc) {
        this.debtableAmountOc = debtableAmountOc;
    }

    @Basic
    @Column(name = "DebtableAmount")
    public BigDecimal getDebtableAmount() {
        return debtableAmount;
    }

    public void setDebtableAmount(BigDecimal debtableAmount) {
        this.debtableAmount = debtableAmount;
    }

    @Basic
    @Column(name = "DebtAmountOC")
    public BigDecimal getDebtAmountOc() {
        return debtAmountOc;
    }

    public void setDebtAmountOc(BigDecimal debtAmountOc) {
        this.debtAmountOc = debtAmountOc;
    }

    @Basic
    @Column(name = "DebtAmount")
    public BigDecimal getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(BigDecimal debtAmount) {
        this.debtAmount = debtAmount;
    }

    @Basic
    @Column(name = "DiscountRate")
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @Basic
    @Column(name = "DiscountAmountOC")
    public BigDecimal getDiscountAmountOc() {
        return discountAmountOc;
    }

    public void setDiscountAmountOc(BigDecimal discountAmountOc) {
        this.discountAmountOc = discountAmountOc;
    }

    @Basic
    @Column(name = "DiscountAmount")
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "DiscountAccount")
    public String getDiscountAccount() {
        return discountAccount;
    }

    public void setDiscountAccount(String discountAccount) {
        this.discountAccount = discountAccount;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "ExchangeDiffAmount")
    public BigDecimal getExchangeDiffAmount() {
        return exchangeDiffAmount;
    }

    public void setExchangeDiffAmount(BigDecimal exchangeDiffAmount) {
        this.exchangeDiffAmount = exchangeDiffAmount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "DebtExchangeRate")
    public BigDecimal getDebtExchangeRate() {
        return debtExchangeRate;
    }

    public void setDebtExchangeRate(BigDecimal debtExchangeRate) {
        this.debtExchangeRate = debtExchangeRate;
    }

    @Basic
    @Column(name = "LastExchangeRate")
    public BigDecimal getLastExchangeRate() {
        return lastExchangeRate;
    }

    public void setLastExchangeRate(BigDecimal lastExchangeRate) {
        this.lastExchangeRate = lastExchangeRate;
    }

    @Basic
    @Column(name = "DebtInvDate")
    public Date getDebtInvDate() {
        return debtInvDate;
    }

    public void setDebtInvDate(Date debtInvDate) {
        this.debtInvDate = debtInvDate;
    }

    @Basic
    @Column(name = "DebtEmployeeID")
    public Integer getDebtEmployeeId() {
        return debtEmployeeId;
    }

    public void setDebtEmployeeId(Integer debtEmployeeId) {
        this.debtEmployeeId = debtEmployeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GlVoucherCrossEntryDetail that = (GlVoucherCrossEntryDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(mappingCrossId, that.mappingCrossId) &&
                Objects.equals(glVoucherRefId, that.glVoucherRefId) &&
                Objects.equals(crossType, that.crossType) &&
                Objects.equals(receiptType, that.receiptType) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(payRefId, that.payRefId) &&
                Objects.equals(isPayVoucherPosted, that.isPayVoucherPosted) &&
                Objects.equals(payRefType, that.payRefType) &&
                Objects.equals(payRefDate, that.payRefDate) &&
                Objects.equals(payPostedDate, that.payPostedDate) &&
                Objects.equals(payRefNo, that.payRefNo) &&
                Objects.equals(payDescription, that.payDescription) &&
                Objects.equals(totalPayableAmountOc, that.totalPayableAmountOc) &&
                Objects.equals(totalPayableAmount, that.totalPayableAmount) &&
                Objects.equals(payableAmountOc, that.payableAmountOc) &&
                Objects.equals(payableAmount, that.payableAmount) &&
                Objects.equals(payAmountOc, that.payAmountOc) &&
                Objects.equals(payAmount, that.payAmount) &&
                Objects.equals(debtRefId, that.debtRefId) &&
                Objects.equals(isDebtVoucherPosted, that.isDebtVoucherPosted) &&
                Objects.equals(debtRefType, that.debtRefType) &&
                Objects.equals(debtRefDate, that.debtRefDate) &&
                Objects.equals(debtPostedDate, that.debtPostedDate) &&
                Objects.equals(debtRefNo, that.debtRefNo) &&
                Objects.equals(debtInvNo, that.debtInvNo) &&
                Objects.equals(debtDueDate, that.debtDueDate) &&
                Objects.equals(debtDescription, that.debtDescription) &&
                Objects.equals(totalDebtableAmountOc, that.totalDebtableAmountOc) &&
                Objects.equals(totalDebtableAmount, that.totalDebtableAmount) &&
                Objects.equals(debtableAmountOc, that.debtableAmountOc) &&
                Objects.equals(debtableAmount, that.debtableAmount) &&
                Objects.equals(debtAmountOc, that.debtAmountOc) &&
                Objects.equals(debtAmount, that.debtAmount) &&
                Objects.equals(discountRate, that.discountRate) &&
                Objects.equals(discountAmountOc, that.discountAmountOc) &&
                Objects.equals(discountAmount, that.discountAmount) &&
                Objects.equals(discountAccount, that.discountAccount) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(exchangeDiffAmount, that.exchangeDiffAmount) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(displayOnBook, that.displayOnBook) &&
                Objects.equals(debtExchangeRate, that.debtExchangeRate) &&
                Objects.equals(lastExchangeRate, that.lastExchangeRate) &&
                Objects.equals(debtInvDate, that.debtInvDate) &&
                Objects.equals(debtEmployeeId, that.debtEmployeeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mappingCrossId, glVoucherRefId, crossType, receiptType, accountNumber, currencyId, accountObjectId, payRefId, isPayVoucherPosted, payRefType, payRefDate, payPostedDate, payRefNo, payDescription, totalPayableAmountOc, totalPayableAmount, payableAmountOc, payableAmount, payAmountOc, payAmount, debtRefId, isDebtVoucherPosted, debtRefType, debtRefDate, debtPostedDate, debtRefNo, debtInvNo, debtDueDate, debtDescription, totalDebtableAmountOc, totalDebtableAmount, debtableAmountOc, debtableAmount, debtAmountOc, debtAmount, discountRate, discountAmountOc, discountAmount, discountAccount, paymentTermId, exchangeDiffAmount, sortOrder, branchId, displayOnBook, debtExchangeRate, lastExchangeRate, debtInvDate, debtEmployeeId);
    }
}
