package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Điều khoản thanh toán.
 */
@Entity
public class PaymentTerm {

    private Integer id; // PK
    private String paymentTermCode; // Mã điều khoản thanh toán
    private String paymentTermName; // Tên điều khoản thanh toán
    private Integer dueTime; // Hạn nợ (Số ngày được nợ)
    private Integer discountTime; // Thời hạn hưởng chiết khấu
    private BigDecimal discountPercent; // Tỷ lệ chiết khấu
    private Boolean activeStatus; // Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PaymentTermCode")
    public String getPaymentTermCode() {
        return paymentTermCode;
    }

    public void setPaymentTermCode(String paymentTermCode) {
        this.paymentTermCode = paymentTermCode;
    }

    @Basic
    @Column(name = "PaymentTermName")
    public String getPaymentTermName() {
        return paymentTermName;
    }

    public void setPaymentTermName(String paymentTermName) {
        this.paymentTermName = paymentTermName;
    }

    @Basic
    @Column(name = "DueTime")
    public Integer getDueTime() {
        return dueTime;
    }

    public void setDueTime(Integer dueTime) {
        this.dueTime = dueTime;
    }

    @Basic
    @Column(name = "DiscountTime")
    public Integer getDiscountTime() {
        return discountTime;
    }

    public void setDiscountTime(Integer discountTime) {
        this.discountTime = discountTime;
    }

    @Basic
    @Column(name = "DiscountPercent")
    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentTerm that = (PaymentTerm) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(paymentTermCode, that.paymentTermCode) &&
                Objects.equals(paymentTermName, that.paymentTermName) &&
                Objects.equals(dueTime, that.dueTime) &&
                Objects.equals(discountTime, that.discountTime) &&
                Objects.equals(discountPercent, that.discountPercent) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paymentTermCode, paymentTermName, dueTime, discountTime, discountPercent, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy);
    }

}
