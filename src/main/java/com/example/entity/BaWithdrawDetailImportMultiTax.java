package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "BAWithdrawDetailImportMultiTax")
public class BaWithdrawDetailImportMultiTax {

    private Integer id;
    private Integer refId;
    private Integer voucherRefId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date voucherRefDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date voucherPostedDate;

    private String voucherRefNoFinance;
    private String voucherRefNoManagement;
    private Integer voucherRefType;
    private Integer sortOrder;
    private Integer voucherRefDetailId;
    private String invNo;
    private String description;
    private BigDecimal turnOverAmount;
    private String invTemplateNo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate;

    private String invSeries;
    private Integer accountObjectId;
    private String accountObjectName;
    private String accountObjectAddress;
    private String accountObjectTaxCode;
    private Integer purchasePurposeId;
    private BigDecimal importRate;
    private BigDecimal importAmount;
    private BigDecimal importPayableAmount;
    private BigDecimal importPaidAmount;
    private BigDecimal importRemainningAmount;
    private String importAccount;
    private BigDecimal specialRate;
    private BigDecimal specialAmount;
    private BigDecimal specialPayableAmount;
    private BigDecimal specialPaidAmount;
    private BigDecimal specialRemainningAmount;
    private String specialAccount;
    private BigDecimal environmentRate;
    private BigDecimal environmentAmount;
    private BigDecimal environmentPayableAmount;
    private BigDecimal environmentPaidAmount;
    private BigDecimal environmentRemainningAmount;
    private String environmentAccount;
    private BigDecimal vatRate;
    private BigDecimal vatAmount;
    private BigDecimal vatPayableAmount;
    private BigDecimal vatPaidAmount;
    private BigDecimal vatRemainningAmount;
    private String vatAccount;
    private String vatCorrespondingAccount;
    private String vatDeductionAccount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "VoucherRefID")
    public Integer getVoucherRefId() {
        return voucherRefId;
    }

    public void setVoucherRefId(Integer voucherRefId) {
        this.voucherRefId = voucherRefId;
    }

    @Basic
    @Column(name = "VoucherRefDate")
    public Date getVoucherRefDate() {
        return voucherRefDate;
    }

    public void setVoucherRefDate(Date voucherRefDate) {
        this.voucherRefDate = voucherRefDate;
    }

    @Basic
    @Column(name = "VoucherPostedDate")
    public Date getVoucherPostedDate() {
        return voucherPostedDate;
    }

    public void setVoucherPostedDate(Date voucherPostedDate) {
        this.voucherPostedDate = voucherPostedDate;
    }

    @Basic
    @Column(name = "VoucherRefNoFinance")
    public String getVoucherRefNoFinance() {
        return voucherRefNoFinance;
    }

    public void setVoucherRefNoFinance(String voucherRefNoFinance) {
        this.voucherRefNoFinance = voucherRefNoFinance;
    }

    @Basic
    @Column(name = "VoucherRefNoManagement")
    public String getVoucherRefNoManagement() {
        return voucherRefNoManagement;
    }

    public void setVoucherRefNoManagement(String voucherRefNoManagement) {
        this.voucherRefNoManagement = voucherRefNoManagement;
    }

    @Basic
    @Column(name = "VoucherRefType")
    public Integer getVoucherRefType() {
        return voucherRefType;
    }

    public void setVoucherRefType(Integer voucherRefType) {
        this.voucherRefType = voucherRefType;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "VoucherRefDetailID")
    public Integer getVoucherRefDetailId() {
        return voucherRefDetailId;
    }

    public void setVoucherRefDetailId(Integer voucherRefDetailId) {
        this.voucherRefDetailId = voucherRefDetailId;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "TurnOverAmount")
    public BigDecimal getTurnOverAmount() {
        return turnOverAmount;
    }

    public void setTurnOverAmount(BigDecimal turnOverAmount) {
        this.turnOverAmount = turnOverAmount;
    }

    @Basic
    @Column(name = "InvTemplateNo")
    public String getInvTemplateNo() {
        return invTemplateNo;
    }

    public void setInvTemplateNo(String invTemplateNo) {
        this.invTemplateNo = invTemplateNo;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "PurchasePurposeID")
    public Integer getPurchasePurposeId() {
        return purchasePurposeId;
    }

    public void setPurchasePurposeId(Integer purchasePurposeId) {
        this.purchasePurposeId = purchasePurposeId;
    }

    @Basic
    @Column(name = "ImportRate")
    public BigDecimal getImportRate() {
        return importRate;
    }

    public void setImportRate(BigDecimal importRate) {
        this.importRate = importRate;
    }

    @Basic
    @Column(name = "ImportAmount")
    public BigDecimal getImportAmount() {
        return importAmount;
    }

    public void setImportAmount(BigDecimal importAmount) {
        this.importAmount = importAmount;
    }

    @Basic
    @Column(name = "ImportPayableAmount")
    public BigDecimal getImportPayableAmount() {
        return importPayableAmount;
    }

    public void setImportPayableAmount(BigDecimal importPayableAmount) {
        this.importPayableAmount = importPayableAmount;
    }

    @Basic
    @Column(name = "ImportPaidAmount")
    public BigDecimal getImportPaidAmount() {
        return importPaidAmount;
    }

    public void setImportPaidAmount(BigDecimal importPaidAmount) {
        this.importPaidAmount = importPaidAmount;
    }

    @Basic
    @Column(name = "ImportRemainningAmount")
    public BigDecimal getImportRemainningAmount() {
        return importRemainningAmount;
    }

    public void setImportRemainningAmount(BigDecimal importRemainningAmount) {
        this.importRemainningAmount = importRemainningAmount;
    }

    @Basic
    @Column(name = "ImportAccount")
    public String getImportAccount() {
        return importAccount;
    }

    public void setImportAccount(String importAccount) {
        this.importAccount = importAccount;
    }

    @Basic
    @Column(name = "SpecialRate")
    public BigDecimal getSpecialRate() {
        return specialRate;
    }

    public void setSpecialRate(BigDecimal specialRate) {
        this.specialRate = specialRate;
    }

    @Basic
    @Column(name = "SpecialAmount")
    public BigDecimal getSpecialAmount() {
        return specialAmount;
    }

    public void setSpecialAmount(BigDecimal specialAmount) {
        this.specialAmount = specialAmount;
    }

    @Basic
    @Column(name = "SpecialPayableAmount")
    public BigDecimal getSpecialPayableAmount() {
        return specialPayableAmount;
    }

    public void setSpecialPayableAmount(BigDecimal specialPayableAmount) {
        this.specialPayableAmount = specialPayableAmount;
    }

    @Basic
    @Column(name = "SpecialPaidAmount")
    public BigDecimal getSpecialPaidAmount() {
        return specialPaidAmount;
    }

    public void setSpecialPaidAmount(BigDecimal specialPaidAmount) {
        this.specialPaidAmount = specialPaidAmount;
    }

    @Basic
    @Column(name = "SpecialRemainningAmount")
    public BigDecimal getSpecialRemainningAmount() {
        return specialRemainningAmount;
    }

    public void setSpecialRemainningAmount(BigDecimal specialRemainningAmount) {
        this.specialRemainningAmount = specialRemainningAmount;
    }

    @Basic
    @Column(name = "SpecialAccount")
    public String getSpecialAccount() {
        return specialAccount;
    }

    public void setSpecialAccount(String specialAccount) {
        this.specialAccount = specialAccount;
    }

    @Basic
    @Column(name = "EnvironmentRate")
    public BigDecimal getEnvironmentRate() {
        return environmentRate;
    }

    public void setEnvironmentRate(BigDecimal environmentRate) {
        this.environmentRate = environmentRate;
    }

    @Basic
    @Column(name = "EnvironmentAmount")
    public BigDecimal getEnvironmentAmount() {
        return environmentAmount;
    }

    public void setEnvironmentAmount(BigDecimal environmentAmount) {
        this.environmentAmount = environmentAmount;
    }

    @Basic
    @Column(name = "EnvironmentPayableAmount")
    public BigDecimal getEnvironmentPayableAmount() {
        return environmentPayableAmount;
    }

    public void setEnvironmentPayableAmount(BigDecimal environmentPayableAmount) {
        this.environmentPayableAmount = environmentPayableAmount;
    }

    @Basic
    @Column(name = "EnvironmentPaidAmount")
    public BigDecimal getEnvironmentPaidAmount() {
        return environmentPaidAmount;
    }

    public void setEnvironmentPaidAmount(BigDecimal environmentPaidAmount) {
        this.environmentPaidAmount = environmentPaidAmount;
    }

    @Basic
    @Column(name = "EnvironmentRemainningAmount")
    public BigDecimal getEnvironmentRemainningAmount() {
        return environmentRemainningAmount;
    }

    public void setEnvironmentRemainningAmount(BigDecimal environmentRemainningAmount) {
        this.environmentRemainningAmount = environmentRemainningAmount;
    }

    @Basic
    @Column(name = "EnvironmentAccount")
    public String getEnvironmentAccount() {
        return environmentAccount;
    }

    public void setEnvironmentAccount(String environmentAccount) {
        this.environmentAccount = environmentAccount;
    }

    @Basic
    @Column(name = "VATRate")
    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    @Basic
    @Column(name = "VATAmount")
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Basic
    @Column(name = "VATPayableAmount")
    public BigDecimal getVatPayableAmount() {
        return vatPayableAmount;
    }

    public void setVatPayableAmount(BigDecimal vatPayableAmount) {
        this.vatPayableAmount = vatPayableAmount;
    }

    @Basic
    @Column(name = "VATPaidAmount")
    public BigDecimal getVatPaidAmount() {
        return vatPaidAmount;
    }

    public void setVatPaidAmount(BigDecimal vatPaidAmount) {
        this.vatPaidAmount = vatPaidAmount;
    }

    @Basic
    @Column(name = "VATRemainningAmount")
    public BigDecimal getVatRemainningAmount() {
        return vatRemainningAmount;
    }

    public void setVatRemainningAmount(BigDecimal vatRemainningAmount) {
        this.vatRemainningAmount = vatRemainningAmount;
    }

    @Basic
    @Column(name = "VATAccount")
    public String getVatAccount() {
        return vatAccount;
    }

    public void setVatAccount(String vatAccount) {
        this.vatAccount = vatAccount;
    }

    @Basic
    @Column(name = "VATCorrespondingAccount")
    public String getVatCorrespondingAccount() {
        return vatCorrespondingAccount;
    }

    public void setVatCorrespondingAccount(String vatCorrespondingAccount) {
        this.vatCorrespondingAccount = vatCorrespondingAccount;
    }

    @Basic
    @Column(name = "VATDeductionAccount")
    public String getVatDeductionAccount() {
        return vatDeductionAccount;
    }

    public void setVatDeductionAccount(String vatDeductionAccount) {
        this.vatDeductionAccount = vatDeductionAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaWithdrawDetailImportMultiTax that = (BaWithdrawDetailImportMultiTax) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(voucherRefId, that.voucherRefId) &&
                Objects.equals(voucherRefDate, that.voucherRefDate) &&
                Objects.equals(voucherPostedDate, that.voucherPostedDate) &&
                Objects.equals(voucherRefNoFinance, that.voucherRefNoFinance) &&
                Objects.equals(voucherRefNoManagement, that.voucherRefNoManagement) &&
                Objects.equals(voucherRefType, that.voucherRefType) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(voucherRefDetailId, that.voucherRefDetailId) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(turnOverAmount, that.turnOverAmount) &&
                Objects.equals(invTemplateNo, that.invTemplateNo) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, that.accountObjectTaxCode) &&
                Objects.equals(purchasePurposeId, that.purchasePurposeId) &&
                Objects.equals(importRate, that.importRate) &&
                Objects.equals(importAmount, that.importAmount) &&
                Objects.equals(importPayableAmount, that.importPayableAmount) &&
                Objects.equals(importPaidAmount, that.importPaidAmount) &&
                Objects.equals(importRemainningAmount, that.importRemainningAmount) &&
                Objects.equals(importAccount, that.importAccount) &&
                Objects.equals(specialRate, that.specialRate) &&
                Objects.equals(specialAmount, that.specialAmount) &&
                Objects.equals(specialPayableAmount, that.specialPayableAmount) &&
                Objects.equals(specialPaidAmount, that.specialPaidAmount) &&
                Objects.equals(specialRemainningAmount, that.specialRemainningAmount) &&
                Objects.equals(specialAccount, that.specialAccount) &&
                Objects.equals(environmentRate, that.environmentRate) &&
                Objects.equals(environmentAmount, that.environmentAmount) &&
                Objects.equals(environmentPayableAmount, that.environmentPayableAmount) &&
                Objects.equals(environmentPaidAmount, that.environmentPaidAmount) &&
                Objects.equals(environmentRemainningAmount, that.environmentRemainningAmount) &&
                Objects.equals(environmentAccount, that.environmentAccount) &&
                Objects.equals(vatRate, that.vatRate) &&
                Objects.equals(vatAmount, that.vatAmount) &&
                Objects.equals(vatPayableAmount, that.vatPayableAmount) &&
                Objects.equals(vatPaidAmount, that.vatPaidAmount) &&
                Objects.equals(vatRemainningAmount, that.vatRemainningAmount) &&
                Objects.equals(vatAccount, that.vatAccount) &&
                Objects.equals(vatCorrespondingAccount, that.vatCorrespondingAccount) &&
                Objects.equals(vatDeductionAccount, that.vatDeductionAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, voucherRefId, voucherRefDate, voucherPostedDate, voucherRefNoFinance, voucherRefNoManagement, voucherRefType, sortOrder, voucherRefDetailId, invNo, description, turnOverAmount, invTemplateNo, invDate, invSeries, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, purchasePurposeId, importRate, importAmount, importPayableAmount, importPaidAmount, importRemainningAmount, importAccount, specialRate, specialAmount, specialPayableAmount, specialPaidAmount, specialRemainningAmount, specialAccount, environmentRate, environmentAmount, environmentPayableAmount, environmentPaidAmount, environmentRemainningAmount, environmentAccount, vatRate, vatAmount, vatPayableAmount, vatPaidAmount, vatRemainningAmount, vatAccount, vatCorrespondingAccount, vatDeductionAccount);
    }
}
