package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Sổ cái theo Đối tượng Kế toán.
 */
@Entity
public class AccountObjectLedger {

    private Integer id;

    private Integer branchId;
    private Integer refId;
    private Integer refDetailId;
    private Integer entryType;
    private Integer refType;
    private String refNo;
    private Timestamp refDate;
    private Timestamp postedDate;
    private Integer invRefId;
    private String invNo;
    private Timestamp invDate;
    private String invSeries;
    private String accountNumber;
    private String accountName;
    private String correspondingAccountNumber;
    private BigDecimal exchangeRate;
    private String currencyId;
    private Integer unitId;
    private BigDecimal unitPriceOc;
    private BigDecimal unitPrice;
    private BigDecimal quantity;
    private BigDecimal debitAmountOc;
    private BigDecimal debitAmount;
    private BigDecimal creditAmountOc;
    private BigDecimal creditAmount;
    private String journalMemo;
    private String description;
    private Timestamp dueDate;
    private Integer organizationUnitId;
    private Integer accountObjectId;
    private String accountObjectCode;
    private String accountObjectName;
    private String accountObjectNameDi;
    private String accountObjectTaxCode;
    private String accountObjectAddress;
    private String accountObjectAddressDi;
    private Integer employeeId;
    private String employeeCode;
    private String employeeName;
    private Integer orderId;
    private Integer jobId;
    private Integer contractId;
    private Timestamp contractSignDate;
    private String contractCode;
    private String contractName;
    private Integer listItemId;
    private Integer budgetItemId;
    private Integer projectWorkId;
    private Integer paymentTermId;
    private Integer inventoryItemId;
    private String inventoryItemCode;
    private String inventoryItemName;
    private Integer puContractId;
    private Timestamp puSignDate;
    private String puContractCode;
    private String puContractName;
    private String refTypeName;
    private boolean isPostToManagementBook;
    private Integer refOrder;
    private Integer sortOrder;
    private boolean isUpdateRedundant;
    private Integer puOrderRefId;
    private Integer mainUnitId;
    private BigDecimal mainUnitPrice;
    private BigDecimal mainQuantity;
    private BigDecimal mainConvertRate;
    private String exchangeRateOperator;
    private String documentIncluded;
    private Integer detailPostOrder;
    private BigDecimal mainUnitPriceOc;
    private BigDecimal cashOutExchangeRateLedger;
    private Integer businessType;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "RefDetailID")
    public Integer getRefDetailId() {
        return refDetailId;
    }

    public void setRefDetailId(Integer refDetailId) {
        this.refDetailId = refDetailId;
    }

    @Basic
    @Column(name = "EntryType")
    public int getEntryType() {
        return entryType;
    }

    public void setEntryType(int entryType) {
        this.entryType = entryType;
    }

    @Basic
    @Column(name = "RefType")
    public int getRefType() {
        return refType;
    }

    public void setRefType(int refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "RefDate")
    public Timestamp getRefDate() {
        return refDate;
    }

    public void setRefDate(Timestamp refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "PostedDate")
    public Timestamp getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Timestamp postedDate) {
        this.postedDate = postedDate;
    }

    @Basic
    @Column(name = "InvRefID")
    public Integer getInvRefId() {
        return invRefId;
    }

    public void setInvRefId(Integer invRefId) {
        this.invRefId = invRefId;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "InvDate")
    public Timestamp getInvDate() {
        return invDate;
    }

    public void setInvDate(Timestamp invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "AccountName")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "CorrespondingAccountNumber")
    public String getCorrespondingAccountNumber() {
        return correspondingAccountNumber;
    }

    public void setCorrespondingAccountNumber(String correspondingAccountNumber) {
        this.correspondingAccountNumber = correspondingAccountNumber;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "UnitID")
    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Basic
    @Column(name = "UnitPriceOC")
    public BigDecimal getUnitPriceOc() {
        return unitPriceOc;
    }

    public void setUnitPriceOc(BigDecimal unitPriceOc) {
        this.unitPriceOc = unitPriceOc;
    }

    @Basic
    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "Quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "DebitAmountOC")
    public BigDecimal getDebitAmountOc() {
        return debitAmountOc;
    }

    public void setDebitAmountOc(BigDecimal debitAmountOc) {
        this.debitAmountOc = debitAmountOc;
    }

    @Basic
    @Column(name = "DebitAmount")
    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    @Basic
    @Column(name = "CreditAmountOC")
    public BigDecimal getCreditAmountOc() {
        return creditAmountOc;
    }

    public void setCreditAmountOc(BigDecimal creditAmountOc) {
        this.creditAmountOc = creditAmountOc;
    }

    @Basic
    @Column(name = "CreditAmount")
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "DueDate")
    public Timestamp getDueDate() {
        return dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectCode")
    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectNameDI")
    public String getAccountObjectNameDi() {
        return accountObjectNameDi;
    }

    public void setAccountObjectNameDi(String accountObjectNameDi) {
        this.accountObjectNameDi = accountObjectNameDi;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectAddressDI")
    public String getAccountObjectAddressDi() {
        return accountObjectAddressDi;
    }

    public void setAccountObjectAddressDi(String accountObjectAddressDi) {
        this.accountObjectAddressDi = accountObjectAddressDi;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "EmployeeCode")
    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Basic
    @Column(name = "EmployeeName")
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "ContractSignDate")
    public Timestamp getContractSignDate() {
        return contractSignDate;
    }

    public void setContractSignDate(Timestamp contractSignDate) {
        this.contractSignDate = contractSignDate;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "ContractName")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "BudgetItemID")
    public Integer getBudgetItemId() {
        return budgetItemId;
    }

    public void setBudgetItemId(Integer budgetItemId) {
        this.budgetItemId = budgetItemId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "InventoryItemCode")
    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    @Basic
    @Column(name = "InventoryItemName")
    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "PUSignDate")
    public Timestamp getPuSignDate() {
        return puSignDate;
    }

    public void setPuSignDate(Timestamp puSignDate) {
        this.puSignDate = puSignDate;
    }

    @Basic
    @Column(name = "PUContractCode")
    public String getPuContractCode() {
        return puContractCode;
    }

    public void setPuContractCode(String puContractCode) {
        this.puContractCode = puContractCode;
    }

    @Basic
    @Column(name = "PUContractName")
    public String getPuContractName() {
        return puContractName;
    }

    public void setPuContractName(String puContractName) {
        this.puContractName = puContractName;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "IsPostToManagementBook")
    public boolean getIsPostToManagementBook() {
        return isPostToManagementBook;
    }

    public void setIsPostToManagementBook(boolean postToManagementBook) {
        isPostToManagementBook = postToManagementBook;
    }

    @Basic
    @Column(name = "RefOrder")
    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "IsUpdateRedundant")
    public boolean getIsUpdateRedundant() {
        return isUpdateRedundant;
    }

    public void setIsUpdateRedundant(boolean updateRedundant) {
        isUpdateRedundant = updateRedundant;
    }

    @Basic
    @Column(name = "PUOrderRefID")
    public Integer getPuOrderRefId() {
        return puOrderRefId;
    }

    public void setPuOrderRefId(Integer puOrderRefId) {
        this.puOrderRefId = puOrderRefId;
    }

    @Basic
    @Column(name = "MainUnitID")
    public Integer getMainUnitId() {
        return mainUnitId;
    }

    public void setMainUnitId(Integer mainUnitId) {
        this.mainUnitId = mainUnitId;
    }

    @Basic
    @Column(name = "MainUnitPrice")
    public BigDecimal getMainUnitPrice() {
        return mainUnitPrice;
    }

    public void setMainUnitPrice(BigDecimal mainUnitPrice) {
        this.mainUnitPrice = mainUnitPrice;
    }

    @Basic
    @Column(name = "MainQuantity")
    public BigDecimal getMainQuantity() {
        return mainQuantity;
    }

    public void setMainQuantity(BigDecimal mainQuantity) {
        this.mainQuantity = mainQuantity;
    }

    @Basic
    @Column(name = "MainConvertRate")
    public BigDecimal getMainConvertRate() {
        return mainConvertRate;
    }

    public void setMainConvertRate(BigDecimal mainConvertRate) {
        this.mainConvertRate = mainConvertRate;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public String getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(String exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "DocumentIncluded")
    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    @Basic
    @Column(name = "DetailPostOrder")
    public Integer getDetailPostOrder() {
        return detailPostOrder;
    }

    public void setDetailPostOrder(Integer detailPostOrder) {
        this.detailPostOrder = detailPostOrder;
    }

    @Basic
    @Column(name = "MainUnitPriceOC")
    public BigDecimal getMainUnitPriceOc() {
        return mainUnitPriceOc;
    }

    public void setMainUnitPriceOc(BigDecimal mainUnitPriceOc) {
        this.mainUnitPriceOc = mainUnitPriceOc;
    }

    @Basic
    @Column(name = "CashOutExchangeRateLedger")
    public BigDecimal getCashOutExchangeRateLedger() {
        return cashOutExchangeRateLedger;
    }

    public void setCashOutExchangeRateLedger(BigDecimal cashOutExchangeRateLedger) {
        this.cashOutExchangeRateLedger = cashOutExchangeRateLedger;
    }

    @Basic
    @Column(name = "BusinessType")
    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountObjectLedger that = (AccountObjectLedger) o;
        return id == that.id &&
                entryType == that.entryType &&
                refType == that.refType &&
                isPostToManagementBook == that.isPostToManagementBook &&
                isUpdateRedundant == that.isUpdateRedundant &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(refDetailId, that.refDetailId) &&
                Objects.equals(refNo, that.refNo) &&
                Objects.equals(refDate, that.refDate) &&
                Objects.equals(postedDate, that.postedDate) &&
                Objects.equals(invRefId, that.invRefId) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invSeries, that.invSeries) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(accountName, that.accountName) &&
                Objects.equals(correspondingAccountNumber, that.correspondingAccountNumber) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(unitId, that.unitId) &&
                Objects.equals(unitPriceOc, that.unitPriceOc) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(debitAmountOc, that.debitAmountOc) &&
                Objects.equals(debitAmount, that.debitAmount) &&
                Objects.equals(creditAmountOc, that.creditAmountOc) &&
                Objects.equals(creditAmount, that.creditAmount) &&
                Objects.equals(journalMemo, that.journalMemo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectCode, that.accountObjectCode) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectNameDi, that.accountObjectNameDi) &&
                Objects.equals(accountObjectTaxCode, that.accountObjectTaxCode) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectAddressDi, that.accountObjectAddressDi) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(employeeCode, that.employeeCode) &&
                Objects.equals(employeeName, that.employeeName) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(contractSignDate, that.contractSignDate) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(contractName, that.contractName) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(budgetItemId, that.budgetItemId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(paymentTermId, that.paymentTermId) &&
                Objects.equals(inventoryItemId, that.inventoryItemId) &&
                Objects.equals(inventoryItemCode, that.inventoryItemCode) &&
                Objects.equals(inventoryItemName, that.inventoryItemName) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(puSignDate, that.puSignDate) &&
                Objects.equals(puContractCode, that.puContractCode) &&
                Objects.equals(puContractName, that.puContractName) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(refOrder, that.refOrder) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(puOrderRefId, that.puOrderRefId) &&
                Objects.equals(mainUnitId, that.mainUnitId) &&
                Objects.equals(mainUnitPrice, that.mainUnitPrice) &&
                Objects.equals(mainQuantity, that.mainQuantity) &&
                Objects.equals(mainConvertRate, that.mainConvertRate) &&
                Objects.equals(exchangeRateOperator, that.exchangeRateOperator) &&
                Objects.equals(documentIncluded, that.documentIncluded) &&
                Objects.equals(detailPostOrder, that.detailPostOrder) &&
                Objects.equals(mainUnitPriceOc, that.mainUnitPriceOc) &&
                Objects.equals(cashOutExchangeRateLedger, that.cashOutExchangeRateLedger) &&
                Objects.equals(businessType, that.businessType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, refId, refDetailId, entryType, refType, refNo, refDate, postedDate, invRefId, invNo, invDate, invSeries, accountNumber, accountName, correspondingAccountNumber, exchangeRate, currencyId, unitId, unitPriceOc, unitPrice, quantity, debitAmountOc, debitAmount, creditAmountOc, creditAmount, journalMemo, description, dueDate, organizationUnitId, accountObjectId, accountObjectCode, accountObjectName, accountObjectNameDi, accountObjectTaxCode, accountObjectAddress, accountObjectAddressDi, employeeId, employeeCode, employeeName, orderId, jobId, contractId, contractSignDate, contractCode, contractName, listItemId, budgetItemId, projectWorkId, paymentTermId, inventoryItemId, inventoryItemCode, inventoryItemName, puContractId, puSignDate, puContractCode, puContractName, refTypeName, isPostToManagementBook, refOrder, sortOrder, isUpdateRedundant, puOrderRefId, mainUnitId, mainUnitPrice, mainQuantity, mainConvertRate, exchangeRateOperator, documentIncluded, detailPostOrder, mainUnitPriceOc, cashOutExchangeRateLedger, businessType);
    }
}
