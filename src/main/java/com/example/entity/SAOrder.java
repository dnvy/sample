package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Bảng master đơn đặt hàng.
 */
@Entity
@Table(name = "SAOrder")
public class SAOrder {

    private Integer id; // NOT NULL. PK

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày đơn hàng

    private String refNo; // NOT NULL. Số đơn hàng
    private Integer status; // NOT NULL. Tình trạng đơn hàng, 0: Chưa thực hiện; 1: Đang thực hiện; 2: Tạm dừng; 3: Hoàn thành; 4: Đã hủy bỏ
    private Integer refType; // NOT NULL. Loại chứng từ (?)
    private Integer branchId; // Chi nhánh
    private Integer accountObjectId; // FK: ID khách hàng là tổ chức hoặc cá nhân
    private String accountObjectName; // Tên khách hàng là tổ chức hoặc cá nhân
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectTaxCode; // Mã số thuế
    private String receiver; // Người nhận hàng
    private Integer employeeId; // Nhân viên bán hàng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliveryDate; // Ngày giao hàng

    private Boolean isCalculatedCost; // NOT NULL. Check tính giá thành
    private Integer paymentTermId; // Mã điều kiện chiết khấu
    private Integer dueDay; // Số ngày được nợ
    private String journalMemo; // Ghi chú
    private String shippingAddress; // Địa chỉ giao hàng
    private String otherTerm; // Điều khoản khác
    private Integer quoteRefId; // FK với bảng báo giá
    private String currencyId; // Loại tiền
    private BigDecimal exchangeRate; // NOT NULL. Tỉ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Tổng tiền hàng
    private BigDecimal totalAmount; // NOT NULL. Tổng tiền hàng quy đổi
    private BigDecimal totalDiscountAmountOc; // NOT NULL. Tổng tiền chiết khấu
    private BigDecimal totalDiscountAmount; // NOT NULL. Tổng tiền chiết khấu quy đổi
    private BigDecimal totalVatAmountOc; // NOT NULL. Tổng tiền thuế
    private BigDecimal totalVatAmount; // NOT NULL. Tổng tiền thuế quy đổi
    private Integer editVersion;
    private String modifiedBy;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private BigDecimal lastYearInvoiceAmountOc; // NOT NULL. Giá trị đã xuất hóa đơn năm trước nguyên tệ
    private BigDecimal lastYearInvoiceAmount; // NOT NULL. Giá trị đã xuất hóa đơn năm trước

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefDate")
    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    @Basic
    @Column(name = "RefNo")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "Status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "Receiver")
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "DeliveryDate")
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @Basic
    @Column(name = "IsCalculatedCost")
    public Boolean getIsCalculatedCost() {
        return isCalculatedCost;
    }

    public void setIsCalculatedCost(Boolean calculatedCost) {
        isCalculatedCost = calculatedCost;
    }

    @Basic
    @Column(name = "PaymentTermID")
    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Basic
    @Column(name = "DueDay")
    public Integer getDueDay() {
        return dueDay;
    }

    public void setDueDay(Integer dueDay) {
        this.dueDay = dueDay;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "ShippingAddress")
    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Basic
    @Column(name = "OtherTerm")
    public String getOtherTerm() {
        return otherTerm;
    }

    public void setOtherTerm(String otherTerm) {
        this.otherTerm = otherTerm;
    }

    @Basic
    @Column(name = "QuoteRefID")
    public Integer getQuoteRefId() {
        return quoteRefId;
    }

    public void setQuoteRefId(Integer quoteRefId) {
        this.quoteRefId = quoteRefId;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "LastYearInvoiceAmountOC")
    public BigDecimal getLastYearInvoiceAmountOc() {
        return lastYearInvoiceAmountOc;
    }

    public void setLastYearInvoiceAmountOc(BigDecimal lastYearInvoiceAmountOc) {
        this.lastYearInvoiceAmountOc = lastYearInvoiceAmountOc;
    }

    @Basic
    @Column(name = "LastYearInvoiceAmount")
    public BigDecimal getLastYearInvoiceAmount() {
        return lastYearInvoiceAmount;
    }

    public void setLastYearInvoiceAmount(BigDecimal lastYearInvoiceAmount) {
        this.lastYearInvoiceAmount = lastYearInvoiceAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SAOrder saOrder = (SAOrder) o;
        return Objects.equals(id, saOrder.id) &&
                Objects.equals(refDate, saOrder.refDate) &&
                Objects.equals(refNo, saOrder.refNo) &&
                Objects.equals(status, saOrder.status) &&
                Objects.equals(refType, saOrder.refType) &&
                Objects.equals(branchId, saOrder.branchId) &&
                Objects.equals(accountObjectId, saOrder.accountObjectId) &&
                Objects.equals(accountObjectName, saOrder.accountObjectName) &&
                Objects.equals(accountObjectAddress, saOrder.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, saOrder.accountObjectTaxCode) &&
                Objects.equals(receiver, saOrder.receiver) &&
                Objects.equals(employeeId, saOrder.employeeId) &&
                Objects.equals(deliveryDate, saOrder.deliveryDate) &&
                Objects.equals(isCalculatedCost, saOrder.isCalculatedCost) &&
                Objects.equals(paymentTermId, saOrder.paymentTermId) &&
                Objects.equals(dueDay, saOrder.dueDay) &&
                Objects.equals(journalMemo, saOrder.journalMemo) &&
                Objects.equals(shippingAddress, saOrder.shippingAddress) &&
                Objects.equals(otherTerm, saOrder.otherTerm) &&
                Objects.equals(quoteRefId, saOrder.quoteRefId) &&
                Objects.equals(currencyId, saOrder.currencyId) &&
                Objects.equals(exchangeRate, saOrder.exchangeRate) &&
                Objects.equals(totalAmountOc, saOrder.totalAmountOc) &&
                Objects.equals(totalAmount, saOrder.totalAmount) &&
                Objects.equals(totalDiscountAmountOc, saOrder.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, saOrder.totalDiscountAmount) &&
                Objects.equals(totalVatAmountOc, saOrder.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, saOrder.totalVatAmount) &&
                Objects.equals(editVersion, saOrder.editVersion) &&
                Objects.equals(modifiedBy, saOrder.modifiedBy) &&
                Objects.equals(createdDate, saOrder.createdDate) &&
                Objects.equals(createdBy, saOrder.createdBy) &&
                Objects.equals(modifiedDate, saOrder.modifiedDate) &&
                Objects.equals(customField1, saOrder.customField1) &&
                Objects.equals(customField2, saOrder.customField2) &&
                Objects.equals(customField3, saOrder.customField3) &&
                Objects.equals(customField4, saOrder.customField4) &&
                Objects.equals(customField5, saOrder.customField5) &&
                Objects.equals(customField6, saOrder.customField6) &&
                Objects.equals(customField7, saOrder.customField7) &&
                Objects.equals(customField8, saOrder.customField8) &&
                Objects.equals(customField9, saOrder.customField9) &&
                Objects.equals(customField10, saOrder.customField10) &&
                Objects.equals(lastYearInvoiceAmountOc, saOrder.lastYearInvoiceAmountOc) &&
                Objects.equals(lastYearInvoiceAmount, saOrder.lastYearInvoiceAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refDate, refNo, status, refType, branchId, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, receiver, employeeId, deliveryDate, isCalculatedCost, paymentTermId, dueDay, journalMemo, shippingAddress, otherTerm, quoteRefId, currencyId, exchangeRate, totalAmountOc, totalAmount, totalDiscountAmountOc, totalDiscountAmount, totalVatAmountOc, totalVatAmount, editVersion, modifiedBy, createdDate, createdBy, modifiedDate, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, lastYearInvoiceAmountOc, lastYearInvoiceAmount);
    }
}
