package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Kiểm kê quỹ chi tiết.
 */
@Entity
@Table(name = "CAAuditDetail")
public class CaAuditDetail {

    private Integer id; // PK.
    private Integer refId; // FK.
    private String description; // Diễn giải
    private Integer valueOfMoney; // Mệnh giá tiền
    private Integer quantity; // Số lượng
    private BigDecimal amount; // Số tiền
    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ValueOfMoney")
    public Integer getValueOfMoney() {
        return valueOfMoney;
    }

    public void setValueOfMoney(Integer valueOfMoney) {
        this.valueOfMoney = valueOfMoney;
    }

    @Basic
    @Column(name = "Quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaAuditDetail that = (CaAuditDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(description, that.description) &&
                Objects.equals(valueOfMoney, that.valueOfMoney) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(sortOrder, that.sortOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, description, valueOfMoney, quantity, amount, sortOrder);
    }

}
