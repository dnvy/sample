package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Bảng lưu trữ Công nợ đầu kỳ theo hóa đơn.
 */
@Entity
public class OpeningAccountEntryDetailInvoice {

    private Integer id; // PK
    private Integer refId; // FK

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate; // Ngày hóa đơn

    private String invNo; // Số hóa đơn

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate; // Hạn thanh toán

    private Integer employeeId; // ID nhân viên
    private BigDecimal exchangeRate; // Tỷ giá
    private BigDecimal invoiceAmountOc; // Giá trị hóa đơn
    private BigDecimal invoiceAmount; // Giá trị hóa đơn quy đổi
    private BigDecimal amountOc; // Số còn phải thu/Số còn phải trả
    private BigDecimal amount; // Số còn phải thu/Số còn phải trả quy đổi
    private Integer sortOrder; // Thứ tự sắp xếp dòng
    private Boolean isAutoGenerate;
    private BigDecimal payAmountOc;
    private BigDecimal payAmount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "DueDate")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "InvoiceAmountOC")
    public BigDecimal getInvoiceAmountOc() {
        return invoiceAmountOc;
    }

    public void setInvoiceAmountOc(BigDecimal invoiceAmountOc) {
        this.invoiceAmountOc = invoiceAmountOc;
    }

    @Basic
    @Column(name = "InvoiceAmount")
    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "IsAutoGenerate")
    public Boolean getIsAutoGenerate() {
        return isAutoGenerate;
    }

    public void setIsAutoGenerate(Boolean autoGenerate) {
        isAutoGenerate = autoGenerate;
    }

    @Basic
    @Column(name = "PayAmountOC")
    public BigDecimal getPayAmountOc() {
        return payAmountOc;
    }

    public void setPayAmountOc(BigDecimal payAmountOc) {
        this.payAmountOc = payAmountOc;
    }

    @Basic
    @Column(name = "PayAmount")
    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpeningAccountEntryDetailInvoice that = (OpeningAccountEntryDetailInvoice) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(invDate, that.invDate) &&
                Objects.equals(invNo, that.invNo) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(invoiceAmountOc, that.invoiceAmountOc) &&
                Objects.equals(invoiceAmount, that.invoiceAmount) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(isAutoGenerate, that.isAutoGenerate) &&
                Objects.equals(payAmountOc, that.payAmountOc) &&
                Objects.equals(payAmount, that.payAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, invDate, invNo, dueDate, employeeId, exchangeRate, invoiceAmountOc, invoiceAmount, amountOc, amount, sortOrder, isAutoGenerate, payAmountOc, payAmount);
    }
}
