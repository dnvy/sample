package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Thiết lập thông tin đại lý thuế.
 */
@Entity
@Table(name = "TATaxAgentInfo")
public class TaTaxAgentInfo {

    private Integer id; // PK
    private String taxAgentName; // Tên đại lý thuế
    private String taxCode; // Mã số thuế
    private String address; // Địa chỉ
    private String district; // Quận/huyện
    private String provinceOrCity; // Tỉnh/ Thành phố
    private String tel; // Điện thoại
    private String fax; // Fax
    private String email; // Email
    private String contractCode; // Hợp đồng số

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date contractDate; // Ngày hợp đồng

    private String employeeName; // Nhân viên đại lý thuế
    private String certificateNo; // Chứng chỉ hành nghề số
    private Boolean isDisplayOnDeclaration; // Có hiển thị thông tin đại lý thuế lên tờ khai hay không
    private Timestamp createdDate; // Ngày lập chứng từ
    private String createdBy; // Người lập chứng từ
    private Timestamp modifiedDate; // Ngày sửa chứng từ lần cuối
    private String modifiedBy; // Người sửa chứng từ lần cuối
    private String taxationBureauCode; // Mã chi cục thuế
    private String taxOrganManagementCode; // mã đơn vị quản lý chi cục thuế
    private Integer branchId;
    private String provideUnit; // Đơn vị cung cấp dịch vụ kế toán
    private String certificateNoUnit; // Số chứng chỉ hành nghề của đơn vị cung cấp DV kế toán

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TaxAgentName")
    public String getTaxAgentName() {
        return taxAgentName;
    }

    public void setTaxAgentName(String taxAgentName) {
        this.taxAgentName = taxAgentName;
    }

    @Basic
    @Column(name = "TaxCode")
    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "District")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "ProvinceOrCity")
    public String getProvinceOrCity() {
        return provinceOrCity;
    }

    public void setProvinceOrCity(String provinceOrCity) {
        this.provinceOrCity = provinceOrCity;
    }

    @Basic
    @Column(name = "Tel")
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "Fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Basic
    @Column(name = "Email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "ContractDate")
    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    @Basic
    @Column(name = "EmployeeName")
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "CertificateNo")
    public String getCertificateNo() {
        return certificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        this.certificateNo = certificateNo;
    }

    @Basic
    @Column(name = "IsDisplayOnDeclaration")
    public Boolean getIsDisplayOnDeclaration() {
        return isDisplayOnDeclaration;
    }

    public void setIsDisplayOnDeclaration(Boolean displayOnDeclaration) {
        isDisplayOnDeclaration = displayOnDeclaration;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "TaxationBureauCode")
    public String getTaxationBureauCode() {
        return taxationBureauCode;
    }

    public void setTaxationBureauCode(String taxationBureauCode) {
        this.taxationBureauCode = taxationBureauCode;
    }

    @Basic
    @Column(name = "TaxOrganManagementCode")
    public String getTaxOrganManagementCode() {
        return taxOrganManagementCode;
    }

    public void setTaxOrganManagementCode(String taxOrganManagementCode) {
        this.taxOrganManagementCode = taxOrganManagementCode;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "ProvideUnit")
    public String getProvideUnit() {
        return provideUnit;
    }

    public void setProvideUnit(String provideUnit) {
        this.provideUnit = provideUnit;
    }

    @Basic
    @Column(name = "CertificateNoUnit")
    public String getCertificateNoUnit() {
        return certificateNoUnit;
    }

    public void setCertificateNoUnit(String certificateNoUnit) {
        this.certificateNoUnit = certificateNoUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaTaxAgentInfo that = (TaTaxAgentInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(taxAgentName, that.taxAgentName) &&
                Objects.equals(taxCode, that.taxCode) &&
                Objects.equals(address, that.address) &&
                Objects.equals(district, that.district) &&
                Objects.equals(provinceOrCity, that.provinceOrCity) &&
                Objects.equals(tel, that.tel) &&
                Objects.equals(fax, that.fax) &&
                Objects.equals(email, that.email) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(contractDate, that.contractDate) &&
                Objects.equals(employeeName, that.employeeName) &&
                Objects.equals(certificateNo, that.certificateNo) &&
                Objects.equals(isDisplayOnDeclaration, that.isDisplayOnDeclaration) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(taxationBureauCode, that.taxationBureauCode) &&
                Objects.equals(taxOrganManagementCode, that.taxOrganManagementCode) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(provideUnit, that.provideUnit) &&
                Objects.equals(certificateNoUnit, that.certificateNoUnit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taxAgentName, taxCode, address, district, provinceOrCity, tel, fax, email, contractCode, contractDate, employeeName, certificateNo, isDisplayOnDeclaration, createdDate, createdBy, modifiedDate, modifiedBy, taxationBureauCode, taxOrganManagementCode, branchId, provideUnit, certificateNoUnit);
    }
}
