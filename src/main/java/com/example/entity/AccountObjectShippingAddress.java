package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Địa chỉ giao nhận hàng, gắn liền với Đối tượng kế toán.
 */
@Entity
public class AccountObjectShippingAddress {

    private Integer id;
    private Integer accountObjectId;
    private String shippingAddress;
    private Integer sortOrder;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "ShippingAddress")
    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountObjectShippingAddress that = (AccountObjectShippingAddress) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(shippingAddress, that.shippingAddress) &&
                Objects.equals(sortOrder, that.sortOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountObjectId, shippingAddress, sortOrder);
    }
}
