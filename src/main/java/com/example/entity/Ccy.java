package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "CCY")
public class Ccy {

    private Integer id;
    private String currencyCode;
    private String currencyName;
    private BigDecimal exchangeRate;
    private Boolean exchangeRateOperator;
    private Boolean activeStatus;
    private String caAccount;
    private String baAccount;
    private String prefix;
    private String ccyName;
    private String decimalSeperate;
    private String afterDecimal;
    private String subfix;
    private BigDecimal convertRate;
    private String prefixEng;
    private String ccyNameEng;
    private String decimalSeperateEng;
    private String afterDecimalEng;
    private String subfixEng;
    private BigDecimal convertRateEng;
    private String prefixDefault;
    private String ccyNameDefault;
    private String decimalSeperateDefault;
    private String afterDecimalDefault;
    private String subfixDefault;
    private BigDecimal convertRateDefault;
    private String prefixDefaultEng;
    private String ccyNameDefaultEng;
    private String decimalSeperateDefaultEng;
    private String afterDecimalDefaultEng;
    private String subfixDefaultEng;
    private BigDecimal convertRateDefaultEng;
    private BigDecimal exampleAmount;
    private Integer sortOrder;
    private String valueOfMoney;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CurrencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Basic
    @Column(name = "CurrencyName")
    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "ExchangeRateOperator")
    public Boolean getExchangeRateOperator() {
        return exchangeRateOperator;
    }

    public void setExchangeRateOperator(Boolean exchangeRateOperator) {
        this.exchangeRateOperator = exchangeRateOperator;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CAAccount")
    public String getCaAccount() {
        return caAccount;
    }

    public void setCaAccount(String caAccount) {
        this.caAccount = caAccount;
    }

    @Basic
    @Column(name = "BAAccount")
    public String getBaAccount() {
        return baAccount;
    }

    public void setBaAccount(String baAccount) {
        this.baAccount = baAccount;
    }

    @Basic
    @Column(name = "Prefix")
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Basic
    @Column(name = "CCYName")
    public String getCcyName() {
        return ccyName;
    }

    public void setCcyName(String ccyName) {
        this.ccyName = ccyName;
    }

    @Basic
    @Column(name = "DecimalSeperate")
    public String getDecimalSeperate() {
        return decimalSeperate;
    }

    public void setDecimalSeperate(String decimalSeperate) {
        this.decimalSeperate = decimalSeperate;
    }

    @Basic
    @Column(name = "AfterDecimal")
    public String getAfterDecimal() {
        return afterDecimal;
    }

    public void setAfterDecimal(String afterDecimal) {
        this.afterDecimal = afterDecimal;
    }

    @Basic
    @Column(name = "Subfix")
    public String getSubfix() {
        return subfix;
    }

    public void setSubfix(String subfix) {
        this.subfix = subfix;
    }

    @Basic
    @Column(name = "ConvertRate")
    public BigDecimal getConvertRate() {
        return convertRate;
    }

    public void setConvertRate(BigDecimal convertRate) {
        this.convertRate = convertRate;
    }

    @Basic
    @Column(name = "PrefixENG")
    public String getPrefixEng() {
        return prefixEng;
    }

    public void setPrefixEng(String prefixEng) {
        this.prefixEng = prefixEng;
    }

    @Basic
    @Column(name = "CCYNameENG")
    public String getCcyNameEng() {
        return ccyNameEng;
    }

    public void setCcyNameEng(String ccyNameEng) {
        this.ccyNameEng = ccyNameEng;
    }

    @Basic
    @Column(name = "DecimalSeperateENG")
    public String getDecimalSeperateEng() {
        return decimalSeperateEng;
    }

    public void setDecimalSeperateEng(String decimalSeperateEng) {
        this.decimalSeperateEng = decimalSeperateEng;
    }

    @Basic
    @Column(name = "AfterDecimalENG")
    public String getAfterDecimalEng() {
        return afterDecimalEng;
    }

    public void setAfterDecimalEng(String afterDecimalEng) {
        this.afterDecimalEng = afterDecimalEng;
    }

    @Basic
    @Column(name = "SubfixENG")
    public String getSubfixEng() {
        return subfixEng;
    }

    public void setSubfixEng(String subfixEng) {
        this.subfixEng = subfixEng;
    }

    @Basic
    @Column(name = "ConvertRateENG")
    public BigDecimal getConvertRateEng() {
        return convertRateEng;
    }

    public void setConvertRateEng(BigDecimal convertRateEng) {
        this.convertRateEng = convertRateEng;
    }

    @Basic
    @Column(name = "PrefixDefault")
    public String getPrefixDefault() {
        return prefixDefault;
    }

    public void setPrefixDefault(String prefixDefault) {
        this.prefixDefault = prefixDefault;
    }

    @Basic
    @Column(name = "CCYNameDefault")
    public String getCcyNameDefault() {
        return ccyNameDefault;
    }

    public void setCcyNameDefault(String ccyNameDefault) {
        this.ccyNameDefault = ccyNameDefault;
    }

    @Basic
    @Column(name = "DecimalSeperateDefault")
    public String getDecimalSeperateDefault() {
        return decimalSeperateDefault;
    }

    public void setDecimalSeperateDefault(String decimalSeperateDefault) {
        this.decimalSeperateDefault = decimalSeperateDefault;
    }

    @Basic
    @Column(name = "AfterDecimalDefault")
    public String getAfterDecimalDefault() {
        return afterDecimalDefault;
    }

    public void setAfterDecimalDefault(String afterDecimalDefault) {
        this.afterDecimalDefault = afterDecimalDefault;
    }

    @Basic
    @Column(name = "SubfixDefault")
    public String getSubfixDefault() {
        return subfixDefault;
    }

    public void setSubfixDefault(String subfixDefault) {
        this.subfixDefault = subfixDefault;
    }

    @Basic
    @Column(name = "ConvertRateDefault")
    public BigDecimal getConvertRateDefault() {
        return convertRateDefault;
    }

    public void setConvertRateDefault(BigDecimal convertRateDefault) {
        this.convertRateDefault = convertRateDefault;
    }

    @Basic
    @Column(name = "PrefixDefaultENG")
    public String getPrefixDefaultEng() {
        return prefixDefaultEng;
    }

    public void setPrefixDefaultEng(String prefixDefaultEng) {
        this.prefixDefaultEng = prefixDefaultEng;
    }

    @Basic
    @Column(name = "CCYNameDefaultENG")
    public String getCcyNameDefaultEng() {
        return ccyNameDefaultEng;
    }

    public void setCcyNameDefaultEng(String ccyNameDefaultEng) {
        this.ccyNameDefaultEng = ccyNameDefaultEng;
    }

    @Basic
    @Column(name = "DecimalSeperateDefaultENG")
    public String getDecimalSeperateDefaultEng() {
        return decimalSeperateDefaultEng;
    }

    public void setDecimalSeperateDefaultEng(String decimalSeperateDefaultEng) {
        this.decimalSeperateDefaultEng = decimalSeperateDefaultEng;
    }

    @Basic
    @Column(name = "AfterDecimalDefaultENG")
    public String getAfterDecimalDefaultEng() {
        return afterDecimalDefaultEng;
    }

    public void setAfterDecimalDefaultEng(String afterDecimalDefaultEng) {
        this.afterDecimalDefaultEng = afterDecimalDefaultEng;
    }

    @Basic
    @Column(name = "SubfixDefaultENG")
    public String getSubfixDefaultEng() {
        return subfixDefaultEng;
    }

    public void setSubfixDefaultEng(String subfixDefaultEng) {
        this.subfixDefaultEng = subfixDefaultEng;
    }

    @Basic
    @Column(name = "ConvertRateDefaultENG")
    public BigDecimal getConvertRateDefaultEng() {
        return convertRateDefaultEng;
    }

    public void setConvertRateDefaultEng(BigDecimal convertRateDefaultEng) {
        this.convertRateDefaultEng = convertRateDefaultEng;
    }

    @Basic
    @Column(name = "ExampleAmount")
    public BigDecimal getExampleAmount() {
        return exampleAmount;
    }

    public void setExampleAmount(BigDecimal exampleAmount) {
        this.exampleAmount = exampleAmount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "ValueOfMoney")
    public String getValueOfMoney() {
        return valueOfMoney;
    }

    public void setValueOfMoney(String valueOfMoney) {
        this.valueOfMoney = valueOfMoney;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ccy ccy = (Ccy) o;
        return Objects.equals(id, ccy.id) &&
                Objects.equals(currencyCode, ccy.currencyCode) &&
                Objects.equals(currencyName, ccy.currencyName) &&
                Objects.equals(exchangeRate, ccy.exchangeRate) &&
                Objects.equals(exchangeRateOperator, ccy.exchangeRateOperator) &&
                Objects.equals(activeStatus, ccy.activeStatus) &&
                Objects.equals(caAccount, ccy.caAccount) &&
                Objects.equals(baAccount, ccy.baAccount) &&
                Objects.equals(prefix, ccy.prefix) &&
                Objects.equals(ccyName, ccy.ccyName) &&
                Objects.equals(decimalSeperate, ccy.decimalSeperate) &&
                Objects.equals(afterDecimal, ccy.afterDecimal) &&
                Objects.equals(subfix, ccy.subfix) &&
                Objects.equals(convertRate, ccy.convertRate) &&
                Objects.equals(prefixEng, ccy.prefixEng) &&
                Objects.equals(ccyNameEng, ccy.ccyNameEng) &&
                Objects.equals(decimalSeperateEng, ccy.decimalSeperateEng) &&
                Objects.equals(afterDecimalEng, ccy.afterDecimalEng) &&
                Objects.equals(subfixEng, ccy.subfixEng) &&
                Objects.equals(convertRateEng, ccy.convertRateEng) &&
                Objects.equals(prefixDefault, ccy.prefixDefault) &&
                Objects.equals(ccyNameDefault, ccy.ccyNameDefault) &&
                Objects.equals(decimalSeperateDefault, ccy.decimalSeperateDefault) &&
                Objects.equals(afterDecimalDefault, ccy.afterDecimalDefault) &&
                Objects.equals(subfixDefault, ccy.subfixDefault) &&
                Objects.equals(convertRateDefault, ccy.convertRateDefault) &&
                Objects.equals(prefixDefaultEng, ccy.prefixDefaultEng) &&
                Objects.equals(ccyNameDefaultEng, ccy.ccyNameDefaultEng) &&
                Objects.equals(decimalSeperateDefaultEng, ccy.decimalSeperateDefaultEng) &&
                Objects.equals(afterDecimalDefaultEng, ccy.afterDecimalDefaultEng) &&
                Objects.equals(subfixDefaultEng, ccy.subfixDefaultEng) &&
                Objects.equals(convertRateDefaultEng, ccy.convertRateDefaultEng) &&
                Objects.equals(exampleAmount, ccy.exampleAmount) &&
                Objects.equals(sortOrder, ccy.sortOrder) &&
                Objects.equals(valueOfMoney, ccy.valueOfMoney) &&
                Objects.equals(createdDate, ccy.createdDate) &&
                Objects.equals(createdBy, ccy.createdBy) &&
                Objects.equals(modifiedDate, ccy.modifiedDate) &&
                Objects.equals(modifiedBy, ccy.modifiedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, currencyCode, currencyName, exchangeRate, exchangeRateOperator, activeStatus, caAccount, baAccount, prefix, ccyName, decimalSeperate, afterDecimal, subfix, convertRate, prefixEng, ccyNameEng, decimalSeperateEng, afterDecimalEng, subfixEng, convertRateEng, prefixDefault, ccyNameDefault, decimalSeperateDefault, afterDecimalDefault, subfixDefault, convertRateDefault, prefixDefaultEng, ccyNameDefaultEng, decimalSeperateDefaultEng, afterDecimalDefaultEng, subfixDefaultEng, convertRateDefaultEng, exampleAmount, sortOrder, valueOfMoney, createdDate, createdBy, modifiedDate, modifiedBy);
    }
}
