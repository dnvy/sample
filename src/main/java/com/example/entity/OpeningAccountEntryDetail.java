package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class OpeningAccountEntryDetail {

    private Integer id; // PK
    private Integer refId; // FK
    private Integer employeeId; // Nhân viên
    private Integer projectWorkId; // Công trình/vụ việc
    private Integer contractId; // Hợp đồng bán
    private BigDecimal debitAmountOc; // NOT NULL. Dự nợ
    private BigDecimal debitAmount; // NOT NULL. Dư nợ quy đổi
    private BigDecimal creditAmountOc; // NOT NULL. Dư có
    private BigDecimal creditAmount; // NOT NULL. Dư có quy đổi
    private Integer sortOrder; // Thứ tự sắp xếp các dòng chi tiết
    private Integer puContractId; // Hợp đồng mua
    private Integer organizationUnitId;
    private Integer expenseItemId; // Khoản mục chi phí.
    private Integer jobId; // Đối tượng tập hợp chi phí.
    private Integer orderId; // Đơn đặt hàng (Đơn hàng bán)
    private Integer listItemId;
    private Integer puOrderRefId; // Đơn hàng mua

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RefID")
    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "ProjectWorkID")
    public Integer getProjectWorkId() {
        return projectWorkId;
    }

    public void setProjectWorkId(Integer projectWorkId) {
        this.projectWorkId = projectWorkId;
    }

    @Basic
    @Column(name = "ContractID")
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "DebitAmountOC")
    public BigDecimal getDebitAmountOc() {
        return debitAmountOc;
    }

    public void setDebitAmountOc(BigDecimal debitAmountOc) {
        this.debitAmountOc = debitAmountOc;
    }

    @Basic
    @Column(name = "DebitAmount")
    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    @Basic
    @Column(name = "CreditAmountOC")
    public BigDecimal getCreditAmountOc() {
        return creditAmountOc;
    }

    public void setCreditAmountOc(BigDecimal creditAmountOc) {
        this.creditAmountOc = creditAmountOc;
    }

    @Basic
    @Column(name = "CreditAmount")
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "PUContractID")
    public Integer getPuContractId() {
        return puContractId;
    }

    public void setPuContractId(Integer puContractId) {
        this.puContractId = puContractId;
    }

    @Basic
    @Column(name = "OrganizationUnitID")
    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    @Basic
    @Column(name = "ExpenseItemID")
    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    @Basic
    @Column(name = "JobID")
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "OrderID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ListItemID")
    public Integer getListItemId() {
        return listItemId;
    }

    public void setListItemId(Integer listItemId) {
        this.listItemId = listItemId;
    }

    @Basic
    @Column(name = "PUOrderRefID")
    public Integer getPuOrderRefId() {
        return puOrderRefId;
    }

    public void setPuOrderRefId(Integer puOrderRefId) {
        this.puOrderRefId = puOrderRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpeningAccountEntryDetail that = (OpeningAccountEntryDetail) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(projectWorkId, that.projectWorkId) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(debitAmountOc, that.debitAmountOc) &&
                Objects.equals(debitAmount, that.debitAmount) &&
                Objects.equals(creditAmountOc, that.creditAmountOc) &&
                Objects.equals(creditAmount, that.creditAmount) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(puContractId, that.puContractId) &&
                Objects.equals(organizationUnitId, that.organizationUnitId) &&
                Objects.equals(expenseItemId, that.expenseItemId) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(listItemId, that.listItemId) &&
                Objects.equals(puOrderRefId, that.puOrderRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refId, employeeId, projectWorkId, contractId, debitAmountOc, debitAmount, creditAmountOc, creditAmount, sortOrder, puContractId, organizationUnitId, expenseItemId, jobId, orderId, listItemId, puOrderRefId);
    }
}
