package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * Công trình.
 */
@Entity
public class ProjectWork {

    private Integer id; // PK
    private Integer projectWorkType; // NOT NULL. Loại công trình/Vụ việc (0=Công trình;1=Hạng mục công trình)
    private Integer projectWorkCategoryId; // FK
    private String projectWorkCode; // NOT NULL. Mã công trình, vụ việc
    private String projectWorkName; // NOT NULL  Tên công trình, vụ việc

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date startDate; // Ngày bắt đầu

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date finishDate; // Ngày kết thúc

    private BigDecimal estimateAmount; // Dự toán

    //TODO: Đặt lại tên là sponsor.
    private String stakeholder; // Chủ đầu tư
    private String stakeholderAddress; // Địa chỉ chủ đầu tư
    private String description; // Mô tả tóm tắt
    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent; // NOT NULL.
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer branchId; // Chi nhánh
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.
    private Integer projectWorkStatus; // NOT NULL. Tình trạng của dự án. 0 = Đang thực hiện; 1 = Đã hoàn thành

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ProjectWorkType")
    public Integer getProjectWorkType() {
        return projectWorkType;
    }

    public void setProjectWorkType(Integer projectWorkType) {
        this.projectWorkType = projectWorkType;
    }

    @Basic
    @Column(name = "ProjectWorkCategoryID")
    public Integer getProjectWorkCategoryId() {
        return projectWorkCategoryId;
    }

    public void setProjectWorkCategoryId(Integer projectWorkCategoryId) {
        this.projectWorkCategoryId = projectWorkCategoryId;
    }

    @Basic
    @Column(name = "ProjectWorkCode")
    public String getProjectWorkCode() {
        return projectWorkCode;
    }

    public void setProjectWorkCode(String projectWorkCode) {
        this.projectWorkCode = projectWorkCode;
    }

    @Basic
    @Column(name = "ProjectWorkName")
    public String getProjectWorkName() {
        return projectWorkName;
    }

    public void setProjectWorkName(String projectWorkName) {
        this.projectWorkName = projectWorkName;
    }

    @Basic
    @Column(name = "StartDate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "FinishDate")
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    @Basic
    @Column(name = "EstimateAmount")
    public BigDecimal getEstimateAmount() {
        return estimateAmount;
    }

    public void setEstimateAmount(BigDecimal estimateAmount) {
        this.estimateAmount = estimateAmount;
    }

    @Basic
    @Column(name = "Stakeholder")
    public String getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(String stakeholder) {
        this.stakeholder = stakeholder;
    }

    @Basic
    @Column(name = "StakeholderAddress")
    public String getStakeholderAddress() {
        return stakeholderAddress;
    }

    public void setStakeholderAddress(String stakeholderAddress) {
        this.stakeholderAddress = stakeholderAddress;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Basic
    @Column(name = "ProjectWorkStatus")
    public Integer getProjectWorkStatus() {
        return projectWorkStatus;
    }

    public void setProjectWorkStatus(Integer projectWorkStatus) {
        this.projectWorkStatus = projectWorkStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectWork that = (ProjectWork) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(projectWorkType, that.projectWorkType) &&
                Objects.equals(projectWorkCategoryId, that.projectWorkCategoryId) &&
                Objects.equals(projectWorkCode, that.projectWorkCode) &&
                Objects.equals(projectWorkName, that.projectWorkName) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(finishDate, that.finishDate) &&
                Objects.equals(estimateAmount, that.estimateAmount) &&
                Objects.equals(stakeholder, that.stakeholder) &&
                Objects.equals(stakeholderAddress, that.stakeholderAddress) &&
                Objects.equals(description, that.description) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(vyCodeId, that.vyCodeId) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(isParent, that.isParent) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(sortVyCodeId, that.sortVyCodeId) &&
                Objects.equals(projectWorkStatus, that.projectWorkStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, projectWorkType, projectWorkCategoryId, projectWorkCode, projectWorkName, startDate, finishDate, estimateAmount, stakeholder, stakeholderAddress, description, parentId, vyCodeId, grade, isParent, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, branchId, sortVyCodeId, projectWorkStatus);
    }
}
