package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "SAInvoice")
public class SaInvoice {

    private Integer id; // NOT NULL. PK
    private Integer branchId; // NOT NULL. Mã chi nhánh
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer refType; // NOT NULL. Loại chứng từ (lấy từ bảng RefType)
    private Integer accountObjectId; // Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectTaxCode; // Mã số thuế của khách hàng
    private String accountObjectBankAccount; // Tài khoản của khách hàng
    private String paymentMethod; // Hình thức thanh toán
    private String buyer; // Người mua hàng
    private Integer employeeId; // Nhân viên
    private Boolean isPaid; // NOT NULL. Đã thanh toán đủ
    private Boolean isPosted; // NOT NULL. Đã hạch toán
    private String invTemplateNo; // Mẫu số hóa đơn
    private String invSeries; // Ký hiệu hóa đơn
    private String invNo; // Số hóa đơn

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date invDate; // Ngày hóa đơn

    private Boolean includeInvoice; // NOT NULL. Có lập kèm hóa đơn hay không? (Cho tiện việc cập nhật, nếu có lập kèm thì cập nhật đồng thời sang Chứng từ bán hàng)
    private Boolean isAttachList; // NOT NULL. In kèm bảng kê
    private String listNo; // Số bảng kê

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date listDate; // ngày in bảng kê

    private String commonInventoryName;
    private String contractCode;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date contractDate;

    private String placeOfDelivery;
    private String placeOfReceipt;
    private String billOfLadingNo;
    private String containerNo;
    private String transportName;
    private String currencyId;
    private BigDecimal exchangeRate;
    private BigDecimal totalSaleAmountOc; // NOT NULL.
    private BigDecimal totalSaleAmount; // NOT NULL.
    private BigDecimal totalDiscountAmountOc; // NOT NULL.
    private BigDecimal totalDiscountAmount; // NOT NULL.
    private BigDecimal totalVatAmountOc; // NOT NULL.
    private BigDecimal totalVatAmount; // NOT NULL.
    private BigDecimal totalAmountOc; // NOT NULL.
    private BigDecimal totalAmount; // NOT NULL.
    private Integer editVersion; // NOT NULL.
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer invTypeId;
    private Boolean isBranchIssued;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Boolean isPostedLastYear; // NOT NULL.
    private Boolean isInvoiceReplace; // NOT NULL.
    private Integer adjustRefId;
    private Integer adjustRefType;
    private String journalMemo;
    private String adjustJournalMemo;
    private Integer adjustPurchasePurposeId;
    private BigDecimal adjustVatRate;
    private Integer adjustTaCareerGroupId;
    private String adjustInvTemplateNo;
    private String adjustInvSeries;
    private String adjustInvNo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date adjustInvDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "DisplayOnBook")
    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "AccountObjectBankAccount")
    public String getAccountObjectBankAccount() {
        return accountObjectBankAccount;
    }

    public void setAccountObjectBankAccount(String accountObjectBankAccount) {
        this.accountObjectBankAccount = accountObjectBankAccount;
    }

    @Basic
    @Column(name = "PaymentMethod")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Basic
    @Column(name = "Buyer")
    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "IsPaid")
    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean paid) {
        isPaid = paid;
    }

    @Basic
    @Column(name = "IsPosted")
    public Boolean getIsPosted() {
        return isPosted;
    }

    public void setIsPosted(Boolean posted) {
        isPosted = posted;
    }

    @Basic
    @Column(name = "InvTemplateNo")
    public String getInvTemplateNo() {
        return invTemplateNo;
    }

    public void setInvTemplateNo(String invTemplateNo) {
        this.invTemplateNo = invTemplateNo;
    }

    @Basic
    @Column(name = "InvSeries")
    public String getInvSeries() {
        return invSeries;
    }

    public void setInvSeries(String invSeries) {
        this.invSeries = invSeries;
    }

    @Basic
    @Column(name = "InvNo")
    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    @Basic
    @Column(name = "InvDate")
    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    @Basic
    @Column(name = "IncludeInvoice")
    public Boolean getIncludeInvoice() {
        return includeInvoice;
    }

    public void setIncludeInvoice(Boolean includeInvoice) {
        this.includeInvoice = includeInvoice;
    }

    @Basic
    @Column(name = "IsAttachList")
    public Boolean getIsAttachList() {
        return isAttachList;
    }

    public void setIsAttachList(Boolean attachList) {
        isAttachList = attachList;
    }

    @Basic
    @Column(name = "ListNo")
    public String getListNo() {
        return listNo;
    }

    public void setListNo(String listNo) {
        this.listNo = listNo;
    }

    @Basic
    @Column(name = "ListDate")
    public Date getListDate() {
        return listDate;
    }

    public void setListDate(Date listDate) {
        this.listDate = listDate;
    }

    @Basic
    @Column(name = "CommonInventoryName")
    public String getCommonInventoryName() {
        return commonInventoryName;
    }

    public void setCommonInventoryName(String commonInventoryName) {
        this.commonInventoryName = commonInventoryName;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "ContractDate")
    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    @Basic
    @Column(name = "PlaceOfDelivery")
    public String getPlaceOfDelivery() {
        return placeOfDelivery;
    }

    public void setPlaceOfDelivery(String placeOfDelivery) {
        this.placeOfDelivery = placeOfDelivery;
    }

    @Basic
    @Column(name = "PlaceOfReceipt")
    public String getPlaceOfReceipt() {
        return placeOfReceipt;
    }

    public void setPlaceOfReceipt(String placeOfReceipt) {
        this.placeOfReceipt = placeOfReceipt;
    }

    @Basic
    @Column(name = "BillOfLadingNo")
    public String getBillOfLadingNo() {
        return billOfLadingNo;
    }

    public void setBillOfLadingNo(String billOfLadingNo) {
        this.billOfLadingNo = billOfLadingNo;
    }

    @Basic
    @Column(name = "ContainerNo")
    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    @Basic
    @Column(name = "TransportName")
    public String getTransportName() {
        return transportName;
    }

    public void setTransportName(String transportName) {
        this.transportName = transportName;
    }

    @Basic
    @Column(name = "CurrencyID")
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "TotalSaleAmountOC")
    public BigDecimal getTotalSaleAmountOc() {
        return totalSaleAmountOc;
    }

    public void setTotalSaleAmountOc(BigDecimal totalSaleAmountOc) {
        this.totalSaleAmountOc = totalSaleAmountOc;
    }

    @Basic
    @Column(name = "TotalSaleAmount")
    public BigDecimal getTotalSaleAmount() {
        return totalSaleAmount;
    }

    public void setTotalSaleAmount(BigDecimal totalSaleAmount) {
        this.totalSaleAmount = totalSaleAmount;
    }

    @Basic
    @Column(name = "TotalDiscountAmountOC")
    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    @Basic
    @Column(name = "TotalDiscountAmount")
    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    @Basic
    @Column(name = "TotalVATAmountOC")
    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    @Basic
    @Column(name = "TotalVATAmount")
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    @Basic
    @Column(name = "TotalAmountOC")
    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    @Basic
    @Column(name = "TotalAmount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "EditVersion")
    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "InvTypeID")
    public Integer getInvTypeId() {
        return invTypeId;
    }

    public void setInvTypeId(Integer invTypeId) {
        this.invTypeId = invTypeId;
    }

    @Basic
    @Column(name = "isBranchIssued")
    public Boolean getIsBranchIssued() {
        return isBranchIssued;
    }

    public void setIsBranchIssued(Boolean branchIssued) {
        isBranchIssued = branchIssued;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Basic
    @Column(name = "IsPostedLastYear")
    public Boolean getIsPostedLastYear() {
        return isPostedLastYear;
    }

    public void setIsPostedLastYear(Boolean postedLastYear) {
        isPostedLastYear = postedLastYear;
    }

    @Basic
    @Column(name = "IsInvoiceReplace")
    public Boolean getIsInvoiceReplace() {
        return isInvoiceReplace;
    }

    public void setIsInvoiceReplace(Boolean invoiceReplace) {
        isInvoiceReplace = invoiceReplace;
    }

    @Basic
    @Column(name = "AdjustRefID")
    public Integer getAdjustRefId() {
        return adjustRefId;
    }

    public void setAdjustRefId(Integer adjustRefId) {
        this.adjustRefId = adjustRefId;
    }

    @Basic
    @Column(name = "AdjustRefType")
    public Integer getAdjustRefType() {
        return adjustRefType;
    }

    public void setAdjustRefType(Integer adjustRefType) {
        this.adjustRefType = adjustRefType;
    }

    @Basic
    @Column(name = "JournalMemo")
    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    @Basic
    @Column(name = "AdjustJournalMemo")
    public String getAdjustJournalMemo() {
        return adjustJournalMemo;
    }

    public void setAdjustJournalMemo(String adjustJournalMemo) {
        this.adjustJournalMemo = adjustJournalMemo;
    }

    @Basic
    @Column(name = "AdjustPurchasePurposeID")
    public Integer getAdjustPurchasePurposeId() {
        return adjustPurchasePurposeId;
    }

    public void setAdjustPurchasePurposeId(Integer adjustPurchasePurposeId) {
        this.adjustPurchasePurposeId = adjustPurchasePurposeId;
    }

    @Basic
    @Column(name = "AdjustVATRate")
    public BigDecimal getAdjustVatRate() {
        return adjustVatRate;
    }

    public void setAdjustVatRate(BigDecimal adjustVatRate) {
        this.adjustVatRate = adjustVatRate;
    }

    @Basic
    @Column(name = "AdjustTACareerGroupID")
    public Integer getAdjustTaCareerGroupId() {
        return adjustTaCareerGroupId;
    }

    public void setAdjustTaCareerGroupId(Integer adjustTaCareerGroupId) {
        this.adjustTaCareerGroupId = adjustTaCareerGroupId;
    }

    @Basic
    @Column(name = "AdjustInvTemplateNo")
    public String getAdjustInvTemplateNo() {
        return adjustInvTemplateNo;
    }

    public void setAdjustInvTemplateNo(String adjustInvTemplateNo) {
        this.adjustInvTemplateNo = adjustInvTemplateNo;
    }

    @Basic
    @Column(name = "AdjustInvSeries")
    public String getAdjustInvSeries() {
        return adjustInvSeries;
    }

    public void setAdjustInvSeries(String adjustInvSeries) {
        this.adjustInvSeries = adjustInvSeries;
    }

    @Basic
    @Column(name = "AdjustInvNo")
    public String getAdjustInvNo() {
        return adjustInvNo;
    }

    public void setAdjustInvNo(String adjustInvNo) {
        this.adjustInvNo = adjustInvNo;
    }

    @Basic
    @Column(name = "AdjustInvDate")
    public Date getAdjustInvDate() {
        return adjustInvDate;
    }

    public void setAdjustInvDate(Date adjustInvDate) {
        this.adjustInvDate = adjustInvDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaInvoice saInvoice = (SaInvoice) o;
        return Objects.equals(id, saInvoice.id) &&
                Objects.equals(branchId, saInvoice.branchId) &&
                Objects.equals(displayOnBook, saInvoice.displayOnBook) &&
                Objects.equals(refType, saInvoice.refType) &&
                Objects.equals(accountObjectId, saInvoice.accountObjectId) &&
                Objects.equals(accountObjectName, saInvoice.accountObjectName) &&
                Objects.equals(accountObjectAddress, saInvoice.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, saInvoice.accountObjectTaxCode) &&
                Objects.equals(accountObjectBankAccount, saInvoice.accountObjectBankAccount) &&
                Objects.equals(paymentMethod, saInvoice.paymentMethod) &&
                Objects.equals(buyer, saInvoice.buyer) &&
                Objects.equals(employeeId, saInvoice.employeeId) &&
                Objects.equals(isPaid, saInvoice.isPaid) &&
                Objects.equals(isPosted, saInvoice.isPosted) &&
                Objects.equals(invTemplateNo, saInvoice.invTemplateNo) &&
                Objects.equals(invSeries, saInvoice.invSeries) &&
                Objects.equals(invNo, saInvoice.invNo) &&
                Objects.equals(invDate, saInvoice.invDate) &&
                Objects.equals(includeInvoice, saInvoice.includeInvoice) &&
                Objects.equals(isAttachList, saInvoice.isAttachList) &&
                Objects.equals(listNo, saInvoice.listNo) &&
                Objects.equals(listDate, saInvoice.listDate) &&
                Objects.equals(commonInventoryName, saInvoice.commonInventoryName) &&
                Objects.equals(contractCode, saInvoice.contractCode) &&
                Objects.equals(contractDate, saInvoice.contractDate) &&
                Objects.equals(placeOfDelivery, saInvoice.placeOfDelivery) &&
                Objects.equals(placeOfReceipt, saInvoice.placeOfReceipt) &&
                Objects.equals(billOfLadingNo, saInvoice.billOfLadingNo) &&
                Objects.equals(containerNo, saInvoice.containerNo) &&
                Objects.equals(transportName, saInvoice.transportName) &&
                Objects.equals(currencyId, saInvoice.currencyId) &&
                Objects.equals(exchangeRate, saInvoice.exchangeRate) &&
                Objects.equals(totalSaleAmountOc, saInvoice.totalSaleAmountOc) &&
                Objects.equals(totalSaleAmount, saInvoice.totalSaleAmount) &&
                Objects.equals(totalDiscountAmountOc, saInvoice.totalDiscountAmountOc) &&
                Objects.equals(totalDiscountAmount, saInvoice.totalDiscountAmount) &&
                Objects.equals(totalVatAmountOc, saInvoice.totalVatAmountOc) &&
                Objects.equals(totalVatAmount, saInvoice.totalVatAmount) &&
                Objects.equals(totalAmountOc, saInvoice.totalAmountOc) &&
                Objects.equals(totalAmount, saInvoice.totalAmount) &&
                Objects.equals(editVersion, saInvoice.editVersion) &&
                Objects.equals(createdDate, saInvoice.createdDate) &&
                Objects.equals(createdBy, saInvoice.createdBy) &&
                Objects.equals(modifiedDate, saInvoice.modifiedDate) &&
                Objects.equals(modifiedBy, saInvoice.modifiedBy) &&
                Objects.equals(invTypeId, saInvoice.invTypeId) &&
                Objects.equals(isBranchIssued, saInvoice.isBranchIssued) &&
                Objects.equals(customField1, saInvoice.customField1) &&
                Objects.equals(customField2, saInvoice.customField2) &&
                Objects.equals(customField3, saInvoice.customField3) &&
                Objects.equals(customField4, saInvoice.customField4) &&
                Objects.equals(customField5, saInvoice.customField5) &&
                Objects.equals(customField6, saInvoice.customField6) &&
                Objects.equals(customField7, saInvoice.customField7) &&
                Objects.equals(customField8, saInvoice.customField8) &&
                Objects.equals(customField9, saInvoice.customField9) &&
                Objects.equals(customField10, saInvoice.customField10) &&
                Objects.equals(isPostedLastYear, saInvoice.isPostedLastYear) &&
                Objects.equals(isInvoiceReplace, saInvoice.isInvoiceReplace) &&
                Objects.equals(adjustRefId, saInvoice.adjustRefId) &&
                Objects.equals(adjustRefType, saInvoice.adjustRefType) &&
                Objects.equals(journalMemo, saInvoice.journalMemo) &&
                Objects.equals(adjustJournalMemo, saInvoice.adjustJournalMemo) &&
                Objects.equals(adjustPurchasePurposeId, saInvoice.adjustPurchasePurposeId) &&
                Objects.equals(adjustVatRate, saInvoice.adjustVatRate) &&
                Objects.equals(adjustTaCareerGroupId, saInvoice.adjustTaCareerGroupId) &&
                Objects.equals(adjustInvTemplateNo, saInvoice.adjustInvTemplateNo) &&
                Objects.equals(adjustInvSeries, saInvoice.adjustInvSeries) &&
                Objects.equals(adjustInvNo, saInvoice.adjustInvNo) &&
                Objects.equals(adjustInvDate, saInvoice.adjustInvDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, displayOnBook, refType, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, accountObjectBankAccount, paymentMethod, buyer, employeeId, isPaid, isPosted, invTemplateNo, invSeries, invNo, invDate, includeInvoice, isAttachList, listNo, listDate, commonInventoryName, contractCode, contractDate, placeOfDelivery, placeOfReceipt, billOfLadingNo, containerNo, transportName, currencyId, exchangeRate, totalSaleAmountOc, totalSaleAmount, totalDiscountAmountOc, totalDiscountAmount, totalVatAmountOc, totalVatAmount, totalAmountOc, totalAmount, editVersion, createdDate, createdBy, modifiedDate, modifiedBy, invTypeId, isBranchIssued, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10, isPostedLastYear, isInvoiceReplace, adjustRefId, adjustRefType, journalMemo, adjustJournalMemo, adjustPurchasePurposeId, adjustVatRate, adjustTaCareerGroupId, adjustInvTemplateNo, adjustInvSeries, adjustInvNo, adjustInvDate);
    }

}
