package com.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Hợp đồng mua hàng.
 */
@Entity
@Table(name = "PUContract")
public class PuContract {

    private Integer id; // NOT NULL. PK
    private Integer branchId; // Chi nhánh
    private String contractCode; // Số hợp đồng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date signDate; // NOT NULL. Ngày ký

    private String subject; // Trích yếu
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá
    private BigDecimal amountOc; // NOT NULL. Giá trị hợp đồng
    private BigDecimal amount; // NOT NULL. Giá trị hợp đồng Quy đổi
    private Integer accountObjectId; // ID Nhà cung cấp
    private String accountObjectName; // Tên nhà cung cấp
    private String accountObjectAddress; // Địa chỉ nhà cung cấp
    private String accountObjectTaxCode; // Mã số thuế NCC
    private String accountObjectContactName; // Người liên hệ
    private Integer status; // NOT NULL. Tình trạng hợp đồng. 0: Chưa thực hiện; 1: Đang thực hiện; 2: Hoàn thành; 3: Đã thanh lý; 4: Đã hủy bỏ
    private BigDecimal closeAmountOc; // NOT NULL. Giá trị thanh lý. OC: Original currency
    private BigDecimal closeAmount; // NOT NULL. Giá trị thanh lý Quy đổi
    private BigDecimal paidAmountOc; // NOT NULL. Số đã trả
    private BigDecimal paidAmount; // NOT NULL. Số đã trả Quy đổi
    private BigDecimal payableAmountOc; // NOT NULL. Số còn phải trả
    private BigDecimal payableAmount; // NOT NULL. Số còn phải trả Quy đổi

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date closeDate; // Ngày thanh lý/Hủy bỏ

    private String closeReason; // Lý do

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliverDueDate; // Hạn giao hàng

    private String deliverAddress;  // Địa chỉ giao hàng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate; // Hạn thanh toán

    private Integer employeeId; // ID nhân viên
    private String paymentTerm; // Điều khoản khác
    private Integer refType; // Loai chung tu
    private Boolean isArisedBeforeUseSoftware; // Là hợp đồng phát sinh trước khi sử dụng phần mềm
    private BigDecimal executedAmountFinance;
    private BigDecimal executedAmountManagement;
    private BigDecimal paidAmountFinance;
    private BigDecimal paidAmountManagement;
    private Integer puOrderId; // Mã đơn mua hàng
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "ContractCode")
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Basic
    @Column(name = "SignDate")
    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    @Basic
    @Column(name = "Subject")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "CurrencyID")
    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "ExchangeRate")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "AmountOC")
    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    @Basic
    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "AccountObjectName")
    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    @Basic
    @Column(name = "AccountObjectAddress")
    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    @Basic
    @Column(name = "AccountObjectTaxCode")
    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    @Basic
    @Column(name = "AccountObjectContactName")
    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    @Basic
    @Column(name = "Status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CloseAmountOC")
    public BigDecimal getCloseAmountOc() {
        return closeAmountOc;
    }

    public void setCloseAmountOc(BigDecimal closeAmountOc) {
        this.closeAmountOc = closeAmountOc;
    }

    @Basic
    @Column(name = "CloseAmount")
    public BigDecimal getCloseAmount() {
        return closeAmount;
    }

    public void setCloseAmount(BigDecimal closeAmount) {
        this.closeAmount = closeAmount;
    }

    @Basic
    @Column(name = "PaidAmountOC")
    public BigDecimal getPaidAmountOc() {
        return paidAmountOc;
    }

    public void setPaidAmountOc(BigDecimal paidAmountOc) {
        this.paidAmountOc = paidAmountOc;
    }

    @Basic
    @Column(name = "PaidAmount")
    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    @Basic
    @Column(name = "PayableAmountOC")
    public BigDecimal getPayableAmountOc() {
        return payableAmountOc;
    }

    public void setPayableAmountOc(BigDecimal payableAmountOc) {
        this.payableAmountOc = payableAmountOc;
    }

    @Basic
    @Column(name = "PayableAmount")
    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    @Basic
    @Column(name = "CloseDate")
    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    @Basic
    @Column(name = "CloseReason")
    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    @Basic
    @Column(name = "DeliverDueDate")
    public Date getDeliverDueDate() {
        return deliverDueDate;
    }

    public void setDeliverDueDate(Date deliverDueDate) {
        this.deliverDueDate = deliverDueDate;
    }

    @Basic
    @Column(name = "DeliverAddress")
    public String getDeliverAddress() {
        return deliverAddress;
    }

    public void setDeliverAddress(String deliverAddress) {
        this.deliverAddress = deliverAddress;
    }

    @Basic
    @Column(name = "DueDate")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "EmployeeID")
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "PaymentTerm")
    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "IsArisedBeforeUseSoftware")
    public Boolean getIsArisedBeforeUseSoftware() {
        return isArisedBeforeUseSoftware;
    }

    public void setIsArisedBeforeUseSoftware(Boolean arisedBeforeUseSoftware) {
        isArisedBeforeUseSoftware = arisedBeforeUseSoftware;
    }

    @Basic
    @Column(name = "ExecutedAmountFinance")
    public BigDecimal getExecutedAmountFinance() {
        return executedAmountFinance;
    }

    public void setExecutedAmountFinance(BigDecimal executedAmountFinance) {
        this.executedAmountFinance = executedAmountFinance;
    }

    @Basic
    @Column(name = "ExecutedAmountManagement")
    public BigDecimal getExecutedAmountManagement() {
        return executedAmountManagement;
    }

    public void setExecutedAmountManagement(BigDecimal executedAmountManagement) {
        this.executedAmountManagement = executedAmountManagement;
    }

    @Basic
    @Column(name = "PaidAmountFinance")
    public BigDecimal getPaidAmountFinance() {
        return paidAmountFinance;
    }

    public void setPaidAmountFinance(BigDecimal paidAmountFinance) {
        this.paidAmountFinance = paidAmountFinance;
    }

    @Basic
    @Column(name = "PaidAmountManagement")
    public BigDecimal getPaidAmountManagement() {
        return paidAmountManagement;
    }

    public void setPaidAmountManagement(BigDecimal paidAmountManagement) {
        this.paidAmountManagement = paidAmountManagement;
    }

    @Basic
    @Column(name = "PUOrderID")
    public Integer getPuOrderId() {
        return puOrderId;
    }

    public void setPuOrderId(Integer puOrderId) {
        this.puOrderId = puOrderId;
    }

    @Basic
    @Column(name = "CustomField1")
    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    @Basic
    @Column(name = "CustomField2")
    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    @Basic
    @Column(name = "CustomField3")
    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    @Basic
    @Column(name = "CustomField4")
    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    @Basic
    @Column(name = "CustomField5")
    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    @Basic
    @Column(name = "CustomField6")
    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    @Basic
    @Column(name = "CustomField7")
    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    @Basic
    @Column(name = "CustomField8")
    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    @Basic
    @Column(name = "CustomField9")
    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    @Basic
    @Column(name = "CustomField10")
    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PuContract that = (PuContract) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(branchId, that.branchId) &&
                Objects.equals(contractCode, that.contractCode) &&
                Objects.equals(signDate, that.signDate) &&
                Objects.equals(subject, that.subject) &&
                Objects.equals(currencyId, that.currencyId) &&
                Objects.equals(exchangeRate, that.exchangeRate) &&
                Objects.equals(amountOc, that.amountOc) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(accountObjectName, that.accountObjectName) &&
                Objects.equals(accountObjectAddress, that.accountObjectAddress) &&
                Objects.equals(accountObjectTaxCode, that.accountObjectTaxCode) &&
                Objects.equals(accountObjectContactName, that.accountObjectContactName) &&
                Objects.equals(status, that.status) &&
                Objects.equals(closeAmountOc, that.closeAmountOc) &&
                Objects.equals(closeAmount, that.closeAmount) &&
                Objects.equals(paidAmountOc, that.paidAmountOc) &&
                Objects.equals(paidAmount, that.paidAmount) &&
                Objects.equals(payableAmountOc, that.payableAmountOc) &&
                Objects.equals(payableAmount, that.payableAmount) &&
                Objects.equals(closeDate, that.closeDate) &&
                Objects.equals(closeReason, that.closeReason) &&
                Objects.equals(deliverDueDate, that.deliverDueDate) &&
                Objects.equals(deliverAddress, that.deliverAddress) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(employeeId, that.employeeId) &&
                Objects.equals(paymentTerm, that.paymentTerm) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(isArisedBeforeUseSoftware, that.isArisedBeforeUseSoftware) &&
                Objects.equals(executedAmountFinance, that.executedAmountFinance) &&
                Objects.equals(executedAmountManagement, that.executedAmountManagement) &&
                Objects.equals(paidAmountFinance, that.paidAmountFinance) &&
                Objects.equals(paidAmountManagement, that.paidAmountManagement) &&
                Objects.equals(puOrderId, that.puOrderId) &&
                Objects.equals(customField1, that.customField1) &&
                Objects.equals(customField2, that.customField2) &&
                Objects.equals(customField3, that.customField3) &&
                Objects.equals(customField4, that.customField4) &&
                Objects.equals(customField5, that.customField5) &&
                Objects.equals(customField6, that.customField6) &&
                Objects.equals(customField7, that.customField7) &&
                Objects.equals(customField8, that.customField8) &&
                Objects.equals(customField9, that.customField9) &&
                Objects.equals(customField10, that.customField10);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, branchId, contractCode, signDate, subject, currencyId, exchangeRate, amountOc, amount, accountObjectId, accountObjectName, accountObjectAddress, accountObjectTaxCode, accountObjectContactName, status, closeAmountOc, closeAmount, paidAmountOc, paidAmount, payableAmountOc, payableAmount, closeDate, closeReason, deliverDueDate, deliverAddress, dueDate, employeeId, paymentTerm, refType, isArisedBeforeUseSoftware, executedAmountFinance, executedAmountManagement, paidAmountFinance, paidAmountManagement, puOrderId, customField1, customField2, customField3, customField4, customField5, customField6, customField7, customField8, customField9, customField10);
    }
}
