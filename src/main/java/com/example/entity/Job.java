package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Đối tượng tập hợp chi phí.
 */
@Entity
public class Job {

    private Integer id; // PK Công việc, công trình dự án
    private String jobCode; // Mã công việc
    private String jobName; // Tên Công việc
    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent;
    private Integer jobType; // Loại có giá trị: 0 = Phân xưởng, 1 = Sản phẩm, 2 = Quy trình sản xuất, 3 = Công đoạn
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isSystem; // Là hệ thống
    private Integer inventoryItemId; // Thành phẩm
    private Integer productionProcessType; // Loại quy trình sản xuất (0=Phân bước liên tục;1=phân bước song song)
    private Integer stage; // Công đoạn thứ n
    private Integer stageId; // Công đoạn trước
    private Integer branchId; // Chi nhánh
    private Integer collectCostInStageType; // 0:Tập hợp chi phí đến công đoạn; 1: Tập hợp chi phi đến từng sản phẩm trong công đoạn.
    private Boolean isSemiProduct; // Là bán thành phẩm của công đoạn
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "JobCode")
    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    @Basic
    @Column(name = "JobName")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "ParentID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "VyCodeID")
    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    @Basic
    @Column(name = "Grade")
    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Basic
    @Column(name = "IsParent")
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    @Basic
    @Column(name = "JobType")
    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "InventoryItemID")
    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Basic
    @Column(name = "ProductionProcessType")
    public Integer getProductionProcessType() {
        return productionProcessType;
    }

    public void setProductionProcessType(Integer productionProcessType) {
        this.productionProcessType = productionProcessType;
    }

    @Basic
    @Column(name = "Stage")
    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    @Basic
    @Column(name = "StageID")
    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    @Basic
    @Column(name = "BranchID")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Basic
    @Column(name = "CollectCostInStageType")
    public Integer getCollectCostInStageType() {
        return collectCostInStageType;
    }

    public void setCollectCostInStageType(Integer collectCostInStageType) {
        this.collectCostInStageType = collectCostInStageType;
    }

    @Basic
    @Column(name = "IsSemiProduct")
    public Boolean getIsSemiProduct() {
        return isSemiProduct;
    }

    public void setIsSemiProduct(Boolean semiProduct) {
        isSemiProduct = semiProduct;
    }

    @Basic
    @Column(name = "SortVyCodeID")
    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return Objects.equals(id, job.id) &&
                Objects.equals(jobCode, job.jobCode) &&
                Objects.equals(jobName, job.jobName) &&
                Objects.equals(parentId, job.parentId) &&
                Objects.equals(vyCodeId, job.vyCodeId) &&
                Objects.equals(grade, job.grade) &&
                Objects.equals(isParent, job.isParent) &&
                Objects.equals(jobType, job.jobType) &&
                Objects.equals(description, job.description) &&
                Objects.equals(activeStatus, job.activeStatus) &&
                Objects.equals(createdDate, job.createdDate) &&
                Objects.equals(createdBy, job.createdBy) &&
                Objects.equals(modifiedDate, job.modifiedDate) &&
                Objects.equals(modifiedBy, job.modifiedBy) &&
                Objects.equals(isSystem, job.isSystem) &&
                Objects.equals(inventoryItemId, job.inventoryItemId) &&
                Objects.equals(productionProcessType, job.productionProcessType) &&
                Objects.equals(stage, job.stage) &&
                Objects.equals(stageId, job.stageId) &&
                Objects.equals(branchId, job.branchId) &&
                Objects.equals(collectCostInStageType, job.collectCostInStageType) &&
                Objects.equals(isSemiProduct, job.isSemiProduct) &&
                Objects.equals(sortVyCodeId, job.sortVyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, jobCode, jobName, parentId, vyCodeId, grade, isParent, jobType, description, activeStatus, createdDate, createdBy, modifiedDate, modifiedBy, isSystem, inventoryItemId, productionProcessType, stage, stageId, branchId, collectCostInStageType, isSemiProduct, sortVyCodeId);
    }

}
