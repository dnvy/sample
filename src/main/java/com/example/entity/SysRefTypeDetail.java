package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Danh sách toàn bộ nghiệp vụ trong hệ thống. Và cách ghi reference vào các table tương ứng.
 */
@Entity
@Table(name = "SYSRefTypeDetail")
public class SysRefTypeDetail {

    private Integer refTypeDetailId;
    private Integer refType;
    private String refTypeName;
    private Integer refTypeCategory;
    private String refNoFinanceColumnName;
    private String refNoManagementColumnName;
    private String refDateColumnName;
    private String postedDateColumnName;
    private String detailTableName;
    private String masterTableName;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RefTypeDetailID")
    public Integer getRefTypeDetailId() {
        return refTypeDetailId;
    }

    public void setRefTypeDetailId(Integer refTypeDetailId) {
        this.refTypeDetailId = refTypeDetailId;
    }

    @Basic
    @Column(name = "RefType")
    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    @Basic
    @Column(name = "RefTypeName")
    public String getRefTypeName() {
        return refTypeName;
    }

    public void setRefTypeName(String refTypeName) {
        this.refTypeName = refTypeName;
    }

    @Basic
    @Column(name = "RefTypeCategory")
    public Integer getRefTypeCategory() {
        return refTypeCategory;
    }

    public void setRefTypeCategory(Integer refTypeCategory) {
        this.refTypeCategory = refTypeCategory;
    }

    @Basic
    @Column(name = "RefNoFinanceColumnName")
    public String getRefNoFinanceColumnName() {
        return refNoFinanceColumnName;
    }

    public void setRefNoFinanceColumnName(String refNoFinanceColumnName) {
        this.refNoFinanceColumnName = refNoFinanceColumnName;
    }

    @Basic
    @Column(name = "RefNoManagementColumnName")
    public String getRefNoManagementColumnName() {
        return refNoManagementColumnName;
    }

    public void setRefNoManagementColumnName(String refNoManagementColumnName) {
        this.refNoManagementColumnName = refNoManagementColumnName;
    }

    @Basic
    @Column(name = "RefDateColumnName")
    public String getRefDateColumnName() {
        return refDateColumnName;
    }

    public void setRefDateColumnName(String refDateColumnName) {
        this.refDateColumnName = refDateColumnName;
    }

    @Basic
    @Column(name = "PostedDateColumnName")
    public String getPostedDateColumnName() {
        return postedDateColumnName;
    }

    public void setPostedDateColumnName(String postedDateColumnName) {
        this.postedDateColumnName = postedDateColumnName;
    }

    @Basic
    @Column(name = "DetailTableName")
    public String getDetailTableName() {
        return detailTableName;
    }

    public void setDetailTableName(String detailTableName) {
        this.detailTableName = detailTableName;
    }

    @Basic
    @Column(name = "MasterTableName")
    public String getMasterTableName() {
        return masterTableName;
    }

    public void setMasterTableName(String masterTableName) {
        this.masterTableName = masterTableName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysRefTypeDetail that = (SysRefTypeDetail) o;
        return Objects.equals(refTypeDetailId, that.refTypeDetailId) &&
                Objects.equals(refType, that.refType) &&
                Objects.equals(refTypeName, that.refTypeName) &&
                Objects.equals(refTypeCategory, that.refTypeCategory) &&
                Objects.equals(refNoFinanceColumnName, that.refNoFinanceColumnName) &&
                Objects.equals(refNoManagementColumnName, that.refNoManagementColumnName) &&
                Objects.equals(refDateColumnName, that.refDateColumnName) &&
                Objects.equals(postedDateColumnName, that.postedDateColumnName) &&
                Objects.equals(detailTableName, that.detailTableName) &&
                Objects.equals(masterTableName, that.masterTableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(refTypeDetailId, refType, refTypeName, refTypeCategory, refNoFinanceColumnName, refNoManagementColumnName, refDateColumnName, postedDateColumnName, detailTableName, masterTableName);
    }
}
