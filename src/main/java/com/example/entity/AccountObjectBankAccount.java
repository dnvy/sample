package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Đối tượng Kế toán là Ngân hàng.
 */
@Entity
public class AccountObjectBankAccount {

    private Integer id; // PK
    private Integer accountObjectId; // FK
    private String bankAccount; // Số TK ngân hàng
    private String bankName; // Tên ngân hàng
    private Integer bankId; // Vý bổ sung.
    private Integer sortOrder;
    private String bankBranchName;
    private String province;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "AccountObjectID")
    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "BankAccount")
    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Basic
    @Column(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "BankId")
    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    @Basic
    @Column(name = "SortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Basic
    @Column(name = "BankBranchName")
    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    @Basic
    @Column(name = "Province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountObjectBankAccount that = (AccountObjectBankAccount) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountObjectId, that.accountObjectId) &&
                Objects.equals(bankAccount, that.bankAccount) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(sortOrder, that.sortOrder) &&
                Objects.equals(bankBranchName, that.bankBranchName) &&
                Objects.equals(province, that.province);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountObjectId, bankAccount, bankName, sortOrder, bankBranchName, province);
    }

}
