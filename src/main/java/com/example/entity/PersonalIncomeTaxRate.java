package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Biểu tính thuế Thu nhập cá nhân.
 */
@Entity
public class PersonalIncomeTaxRate {

    private Integer id; // PK - Biểu thuế thu nhập các nhân
    private Integer taxGrade; // Bậc thuế (1 - 8)
    private BigDecimal taxRate; // Phần trăm chịu thuế (5%; 10%;  20%...)
    private BigDecimal fromIncomeByMonth; // Thu nhập tính thuế theo tháng từ mức này
    private BigDecimal toIncomeByMonth; // Thu nhập tính thuế theo tháng đến mức này
    private BigDecimal fromIncomeByYear; // Thu nhập tính thuế theo năm từ mức này
    private BigDecimal toIncomeByYear; // Thu nhập tính thuế theo năm đến mức này
    private Boolean activeStatus; // Trạng thái theo dõi

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TaxGrade")
    public Integer getTaxGrade() {
        return taxGrade;
    }

    public void setTaxGrade(Integer taxGrade) {
        this.taxGrade = taxGrade;
    }

    @Basic
    @Column(name = "TaxRate")
    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    @Basic
    @Column(name = "FromIncomeByMonth")
    public BigDecimal getFromIncomeByMonth() {
        return fromIncomeByMonth;
    }

    public void setFromIncomeByMonth(BigDecimal fromIncomeByMonth) {
        this.fromIncomeByMonth = fromIncomeByMonth;
    }

    @Basic
    @Column(name = "ToIncomeByMonth")
    public BigDecimal getToIncomeByMonth() {
        return toIncomeByMonth;
    }

    public void setToIncomeByMonth(BigDecimal toIncomeByMonth) {
        this.toIncomeByMonth = toIncomeByMonth;
    }

    @Basic
    @Column(name = "FromIncomeByYear")
    public BigDecimal getFromIncomeByYear() {
        return fromIncomeByYear;
    }

    public void setFromIncomeByYear(BigDecimal fromIncomeByYear) {
        this.fromIncomeByYear = fromIncomeByYear;
    }

    @Basic
    @Column(name = "ToIncomeByYear")
    public BigDecimal getToIncomeByYear() {
        return toIncomeByYear;
    }

    public void setToIncomeByYear(BigDecimal toIncomeByYear) {
        this.toIncomeByYear = toIncomeByYear;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalIncomeTaxRate that = (PersonalIncomeTaxRate) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(taxGrade, that.taxGrade) &&
                Objects.equals(taxRate, that.taxRate) &&
                Objects.equals(fromIncomeByMonth, that.fromIncomeByMonth) &&
                Objects.equals(toIncomeByMonth, that.toIncomeByMonth) &&
                Objects.equals(fromIncomeByYear, that.fromIncomeByYear) &&
                Objects.equals(toIncomeByYear, that.toIncomeByYear) &&
                Objects.equals(activeStatus, that.activeStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taxGrade, taxRate, fromIncomeByMonth, toIncomeByMonth, fromIncomeByYear, toIncomeByYear, activeStatus);
    }
}
