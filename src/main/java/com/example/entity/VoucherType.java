package com.example.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Loại chứng từ. Xem http://localhost:8080/voucher_types
 */
@Entity
public class VoucherType {

    private Integer id; // PK của bảng
    private Integer movementBy; // Phát sinh theo. 0: Phát sinh theo TK; 1: Phát sinh theo nhóm chứng từ
    private String voucherTypeCode; // Mã loại chứng từ
    private String voucherTypeName; // Tên loại chứng từ
    private String debitAccount; // Tài khoản Nợ
    private String creditAccount; // Tài khoản Có
    private Boolean isSystem; // Là dữ liệu hệ thống
    private String description; // Diễn giải
    private String voucherTypeCategory; // Nhóm loại chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean activeStatus;
    private String printedCreditAccount;
    private String printedDebitAccount;
    private Integer correspondingSummary;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MovementBy")
    public Integer getMovementBy() {
        return movementBy;
    }

    public void setMovementBy(Integer movementBy) {
        this.movementBy = movementBy;
    }

    @Basic
    @Column(name = "VoucherTypeCode")
    public String getVoucherTypeCode() {
        return voucherTypeCode;
    }

    public void setVoucherTypeCode(String voucherTypeCode) {
        this.voucherTypeCode = voucherTypeCode;
    }

    @Basic
    @Column(name = "VoucherTypeName")
    public String getVoucherTypeName() {
        return voucherTypeName;
    }

    public void setVoucherTypeName(String voucherTypeName) {
        this.voucherTypeName = voucherTypeName;
    }

    @Basic
    @Column(name = "DebitAccount")
    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    @Basic
    @Column(name = "CreditAccount")
    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Basic
    @Column(name = "IsSystem")
    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "VoucherTypeCategory")
    public String getVoucherTypeCategory() {
        return voucherTypeCategory;
    }

    public void setVoucherTypeCategory(String voucherTypeCategory) {
        this.voucherTypeCategory = voucherTypeCategory;
    }

    @Basic
    @Column(name = "CreatedDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "ModifiedDate")
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "ModifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "ActiveStatus")
    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Basic
    @Column(name = "PrintedCreditAccount")
    public String getPrintedCreditAccount() {
        return printedCreditAccount;
    }

    public void setPrintedCreditAccount(String printedCreditAccount) {
        this.printedCreditAccount = printedCreditAccount;
    }

    @Basic
    @Column(name = "PrintedDebitAccount")
    public String getPrintedDebitAccount() {
        return printedDebitAccount;
    }

    public void setPrintedDebitAccount(String printedDebitAccount) {
        this.printedDebitAccount = printedDebitAccount;
    }

    @Basic
    @Column(name = "CorrespondingSummary")
    public Integer getCorrespondingSummary() {
        return correspondingSummary;
    }

    public void setCorrespondingSummary(Integer correspondingSummary) {
        this.correspondingSummary = correspondingSummary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoucherType that = (VoucherType) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(movementBy, that.movementBy) &&
                Objects.equals(voucherTypeCode, that.voucherTypeCode) &&
                Objects.equals(voucherTypeName, that.voucherTypeName) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount) &&
                Objects.equals(isSystem, that.isSystem) &&
                Objects.equals(description, that.description) &&
                Objects.equals(voucherTypeCategory, that.voucherTypeCategory) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedDate, that.modifiedDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(activeStatus, that.activeStatus) &&
                Objects.equals(printedCreditAccount, that.printedCreditAccount) &&
                Objects.equals(printedDebitAccount, that.printedDebitAccount) &&
                Objects.equals(correspondingSummary, that.correspondingSummary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, movementBy, voucherTypeCode, voucherTypeName, debitAccount, creditAccount, isSystem, description, voucherTypeCategory, createdDate, createdBy, modifiedDate, modifiedBy, activeStatus, printedCreditAccount, printedDebitAccount, correspondingSummary);
    }
}
