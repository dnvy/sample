package com.example.report;

import java.math.BigDecimal;

public class CaAuditDetail {

    private Integer id; // PK.
    private Integer refId; // FK.
    private String description; // Diễn giải
    private Integer valueOfMoney; // Mệnh giá tiền
    private Integer quantity; // Số lượng
    private BigDecimal amount; // Số tiền

    public CaAuditDetail() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getValueOfMoney() {
        return valueOfMoney;
    }

    public void setValueOfMoney(Integer valueOfMoney) {
        this.valueOfMoney = valueOfMoney;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
