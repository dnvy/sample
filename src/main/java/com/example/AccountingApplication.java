package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
// @ComponentScan(basePackages = {"com.donhuvy.repository"}, basePackageClasses = com.donhuvy.repository.EmployeeRepository.class)
public class AccountingApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AccountingApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(AccountingApplication.class);
    }

}
