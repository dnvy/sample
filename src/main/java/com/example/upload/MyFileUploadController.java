package com.example.upload;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Example upload one file, many files.
 */
@Controller
public class MyFileUploadController {

    /**
     * Trang đi vào.
     *
     * @return
     */
    @RequestMapping(value = "/upload")
    public String homePage() {
        return "upload/start";
    }

    /**
     * Hiển thị form upload.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/upload_one_file", method = RequestMethod.GET)
    public String uploadOneFileHandler(Model model) {
        MyUploadForm myUploadForm = new MyUploadForm();
        model.addAttribute("myUploadForm", myUploadForm);
        return "upload/one_file";
    }

    /**
     * Xử lý upload.
     *
     * @param request
     * @param model
     * @param myUploadForm
     * @return
     */
    @RequestMapping(value = "/upload_one_file", method = RequestMethod.POST)
    public String uploadOneFileHandlerPOST(HttpServletRequest request, Model model, @ModelAttribute("myUploadForm") MyUploadForm myUploadForm) {
        return this.doUpload(request, model, myUploadForm);
    }

    /**
     * Hiển thị form upload.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/upload_many_files", method = RequestMethod.GET)
    public String uploadMultiFileHandler(Model model) {
        MyUploadForm myUploadForm = new MyUploadForm();
        model.addAttribute("myUploadForm", myUploadForm);
        return "upload/multiple_files";
    }

    /**
     * POST: Xử lý upload.
     *
     * @param request
     * @param model
     * @param myUploadForm
     * @return
     */
    @RequestMapping(value = "/upload_many_files", method = RequestMethod.POST)
    public String uploadMultiFileHandlerPOST(HttpServletRequest request, Model model, @ModelAttribute("myUploadForm") MyUploadForm myUploadForm) {
        return this.doUpload(request, model, myUploadForm);
    }

    /**
     * Action upload.
     *
     * @param request
     * @param model
     * @param myUploadForm
     * @return
     */
    private String doUpload(HttpServletRequest request, Model model, MyUploadForm myUploadForm) {
        String description = myUploadForm.getDescription();
        // Thư mục gốc upload file.
        String uploadRootPath = request.getServletContext().getRealPath("upload");
        System.out.println("uploadRootPath=" + uploadRootPath);

        // Nếu muốn chỉ ra tường minh thư mục lưu trữ file upload.
        //String uploadRootPath = "E:\\spring_upload\\";

        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        MultipartFile[] multipartFiles = myUploadForm.getFileDatas();
        List<File> uploadedFiles = new ArrayList<>();
        List<String> failedFiles = new ArrayList<>();
        for (MultipartFile multipartFile : multipartFiles) {
            // Tên file gốc tại Client.
            String originalFilename = multipartFile.getOriginalFilename();
            System.out.println("Client file name = " + originalFilename);
            if (originalFilename != null && originalFilename.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + originalFilename);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    bufferedOutputStream.write(multipartFile.getBytes());
                    bufferedOutputStream.close();
                    uploadedFiles.add(serverFile);
                    System.out.println("Write file success: " + serverFile);
                } catch (Exception e) {
                    System.err.println("Write file error: " + originalFilename);
                    failedFiles.add(originalFilename);
                }
            }
        }
        model.addAttribute("description", description);
        model.addAttribute("uploadedFiles", uploadedFiles);
        model.addAttribute("failedFiles", failedFiles);
        return "upload/result";
    }

}