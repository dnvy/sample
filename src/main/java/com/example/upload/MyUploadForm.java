package com.example.upload;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * Demo upload one file, many files.
 */
public class MyUploadForm implements Serializable {

    private static final long serialVersionUID = 152726635202220073L;

    private String description;

    private MultipartFile[] fileDatas;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile[] getFileDatas() {
        return fileDatas;
    }

    public void setFileDatas(MultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }

}