package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.PersonalIncomeTaxRate;

import java.util.List;

@Repository
public interface PersonalIncomeTaxRateRepository extends JpaRepository<PersonalIncomeTaxRate, Integer> {

    // Find all then sort by tax grade ASC.
    @Query(value = "SELECT * FROM PersonalIncomeTaxRate ORDER BY TaxGrade", nativeQuery = true)
    List<PersonalIncomeTaxRate> findAllInOrder();

}
