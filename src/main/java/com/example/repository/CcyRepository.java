package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.Ccy;

import java.util.List;

@Repository
public interface CcyRepository extends JpaRepository<Ccy, Integer> {

    List<Ccy> findAllByOrderBySortOrder();

}
