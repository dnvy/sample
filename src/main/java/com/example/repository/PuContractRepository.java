package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.PuContract;

@Repository
public interface PuContractRepository extends JpaRepository<PuContract, Integer> {

}
