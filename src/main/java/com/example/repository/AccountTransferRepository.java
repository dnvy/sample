package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountTransfer;

@Repository
public interface AccountTransferRepository extends JpaRepository<AccountTransfer, Integer> {

}
