package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.SysdbOption;

/**
 * Menu [Hệ thống] \ [Tùy chọn]
 * Là option của [Tab 8. Hiển thị các nghiệp vụ].
 */
@Repository
public interface SysdbOptionRepository extends JpaRepository<SysdbOption, Integer> {

}
