package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.ContractDetailInventoryItem;

@Repository
public interface ContractDetailInventoryItemRepository extends JpaRepository<ContractDetailInventoryItem, Integer> {

}
