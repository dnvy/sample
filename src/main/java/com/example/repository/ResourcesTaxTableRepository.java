package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.ResourcesTaxTable;

/**
 * Danh mục Biểu thuế tài nguyên.
 */
@Repository
public interface ResourcesTaxTableRepository extends JpaRepository<ResourcesTaxTable, Integer> {

}
