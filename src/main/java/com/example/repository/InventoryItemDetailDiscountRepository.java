package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.InventoryItemDetailDiscount;

@Repository
public interface InventoryItemDetailDiscountRepository extends JpaRepository<InventoryItemDetailDiscount, Integer> {

}
