package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CabaReasonType;

import java.util.List;

@Repository
public interface CabaReasonTypeRepository extends JpaRepository<CabaReasonType, Integer> {

    /**
     * Find by RefType.
     *
     * @param refType
     * @return
     */
    List<CabaReasonType> findByRefType(Integer refType);

}
