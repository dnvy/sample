package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.FunctionPath;

import java.util.List;

@Repository
public interface FunctionPathRepository extends JpaRepository<FunctionPath, Integer> {

    /**
     * Find all function paths by role.
     *
     * @param role
     * @return
     */
    List<FunctionPath> findAllByRole(String role);

}
