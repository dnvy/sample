package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountObjectGroup;

import java.util.List;

/**
 * Account object group.
 */
@Repository
public interface AccountObjectGroupRepository extends JpaRepository<AccountObjectGroup, Integer> {

    /**
     * Find all AccountObjectGroup has ActiveStatus = TRUE .
     *
     * @param activeStatus
     * @return
     */
    List<AccountObjectGroup> findAllByActiveStatus(Boolean activeStatus);

}
