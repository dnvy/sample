package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.BaDeposit;

/**
 * Bảng master của chứng từ thu tiền gửi
 * Là Master của các bảng detail sau:
 *  -BADepositeDetail
 *  -BADepositeDetailSale
 *  -BADepositeDetailFixedAsset
 */
@Repository
public interface BaDepositRepository extends JpaRepository<BaDeposit, Integer> {

}
