package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.InInwardDetail;

/**
 * Nội dung Chi tiết của phiếu Nhập, xuất kho.
 */
@Repository
public interface InInwardDetailRepository extends JpaRepository<InInwardDetail, Integer> {

}
