package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountObjectBankAccount;

import java.util.List;

/**
 * Tài khoản ngân hàng đi kèm với Đối tượng kế toán.
 */
@Repository
public interface AccountObjectBankAccountRepository extends JpaRepository<AccountObjectBankAccount, Integer> {

    /**
     * Tìm các tài khoản ngân hàng theo id của Đối tượng kế toán.
     * Có thể trả về 0, 1 hoặc nhiều tài khoản ngân hàng.
     *
     * @param accountObjectId
     * @return
     */
    List<AccountObjectBankAccount> findByAccountObjectId(Integer accountObjectId);

}
