package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.ProjectWorkCategory;

/**
 * Loại công trình.
 */
@Repository
public interface ProjectWorkCategoryRepository extends JpaRepository<ProjectWorkCategory, Integer> {

}
