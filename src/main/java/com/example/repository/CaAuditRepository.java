package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CaAudit;

/**
 * Kiểm kê quỹ.
 */
@Repository
public interface CaAuditRepository extends JpaRepository<CaAudit, Integer> {

}
