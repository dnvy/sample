package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.DebtList;

/**
 * Đợt thu nợ.
 */
@Repository
public interface DebtListRepository extends JpaRepository<DebtList, Integer> {

}
