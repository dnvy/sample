package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountDefault;

/**
 * Tài khoản ngầm định.
 */
@Repository
public interface AccountDefaultRepository extends JpaRepository<AccountDefault, Integer> {

}
