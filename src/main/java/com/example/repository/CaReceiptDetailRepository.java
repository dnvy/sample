package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CaReceiptDetail;

@Repository
public interface CaReceiptDetailRepository extends JpaRepository<CaReceiptDetail, Integer> {

}
