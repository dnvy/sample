package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.SysAutoId;

import java.util.List;
import java.util.Optional;

/**
 * Bảng lưu cấu hình tự tăng của Số chứng từ.
 */
@Repository
public interface SysAutoIdRepository extends JpaRepository<SysAutoId, Integer> {

    /**
     * Lưu ý, có dấu cách ở cuối câu truy vấn, bỏ dấu ";"
     * Dùng để hiển thị ra màn hình list.
     *
     * @param branchId
     * @return
     *
     * @see com.example.controller.SysAutoIdController#showListOfAutoId()
     */
    @Query(value = "SELECT * FROM SYSAutoID WHERE BranchID = ?1 ", nativeQuery = true)
    List<SysAutoId> findAllAutoIdOfCurrentBranch(Integer branchId);

    ///**
    // * Dùng để sinh số chứng từ tự động.
    // * Ví dụ: SELECT * FROM SYSAutoID WHERE RefTypeCategory = 101 AND BranchID = 1;
    // *
    // * @param refTypeCategory
    // * @param branchId
    // * @return
    // */
    //@Query(value = "SELECT * FROM SYSAutoID WHERE RefTypeCategory = ?1 AND BranchID = ?2 ", nativeQuery = true)
    //List<SysAutoId> findSpecific(Integer refTypeCategory, Integer branchId);

    /**
     * Dùng để sinh số chứng từ tự động.
     * Ví dụ: SELECT * FROM SYSAutoID WHERE RefTypeCategory = 101 AND BranchID = 1;
     *
     * @param refTypeCategory
     * @param branchId
     * @return
     */
    @Query(value = "SELECT * FROM SYSAutoID WHERE RefTypeCategory = ?1 AND BranchID = ?2 ", nativeQuery = true)
    Optional<SysAutoId> findSpecific(Integer refTypeCategory, Integer branchId);

}
















