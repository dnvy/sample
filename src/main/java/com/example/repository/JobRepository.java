package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.Job;

import java.util.List;

/**
 * Khoản mục chi phí.
 */
@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

    /**
     * Lấy tất cả các Đối tượng tập hợp chi phí có ActiveStatus = 1 .
     *
     * @return
     */
    @Query(value = " SELECT * FROM Job WHERE ActiveStatus = 1 ", nativeQuery = true)
    List<Job> getActiveJobs();

}
