package com.example.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.Account;

import java.util.List;

/**
 * Danh mục tài khoản.
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query(value = "SELECT a.id, a.accountNumber, a.accountName FROM Account a WHERE a.grade = 2")
    List<Object[]> findAllAccount(Sort sort);

    @Query(value = "SELECT * FROM Account WHERE ActiveStatus = 1 ORDER BY AccountNumber;", nativeQuery = true)
    List<Account> findAllActiveAccount();

    @Query(value = "SELECT ID, AccountNumber, AccountName FROM Account WHERE ActiveStatus = 1 ORDER BY AccountNumber;", nativeQuery = true)
    List<Object[]> findActiveAccount345();

}
