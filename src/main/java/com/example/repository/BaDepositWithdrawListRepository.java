package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.BaDepositWithdrawList;

@Repository
public interface BaDepositWithdrawListRepository extends JpaRepository<BaDepositWithdrawList, Integer> {

}
