package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.Location;

import java.util.List;

/**
 * Get province, district, commune.
 * Kind = 1. Ha Noi, LocationID = VN101
 * Kind = 2. Huyện Hoài Đức, LocationID = VN10137
 * Kind = 3. Xa Cat Que, LocationID = VN1013723
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    /**
     * Get list of provinces.
     *
     * @param locationId
     * @return
     */
    @Query(value = "SELECT * FROM Location WHERE Kind = '1' AND LEFT(LocationID, 2) = ?1 ORDER BY LocationName", nativeQuery = true)
    List<Location> listVietnamProvinces(String locationId);

    /**
     * Get list of districts.
     * For example SELECT * FROM Location WHERE Kind = '2' and LEFT(LocationID, 5) = 'VN101' ORDER BY LocationName;
     *
     * @param prefixOfProvinceInLocationID
     * @return
     */
    @Query(value = "SELECT * FROM Location WHERE Kind = '2' and LEFT(LocationID, 5) = ?1 ORDER BY LocationName;", nativeQuery = true)
    List<Location> getDistrics(String prefixOfProvinceInLocationID);

    /**
     * Get list communes by District.
     * For example SELECT * FROM Location WHERE Kind = '3' and LEFT(LocationID, 7) = 'VN10137' ORDER BY LocationName;
     *
     * @param prefixOfDistrictInLocationID
     * @return
     */
    @Query(value = "SELECT * FROM Location WHERE Kind = '3' and LEFT(LocationID, 7) = ?1 ORDER BY LocationName;", nativeQuery = true)
    List<Location> getCommunes(String prefixOfDistrictInLocationID);

    /**
     * Get all countries.
     *
     * @return
     */
    @Query(value = "" +
            "SELECT * " +
            "FROM Location " +
            "WHERE LEN(LocationID) = 2 " +
            "ORDER BY SortOrder, LocationID;", nativeQuery = true)
    List<Location> getCountries();

    /**
     * Get all provinces.
     *
     * @return
     */
    @Query(value = "" +
            "SELECT * " +
            "FROM Location " +
            "WHERE LEN(LocationID) = 5 AND LocationID LIKE 'VN%' " +
            "ORDER BY SortOrder, LocationID;", nativeQuery = true)
    List<Location> getProvinces();

    /**
     * Get all districts.
     *
     * @return
     */
    @Query(value = "" +
            "SELECT * " +
            "FROM Location " +
            "WHERE LEN(LocationID) = 7 AND LocationID LIKE 'VN%' " +
            "ORDER BY LocationID;", nativeQuery = true)
    List<Location> getDistricts();

    /**
     * Get all wards.
     *
     * @return
     */
    @Query(value = "" +
            "SELECT * " +
            "FROM Location " +
            "WHERE LEN(LocationID) = 9 AND LocationID LIKE 'VN%' " +
            "ORDER BY LocationID;", nativeQuery = true)
    List<Location> getWards();


    /**
     * Get wards by district name.
     *
     * @param districtName
     * @return
     */
    @Query(value = "SELECT * FROM Location WHERE Kind = 3 AND substring(LocationID, 0, 8) = substring((select LocationID from Location where LocationName = N'?1'), 0, 8)ORDER BY LocationID;", nativeQuery = true)
    List<Location> getWardsByDistrict(String districtName);

}
