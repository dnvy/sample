package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.UserRole;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    @Query(value = "SELECT ROLE_NAME FROM APP_ROLE INNER JOIN USER_ROLE ON USER_ROLE.ROLE_ID = APP_ROLE.ROLE_ID WHERE USER_ROLE.ROLE_ID = 1;", nativeQuery = true)
    List<String> getRoleNames(Long userId);

}
