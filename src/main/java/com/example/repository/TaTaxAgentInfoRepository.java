package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.TaTaxAgentInfo;

/**
 * Thiết lập thông tin đại lý thuế.
 */
@Repository
public interface TaTaxAgentInfoRepository extends JpaRepository<TaTaxAgentInfo, Integer> {

}
