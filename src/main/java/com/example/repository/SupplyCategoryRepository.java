package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.SupplyCategory;

@Repository
public interface SupplyCategoryRepository extends JpaRepository<SupplyCategory, Integer> {

}
