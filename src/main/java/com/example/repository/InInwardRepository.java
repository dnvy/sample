package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.InInward;

/**
 * Bảng Master của phiếu nhập kho.
 */
@Repository
public interface InInwardRepository extends JpaRepository<InInward, Integer> {

}
