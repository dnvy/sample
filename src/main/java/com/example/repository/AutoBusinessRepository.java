package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AutoBusiness;

@Repository
public interface AutoBusinessRepository extends JpaRepository<AutoBusiness, Integer> {

}
