package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.Stock;

import java.util.List;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {

    /**
     * Tìm tất cả các kho có Trạng thái theo dõi = 1, dùng cho drop-down list ở
     * màn hình Thêm mới Hàng hóa, vật tư.
     *
     * @return
     */
    @Query(value = "SELECT * FROM Stock WHERE ActiveStatus = 1", nativeQuery = true)
    List<Stock> findAllActiveStocks();

}
