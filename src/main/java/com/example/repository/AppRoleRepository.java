package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AppRole;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole, Integer> {

}
