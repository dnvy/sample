package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.BaWithDrawDetail;

@Repository
public interface BaWithDrawDetailRepository extends JpaRepository<BaWithDrawDetail, Integer> {

}
