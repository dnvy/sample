package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.entity.PurchaseLedger;

/**
 * Bảng Ledger mua hàng.
 */
public interface PurchaseLedgerRepository extends JpaRepository<PurchaseLedger, Integer> {

}
