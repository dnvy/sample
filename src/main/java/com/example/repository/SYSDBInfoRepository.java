package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.SysdbInfo;

@Repository
public interface SYSDBInfoRepository extends JpaRepository<SysdbInfo, Integer> {

    /**
     * Get latest record (is also latest version).
     *
     * @return
     */
    @Query(value = "SELECT * FROM SYSDBInfo WHERE ID = (SELECT COALESCE(MAX(SYSDBInfo.ID), 0) FROM SYSDBInfo)", nativeQuery = true)
    SysdbInfo getLatestRecord();

}
