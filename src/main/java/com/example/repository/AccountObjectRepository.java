package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountObject;

import java.util.List;

/**
 * Khách hàng, nhà cung cấp, nhân viên.
 */
@Repository
public interface AccountObjectRepository extends JpaRepository<AccountObject, Integer> {

    /**
     * List all employees.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsEmployee = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> listEmployee();

    /**
     * List all customers.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsCustomer = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> listCustomers();

    /**
     * List all vendors.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsVendor = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> listVendors();


    /**
     * Get list of suppliers.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsVendor = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> getListOfSuppliers();

    /**
     * Get list of customers.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsCustomer = 1 AND ActiveStatus = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> getListOfCustomers();

    /**
     * Get list of employees.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE IsEmployee = 1 AND ActiveStatus = 1", nativeQuery = true)
    List<AccountObject> getListOfActiveEmployees();

    /**
     * Find account object by account object code.
     *
     * @param accountObjectCode
     * @return
     */
    List<AccountObject> findAccountObjectByAccountObjectCode(String accountObjectCode);


    /**
     * Get all account object has active_status = 1.
     *
     * @return
     */
    @Query(value = "SELECT * FROM AccountObject WHERE ActiveStatus = 1 ORDER BY AccountObjectCode", nativeQuery = true)
    List<AccountObject> getActiveAccountObject();
}
