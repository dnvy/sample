package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.entity.PuOrder;

/**
 * Đơn mua hàng.
 */
public interface PuOrderRepository extends JpaRepository<PuOrder, Integer> {

}
