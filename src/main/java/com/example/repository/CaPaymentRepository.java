package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CaPayment;

@Repository
public interface CaPaymentRepository extends JpaRepository<CaPayment, Integer> {

}
