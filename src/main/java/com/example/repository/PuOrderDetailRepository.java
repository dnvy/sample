package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.entity.PuOrderDetail;

/**
 * Chi tiết đơn mua hàng.
 */
public interface PuOrderDetailRepository extends JpaRepository<PuOrderDetail, Integer> {

}
