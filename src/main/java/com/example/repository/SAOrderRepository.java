package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.SAOrder;

/**
 * Bảng master đơn đặt hàng.
 */
@Repository
public interface SAOrderRepository extends JpaRepository<SAOrder, Integer> {

}
