package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.InventoryItemCategorySpecialTax;

import java.util.List;

/**
 * Bảng nhóm Hàng hóa dịch vụ chịu Thuế tiêu thụ đặc biệt.
 */
@Repository
public interface InventoryItemCategorySpecialTaxRepository extends JpaRepository<InventoryItemCategorySpecialTax, Integer> {

    @Query(value = "SELECT * FROM InventoryItemCategorySpecialTax WHERE ActiveStatus = 1 ORDER BY VyCodeID", nativeQuery = true)
    List<InventoryItemCategorySpecialTax> findAllActiveSpecialTaxRates();

}
