package com.example.repository;

public class AccountObjectNotFoundException extends RuntimeException {

    public AccountObjectNotFoundException(String exception) {
        super(exception);
    }

}
