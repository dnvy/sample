package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.GlVoucher;

/**
 * Chứng từ nghiệp vụ khác, Hạch toán chi phí lương, Xử lý chênh lệch tỷ giá.
 */
@Repository
public interface GlVoucherRepository extends JpaRepository<GlVoucher, Integer> {

}
