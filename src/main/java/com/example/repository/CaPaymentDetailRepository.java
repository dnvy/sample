package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CaPaymentDetail;

/**
 * Chi tiết hạch toán phiếu thu.
 */
@Repository
public interface CaPaymentDetailRepository extends JpaRepository<CaPaymentDetail, Integer> {

}
