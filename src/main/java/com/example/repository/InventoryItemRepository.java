package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.entity.InventoryItem;

import java.util.List;

public interface InventoryItemRepository extends JpaRepository<InventoryItem, Integer> {

    /**
     * List all material products.
     *
     * @return
     */
    @Query(value = "SELECT * FROM InventoryItem ORDER BY ID", nativeQuery = true)
    List<InventoryItem> listInventoryItems();

    @Query(value = "SELECT * FROM InventoryItem  WHERE ActiveStatus = 1", nativeQuery = true)
    List<InventoryItem> findAllActiveInventoryItems();

}
