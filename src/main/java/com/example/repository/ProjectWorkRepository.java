package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.ProjectWork;

import java.util.List;

@Repository
public interface ProjectWorkRepository extends JpaRepository<ProjectWork, Integer> {

    /**
     * Get all project_works has active_status = 1.
     *
     * @return
     */
    @Query(value = "SELECT * FROM ProjectWork WHERE ActiveStatus = 1 ORDER BY ID", nativeQuery = true)
    List<ProjectWork> getAllActiveProjectWorks();

}
