package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.controller.ExpenseItemController;
import com.example.entity.ExpenseItem;

import java.util.List;

@Repository
public interface ExpenseItemRepository extends JpaRepository<ExpenseItem, Integer> {

    /**
     * Find all active expense_items.
     *
     * @return
     * @see ExpenseItemController#expenseItemForDropDownListJSON()
     */
    @Query(value = "SELECT * FROM ExpenseItem WHERE ActiveStatus = 1 ORDER BY ID", nativeQuery = true)
    List<ExpenseItem> findAllActiveExpenseItem();
}
