package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.Unit;

import java.util.List;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Integer> {

    @Query(value = "SELECT * FROM Unit WHERE ActiveStatus = 1;", nativeQuery = true)
    List<Unit> getActiveUnits();

}
