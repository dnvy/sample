package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.OrganizationUnit;

@Repository
public interface OrganizationUnitRepository extends JpaRepository<OrganizationUnit, Integer> {

    //FIXME. thường xuyên gây ra lỗi.
    @Query(value = "select TOP 1 ID FROM OrganizationUnit WHERE Grade =1 AND ActiveStatus = 1 AND IsSystem =1 ORDER BY BranchID;", nativeQuery = true)
    Integer findOneAtTopLevel();

}
