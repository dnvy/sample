package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.ListItem;

@Repository
public interface ListItemRepository extends JpaRepository<ListItem, Integer> {

}
