package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.TimeSheetSign;

@Repository
public interface TimeSheetSignRepository extends JpaRepository<TimeSheetSign, Integer> {

}
