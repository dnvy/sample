package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.CaAuditMemberDetail;

/**
 * Bảng chi tiết các thành viên tham gia kiểm kê.
 */
@Repository
public interface CaAuditMemberDetailRepository extends JpaRepository<CaAuditMemberDetail, Integer> {

}
