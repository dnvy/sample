package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.entity.InventoryItemCategory;

import java.util.List;

/**
 * Danh mục nhóm vật tư, hàng hóa, dịch vụ.
 */
@Repository
public interface InventoryItemCategoryRepository extends JpaRepository<InventoryItemCategory, Integer> {

    /**
     * Tìm tất cả các nhóm vật tư/hàng hóa có Trạng thái theo dõi = 1.
     *
     * @return
     */
    @Query(value = "SELECT * FROM InventoryItemCategory WHERE ActiveStatus = 1;", nativeQuery = true)
    List<InventoryItemCategory> getAllActiveInventoryItemCategories();

    /**
     * Tìm các item theo ParentID (cả trường hợp ActiveStatus = 1 và 0).
     *
     * @return
     */
    List<InventoryItemCategory> findByParentId(Integer parentId);

}
