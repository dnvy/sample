package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.AccountObjectShippingAddress;

import java.util.List;

/**
 * Địa chỉ giao nhận hàng, gắn liền với Đối tượng kế toán.
 */
@Repository
public interface AccountObjectShippingAddressRepository extends JpaRepository<AccountObjectShippingAddress, Integer> {

    /**
     * Tìm các địa chỉ giao nhận theo id của Đối tượng kế toán.
     *
     * @param accountObjectId
     * @return
     */
    List<AccountObjectShippingAddress> findByAccountObjectId(Integer accountObjectId);

}
