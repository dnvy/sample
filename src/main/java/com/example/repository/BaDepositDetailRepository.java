package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.BaDepositDetail;


@Repository
public interface BaDepositDetailRepository extends JpaRepository<BaDepositDetail, Integer> {

}
