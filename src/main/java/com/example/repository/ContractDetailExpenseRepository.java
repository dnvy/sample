package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.ContractDetailExpense;

/**
 * Dùng ở form thêm mới Hợp đồng bán hàng.
 */
@Repository
public interface ContractDetailExpenseRepository extends JpaRepository<ContractDetailExpense, Integer> {

}
