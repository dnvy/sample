package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.OpeningAccountEntryDetailInvoice;

@Repository
public interface OpeningAccountEntryDetailInvoiceRepository extends JpaRepository<OpeningAccountEntryDetailInvoice, Long> {

}
