package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.SaQuote;

/**
 * Báo giá. Phân hệ Bán hàng.
 */
@Repository
public interface SaQuoteRepository extends JpaRepository<SaQuote, Integer> {

}
