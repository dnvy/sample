package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.GeneralLedger;

@Repository
public interface GeneralLedgerRepository extends JpaRepository<GeneralLedger, Integer> {

}
