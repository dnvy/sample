package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.BaReconcile;

@Repository
public interface BaReconcileRepository extends JpaRepository<BaReconcile, Integer> {

}
