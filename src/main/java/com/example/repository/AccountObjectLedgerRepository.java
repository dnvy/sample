package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.entity.AccountObjectLedger;

public interface AccountObjectLedgerRepository extends JpaRepository<AccountObjectLedger, Integer> {


}
