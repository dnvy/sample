package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.entity.GlVoucherCrossEntryDetail;

@Repository
public interface GlVoucherCrossEntryDetailRepository extends JpaRepository<GlVoucherCrossEntryDetail, Integer> {

}
