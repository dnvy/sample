package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.form.DebtListForm;
import com.example.repository.DebtListRepository;

/**
 * Đợt thu nợ.
 */
@Controller
public class DebtListController {

    @Autowired
    DebtListRepository debtListRepository;

    /**
     * Show page for adding debt list.
     *
     * @return
     */
    @RequestMapping(value = "/add_debt_list", method = RequestMethod.GET)
    public ModelAndView showPageForAddDebtList() {
        ModelAndView modelAndView = new ModelAndView("debt_list/add");
        DebtListForm debtListForm = new DebtListForm();
        modelAndView.addObject("debtListForm", debtListForm);
        return modelAndView;
    }

}
