package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dto.BankAccountDTO;
import com.example.dropdownlist.BankAccountDropDownItem;
import com.example.entity.Bank;
import com.example.entity.BankAccount;
import com.example.form.BankAccountForm;
import com.example.repository.BankAccountRepository;
import com.example.repository.BankRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Hiển thị danh sách Tài khoản Ngân hàng của 1 công ty.
 */
@Controller
public class BankAccountController {

    @Autowired
    BankRepository bankRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    /**
     * Show all bank accounts.
     *
     * @return
     */
    @RequestMapping(value = "/bank_accounts", method = RequestMethod.GET)
    public ModelAndView showAllBankAccounts() {
        ModelAndView modelAndView = new ModelAndView("bank_account/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<BankAccount> bankAccountListOriginal = bankAccountRepository.findAll();
        List<BankAccountDTO> bankAccountList = new ArrayList<>();
        for (BankAccount bankAccount : bankAccountListOriginal) {
            BankAccountDTO bankAccountDTO = new BankAccountDTO();
            bankAccountDTO.setId(bankAccount.getId());
            bankAccountDTO.setBankAccountNumber(bankAccount.getBankAccountNumber());
            Integer bankId = bankAccount.getBankId();
            bankAccountDTO.setBankId(bankId);
            bankAccountDTO.setBankName(bankAccount.getBankName());
            Optional<Bank> bank = bankRepository.findById(bankId);
            // Lưu ý: Ý nghĩa của BankName trong entity Bank và BankAccount là khác nhau.
            bank.ifPresent(obj -> bankAccountDTO.setBankMainName(obj.getBankName()));
            bankAccountDTO.setAddress(bankAccount.getAddress());
            bankAccountDTO.setDescription(bankAccount.getDescription());
            bankAccountDTO.setActiveStatus(bankAccount.getActiveStatus());
            bankAccountDTO.setCreatedBy(bankAccount.getCreatedBy());
            bankAccountDTO.setCreatedDate(bankAccount.getCreatedDate());
            bankAccountDTO.setModifiedBy(bankAccount.getModifiedBy());
            bankAccountDTO.setModifiedDate(bankAccount.getModifiedDate());
            bankAccountDTO.setBranchId(bankAccount.getBranchId());
            bankAccountDTO.setAccountHolder(bankAccount.getAccountHolder());
            bankAccountDTO.setProvinceOrCity(bankAccount.getProvinceOrCity());
            bankAccountList.add(bankAccountDTO);
        }
        modelAndView.addObject("bankAccountList", bankAccountList);
        modelAndView.getModel().put("page_title", "Danh sách Tài khoản ngân hàng");
        return modelAndView;
    }

    /**
     * Show page for adding bank account.
     *
     * @return
     */
    @RequestMapping(value = "/add_bank_account", method = RequestMethod.GET)
    public ModelAndView showPageAddBankAccount() {
        ModelAndView modelAndView = new ModelAndView("bank_account/add");
        BankAccountForm bankAccountForm = new BankAccountForm();
        modelAndView.addObject("bankAccountForm", bankAccountForm);
        modelAndView.getModel().put("page_title", "Thêm Tài khoản ngân hàng");
        return modelAndView;
    }

    /**
     * Submit data add bank account.
     *
     * @return
     */
    @RequestMapping(value = "/add_bank_account", method = RequestMethod.POST)
    public String submitDataAddBankAccount(@ModelAttribute("bankAccountForm") @Valid BankAccountForm bankAccountForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "bank_account/add";
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBankAccountNumber(bankAccountForm.getBankAccountNumber());
        bankAccount.setBankId(bankAccountForm.getBankId());
        bankAccount.setBankName(bankAccountForm.getBankName());
        bankAccount.setAddress(bankAccountForm.getAddress());
        bankAccount.setDescription(bankAccount.getDescription());
        // Khi thêm vào, thì mặc định Trạng thái theo dõi = 1.
        bankAccount.setActiveStatus(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        bankAccount.setCreatedDate(now);
        bankAccount.setCreatedBy(authentication.getName());
        bankAccount.setBankId(bankAccountForm.getBankId());
        bankAccount.setAccountHolder(bankAccountForm.getAccountHolder());
        bankAccount.setProvinceOrCity(bankAccountForm.getProvinceOrCity());
        // Tạm thời thiết lập chi nhánh đang trong phiên làm việc như thế này.
        bankAccount.setBranchId(1);
        bankAccountRepository.save(bankAccount);
        return "redirect:/bank_accounts";
    }

    /**
     * Show page for adding a bank account.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/bank_account/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditBankAccount(@PathVariable("id") Integer id) {
        Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(id);
        BankAccount bankAccount = bankAccountOptional.get();
        ModelAndView modelAndView = new ModelAndView("bank_account/edit");
        BankAccountForm bankAccountForm = new BankAccountForm();
        bankAccountForm.setBankAccountNumber(bankAccount.getBankAccountNumber());
        bankAccountForm.setId(bankAccount.getId());
        bankAccountForm.setBankId(bankAccount.getBankId());
        bankAccountForm.setBankName(bankAccount.getBankName());
        bankAccountForm.setAddress(bankAccount.getAddress());
        bankAccountForm.setDescription(bankAccount.getDescription());
        bankAccountForm.setActiveStatus(bankAccount.getActiveStatus());
        bankAccountForm.setBranchId(bankAccount.getBranchId());
        bankAccountForm.setAccountHolder(bankAccount.getAccountHolder());
        bankAccountForm.setProvinceOrCity(bankAccount.getProvinceOrCity());
        modelAndView.addObject("bankAccountForm", bankAccountForm);
        modelAndView.getModel().put("page_title", "Sửa Tài khoản ngân hàng");
        return modelAndView;
    }

    /**
     * Submit data editing bank account.
     *
     * @param bankAccountForm
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/edit_bank_account", method = RequestMethod.POST)
    public String submitDataEditBankAccount(@ModelAttribute("bankAccountForm") @Valid BankAccountForm bankAccountForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "bank_account/edit";
        }
        Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(bankAccountForm.getId());
        BankAccount bankAccountOld = bankAccountOptional.get();
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(bankAccountOld.getId());
        bankAccount.setBankAccountNumber(bankAccountForm.getBankAccountNumber());
        bankAccount.setBankId(bankAccountForm.getBankId());
        bankAccount.setBankName(bankAccountForm.getBankName());
        bankAccount.setAddress(bankAccountForm.getAddress());
        bankAccount.setDescription(bankAccountForm.getDescription());
        if (bankAccountForm.getActiveStatus() != null) {
            bankAccount.setActiveStatus(Boolean.TRUE);
        } else {
            bankAccount.setActiveStatus(Boolean.FALSE);
        }
        bankAccount.setCreatedBy(bankAccountOld.getCreatedBy());
        bankAccount.setCreatedDate(bankAccountOld.getCreatedDate());
        bankAccount.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        bankAccount.setModifiedDate(now);
        bankAccount.setBranchId(bankAccountForm.getBranchId());
        bankAccount.setProvinceOrCity(bankAccountForm.getProvinceOrCity());
        bankAccount.setAccountHolder(bankAccountForm.getAccountHolder());
        bankAccountRepository.save(bankAccount);
        return "redirect:/bank_accounts";
    }

    /**
     * View detail a bank_account .
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/bank_account/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewDetailBankAccount(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("bank_account/view");
        BankAccountForm bankAccountForm = new BankAccountForm();
        Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(id);
        BankAccount bankAccount = bankAccountOptional.get();
        bankAccountForm.setBankAccountNumber(bankAccount.getBankAccountNumber());
        bankAccountForm.setBankId(bankAccount.getBankId());
        bankAccountForm.setBankName(bankAccount.getBankName());
        bankAccountForm.setAddress(bankAccount.getAddress());
        bankAccountForm.setDescription(bankAccount.getDescription());
        bankAccountForm.setActiveStatus(bankAccount.getActiveStatus());
        bankAccountForm.setBranchId(bankAccount.getBranchId());
        bankAccountForm.setAccountHolder(bankAccount.getAccountHolder());
        bankAccountForm.setProvinceOrCity(bankAccount.getProvinceOrCity());
        modelAndView.addObject("bankAccountForm", bankAccountForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Tài khoản ngân hàng");
        return modelAndView;
    }

    /**
     * Delete a bank_account .
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/bank_account/delete/{id}", method = RequestMethod.POST)
    public String deleteBankAccount(@PathVariable("id") Integer id) {
        BankAccount bankAccount = bankAccountRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        //TODO: Check related FK.
        bankAccountRepository.delete(bankAccount);
        return "redirect:/bank_accounts";
    }


    /**
     * All BankAccount has active_status = TRUE in JSON. Gồm các cột
     * 1. Số tài khoản
     * 2. Tên ngân hàng
     * 3. Tên chi nhánh
     *
     * @return
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @RequestMapping(value = "/active_bank_accounts_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String bankJSON() throws JsonProcessingException {
        List<BankAccount> bankAccountList = bankAccountRepository.findByActiveStatus(Boolean.TRUE);
        List<BankAccountDropDownItem> bankAccountDropDownItemList = new ArrayList<>();
        for (BankAccount bankAccount : bankAccountList) {
            BankAccountDropDownItem bankAccountDropDownItem = new BankAccountDropDownItem();
            bankAccountDropDownItem.setId(bankAccount.getId());
            bankAccountDropDownItem.setBankId(bankAccount.getBankId());
            Optional<Bank> bankOptional = bankRepository.findById(bankAccount.getBankId());
            Bank bank = bankOptional.get();
            bankAccountDropDownItem.setBankIdString(bank.getBankName());
            bankAccountDropDownItem.setBankAccountNumber(bankAccount.getBankAccountNumber());
            bankAccountDropDownItem.setBankName(bankAccount.getBankName());
            bankAccountDropDownItemList.add(bankAccountDropDownItem);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(bankAccountDropDownItemList);
        return jsonDataString;
    }

}






























