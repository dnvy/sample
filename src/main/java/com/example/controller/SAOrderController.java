package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.SAOrder;
import com.example.entity.SAOrderDetail;
import com.example.form.SAOrderForm;
import com.example.repository.SAOrderDetailRepository;
import com.example.repository.SAOrderRepository;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Bảng master đơn đặt hàng.
 */
@Controller
public class SAOrderController {

    @Autowired
    SAOrderRepository saOrderRepository;

    @Autowired
    SAOrderDetailRepository saOrderDetailRepository;

    /**
     * Reference type cho Đơn đặt hàng.
     */
    private static final Integer REF_TYPE = 3520;

    /**
     * Show page for adding sale order.
     *
     * @return
     */
    @RequestMapping(value = "/add_sa_order", method = RequestMethod.GET)
    public ModelAndView showPageAddSaleOrder() {
        ModelAndView modelAndView = new ModelAndView("sa_order/add");
        SAOrderForm saOrderForm = new SAOrderForm();
        List<SAOrderDetail> saOrderDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            SAOrderDetail saOrderDetail = new SAOrderDetail();
            saOrderDetailList.add(saOrderDetail);
        }
        saOrderForm.setSaOrderDetailList(saOrderDetailList);
        modelAndView.addObject("saOrderForm", saOrderForm);
        modelAndView.getModel().put("page_title", "Thêm Đơn đặt hàng");
        return modelAndView;
    }

    /**
     * Submit data adding sale order.
     *
     * @param saOrderForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_sa_order", method = RequestMethod.POST)
    public String submitDataAddSaleOrder(@ModelAttribute("saOrderForm") @Valid SAOrderForm saOrderForm, BindingResult bindingResult, Authentication authentication){
        if (bindingResult.hasErrors()){
            return "sa_order/add";
        }
        SAOrder saOrder = new SAOrder();
        saOrder.setRefNo(saOrderForm.getRefNo());
        saOrder.setRefType(REF_TYPE);
        saOrder.setRefDate(saOrderForm.getRefDate());
        saOrder.setStatus(saOrderForm.getStatus());
        saOrder.setBranchId(1); // Mã đơn vị là công ty.
        saOrder.setAccountObjectId(saOrderForm.getAccountObjectId());
        saOrder.setAccountObjectAddress(saOrderForm.getAccountObjectAddress());
        saOrder.setAccountObjectTaxCode(saOrderForm.getAccountObjectTaxCode());
        saOrder.setReceiver(saOrderForm.getReceiver());
        saOrder.setEmployeeId(saOrderForm.getEmployeeId());
        saOrder.setDeliveryDate(saOrderForm.getDeliveryDate());
        if (saOrderForm.getIsCalculatedCost() != null){
            saOrder.setIsCalculatedCost(Boolean.TRUE);
        }else {
            saOrder.setIsCalculatedCost(Boolean.FALSE);
        }
        saOrder.setPaymentTermId(saOrderForm.getPaymentTermId());
        saOrder.setDueDay(saOrderForm.getDueDay());
        saOrder.setJournalMemo(saOrderForm.getJournalMemo());
        saOrder.setShippingAddress(saOrderForm.getShippingAddress());
        saOrder.setOtherTerm(saOrderForm.getOtherTerm());
        saOrder.setQuoteRefId(saOrderForm.getQuoteRefId());
        saOrder.setCurrencyId(saOrderForm.getCurrencyId());
        saOrder.setExchangeRate(saOrderForm.getExchangeRate());
        saOrder.setTotalAmountOc(saOrderForm.getTotalAmountOc());
        saOrder.setTotalAmount(saOrderForm.getTotalAmount());
        saOrder.setTotalDiscountAmountOc(saOrderForm.getTotalDiscountAmountOc());
        saOrder.setTotalDiscountAmount(saOrderForm.getTotalDiscountAmount());
        saOrder.setTotalVatAmountOc(saOrderForm.getTotalVatAmountOc());
        saOrder.setTotalVatAmount(saOrderForm.getTotalVatAmount());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        saOrder.setCreatedDate(now);
        saOrder.setCreatedBy(authentication.getName());
        saOrder.setLastYearInvoiceAmountOc(BigDecimal.ZERO); // FIXME. NOT NULL.
        saOrder.setLastYearInvoiceAmount(BigDecimal.ZERO); //FIXME. NOT NULL.
        saOrderRepository.save(saOrder);
        Integer saOrderId = saOrder.getId();
        for (int i = 0; i < 10; i++){
            if (!saOrderForm.getSaOrderDetailList().get(i).getDescription().isEmpty() && !saOrderForm.getSaOrderDetailList().get(i).getDescription().isBlank() &&
                    saOrderForm.getSaOrderDetailList().get(i).getDescription() != null) {
                SAOrderDetail saOrderDetail = new SAOrderDetail();
                SAOrderDetail subForm = saOrderForm.getSaOrderDetailList().get(i);
                saOrderDetail.setId(subForm.getId());
                saOrderDetail.setDescription(subForm.getDescription());
                saOrderDetail.setAmountOc(subForm.getAmountOc());
                saOrderDetail.setAmount(subForm.getAmount());
                saOrderDetail.setContractId(subForm.getContractId());
                saOrderDetail.setCustomField1(subForm.getCustomField1());
                saOrderDetail.setCustomField2(subForm.getCustomField2());
                saOrderDetail.setCustomField3(subForm.getCustomField3());
                saOrderDetail.setCustomField4(subForm.getCustomField4());
                saOrderDetail.setCustomField5(subForm.getCustomField5());
                saOrderDetail.setCustomField6(subForm.getCustomField6());
                saOrderDetail.setCustomField7(subForm.getCustomField7());
                saOrderDetail.setCustomField8(subForm.getCustomField8());
                saOrderDetail.setCustomField9(subForm.getCustomField9());
                saOrderDetail.setCustomField10(subForm.getCustomField10());
                saOrderDetailRepository.save(saOrderDetail);
            }
        }
        return "redirect:/desktop";
    }

}
