package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.CollectMoneyForm;
import com.example.dto.CollectMoneySearchForm;
import com.example.repository.GeneralLedgerRepository;

/**
 * Cho màn hình Thu tiền Khách hàng. Các chức năng thu tiền khác như Thu tiền hoàn ứng,
 * Thu tiền hoàn thuế GTGT, v.v.. sử dụng /add_cash_receipt
 */
@Controller
public class CashSubSystemController {

    /**
     * Chắc chắn sử dụng do Thay đổi Nợ, Có.
     */
    @Autowired
    GeneralLedgerRepository generalLedgerRepository;

    /**
     * Hiển thị Trang Thu tiền Khách hàng.
     *
     * @return
     */
    @RequestMapping(value = "/collect_money", method = RequestMethod.GET)
    public ModelAndView showCollectMoney() {
        ModelAndView modelAndView = new ModelAndView("cash/collect_money");
        modelAndView.getModel().put("page_title", "Thu tiền khách hàng");
        return modelAndView;
    }

    /**
     * Trả về kết quả tìm kiếm chứng từ theo Khách hàng.
     *
     * @param collectMoneySearchForm
     * @return
     */
    @RequestMapping(value = "/collect_money_search", method = RequestMethod.POST)
    public ModelAndView collectMoney(@ModelAttribute("collectMoneySearchForm") CollectMoneySearchForm collectMoneySearchForm) {
        CollectMoneyForm collectMoneyForm = new CollectMoneyForm();
        Integer accountObjectCustomerId = collectMoneySearchForm.getAccountObjectId();
        System.out.println("accountObjectCustomerId = " + accountObjectCustomerId);

        //TODO: Màn hình này tạm dừng do chưa có chứng từ.
        // Phải có chứng từ thì mới có căn cứ để thu tiền.

        ModelAndView modelAndView = new ModelAndView("cash/collect_money");
        modelAndView.getModel().put("page_title", "Thu tiền khách hàng");
        // collectMoneyForm.setAccountObjectCustomerCode(""); //FIXME: change to AccountObjetCustomerId
        modelAndView.addObject("collectMoneyForm", collectMoneyForm);
        // Không phải trả về String, vì đây là để trả về kết quả tìm kiếm chứng từ.
        return modelAndView;
    }




}
