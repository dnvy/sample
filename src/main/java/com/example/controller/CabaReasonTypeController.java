package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.entity.CabaReasonType;
import com.example.repository.CabaReasonTypeRepository;

import java.util.List;

/**
 * Kiểu lý do của Giao dịch Tiền mặt - Ngân hàng.
 */
@Controller
public class CabaReasonTypeController {

    /**
     * Phiếu thu.
     */
    private static final Integer REF_TYPE_CASH_RECEIPT = 1010;

    /**
     * Phiếu thu có mã loại = 1020. Xem table SYSAddNewDefaultValue .
     */
    private static final Integer REF_TYPE_CA_PAYMENT = 1020;

    @Autowired
    CabaReasonTypeRepository cabaReasonTypeRepository;

    /**
     * Cash - bank reason type for cash receipt JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/caba_reason_type_for_cash_receipt_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String cashBankTypeReasonForCashReceiptJSON() throws JsonProcessingException {
        List<CabaReasonType> cabaReasonTypeList = cabaReasonTypeRepository.findByRefType(REF_TYPE_CASH_RECEIPT);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(cabaReasonTypeList);
        return jsonDataString;
    }

    /**
     * List refType cho màn hình Phiếu chi.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/ref_type_cash_payment_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String refTypeCashPaymentJSON() throws JsonProcessingException {
        List<CabaReasonType> cabaReasonTypeList = cabaReasonTypeRepository.findByRefType(REF_TYPE_CA_PAYMENT);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(cabaReasonTypeList);
        return jsonDataString;
    }

}
