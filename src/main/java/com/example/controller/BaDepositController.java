package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.BaDeposit;
import com.example.entity.BaDepositDetail;
import com.example.entity.SysAutoId;
import com.example.form.BaDepositForm;
import com.example.repository.BaDepositDetailRepository;
import com.example.repository.BaDepositRepository;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Bảng master của chứng từ thu tiền gửi
 * Là Master của các bảng detail sau:
 * -BADepositeDetail
 * -BADepositeDetailSale
 * -BADepositeDetailFixedAsset
 */
@Controller
public class BaDepositController {

    @Autowired
    BaDepositRepository baDepositRepository;

    @Autowired
    BaDepositDetailRepository baDepositDetailRepository;

    /**
     * Mã tham chiếu màn hình Nộp tiền vào tài khoản Ngân hàng.
     */
    private static final Integer REF_TYPE_BANK_DEPOSIT = 1500;

    /**
     * SELECT * FROM SYSAutoID WHERE RefTypeCategory = 150;
     */
    private static final Integer REF_TYPE_CATEGORY_SYS_AUTOID = 150;

    /**
     * Để nhảy số chứng từ tự động.
     */
    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    /**
     * Show page add bank deposit. Dùng chung cho cả 5 lý do thu
     * 1. Vay nợ
     * 2. Thu lãi đầu tư tài chính
     * 3. Thu hoàn ứng
     * 4. Thu hoàn thuế GTGT
     * 5. Thu khác
     *
     * @return
     */
    @RequestMapping(value = "/add_ba_deposit", method = RequestMethod.GET)
    public ModelAndView showPageAddBankDeposit() {
        ModelAndView modelAndView = new ModelAndView("ba_deposit/add");
        BaDepositForm baDepositForm = new BaDepositForm();
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        String voucherNumber = sysAutoIdController.generateVoucherNumber(REF_TYPE_CATEGORY_SYS_AUTOID, sysAutoIdRepository);
        baDepositForm.setRefNoFinance(voucherNumber);
        baDepositForm.setRefNoManagement(voucherNumber);
        baDepositForm.setRefNoCommon(voucherNumber);
        List<BaDepositDetail> baDepositDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            BaDepositDetail baDepositDetail = new BaDepositDetail();
            baDepositDetailList.add(baDepositDetail);
        }
        baDepositForm.setBaDepositDetailList(baDepositDetailList);
        modelAndView.addObject("baDepositForm", baDepositForm);
        modelAndView.getModel().put("page_title", "Nộp tiền vào Tài khoản ngân hàng");
        return modelAndView;
    }


    /**
     * Submit data add bank deposit.
     *
     * @param baDepositForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_ba_deposit", method = RequestMethod.POST)
    public String submitDataAddBankDeposit(@ModelAttribute("baDepositForm") @Valid BaDepositForm baDepositForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "ba_deposit/add";
        }
        BaDeposit baDeposit = new BaDeposit();
        baDeposit.setAccountObjectId(baDepositForm.getAccountObjectId());
        baDeposit.setRefType(REF_TYPE_BANK_DEPOSIT);
        baDeposit.setPostedDate(baDepositForm.getPostedDate());
        baDeposit.setRefDate(baDepositForm.getRefDate());
        if (baDepositForm.getDisplayOnBook() == 2) {
            baDeposit.setDisplayOnBook(2);
            baDeposit.setRefNoFinance(baDepositForm.getRefNoCommon());
            baDeposit.setRefNoManagement(baDepositForm.getRefNoCommon());
            baDeposit.setIsPostedFinance(Boolean.TRUE);
            baDeposit.setIsPostedManagement(Boolean.TRUE);
        } else if (baDepositForm.getDisplayOnBook() == 0) { // Finance
            baDeposit.setDisplayOnBook(0);
            baDeposit.setRefNoFinance(baDepositForm.getRefNoCommon());
            baDeposit.setIsPostedFinance(Boolean.TRUE);
            baDeposit.setIsPostedManagement(Boolean.FALSE);
        } else if (baDepositForm.getDisplayOnBook() == 1) { // Finance
            baDeposit.setDisplayOnBook(1);
            baDeposit.setRefNoManagement(baDepositForm.getRefNoCommon());
            baDeposit.setIsPostedFinance(Boolean.FALSE);
            baDeposit.setIsPostedManagement(Boolean.TRUE);
        }
        baDeposit.setAccountObjectId(baDepositForm.getAccountObjectId());
        baDeposit.setAccountObjectAddress(baDepositForm.getAccountObjectAddress());
        baDeposit.setBankAccountId(baDepositForm.getBankAccountId());
        baDeposit.setBankName(baDepositForm.getBankName());
        baDeposit.setReasonTypeId(baDepositForm.getReasonTypeId());
        baDeposit.setJournalMemo(baDepositForm.getJournalMemo());
        baDeposit.setCurrencyId(baDepositForm.getCurrencyId());
        baDeposit.setExchangeRate(baDepositForm.getExchangeRate());
        baDeposit.setTotalAmount(new BigDecimal(1)); // FIXME.
        baDeposit.setTotalAmountOc(new BigDecimal(0)); // FIXME.
        baDeposit.setEmployeeId(baDepositForm.getEmployeeId());
        baDeposit.setBranchId(1);
        baDeposit.setEditVersion(1);
        baDeposit.setRefOrder(1); // Số thứ tự nhập vào database.
        baDeposit.setIsCreateFromEbHistory(Boolean.FALSE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        baDeposit.setCreatedBy(authentication.getName());
        baDeposit.setCreatedDate(now);
        baDepositRepository.save(baDeposit);
        Integer baDepositId = baDeposit.getId();
        for (int i = 0; i < 10; i++) {
            if (!baDepositForm.getBaDepositDetailList().get(i).getDescription().isBlank() && !baDepositForm.getBaDepositDetailList().get(i).getDescription().isEmpty() &&
                    baDepositForm.getBaDepositDetailList().get(i).getAmountOc().compareTo(BigDecimal.ZERO) > 0) {
                BaDepositDetail subForm = baDepositForm.getBaDepositDetailList().get(i);
                BaDepositDetail baDepositDetail = new BaDepositDetail();
                baDepositDetail.setRefId(baDepositId);
                baDepositDetail.setDescription(subForm.getDescription());
                baDepositDetail.setDebitAccount(subForm.getDebitAccount());
                baDepositDetail.setCreditAccount(subForm.getCreditAccount());
                baDepositDetail.setAmountOc(subForm.getAmountOc());
                // baDepositDetail.setAmount(subForm.getAmount());
                baDepositDetail.setAmount(new BigDecimal(1));

                baDepositDetail.setAccountObjectId(subForm.getAccountObjectId());
                baDepositDetail.setBusinessType(subForm.getBusinessType());
                baDepositDetail.setBudgetItemId(subForm.getBudgetItemId());
                baDepositDetail.setOrganizationUnitId(subForm.getOrganizationUnitId());
                baDepositDetail.setListItemId(subForm.getListItemId());
                baDepositDetail.setBudgetItemId(subForm.getBudgetItemId());
                baDepositDetail.setOrderId(subForm.getOrderId());
                baDepositDetail.setContractId(subForm.getContractId());
                baDepositDetail.setProjectWorkId(subForm.getProjectWorkId());
                baDepositDetail.setExpenseItemId(subForm.getExpenseItemId());
                baDepositDetail.setDebtAgreementId(subForm.getDebtAgreementId());
                baDepositDetail.setJobId(subForm.getJobId());

                // baDepositDetail.setUnResonableCost(subForm.getUnResonableCost());
                baDepositDetail.setUnResonableCost(Boolean.FALSE); //FIXME.

                baDepositDetail.setBusinessType(subForm.getBusinessType());
                baDepositDetail.setPuContractId(subForm.getPuContractId());
                baDepositDetail.setPuOrderRefId(subForm.getPuOrderRefId());
                baDepositDetail.setCustomField1(subForm.getCustomField1());
                baDepositDetail.setCustomField2(subForm.getCustomField2());
                baDepositDetail.setCustomField3(subForm.getCustomField3());
                baDepositDetail.setCustomField4(subForm.getCustomField4());
                baDepositDetail.setCustomField5(subForm.getCustomField5());
                baDepositDetail.setCustomField6(subForm.getCustomField6());
                baDepositDetail.setCustomField7(subForm.getCustomField7());
                baDepositDetail.setCustomField8(subForm.getCustomField8());
                baDepositDetail.setCustomField9(subForm.getCustomField9());
                baDepositDetail.setCustomField10(subForm.getCustomField10());
                baDepositDetailRepository.save(baDepositDetail);
            }
        }
        Optional<SysAutoId> sysAutoIdOptional = sysAutoIdRepository.findById(1);
        SysAutoId sysAutoIdOld = sysAutoIdOptional.get();
        SysAutoId sysAutoId = new SysAutoId();
        sysAutoId.setId(1);
        sysAutoId.setRefTypeCategory(sysAutoIdOld.getRefTypeCategory());
        sysAutoId.setRefTypeCategoryName(sysAutoIdOld.getRefTypeCategoryName());
        sysAutoId.setPrefixString(sysAutoIdOld.getPrefixString());
        sysAutoId.setMaxExistValue(sysAutoIdOld.getMaxExistValue() + 1);
        sysAutoId.setLengthOfValue(sysAutoIdOld.getLengthOfValue());
        sysAutoId.setSuffix(sysAutoIdOld.getSuffix());
        sysAutoId.setBranchId(1);
        sysAutoId.setCreatedBy(sysAutoIdOld.getCreatedBy());
        sysAutoId.setCreatedDate(sysAutoIdOld.getCreatedDate());
        sysAutoId.setModifiedBy(authentication.getName());
        sysAutoId.setModifiedDate(now);
        sysAutoIdRepository.save(sysAutoId);
        return "redirect:/desktop";
    }

}
