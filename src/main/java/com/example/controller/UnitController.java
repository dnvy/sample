package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.Unit;
import com.example.form.UnitForm;
import com.example.repository.UnitRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Đơn vị tính.
 */
@Controller
public class UnitController {

    @Autowired
    UnitRepository unitRepository;

    /**
     * Show all units.
     *
     * @return
     */
    @RequestMapping(value = "/units", method = RequestMethod.GET)
    public ModelAndView showUnits() {
        ModelAndView modelAndView = new ModelAndView("unit/list");
        List<Unit> unitList = unitRepository.findAll();
        modelAndView.addObject("unitList", unitList);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Đơn vị tính");
        return modelAndView;
    }

    /**
     * Show page for adding new unit.
     *
     * @return
     */
    @RequestMapping(value = "/add_unit", method = RequestMethod.GET)
    public ModelAndView showPageAddUnit() {
        ModelAndView modelAndView = new ModelAndView("unit/add");
        UnitForm unitForm = new UnitForm();
        modelAndView.addObject("unitForm", unitForm);
        modelAndView.getModel().put("page_title", "Thêm Đơn vị tính");
        return modelAndView;
    }

    /**
     * Submit data when add unit.
     *
     * @param unitForm
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/add_unit", method = RequestMethod.POST)
    public String submitAddUnit(@ModelAttribute("unitForm") @Valid UnitForm unitForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "unit/add";
        }
        Unit unit = new Unit();
        unit.setActiveStatus(Boolean.TRUE);
        unit.setUnitName(unitForm.getUnitName());
        unit.setDescription(unitForm.getDescription());
        unitRepository.save(unit);
        return "redirect:/units";
    }

    /**
     * Show page for editing unit.
     *
     * @return
     */
    @RequestMapping(value = "/unit/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageEditUnit(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("unit/edit");
        Optional<Unit> unitOptional = unitRepository.findById(id);
        Unit unit = unitOptional.get();
        UnitForm unitForm = new UnitForm();
        unitForm.setId(id);
        unitForm.setActiveStatus(unit.getActiveStatus());
        unitForm.setUnitName(unit.getUnitName());
        unitForm.setDescription(unit.getDescription());
        modelAndView.addObject("unitForm", unitForm);
        modelAndView.getModel().put("page_title", "Sửa Đơn vị tính");
        return modelAndView;
    }

    /**
     * Submit data edit unit.
     *
     * @param unitForm
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/edit_unit", method = RequestMethod.POST)
    public String submitDataEditUnit(@ModelAttribute("unitForm") UnitForm unitForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "/unit/edit";
        }
        Unit unit = new Unit();
        unit.setId(unitForm.getId());
        unit.setUnitName(unitForm.getUnitName());
        unit.setDescription(unitForm.getDescription());
        // Bởi vì Kendo UI checkbox, khi uncheck, thì trả về NULL.
        if (unitForm.getActiveStatus() == null){
            unit.setActiveStatus(Boolean.FALSE);
        }else {
            unit.setActiveStatus(unitForm.getActiveStatus());
        }
        unitRepository.save(unit);
        return "redirect:/units";
    }

    /**
     * View detail an unit.
     *
     * @return
     */
    @RequestMapping(value = "/unit/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewUnit(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("unit/view");
        Optional<Unit> unitOptional = unitRepository.findById(id);
        Unit unit = unitOptional.get();
        UnitForm unitForm = new UnitForm();
        BeanUtils.copyProperties(unit, unitForm);
        modelAndView.addObject("unitForm", unitForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Đơn vị tính");
        return modelAndView;
    }

    /**
     * Delete an unit item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/unit/delete/{id}", method = RequestMethod.POST)
    public String deleteUnit(@PathVariable("id") Integer id, HttpServletRequest request) {
        Unit unit = unitRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid unit id: " + id));
        //FIXME: Kiểm tra constrain.
        // Các table có FK:

        unitRepository.delete(unit);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * All units has active_status = TRUE in JSON.
     *
     * @return
     * @throws JsonProcessingException
     * @see InventoryItemController#addInventoryItem()
     */
    @RequestMapping(value = "/active_units_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String activeUnitJSON() throws JsonProcessingException {
        List<Unit> activeUnitList = unitRepository.getActiveUnits();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(activeUnitList);
        return jsonDataString;
    }

}
