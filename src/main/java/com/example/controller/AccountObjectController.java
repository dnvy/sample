package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.AccountObjectDropDownItem;
import com.example.common.DropDownListItem;
import com.example.common.NumberStringDropDownListItem;
import com.example.common.UtilityList;
import com.example.dto.EmployeeDTO;
import com.example.dropdownlist.EmployeeDropDownListDTO;
import com.example.entity.AccountObject;
import com.example.entity.AccountObjectBankAccount;
import com.example.entity.AccountObjectGroup;
import com.example.entity.AccountObjectShippingAddress;
import com.example.entity.OrganizationUnit;
import com.example.form.AccountObjectForm;
import com.example.repository.AccountObjectBankAccountRepository;
import com.example.repository.AccountObjectGroupRepository;
import com.example.repository.AccountObjectRepository;
import com.example.repository.AccountObjectShippingAddressRepository;
import com.example.repository.OrganizationUnitRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Đối tượng kế toán
 */
@Controller
public class AccountObjectController {

    @Autowired
    AccountObjectRepository accountObjectRepository;

    @Autowired
    AccountObjectBankAccountRepository accountObjectBankAccountRepository;

    @Autowired
    AccountObjectGroupRepository accountObjectGroupRepository;

    @Autowired
    AccountObjectShippingAddressRepository accountObjectShippingAddressRepository;

    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    /**
     * AccountObject is organization has AccountObjectType = 0.
     */
    private static final Integer ORGANIZATIONAL_ACCOUNT_OBJECT_TYPE = 0;

    /**
     * AccountObject is person has AccountObjectType = 1.
     */
    private static final Integer PERSONAL_ACCOUNT_OBJECT_TYPE = 1;

    // Đối tượng kế toán.

    // 1. Xóa Đối tượng kế toán.

    /**
     * Delete account object. Then redirect to itself. Use for also customer, employee, supplier.
     * Dùng chung cho cả Người mua hàng, Người bán hàng, Nhân viên.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/account_object/delete/{id}", method = RequestMethod.POST)
    public String deleteAccountObject(@PathVariable("id") Integer id, HttpServletRequest request) {
        AccountObject accountObject = accountObjectRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        accountObjectRepository.delete(accountObject);
        return "redirect:" + request.getHeader("Referer");
    }

    // 2. Khách hàng.

    // 2.1. Danh sách Khách hàng (Tổ chức + Cá nhân)

    /**
     * Show all customers (organization + personal).
     *
     * @return
     */
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public ModelAndView orgCustomerList() {
        ModelAndView modelAndView = new ModelAndView("account_object/customer/list");
        List<AccountObject> accountObjectList = accountObjectRepository.getListOfCustomers();
        modelAndView.addObject("accountObjectList", accountObjectList);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Danh sách Khách hàng (Tổ chức + cá nhân)");
        return modelAndView;
    }

    // 2.2. Khách hàng tổ chức

    // 2.2.1. Thêm khách hàng tổ chức.

    // 2.2.1.1. Hiển thị trang để thêm Khách hàng tổ chức.

    /**
     * Show page for adding org customer.
     *
     * @return
     */
    @RequestMapping(value = "/add_org_customer", method = RequestMethod.GET)
    public ModelAndView showPageAddOrgCustomer() {
        // TODO: Sử dụng cascadeFormField để tăng performance khi chọn Location.
        // https://docs.telerik.com/kendo-ui/api/javascript/ui/dropdownlist/configuration/cascadefromfield

        ModelAndView modelAndView = new ModelAndView("account_object/customer/org/add");
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAllByActiveStatus(Boolean.TRUE);
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
            accountObjectShippingAddressList.add(accountObjectShippingAddress);
        }
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.addObject("accountObjectGroupList", accountObjectGroupList);
        modelAndView.getModel().put("page_title", "Thêm Khách hàng Tổ chức");
        return modelAndView;
    }

    // 2.2.1.2. Submit dữ liệu thêm Khách hàng tổ chức.

    /**
     * Submit data Thêm Khách hàng tổ chức.
     *
     * @param accountObjectForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_org_customer", method = RequestMethod.POST)
    public String addOrganizationCustomer(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, Authentication authentication) {
        // Ghi vào bảng AccountObject.
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        accountObject.setIsCustomer(Boolean.TRUE);
        // Khi thêm mới, trạng thái theo dõi luôn là TRUE.
        accountObject.setActiveStatus(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setCreatedDate(now);
        accountObject.setCreatedBy(authentication.getName());
        accountObject.setAccountObjectType(ORGANIZATIONAL_ACCOUNT_OBJECT_TYPE);
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountObjectShippingAddress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/customers";
    }

    // 2.2.2. Sửa khách hàng tổ chức.

    // 2.2.2.1. Hiển thị trang để sửa Khách hàng tổ chức.

    /**
     * Hiển thị trang để sửa khách hàng tổ chức.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/edit/0/{id}")
    public ModelAndView showPageForEditOrgCustomer(@PathVariable("id") Integer id) {
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
        AccountObject accountObject = accountObjectOptional.get();
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        BeanUtils.copyProperties(accountObject, accountObjectForm);

        List<AccountObjectBankAccount> accountObjectBankAccountListOld = accountObjectBankAccountRepository.findByAccountObjectId(id);
        int sizeBankList = accountObjectBankAccountListOld.size();
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            if (i < sizeBankList) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectBankAccountListOld.get(i);
                accountObjectBankAccountList.add(accountObjectBankAccount);
            } else {
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccountList.add(accountObjectBankAccount);
            }
        }
        // Hiển thị danh sách Địa chỉ giao nhận. Số lượng 5.
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        List<AccountObjectShippingAddress> accountObjectShippingAddressListOld = accountObjectShippingAddressRepository.findByAccountObjectId(id);
        int sizeShippingAddressList = accountObjectShippingAddressListOld.size();
        for (int i = 0; i < 5; i++) {
            if (i < sizeShippingAddressList) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectShippingAddressListOld.get(i);
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            } else {
                AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            }
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        ModelAndView modelAndView = new ModelAndView("account_object/customer/org/edit");
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        return modelAndView;
    }

    // 2.2.2.1. Submit dữ liệu sửa Khách hàng tổ chức.

    /**
     * Submit data Update Khách hàng tổ chức.
     *
     * @param accountObjectForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/update_org_customer", method = RequestMethod.POST)
    public String updateOrganizationCustomer(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, BindingResult bindingResult, Authentication authentication) {
        // Validate dữ liệu.
        if (bindingResult.hasErrors()) {
            return "account_object/customer/edit";
        }
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(accountObjectForm.getId());
        AccountObject accountObjectOld = accountObjectOptional.get();

        // Ghi vào bảng AccountObject.
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        accountObject.setCreatedBy(accountObjectOld.getCreatedBy());
        accountObject.setCreatedDate(accountObjectOld.getCreatedDate());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setModifiedDate(now);
        accountObject.setModifiedBy(authentication.getName());
        accountObject.setAccountObjectType(ORGANIZATIONAL_ACCOUNT_OBJECT_TYPE);
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                // Lưu ý, đây là lệnh update.
                //FIXME:
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountObjectShippingAddress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                // Lưu ý, đây là lệnh update.
                //FIXME:
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/customers";
    }

    // 2.2.3. View khách hàng tổ chức.

    /**
     * View detail organization customer.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/view/0/{id}")
    public ModelAndView viewOrgCustomer(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/customer/org/view");
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
        AccountObject accountObject = accountObjectOptional.get();
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        accountObjectForm.setId(id);
        accountObjectForm.setAccountObjectCode(accountObject.getAccountObjectCode());
        accountObjectForm.setAccountObjectName(accountObject.getAccountObjectName());
        accountObjectForm.setGender(accountObject.getGender());
        accountObjectForm.setBirthDate(accountObject.getBirthDate());
        accountObjectForm.setBirthPlace(accountObject.getBirthPlace());
        accountObjectForm.setAgreementSalary(accountObject.getAgreementSalary());
        accountObjectForm.setSalaryCoefficient(accountObject.getSalaryCoefficient());
        accountObjectForm.setNumberOfDependent(accountObject.getNumberOfDependent());
        accountObjectForm.setInsuranceSalary(accountObject.getInsuranceSalary());
        accountObjectForm.setBankAccount(accountObject.getBankAccount());
        accountObjectForm.setBankName(accountObject.getBankName());
        accountObjectForm.setAddress(accountObject.getAddress());
        accountObjectForm.setAccountObjectGroupList(accountObject.getAccountObjectGroupList());
        accountObjectForm.setAccountObjectGroupListCode(accountObject.getAccountObjectGroupListCode());
        accountObjectForm.setCompanyTaxCode(accountObject.getCompanyTaxCode());
        accountObjectForm.setTel(accountObject.getTel());
        accountObjectForm.setMobile(accountObject.getMobile());
        accountObjectForm.setFax(accountObject.getFax());
        accountObjectForm.setEmailAddress(accountObject.getEmailAddress());
        accountObjectForm.setWebsite(accountObject.getWebsite());
        accountObjectForm.setPaymentTermId(accountObject.getPaymentTermId());
        accountObjectForm.setMaxDebtAmount(accountObject.getMaxDebtAmount());
        accountObjectForm.setDueTime(accountObject.getDueTime());
        accountObjectForm.setIdentificationNumber(accountObject.getIdentificationNumber());
        accountObjectForm.setIssueDate(accountObject.getIssueDate());
        accountObjectForm.setIssueBy(accountObject.getIssueBy());
        accountObjectForm.setCountry(accountObject.getCountry());
        accountObjectForm.setProvinceOrCity(accountObject.getBankProvinceOrCity());
        accountObjectForm.setDistrict(accountObject.getDistrict());
        accountObjectForm.setWardOrCommune(accountObject.getWardOrCommune());
        accountObjectForm.setPrefix(accountObject.getPrefix());
        accountObjectForm.setContactName(accountObject.getContactName());
        accountObjectForm.setContactTitle(accountObject.getContactTitle());
        accountObjectForm.setContactMobile(accountObject.getContactMobile());
        accountObjectForm.setOtherContactMobile(accountObject.getOtherContactMobile());
        accountObjectForm.setContactFixedTel(accountObject.getContactFixedTel());
        accountObjectForm.setContactEmail(accountObject.getContactEmail());
        accountObjectForm.setContactAddress(accountObject.getContactAddress());
        accountObjectForm.setIsVendor(accountObject.getIsVendor());
        accountObjectForm.setIsCustomer(accountObject.getIsCustomer());
        accountObjectForm.setIsEmployee(accountObject.getIsEmployee());
        accountObjectForm.setAccountObjectType(accountObject.getAccountObjectType());
        accountObjectForm.setActiveStatus(accountObject.getActiveStatus());
        accountObjectForm.setOrganizationUnitId(accountObject.getOrganizationUnitId());
        accountObjectForm.setBranchId(accountObject.getBranchId());
        accountObjectForm.setCreatedBy(accountObject.getCreatedBy());
        accountObjectForm.setCreatedDate(accountObject.getCreatedDate());
        accountObjectForm.setModifiedBy(accountObject.getModifiedBy());
        accountObjectForm.setModifiedDate(accountObject.getModifiedDate());
        accountObjectForm.setReceiptableDebtAmount(accountObject.getReceiptableDebtAmount());
        accountObjectForm.setShippingAddress(accountObject.getShippingAddress());
        accountObjectForm.setAccountObjectGroupListName(accountObject.getAccountObjectGroupListName());
        accountObjectForm.setEmployeeId(accountObject.getEmployeeId());
        accountObjectForm.setDescription(accountObject.getDescription());
        accountObjectForm.setBankBranchName(accountObject.getBankBranchName());
        accountObjectForm.setBankProvinceOrCity(accountObject.getBankProvinceOrCity());
        accountObjectForm.setLegalRepresentative(accountObject.getLegalRepresentative());
        accountObjectForm.seteInvoiceContactName(accountObject.geteInvoiceContactName());
        accountObjectForm.seteInvoiceContactEmail(accountObject.geteInvoiceContactEmail());
        accountObjectForm.seteInvoiceContactAddress(accountObject.geteInvoiceContactAddress());
        accountObjectForm.seteInvoiceContactMobile(accountObject.geteInvoiceContactMobile());
        // Danh sách tài khoản ngân hàng.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        List<AccountObjectBankAccount> accountObjectBankAccountListHasValue = accountObjectBankAccountRepository.findByAccountObjectId(id);
        for (int i = 0; i < 5; i++){
            if(i < accountObjectBankAccountListHasValue.size()){
                accountObjectBankAccountList.add(accountObjectBankAccountListHasValue.get(i));
            }else {
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccountList.add(accountObjectBankAccount);
            }
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        // Danh sách địa chỉ giao nhận hàng.
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        List<AccountObjectShippingAddress> accountObjectShippingAddressListHasValue = accountObjectShippingAddressRepository.findByAccountObjectId(id);
        for (int i = 0; i < 5; i++){
            if(i < accountObjectShippingAddressListHasValue.size()){
                accountObjectShippingAddressList.add(accountObjectShippingAddressListHasValue.get(i));
            }else {
                AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            }
        }
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Khách hàng Tổ chức");
        return modelAndView;
    }

    /**
     * Xem chi tiết khách hàng cá nhân.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/view/1/{id}")
    public ModelAndView viewPersonalCustomer(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/customer/personal/view");
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
        AccountObject accountObject = accountObjectOptional.get();
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        accountObjectForm.setId(id);
        accountObjectForm.setAccountObjectCode(accountObject.getAccountObjectCode());
        accountObjectForm.setAccountObjectName(accountObject.getAccountObjectName());
        accountObjectForm.setGender(accountObject.getGender());
        accountObjectForm.setBirthDate(accountObject.getBirthDate());
        accountObjectForm.setBirthPlace(accountObject.getBirthPlace());
        accountObjectForm.setAgreementSalary(accountObject.getAgreementSalary());
        accountObjectForm.setSalaryCoefficient(accountObject.getSalaryCoefficient());
        accountObjectForm.setNumberOfDependent(accountObject.getNumberOfDependent());
        accountObjectForm.setInsuranceSalary(accountObject.getInsuranceSalary());
        accountObjectForm.setBankAccount(accountObject.getBankAccount());
        accountObjectForm.setBankName(accountObject.getBankName());
        accountObjectForm.setAddress(accountObject.getAddress());
        accountObjectForm.setAccountObjectGroupList(accountObject.getAccountObjectGroupList());
        accountObjectForm.setAccountObjectGroupListCode(accountObject.getAccountObjectGroupListCode());
        accountObjectForm.setCompanyTaxCode(accountObject.getCompanyTaxCode());
        accountObjectForm.setTel(accountObject.getTel());
        accountObjectForm.setMobile(accountObject.getMobile());
        accountObjectForm.setFax(accountObject.getFax());
        accountObjectForm.setEmailAddress(accountObject.getEmailAddress());
        accountObjectForm.setWebsite(accountObject.getWebsite());
        accountObjectForm.setPaymentTermId(accountObject.getPaymentTermId());
        accountObjectForm.setMaxDebtAmount(accountObject.getMaxDebtAmount());
        accountObjectForm.setDueTime(accountObject.getDueTime());
        accountObjectForm.setIdentificationNumber(accountObject.getIdentificationNumber());
        accountObjectForm.setIssueDate(accountObject.getIssueDate());
        accountObjectForm.setIssueBy(accountObject.getIssueBy());
        accountObjectForm.setCountry(accountObject.getCountry());
        accountObjectForm.setProvinceOrCity(accountObject.getBankProvinceOrCity());
        accountObjectForm.setDistrict(accountObject.getDistrict());
        accountObjectForm.setWardOrCommune(accountObject.getWardOrCommune());
        accountObjectForm.setPrefix(accountObject.getPrefix());
        accountObjectForm.setContactName(accountObject.getContactName());
        accountObjectForm.setContactTitle(accountObject.getContactTitle());
        accountObjectForm.setContactMobile(accountObject.getContactMobile());
        accountObjectForm.setOtherContactMobile(accountObject.getOtherContactMobile());
        accountObjectForm.setContactFixedTel(accountObject.getContactFixedTel());
        accountObjectForm.setContactEmail(accountObject.getContactEmail());
        accountObjectForm.setContactAddress(accountObject.getContactAddress());
        accountObjectForm.setIsVendor(accountObject.getIsVendor());
        accountObjectForm.setIsCustomer(accountObject.getIsCustomer());
        accountObjectForm.setIsEmployee(accountObject.getIsEmployee());
        accountObjectForm.setAccountObjectType(accountObject.getAccountObjectType());
        accountObjectForm.setActiveStatus(accountObject.getActiveStatus());
        accountObjectForm.setOrganizationUnitId(accountObject.getOrganizationUnitId());
        accountObjectForm.setBranchId(accountObject.getBranchId());
        accountObjectForm.setCreatedBy(accountObject.getCreatedBy());
        accountObjectForm.setCreatedDate(accountObject.getCreatedDate());
        accountObjectForm.setModifiedBy(accountObject.getModifiedBy());
        accountObjectForm.setModifiedDate(accountObject.getModifiedDate());
        accountObjectForm.setReceiptableDebtAmount(accountObject.getReceiptableDebtAmount());
        accountObjectForm.setShippingAddress(accountObject.getShippingAddress());
        accountObjectForm.setAccountObjectGroupListName(accountObject.getAccountObjectGroupListName());
        accountObjectForm.setEmployeeId(accountObject.getEmployeeId());
        accountObjectForm.setDescription(accountObject.getDescription());
        accountObjectForm.setBankBranchName(accountObject.getBankBranchName());
        accountObjectForm.setBankProvinceOrCity(accountObject.getBankProvinceOrCity());
        accountObjectForm.setLegalRepresentative(accountObject.getLegalRepresentative());
        accountObjectForm.seteInvoiceContactName(accountObject.geteInvoiceContactName());
        accountObjectForm.seteInvoiceContactEmail(accountObject.geteInvoiceContactEmail());
        accountObjectForm.seteInvoiceContactAddress(accountObject.geteInvoiceContactAddress());
        accountObjectForm.seteInvoiceContactMobile(accountObject.geteInvoiceContactMobile());
        // Danh sách tài khoản ngân hàng.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        List<AccountObjectBankAccount> accountObjectBankAccountListHasValue = accountObjectBankAccountRepository.findByAccountObjectId(id);
        for (int i = 0; i < 5; i++){
            if(i < accountObjectBankAccountListHasValue.size()){
                accountObjectBankAccountList.add(accountObjectBankAccountListHasValue.get(i));
            }else {
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccountList.add(accountObjectBankAccount);
            }
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        // Danh sách địa chỉ giao nhận hàng.
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        List<AccountObjectShippingAddress> accountObjectShippingAddressListHasValue = accountObjectShippingAddressRepository.findByAccountObjectId(id);
        for (int i = 0; i < 5; i++){
            if(i < accountObjectShippingAddressListHasValue.size()){
                accountObjectShippingAddressList.add(accountObjectShippingAddressListHasValue.get(i));
            }else {
                AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            }
        }
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Khách hàng Cá nhân");
        return modelAndView;
    }

    /**
     * Show page for adding personal customer.
     *
     * @return
     */
    @RequestMapping(value = "/add_personal_customer", method = RequestMethod.GET)
    public ModelAndView showPageForAddPersonalCustomer() {
        ModelAndView modelAndView = new ModelAndView("account_object/customer/personal/add");
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAllByActiveStatus(Boolean.TRUE);
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
            accountObjectShippingAddressList.add(accountObjectShippingAddress);
        }
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.addObject("accountObjectGroupList", accountObjectGroupList);
        modelAndView.getModel().put("page_title", "Thêm Khách hàng Cá nhân");
        return modelAndView;
    }

    // 2.3.1.2. Submit dữ liệu thêm khách hàng cá nhân.

    /**
     * Submit data Thêm Khách hàng tổ chức.
     *
     * @param accountObjectForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_personal_customer", method = RequestMethod.POST)
    public String addPersonalCustomer(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, Authentication authentication) {
        // Ghi vào bảng AccountObject.
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        accountObject.setIsCustomer(Boolean.TRUE);
        // Khi thêm mới, trạng thái theo dõi luôn là TRUE.
        accountObject.setActiveStatus(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setCreatedDate(now);
        accountObject.setCreatedBy(authentication.getName());
        accountObject.setAccountObjectType(PERSONAL_ACCOUNT_OBJECT_TYPE);
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountObjectShippingAddress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/customers";
    }

    // 2.3.2. Sửa Khách hàng cá nhân.


    /**
     * Show page for adding employee.
     *
     * @return
     */
    @RequestMapping(value = "/add_employee", method = RequestMethod.GET)
    public ModelAndView showAddEmployee() {
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        ModelAndView modelAndView = new ModelAndView("account_object/employee/add");
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Thêm Nhân viên");
        return modelAndView;
    }

    /**
     * Show page for adding org supplier.
     *
     * @return
     */
    @RequestMapping(value = "/add_org_supplier", method = RequestMethod.GET)
    public ModelAndView addOrgSupplier() {
        ModelAndView modelAndView = new ModelAndView("account_object/supplier/org/add");
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAllByActiveStatus(Boolean.TRUE);
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
            accountObjectShippingAddressList.add(accountObjectShippingAddress);
        }
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.addObject("accountObjectGroupList", accountObjectGroupList);
        modelAndView.getModel().put("page_title", "Thêm Nhà cung cấp là Tổ chức");
        return modelAndView;
    }

    /**
     * Show page for adding personal supplier.
     *
     * @return
     */
    @RequestMapping(value = "/add_personal_supplier", method = RequestMethod.GET)
    public ModelAndView addPersonalSupplier() {
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        ModelAndView modelAndView = new ModelAndView("account_object/supplier/personal/add");
        // Thêm 5 AccountObjectBankAccount and 5 AccountObjectShippingAddress holders.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
            accountObjectShippingAddressList.add(accountObjectShippingAddress);
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Thêm Nhà cung cấp cá nhân");
        return modelAndView;
    }

    /**
     * Show all employees.
     *
     * @return
     */
    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    public ModelAndView employeeList() {
        ModelAndView modelAndView = new ModelAndView("account_object/employee/list");
        List<AccountObject> accountObjectList = accountObjectRepository.getListOfActiveEmployees();
        List<EmployeeDTO> employeeDTOList = new ArrayList<>();
        for (AccountObject accountObject : accountObjectList) {
            EmployeeDTO employeeDTO = new EmployeeDTO();
            employeeDTO.setId(accountObject.getId());
            employeeDTO.setAccountObjectCode(accountObject.getAccountObjectCode());
            employeeDTO.setAccountObjectName(accountObject.getAccountObjectName());
            employeeDTO.setBankAccount(accountObject.getBankAccount());
            employeeDTO.setBankName(accountObject.getBankName());
            employeeDTO.setContactTitle(accountObject.getContactTitle());
            if (accountObject.getOrganizationUnitId() != null) {
                Optional<OrganizationUnit> organizationUnit = organizationUnitRepository.findById(accountObject.getOrganizationUnitId());
                organizationUnit.ifPresent(orgUnit -> employeeDTO.setOrganizationUnitName(orgUnit.getOrganizationUnitName()));
            }
            employeeDTO.setActiveStatus(accountObject.getActiveStatus());
            employeeDTO.setInsuranceSalary(accountObject.getInsuranceSalary());
            employeeDTO.setIssueBy(accountObject.getIssueBy());
            employeeDTO.setNumberOfDependent(accountObject.getNumberOfDependent());
            employeeDTO.setAddress(accountObject.getAddress());
            employeeDTO.setMobile(accountObject.getMobile());
            employeeDTO.setEmailAddress(accountObject.getEmailAddress());
            employeeDTO.setTel(accountObject.getTel());
            employeeDTOList.add(employeeDTO);
        }
        modelAndView.addObject("employeeDTOList", employeeDTOList);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Danh sách Nhân viên");
        return modelAndView;
    }

    /**
     * View detail information of an employee.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/employee/view/{id}")
    public ModelAndView viewEmployee(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/employee/view");
        Optional<AccountObject> accountObject = accountObjectRepository.findById(id);
        EmployeeDTO employeeDTO = new EmployeeDTO();
        accountObject.ifPresent(obj -> {
            employeeDTO.setAccountObjectCode(obj.getAccountObjectCode());
            employeeDTO.setContactTitle(obj.getContactTitle());
            employeeDTO.setOrganizationUnitId(obj.getOrganizationUnitId());
            employeeDTO.setGender(obj.getGender());
            employeeDTO.setBirthDate(obj.getBirthDate());
            employeeDTO.setAgreementSalary(obj.getAgreementSalary());
            employeeDTO.setIdentificationNumber(obj.getIdentificationNumber());
            employeeDTO.setSalaryCoefficient(obj.getSalaryCoefficient());
            employeeDTO.setIssueDate(obj.getIssueDate());
            employeeDTO.setIssueBy(obj.getIssueBy());
        });
        UtilityList.setGenderList(modelAndView);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("employeeDTO", employeeDTO);
        Map<Integer, String> accountObjectTypeList = new HashMap<>();
        accountObjectTypeList.put(0, "Nhà cung cấp");
        accountObjectTypeList.put(1, "Khách hàng");
        accountObjectTypeList.put(2, "Nhân viên");
        modelAndView.addObject("accountObjectTypeList", accountObjectTypeList);
        return modelAndView;
    }

    /**
     * List all suppliers (organizational and personal suppliers).
     *
     * @return
     */
    @RequestMapping(value = "/suppliers", method = RequestMethod.GET)
    public ModelAndView suppliers() {
        ModelAndView modelAndView = new ModelAndView("account_object/supplier/list");
        List<AccountObject> suppliers = accountObjectRepository.listVendors();
        modelAndView.addObject("suppliers", suppliers);
        modelAndView.getModel().put("page_title", "Nhà cung cấp");
        UtilityList.setActiveStatusList(modelAndView);
        return modelAndView;
    }

    /**
     * Add an employee.
     *
     * @param accountObjectForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_employee", method = RequestMethod.POST)
    public String addEmployee(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, BindingResult bindingResult, Authentication authentication) {
        // Validate dữ liệu.
        List<AccountObjectBankAccount> accountObjectBankAccountList = accountObjectForm.getAccountObjectBankAccountList();
        for (int i = 0; i < accountObjectBankAccountList.size(); i++) {
            if (!accountObjectBankAccountList.get(i).getBankAccount().isBlank() && !accountObjectBankAccountList.get(i).getBankAccount().isEmpty()) {
                // If bank_account and bank are not null, it is a valid bank_account, then allow to save.
                // If not, return error.
                if (!accountObjectBankAccountList.get(i).getBankAccount().isEmpty() && !accountObjectBankAccountList.get(i).getBankAccount().isBlank()) {
                    if (accountObjectBankAccountList.get(i).getBankId() == null) {
                        ObjectError objectError = new ObjectError("id", "Số tài khoản ngân hàng không hợp lệ");
                        bindingResult.addError(objectError);
                    }
                }
            }
        }
        if (bindingResult.hasErrors()) {
            return "account_object/employee/add";
        }
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        accountObject.setIsEmployee(Boolean.TRUE);
        accountObject.setActiveStatus(Boolean.TRUE);
        accountObject.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setCreatedDate(now);
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.

        for (int i = 0; i < accountObjectBankAccountList.size(); i++) {
            if (!accountObjectBankAccountList.get(i).getBankAccount().isBlank() && !accountObjectBankAccountList.get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                accountObjectBankAccount.setBankAccount(accountObjectBankAccountList.get(i).getBankAccount());
                accountObjectBankAccount.setBankId(accountObjectBankAccountList.get(i).getBankId());
                accountObjectBankAccount.setBankName(accountObjectBankAccountList.get(i).getBankName());
                accountObjectBankAccount.setProvince(accountObjectBankAccountList.get(i).getProvince());
                // If bank_account and bank are not null, it is a valid bank_account, then allow to save.
                // If not, return error.
                if (!accountObjectBankAccountList.get(i).getBankAccount().isEmpty() && !accountObjectBankAccountList.get(i).getBankAccount().isBlank()) {
                    accountObjectBankAccountRepository.save(accountObjectBankAccount);
                } else {
                    ObjectError objectError = new ObjectError("email", "Số tài khoản ngân hàng không hợp lệ");
                    bindingResult.addError(objectError);
                }

            }
        }

        return "redirect:/employees";
    }

    /**
     * Add new organizational supplier.
     *
     * @return
     */
    @RequestMapping(value = "/add_org_supplier", method = RequestMethod.POST)
    public String addOrgSupplier(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, Authentication authentication) {
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        accountObject.setIsVendor(Boolean.TRUE);
        accountObject.setActiveStatus(Boolean.TRUE);
        accountObject.setAccountObjectType(ORGANIZATIONAL_ACCOUNT_OBJECT_TYPE);
        accountObject.setIsEmployee(Boolean.FALSE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setCreatedDate(now);
        accountObject.setCreatedBy(authentication.getName());
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountObjectShippingAddress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/suppliers";
    }

    /**
     * Xem chi tiết nhà cung cấp là Tổ chức. Số 0 là type của Tổ chức.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/supplier/view/0/{id}", method = RequestMethod.GET)
    public ModelAndView viewDetailSupplier(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/supplier/org/view");
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
        AccountObject accountObject = accountObjectOptional.get();
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        List<AccountObjectBankAccount> accountObjectBankAccountList = accountObjectBankAccountRepository.findByAccountObjectId(accountObject.getId());
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = accountObjectShippingAddressRepository.findByAccountObjectId(accountObject.getId());
        // Hiển thị danh sách tài khoản ngân hàng. Số lượng 5.
        for (int i = 0; i < 5; i++) {
            AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
            accountObjectBankAccountList.add(accountObjectBankAccount);
        }
        for (int i = 0; i < 5; i++) {
            AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
            accountObjectShippingAddressList.add(accountObjectShippingAddress);
        }
        BeanUtils.copyProperties(accountObject, accountObjectForm);
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Nhà cung cấp");
        return modelAndView;
    }

    /**
     * Hiển thị trang để Edit nhà cung cấp là Tổ chức. Số 0 là type của Tổ chức.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/supplier/edit/0/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditSupplier(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/supplier/org/view");
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
        AccountObject accountObject = accountObjectOptional.get();
        modelAndView.addObject("accountObject", accountObject);
        modelAndView.getModel().put("page_title", "Sửa thông tin Nhà cung cấp");
        return modelAndView;
    }

    /**
     * Sửa thông tin Nhà cung cấp là tổ chức.
     *
     * @return
     */
    @RequestMapping(value = "/edit_org_supplier", method = RequestMethod.POST)
    public String updateOrgSupplier(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, BindingResult bindingResult, Authentication authentication) {
        // Validate dữ liệu.
        if (bindingResult.hasErrors()) {
            return "/account_object/supplier/edit";
        }
        // Lấy ra object cũ.
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(accountObjectForm.getId());
        AccountObject accountObjectOld = accountObjectOptional.get();
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        // Giữ lại giá trị cũ.
        accountObject.setCreatedBy(accountObjectOld.getCreatedBy());
        accountObject.setCreatedDate(accountObjectOld.getCreatedDate());
        accountObject.setAccountObjectType(ORGANIZATIONAL_ACCOUNT_OBJECT_TYPE);
        // Ngữ cảnh mới.
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setModifiedDate(now);
        accountObject.setCreatedBy(authentication.getName());
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                // Lưu ý: Đây là lệnh update.
                // FIXME: set ID cho object.
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountObjectShippingAddress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                // Lưu ý đây là lệnh update.
                // FIXME:
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/suppliers";
    }

    /**
     * Add new personal supplier.
     *
     * @return
     */
    @RequestMapping(value = "/add_personal_supplier", method = RequestMethod.POST)
    public String addPersonalSupplier(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, Authentication authentication) {
        AccountObject accountObject = new AccountObject();
        BeanUtils.copyProperties(accountObjectForm, accountObject);
        // Khi Thêm mới, trạng thái theo dõi mặc định là TRUE.
        accountObject.setActiveStatus(Boolean.TRUE);
        accountObject.setAccountObjectType(PERSONAL_ACCOUNT_OBJECT_TYPE);
        accountObject.setIsVendor(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setCreatedDate(now);
        accountObject.setCreatedBy(authentication.getName());
        accountObjectRepository.save(accountObject);
        Integer accountObjectId = accountObject.getId();
        // Ghi vào bảng AccountObjectBankAccount.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank() &&
                    !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()) {
                AccountObjectBankAccount accountObjectBankAccount = accountObjectForm.getAccountObjectBankAccountList().get(i);
                accountObjectBankAccount.setAccountObjectId(accountObjectId);
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Ghi vào bảng AccountShippingAdress.
        for (int i = 0; i < 5; i++) {
            if (!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank() &&
                    !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()) {
                AccountObjectShippingAddress accountObjectShippingAddress = accountObjectForm.getAccountObjectShippingAddressList().get(i);
                accountObjectShippingAddress.setAccountObjectId(accountObjectId);
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/suppliers";
    }

    /**
     * Export Excel all customers.
     *
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/customers.xlsx", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> excelCustomersReport() throws IOException {
        List<AccountObject> customers = accountObjectRepository.findAll();
        ByteArrayInputStream byteArrayInputStream = customersToExcel(customers);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(byteArrayInputStream));
    }

    /**
     * Support export to Excel.
     *
     * @param customers
     * @return
     * @throws IOException
     */
    public static ByteArrayInputStream customersToExcel(List<AccountObject> customers) throws IOException {
        String[] COLUMNs = {"Id", "Name", "Address", "Age"};
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Customers");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Row for Header
            Row headerRow = sheet.createRow(0);
            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }
            // CellStyle for Age
            CellStyle ageCellStyle = workbook.createCellStyle();
            ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
            int rowIdx = 1;
            for (AccountObject customer : customers) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(customer.getAccountObjectCode());
                row.createCell(1).setCellValue(customer.getAccountObjectName());
                row.createCell(2).setCellValue(customer.getAddress());
                Cell ageCell = row.createCell(3);
                ageCell.setCellValue(customer.getCompanyTaxCode());
                ageCell.setCellStyle(ageCellStyle);
            }
            // Auto setActiveStatusList corresponding width of columns.
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    /**
     * Show màn hình Sửa thông tin Nhân viên.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/employee/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editEmployee(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object/employee/edit");
        AccountObjectForm accountObjectForm = new AccountObjectForm();
        AccountObject accountObject = accountObjectRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        BeanUtils.copyProperties(accountObject, accountObjectForm);
        List<AccountObjectShippingAddress> oldShipAddressList = accountObjectShippingAddressRepository.findByAccountObjectId(id);
        List<AccountObjectBankAccount> oldBankAccountList = accountObjectBankAccountRepository.findByAccountObjectId(id);
        // Hiển thị 5 bank_accounts.
        List<AccountObjectBankAccount> accountObjectBankAccountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            if (i < oldShipAddressList.size()) {
                AccountObjectBankAccount accountObjectBankAccount = oldBankAccountList.get(i);
                accountObjectBankAccountList.add(accountObjectBankAccount);
            } else {
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccountList.add(accountObjectBankAccount);
            }
        }
        accountObjectForm.setAccountObjectBankAccountList(accountObjectBankAccountList);
        // Hiển thị 5 shipping_address.
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            if (i < oldShipAddressList.size()) {
                AccountObjectShippingAddress accountObjectShippingAddress = oldShipAddressList.get(i);
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            } else {
                AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
                accountObjectShippingAddressList.add(accountObjectShippingAddress);
            }
        }
        accountObjectForm.setAccountObjectShippingAddressList(accountObjectShippingAddressList);
        modelAndView.addObject("accountObjectForm", accountObjectForm);
        modelAndView.getModel().put("page_title", "Sửa thông tin nhân viên");
        return modelAndView;
    }

    /**
     * Submit data khi edit employee.
     *
     * @param accountObjectForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/edit_employee", method = RequestMethod.POST)
    public String submitEditEmployee(@ModelAttribute("accountObjectForm") AccountObjectForm accountObjectForm, Authentication authentication) {
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(accountObjectForm.getId());
        AccountObject employeeOld = accountObjectOptional.get();
        AccountObject accountObject = new AccountObject();
        accountObject.setId(accountObjectForm.getId());
        accountObject.setAccountObjectCode(accountObjectForm.getAccountObjectCode());
        //FIXME: get set thêm ở đây.
        accountObject.setCreatedBy(employeeOld.getCreatedBy());
        accountObject.setCreatedDate(employeeOld.getCreatedDate());
        accountObject.setIsEmployee(Boolean.TRUE);
        //accountObject.setIsVendor(Boolean.FALSE); FIXME: phải lấy từ input element.
        // accountObject.setActiveStatus(Boolean.TRUE); FIXME: Phải lấy từ input element.
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObject.setModifiedDate(now);
        accountObject.setModifiedBy(authentication.getName());
        accountObjectRepository.save(accountObject);
        // Xóa dữ liệu cũ trong bảng AccountObjectBankAccount. Kiểm tra constraint.
        List<AccountObjectBankAccount> accountObjectBankAccountList = accountObjectBankAccountRepository.findByAccountObjectId(accountObject.getId());
        accountObjectBankAccountRepository.deleteAll(accountObjectBankAccountList);
        for (int i = 0; i < 5; i++){
            if(!accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isEmpty()
                    && !accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount().isBlank()
                    && accountObjectForm.getAccountObjectBankAccountList().get(i).getBankId() > 0){
                AccountObjectBankAccount accountObjectBankAccount = new AccountObjectBankAccount();
                accountObjectBankAccount.setBankId(accountObjectForm.getAccountObjectBankAccountList().get(i).getBankId());
                accountObjectBankAccount.setBankAccount(accountObjectForm.getAccountObjectBankAccountList().get(i).getBankAccount());
                accountObjectBankAccountRepository.save(accountObjectBankAccount);
            }
        }
        // Xóa dữ liệu cũ trong bảng AccountObjectShippingAddress.
        List<AccountObjectShippingAddress> accountObjectShippingAddressList = accountObjectShippingAddressRepository.findByAccountObjectId(accountObject.getId());
        accountObjectShippingAddressRepository.deleteAll(accountObjectShippingAddressList);
        for (int i = 0; i < 5; i++){
            if(!accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isEmpty()
                    && !accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress().isBlank()){
                AccountObjectShippingAddress accountObjectShippingAddress = new AccountObjectShippingAddress();
                accountObjectShippingAddress.setShippingAddress(accountObjectForm.getAccountObjectShippingAddressList().get(i).getShippingAddress());
                accountObjectShippingAddressRepository.save(accountObjectShippingAddress);
            }
        }
        return "redirect:/employees";
    }

    /**
     * Show page for editing an exist account object.
     *
     * @param id
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/account_object/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editAccountObject(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("system/edit_org_customer");
        AccountObject accountObject = accountObjectRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        modelAndView.addObject("accountObject", accountObject);
        modelAndView.getModel().put("page_title", "Sửa thông tin");
        return modelAndView;
    }

    /**
     * For drop-down list. Used in /add_cash_receipt.
     * Change store JSON string inside SQl Server will help improving application
     * performance.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/acc_obj_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accObjJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> accObjList = new ArrayList<>();
        for (AccountObject accountObject : accountObjectList) {
            DropDownListItem accObj = new DropDownListItem();
            accObj.setValue(accountObject.getAccountObjectCode());
            accObj.setText(accountObject.getAccountObjectName());
            accObjList.add(accObj);
        }
        String jsonDataString = objectMapper.writeValueAsString(accObjList);
        return jsonDataString;
    }

    /**
     * List of employee JSON. Used in page /add_cash_receipt .
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/employee_multi_columns_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String employeeMutliColumnsJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.getListOfActiveEmployees();
        List<EmployeeDropDownListDTO> employeeDropDownListDTOList = new ArrayList<>();
        List<OrganizationUnit> organizationUnitList = organizationUnitRepository.findAll();
        if (accountObjectList.size() > 0) {
            for (AccountObject accountObject : accountObjectList) {
                EmployeeDropDownListDTO employeeDropDownListDTO = new EmployeeDropDownListDTO();
                BeanUtils.copyProperties(accountObject, employeeDropDownListDTO);
                for (OrganizationUnit organizationUnit : organizationUnitList){
                    if (accountObject.getOrganizationUnitId() == organizationUnit.getId()){
                        employeeDropDownListDTO.setOrganizationUnitName(organizationUnit.getOrganizationUnitName());
                        break;
                    }
                }
                // Bad performance.
                // Optional<OrganizationUnit> organizationUnit = organizationUnitRepository.findById(accountObject.getOrganizationUnitId());
                // organizationUnit.ifPresent(orgUnit -> employeeDropDownListDTO.setOrganizationUnitName(orgUnit.getOrganizationUnitName()));
                employeeDropDownListDTOList.add(employeeDropDownListDTO);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(employeeDropDownListDTOList);
        return jsonDataString;
    }

    /**
     * JSON all customers.
     *
     * @return
     * @throws JsonProcessingException
     * @see #customerMulColJSON()
     */
    @RequestMapping(value = "/customer_id_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    @Deprecated
    public String customerIdJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.listCustomers();
        ObjectMapper objectMapper = new ObjectMapper();
        List<NumberStringDropDownListItem> accObjList = new ArrayList<>();
        for (AccountObject accountObject : accountObjectList) {
            NumberStringDropDownListItem accObj = new NumberStringDropDownListItem();
            accObj.setNum(accountObject.getId());
            accObj.setText(accountObject.getAccountObjectName());
            accObjList.add(accObj);
        }
        String jsonDataString = objectMapper.writeValueAsString(accObjList);
        return jsonDataString;
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã khách hàng
     * 2. Tên khách hàng
     * 3. Địa chỉ
     * 4. Mã số thuế
     * 5. Điện thoại
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/customers_mulcol_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String customerMulColJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.getListOfCustomers();
        List<AccountObjectDropDownItem> accountObjectDropDownItemList = new ArrayList<>();
        if (accountObjectList.size() > 0) {
            for (AccountObject accountObject : accountObjectList) {
                AccountObjectDropDownItem accountObjectDropDownItem = new AccountObjectDropDownItem();
                accountObjectDropDownItem.setId(accountObject.getId());
                accountObjectDropDownItem.setAccountObjectCode(accountObject.getAccountObjectCode());
                accountObjectDropDownItem.setAccountObjectName(accountObject.getAccountObjectName());
                accountObjectDropDownItem.setAddress(accountObject.getAddress());
                accountObjectDropDownItem.setCompanyTaxCode(accountObject.getCompanyTaxCode());
                accountObjectDropDownItemList.add(accountObjectDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectDropDownItemList);
        System.out.println(jsonDataString);
        return jsonDataString;
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã nhà cung cấp
     * 2. Tên nhà cung cấp
     * 3. Địa chỉ
     * 4. Mã số thuế
     * 5. Điện thoại
     *
     * @return
     * @throws JsonProcessingException
     * @see PuContractController#showPageForAddPuContract()
     */
    @RequestMapping(value = "/vendors_mulcol_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String vendorsMulColJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.listVendors();
        List<AccountObjectDropDownItem> accountObjectDropDownItemList = new ArrayList<>();
        if (accountObjectList.size() > 0) {
            for (AccountObject accountObject : accountObjectList) {
                AccountObjectDropDownItem accountObjectDropDownItem = new AccountObjectDropDownItem();
                accountObjectDropDownItem.setId(accountObject.getId());
                accountObjectDropDownItem.setAccountObjectCode(accountObject.getAccountObjectCode());
                accountObjectDropDownItem.setAccountObjectName(accountObject.getAccountObjectName());
                accountObjectDropDownItem.setAddress(accountObject.getAddress());
                accountObjectDropDownItem.setCompanyTaxCode(accountObject.getCompanyTaxCode());
                accountObjectDropDownItemList.add(accountObjectDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectDropDownItemList);
        System.out.println(jsonDataString);
        return jsonDataString;
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã đối tượng kế toán
     * 2. Tên Đối tượng kế toán
     * 3. Địa chỉ
     * 4. Mã số thuế
     * 5. Điện thoại
     * <p>
     * Dùng trong form AddCashReceipt.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/acc_obj_mulcol_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accountObjMulColJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.getActiveAccountObject();
        List<AccountObjectDropDownItem> accountObjectDropDownItemList = new ArrayList<>();
        if (accountObjectList.size() > 0) {
            for (AccountObject accountObject : accountObjectList) {
                AccountObjectDropDownItem accountObjectDropDownItem = new AccountObjectDropDownItem();
                accountObjectDropDownItem.setId(accountObject.getId());
                accountObjectDropDownItem.setAccountObjectCode(accountObject.getAccountObjectCode());
                accountObjectDropDownItem.setAccountObjectName(accountObject.getAccountObjectName());
                accountObjectDropDownItem.setAddress(accountObject.getAddress());
                accountObjectDropDownItem.setCompanyTaxCode(accountObject.getCompanyTaxCode());
                accountObjectDropDownItemList.add(accountObjectDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectDropDownItemList);
        return jsonDataString;
    }


    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã đối tượng kế toán
     * 2. Tên Đối tượng kế toán
     * 3. Địa chỉ
     * 4. Mã số thuế
     * 5. Điện thoại
     * <p>
     * Dùng trong form AddCashReceipt.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/all_active_customers_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String allActiveCustomersJSON() throws JsonProcessingException {
        List<AccountObject> accountObjectList = accountObjectRepository.getListOfCustomers();
        List<AccountObjectDropDownItem> accountObjectDropDownItemList = new ArrayList<>();
        if (accountObjectList.size() > 0) {
            for (AccountObject accountObject : accountObjectList) {
                AccountObjectDropDownItem accountObjectDropDownItem = new AccountObjectDropDownItem();
                accountObjectDropDownItem.setId(accountObject.getId());
                accountObjectDropDownItem.setAccountObjectCode(accountObject.getAccountObjectCode());
                accountObjectDropDownItem.setAccountObjectName(accountObject.getAccountObjectName());
                accountObjectDropDownItem.setAddress(accountObject.getAddress());
                accountObjectDropDownItem.setCompanyTaxCode(accountObject.getCompanyTaxCode());
                accountObjectDropDownItemList.add(accountObjectDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectDropDownItemList);
        return jsonDataString;
    }

}
