package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.SaQuote;
import com.example.entity.SaQuoteDetail;
import com.example.form.SaQuoteForm;
import com.example.repository.SaQuoteDetailRepository;
import com.example.repository.SaQuoteRepository;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Báo giá. Phân hệ bán hàng.
 */
@Controller
public class SaQuoteController {

    @Autowired
    SaQuoteRepository saQuoteRepository;

    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    @Autowired
    SaQuoteDetailRepository saQuoteDetailRepository;

    /**
     * SELECT * FROM SYSAutoID WHERE RefTypeCategory = 351 AND BranchID = 1;
     */
    private static final Integer REF_TYPE_VOUCHER_SALE_ORDER = 351;


    /**
     * Hiển thị màn hình Thêm Báo giá.
     *
     * @return
     */
    @RequestMapping(value = "/add_sa_quote", method = RequestMethod.GET)
    public ModelAndView addSaleQuote() {
        ModelAndView modelAndView = new ModelAndView("sa_quote/add");
        SaQuoteForm saQuoteForm = new SaQuoteForm();
        List<SaQuoteDetail> saQuoteDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            SaQuoteDetail saQuoteDetail = new SaQuoteDetail();
            saQuoteDetailList.add(saQuoteDetail);
        }
        saQuoteForm.setSaQuoteDetailList(saQuoteDetailList);
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        String voucherNumber = sysAutoIdController.generateVoucherNumber(REF_TYPE_VOUCHER_SALE_ORDER, sysAutoIdRepository);
        saQuoteForm.setRefNo(voucherNumber);
        modelAndView.addObject("saQuoteForm", saQuoteForm);
        modelAndView.getModel().put("page_title", "Thêm Hóa đơn Bán hàng");
        return modelAndView;
    }

    /**
     * Submit data add sale quote.
     *
     * @param saQuoteForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_sa_quote", method = RequestMethod.POST)
    public String submitDataAddSaleQuote(@ModelAttribute("saQuoteForm") @Valid SaQuoteForm saQuoteForm, BindingResult bindingResult, Authentication authentication){
        if (bindingResult.hasErrors()){
            return "/sa_quote/add";
        }
        SaQuote saQuote = new SaQuote();
        saQuote.setRefNo(saQuoteForm.getRefNo());
        saQuote.setRefDate(saQuoteForm.getRefDate());
        saQuote.setEffectiveDate(saQuoteForm.getEffectiveDate());
        saQuote.setRefType(saQuoteForm.getRefType());
        saQuote.setBranchId(1); //TODO: tạm thời.
        saQuote.setAccountObjectId(saQuoteForm.getAccountObjectId());
        saQuote.setAccountObjectName(saQuoteForm.getAccountObjectName());
        saQuote.setAccountObjectTaxCode(saQuoteForm.getAccountObjectTaxCode());
        saQuote.setAccountObjectContactName(saQuoteForm.getAccountObjectContactName());
        saQuote.setJournalMemo(saQuoteForm.getJournalMemo());
        saQuote.setCurrencyId(saQuoteForm.getCurrencyId());
        saQuote.setExchangeRate(saQuoteForm.getExchangeRate());
        saQuote.setTotalAmountOc(saQuoteForm.getTotalAmountOc());
        saQuote.setTotalAmount(saQuoteForm.getTotalAmount());
        saQuote.setTotalAmount(saQuoteForm.getTotalAmount());
        saQuote.setTotalDiscountAmount(saQuoteForm.getTotalDiscountAmount());
        saQuote.setTotalVatAmount(saQuoteForm.getTotalVatAmount());
        saQuote.setTotalAmountOc(saQuoteForm.getTotalAmountOc());
        saQuote.setEditVersion(saQuoteForm.getEditVersion());
        saQuote.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        saQuote.setCreatedDate(now);
        saQuote.setEmployeeId(saQuoteForm.getEmployeeId());
        saQuoteRepository.save(saQuote);
        Integer saQuoteId = saQuote.getId();
        for (int i = 0; i < 20; i++){
            if (!saQuoteForm.getSaQuoteDetailList().get(i).getDescription().isEmpty() &&
            !saQuoteForm.getSaQuoteDetailList().get(i).getDescription().isBlank() &&
            saQuoteForm.getSaQuoteDetailList().get(i).getDescription() != null){
                SaQuoteDetail subForm = saQuoteForm.getSaQuoteDetailList().get(i);
                SaQuoteDetail saQuoteDetail = new SaQuoteDetail();
                saQuoteDetail.setDescription(subForm.getDescription());
                saQuoteDetail.setRefId(saQuoteId);
                saQuoteDetail.setQuantity(subForm.getQuantity());
                saQuoteDetail.setUnitPrice(subForm.getUnitPrice());
                saQuoteDetail.setAmountOc(subForm.getAmountOc());
                saQuoteDetail.setAmount(subForm.getAmount());
                saQuoteDetail.setDiscountRate(subForm.getDiscountRate());
                saQuoteDetail.setDiscountAmountOc(subForm.getDiscountAmountOc());
                saQuoteDetail.setDiscountAmount(subForm.getDiscountAmount());
                saQuoteDetail.setVatRate(subForm.getVatRate());
                saQuoteDetail.setVatAmountOc(subForm.getVatAmountOc());
                saQuoteDetail.setVatAmount(subForm.getVatAmount());
                saQuoteDetail.setSortOrder(1);
                saQuoteDetail.setUnitId(subForm.getUnitId());
                saQuoteDetail.setUnitPriceAfterTax(subForm.getUnitPriceAfterTax());
                saQuoteDetail.setCustomField1(subForm.getCustomField1());
                saQuoteDetail.setCustomField2(subForm.getCustomField2());
                saQuoteDetail.setCustomField3(subForm.getCustomField3());
                saQuoteDetail.setCustomField4(subForm.getCustomField4());
                saQuoteDetail.setCustomField5(subForm.getCustomField5());
                saQuoteDetail.setCustomField6(subForm.getCustomField6());
                saQuoteDetail.setCustomField7(subForm.getCustomField7());
                saQuoteDetail.setCustomField8(subForm.getCustomField8());
                saQuoteDetail.setCustomField9(subForm.getCustomField9());
                saQuoteDetail.setCustomField10(subForm.getCustomField10());
                saQuoteDetail.setOrderId(subForm.getOrderId());
                saQuoteDetail.setContractId(subForm.getContractId());
                saQuoteDetail.setProjectWorkId(subForm.getProjectWorkId());
                saQuoteDetail.setExpenseItemId(subForm.getExpenseItemId());
                saQuoteDetail.setOrganizationUnitId(subForm.getOrganizationUnitId());
                saQuoteDetail.setJobId(subForm.getJobId());
                saQuoteDetail.setListItemId(subForm.getListItemId());
                saQuoteDetail.setSpecificity(subForm.getSpecificity());
                saQuoteDetail.setExchangeRateOperator(subForm.getExchangeRateOperator());
                saQuoteDetail.setMainConvertRate(subForm.getMainConvertRate());
                saQuoteDetail.setMainConvertRate(subForm.getMainConvertRate());
                saQuoteDetail.setIsPromotion(subForm.getIsPromotion());
                saQuoteDetail.setGuarantyPeriod(subForm.getGuarantyPeriod());
                saQuoteDetail.setPanelLengthQuantity(subForm.getPanelLengthQuantity());
                saQuoteDetail.setPanelWidthQuantity(subForm.getPanelWidthQuantity());
                saQuoteDetail.setPanelHeightQuantity(subForm.getPanelHeightQuantity());
                saQuoteDetail.setPanelRadiusQuantity(subForm.getPanelRadiusQuantity());
                saQuoteDetail.setPanelQuantity(subForm.getPanelQuantity());
                saQuoteDetailRepository.save(saQuoteDetail);
            }
        }
        return "redirect:/desktop";
    }

}
