package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.repository.SysdbOptionRepository;

/**
 * Menu [Hệ thống] \ [Tùy chọn]
 * Là option của [Tab 8. Hiển thị các nghiệp vụ].
 */
@Controller
public class SysdbOptionController {

    @Autowired
    SysdbOptionRepository sysdbOptionRepository;

    /**
     * Show page for choosing accounting book.
     *
     * @return
     */
    @RequestMapping(value = "/choose_accounting_book", method = RequestMethod.GET)
    public ModelAndView showPageChooseAccountingBook() {
        ModelAndView modelAndView = new ModelAndView("sysdboption/options");
        return modelAndView;
    }

}
