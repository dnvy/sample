package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.FixedAssetCategory;
import com.example.repository.FixedAssetCategoryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Fixed asset category.
 */
@Controller
public class FixedAssetCategoryController {

    @Autowired
    FixedAssetCategoryRepository fixedAssetCategoryRepository;

    /**
     * Show list all Fixed asset categories.
     *
     * @return
     */
    @RequestMapping(value = "/fixed_asset_categories", method = RequestMethod.GET)
    public ModelAndView fixedAssetCategories() {
        ModelAndView modelAndView = new ModelAndView("fixed_asset_category/list");
        List<FixedAssetCategory> fixedAssetCategoryList = fixedAssetCategoryRepository.findAll();
        modelAndView.addObject("fixedAssetCategoryList", fixedAssetCategoryList);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Loại Tài sản cố định");
        return modelAndView;
    }

    /**
     * Delete a fixed asset category item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/fixed_asset_category/delete/{id}", method = RequestMethod.POST)
    public String deleteFixedAssetCategory(@PathVariable("id") Integer id, HttpServletRequest request) {
        FixedAssetCategory fixedAssetCategory = fixedAssetCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        fixedAssetCategoryRepository.delete(fixedAssetCategory);
        return "redirect:" + request.getHeader("Referer");
    }

}
