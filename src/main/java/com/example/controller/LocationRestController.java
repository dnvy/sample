package com.example.controller;

import com.example.entity.Location;
import com.example.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LocationRestController {

    @Autowired
    LocationRepository locationRepository;

    /**
     * Get all Locations.
     *
     * @return
     */
    @GetMapping("/location")
    public List<Location> retrieveAllLocations() {
        return locationRepository.findAll();
    }

    /**
     * Get all countries.
     * http://localhost:8080/accounting/location/countries .
     *
     * @return
     */
    @GetMapping("/location/countries")
    public List<Location> retrieveAllCountries() {
        Location location = new Location();
        location.setKind(0);
        Example<Location> example = Example.of(location);
        return locationRepository.findAll(example);
    }

    /**
     * List all Vietnam provinces.
     *
     * @return
     */
    @GetMapping("/location/provinces/{countryId}")
    public List<Location> retrieveAllVietnamProvince(@PathVariable String countryId) {
        return locationRepository.listVietnamProvinces(countryId);
    }

    @GetMapping("/location/district/{prefixOfProvinceInLocationID}")
    public List<Location> retrieveAllDistricts(@PathVariable String prefixOfProvinceInLocationID) {
        return locationRepository.getDistrics(prefixOfProvinceInLocationID);
    }

    /**
     * Get list of communes by district.
     *
     * @param prefixOfDistrictInLocationID
     * @return
     */
    @GetMapping("/location/commune/{prefixOfDistrictInLocationID}")
    public List<Location> getCommunes(@PathVariable String prefixOfDistrictInLocationID) {
        return locationRepository.getCommunes(prefixOfDistrictInLocationID);
    }

}
