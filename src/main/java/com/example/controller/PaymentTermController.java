package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.PaymentTerm;
import com.example.repository.PaymentTermRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Điều khoản thanh toán.
 */
@Controller
public class PaymentTermController {

    @Autowired
    PaymentTermRepository paymentTermRepository;

    /**
     * Show list all payment terms.
     *
     * @return
     */
    @RequestMapping(value = "/payment_terms", method = RequestMethod.GET)
    public ModelAndView listAllPaymentTerms() {
        ModelAndView modelAndView = new ModelAndView("payment_term/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<PaymentTerm> paymentTermList = paymentTermRepository.findAll();
        modelAndView.addObject("paymentTermList", paymentTermList);
        modelAndView.getModel().put("page_title", "Danh sách Điều khoản thanh toán");
        return modelAndView;
    }

    /**
     * Delete a payment term.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/payment_term/delete/{id}", method = RequestMethod.POST)
    public String deletePaymentTerm(@PathVariable("id") Integer id, HttpServletRequest request) {
        PaymentTerm paymentTerm = paymentTermRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid payment_term id: " + id));
        //TODO: Check related FK.
        paymentTermRepository.delete(paymentTerm);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Return JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/payment_terms_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String paymentTermsJSON() throws JsonProcessingException {
        List<PaymentTerm> paymentTermList = paymentTermRepository.findByActiveStatus(Boolean.TRUE);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(paymentTermList);
        System.out.println(jsonDataString);
        return jsonDataString;
    }

}
