package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dto.AccountObjectGroupDTO;
import com.example.entity.AccountObjectGroup;
import com.example.repository.AccountObjectGroupRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Nhóm khách hàng, Nhà cung cấp.
 */
@Controller
public class AccountObjectGroupController {

    @Autowired
    AccountObjectGroupRepository accountObjectGroupRepository;

    /**
     * Show list of object groups. Use alternative /account_object_groups
     *
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/account_object_groups_flat", method = RequestMethod.GET)
    public ModelAndView showAllAccountObjectGroupsFlat() {
        ModelAndView modelAndView = new ModelAndView("account_object_group/list_flat");
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAll();
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("accountObjectGroupList", accountObjectGroupList);
        modelAndView.getModel().put("page_title", "Danh sách Nhóm Đối tượng");
        return modelAndView;
    }

    /**
     * Show hierarchy list of all account_object_group. Example of datasource tree hierarchy
     * https://demos.telerik.com/kendo-ui/treelist/index
     * https://demos.telerik.com/kendo-ui/service/EmployeeDirectory/All
     *
     * @return
     * @see #accObjGroupDtoJSON()
     */
    @RequestMapping(value = "/account_object_groups", method = RequestMethod.GET)
    public ModelAndView showAllAccountObjectGroups() {
        ModelAndView modelAndView = new ModelAndView("account_object_group/list");
        modelAndView.getModel().put("page_title", "Danh sách Nhóm Đối tượng");
        return modelAndView;
    }

    /**
     * Show page for adding AccountObjectGroup.
     *
     * @return
     */
    @RequestMapping(value = "/add_account_object_group", method = RequestMethod.GET)
    public ModelAndView addAccountObjectGroup() {
        ModelAndView modelAndView = new ModelAndView("account_object_group/add");
        AccountObjectGroup accountObjectGroup = new AccountObjectGroup();
        modelAndView.addObject("accountObjectGroup", accountObjectGroup);
        modelAndView.getModel().put("page_title", "Thêm nhóm đối tượng");
        return modelAndView;
    }

    /**
     * View detail account_object_group.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/account_object_group/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewAccountObjectGroup(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object_group/view");
        AccountObjectGroup accountObjectGroup = accountObjectGroupRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account_object_group id: " + id));
        modelAndView.addObject("accountObjectGroup", accountObjectGroup);
        modelAndView.getModel().put("page_title", "Xem chi tiết Nhóm đối tượng kế toán");
        return modelAndView;
    }

    /**
     * Show page for editing AccountObjectGroup.
     *
     * @return
     */
    @RequestMapping(value = "/account_object_group/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editAccountObjectGroup(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account_object_group/edit");
        AccountObjectGroup accountObjectGroup = accountObjectGroupRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account_object_group id: " + id));
        modelAndView.addObject("accountObjectGroup", accountObjectGroup);
        modelAndView.getModel().put("page_title", "Sửa Nhóm đối tượng kế toán");
        return modelAndView;
    }

    /**
     * Submit data for updating accountObjectGroup
     *
     * @param accountObjectGroup
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/update_account_object_group", method = RequestMethod.POST)
    public String updateAccountObjectGroup(@ModelAttribute("accountObjectGroup") @Valid AccountObjectGroup accountObjectGroup, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "account_object_group/add";
        }
        Optional<AccountObjectGroup> accountObjectGroupOptional = accountObjectGroupRepository.findById(accountObjectGroup.getId());
        AccountObjectGroup accountObjectGroupOld = accountObjectGroupOptional.get();
        accountObjectGroup.setCreatedBy(accountObjectGroupOld.getCreatedBy());
        accountObjectGroup.setCreatedDate(accountObjectGroupOld.getCreatedDate());
        accountObjectGroup.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObjectGroup.setModifiedDate(now);
        accountObjectGroup.setIsParent(Boolean.FALSE);
        accountObjectGroup.setIsSystem(Boolean.FALSE);
        accountObjectGroupRepository.save(accountObjectGroup);
        return "redirect:/account_object_groups";
    }

    /**
     * Delete account_object_group.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/account_object_group/delete/{id}", method = RequestMethod.POST)
    public String deleteAccountObject(@PathVariable("id") Integer id, HttpServletRequest request) {
        AccountObjectGroup accountObjectGroup = accountObjectGroupRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account_object_group id: " + id));
        accountObjectGroupRepository.delete(accountObjectGroup);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Submit account object group.
     *
     * See example https://spring.io/guides/gs/validating-form-input/
     * Hierarchy query for tree JSON.
     * https://www.mkyong.com/java/jackson-tree-model-example/
     * https://demos.telerik.com/kendo-ui/dropdowntree/index
     * https://jivimberg.io/blog/2018/08/04/recursive-queries-on-rdbms-with-jpa/
     * https://stackoverflow.com/questions/3638082/recursive-jpa-query
     * https://dzone.com/articles/manage-hierarchical-data-using
     *
     * @return
     */
    @RequestMapping(value = "/add_account_object_group", method = RequestMethod.POST)
    public String submitAccountObjectGroup(@ModelAttribute("accountObjectGroup") @Valid AccountObjectGroup accountObjectGroup, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "account_object_group/add";
        }
        // Khi thêm mới, thì ActiveStatus = 1.
        accountObjectGroup.setActiveStatus(Boolean.TRUE);
        accountObjectGroup.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountObjectGroup.setCreatedDate(now);
        accountObjectGroup.setIsParent(Boolean.FALSE); // Chưa rõ.
        accountObjectGroup.setIsSystem(Boolean.FALSE);
        accountObjectGroupRepository.save(accountObjectGroup);
        return "redirect:/account_object_groups";
    }

    /**
     * JSON multi-columns.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/account_obj_group_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accObjGroupJSON() throws JsonProcessingException {
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectGroupList);
        return jsonDataString;
    }

    /**
     * JSON for show list all account_object_group in TreeList.
     *
     * @return
     * @throws JsonProcessingException
     * @see #showAllAccountObjectGroups()
     */
    @RequestMapping(value = "/account_obj_group_dto_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accObjGroupDtoJSON() throws JsonProcessingException {
        List<AccountObjectGroup> accountObjectGroupList = accountObjectGroupRepository.findAll();
        List<AccountObjectGroupDTO> accountObjectGroupDTOList = new ArrayList<>();
        for (AccountObjectGroup accountObjectGroup : accountObjectGroupList){
            AccountObjectGroupDTO accountObjectGroupDTO = new AccountObjectGroupDTO();
            try {
                BeanUtils.copyProperties(accountObjectGroupDTO, accountObjectGroup);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            if(accountObjectGroup.getActiveStatus() == Boolean.TRUE){
                accountObjectGroupDTO.setActiveStatusString("Có");
            }else {
                accountObjectGroupDTO.setActiveStatusString("Không");
            }
            if(accountObjectGroup.getIsSystem() == Boolean.TRUE){
                accountObjectGroupDTO.setIsSystemString("Có");
            }else {
                accountObjectGroupDTO.setIsSystemString("Không");
            }
            accountObjectGroupDTOList.add(accountObjectGroupDTO);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountObjectGroupDTOList);
        return jsonDataString;
    }

}
