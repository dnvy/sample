package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.repository.GlVoucherRepository;

/**
 * Chứng từ.
 */
@Controller
public class GlVoucherController {

    @Autowired
    GlVoucherRepository glVoucherRepository;

    /**
     * List of cash receipts and cash payment receipts
     *
     * @return
     */
    @RequestMapping(value = "/cash_receipt_cash_payment_receipts", method = RequestMethod.GET)
    public ModelAndView showCashReceiptCashPaymentReceipts(){
        ModelAndView modelAndView = new ModelAndView("gl_voucher/cash_receipt_cash_payment_receipts");

        return modelAndView;
    }


}
