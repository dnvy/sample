package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dropdownlist.ProjectWorkDropDown;
import com.example.entity.ProjectWork;
import com.example.repository.ProjectWorkRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Công trình.
 */
@Controller
public class ProjectWorkController {

    @Autowired
    ProjectWorkRepository projectWorkRepository;

    /**
     * Show list all project works.
     *
     * @return
     */
    @RequestMapping(value = "/project_works", method = RequestMethod.GET)
    public ModelAndView showAllProjectWorks() {
        ModelAndView modelAndView = new ModelAndView("project_work/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<ProjectWork> projectWorkList = projectWorkRepository.findAll();
        modelAndView.addObject("projectWorkList", projectWorkList);
        modelAndView.getModel().put("page_title", "Danh sách Công trình");
        return modelAndView;
    }

    /**
     * Delete a project work.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/project_work/delete/{id}", method = RequestMethod.POST)
    public String deleteProjectWork(@PathVariable("id") Integer id, HttpServletRequest request) {
        ProjectWork projectWork = projectWorkRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exepense_item id: " + id));
        //TODO: Check related FK.
        projectWorkRepository.delete(projectWork);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Show page for add project_work.
     *
     * @return
     */
    @RequestMapping(value = "/add_project_work", method = RequestMethod.GET)
    public ModelAndView showPageForAddProjectWork() {
        ProjectWork projectWork = new ProjectWork();
        ModelAndView modelAndView = new ModelAndView("project_work/add");
        modelAndView.addObject("projectWork", projectWork);
        return modelAndView;
    }

    /**
     * Submit data for adding Project_work.
     *
     * @return
     */
    @RequestMapping(value = "/submit_add_project_work", method = RequestMethod.POST)
    public String submitAddProjectWork(@ModelAttribute("projectWork") @Valid ProjectWork projectWork, BindingResult bindingResult, Authentication authentication){
        if (bindingResult.hasErrors()) {
            return "project_work/add";
        }
        projectWork.setActiveStatus(Boolean.TRUE);
        projectWork.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        projectWork.setCreatedDate(now);
        //TODO: What is property IsParent (NOT NULL)
        projectWork.setIsParent(Boolean.FALSE);
        projectWorkRepository.save(projectWork);
        return "redirect:/project_works";
    }

    /**
     * List all project works has IsActive = 1, in JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/project_works_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getActiveProjectWorksJSON() throws JsonProcessingException {
        List<ProjectWork> projectWorkList = projectWorkRepository.getAllActiveProjectWorks();
        List<ProjectWorkDropDown> projectWorkDropDownList = new ArrayList<>();
        for (ProjectWork projectWork : projectWorkList){
            ProjectWorkDropDown projectWorkDropDown = new ProjectWorkDropDown();
            projectWorkDropDown.setId(projectWork.getId());
            projectWorkDropDown.setProjectWorkCode(projectWork.getProjectWorkCode());
            projectWorkDropDown.setProjectWorkName(projectWork.getProjectWorkName());
            projectWorkDropDownList.add(projectWorkDropDown);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(projectWorkDropDownList);
        return jsonDataString;
    }

}
