package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.ProjectWorkCategory;
import com.example.repository.ProjectWorkCategoryRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Loại công trình.
 */
@Controller
public class ProjectWorkCategoryController {

    @Autowired
    ProjectWorkCategoryRepository projectWorkCategoryRepository;

    /**
     * Show list all project work categories.
     *
     * @return
     */
    @RequestMapping(value = "/project_work_categories", method = RequestMethod.GET)
    public ModelAndView showAllProjectWorkCategories() {
        ModelAndView modelAndView = new ModelAndView("project_work_category/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<ProjectWorkCategory> projectWorkCategoryList = projectWorkCategoryRepository.findAll();
        modelAndView.addObject("projectWorkCategoryList", projectWorkCategoryList);
        modelAndView.getModel().put("page_title", "Danh sách Loại công trình");
        return modelAndView;
    }

    /**
     * Hiển thị trang để Thêm Loại công trình.
     *
     * @return
     */
    @RequestMapping(value = "/add_project_work_category", method = RequestMethod.GET)
    public ModelAndView showPageAddProjectWorkCategory(ProjectWorkCategory projectWorkCategory) {
        ModelAndView modelAndView = new ModelAndView("project_work_category/add");
        modelAndView.getModel().put("page_title", "Thêm Loại công trình");
        return modelAndView;
    }

    /**
     * Submit form add Project_Work_Category.
     *
     * @param projectWorkCategory
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_project_work_category", method = RequestMethod.POST)
    public String submitProjectWorkCategory(
            @ModelAttribute("projectWorkCategory") @Valid ProjectWorkCategory projectWorkCategory,
            BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "project_work_category/add";
        }
        projectWorkCategory.setActiveStatus(Boolean.TRUE);
        projectWorkCategory.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        projectWorkCategory.setCreatedDate(now);
        projectWorkCategoryRepository.save(projectWorkCategory);
        return "redirect:/project_work_categories";
    }

    /**
     * Delete a project work category.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/project_work_category/delete/{id}", method = RequestMethod.POST)
    public String deleteProjectWorkCategory(@PathVariable("id") Integer id, HttpServletRequest request) {
        ProjectWorkCategory projectWorkCategory = projectWorkCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exepense_item id: " + id));
        //TODO: Check related FK.
        projectWorkCategoryRepository.delete(projectWorkCategory);
        return "redirect:" + request.getHeader("Referer");
    }

//    /**
//     * List all project works has IsActive = 1, in JSON.
//     *
//     * @return
//     * @throws JsonProcessingException
//     */
//    @RequestMapping(value = "/project_work_categories_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
//    @ResponseBody
//    public String projectWorkCategoriesJSON() throws JsonProcessingException {
//        List<ProjectWork> projectWorkList = proj.getListOfActiveEmployees();
//        List<EmployeeDropDownListDTO> employeeDropDownListDTOList = new ArrayList<>();
//        for (AccountObject accountObject : accountObjectList){
//            EmployeeDropDownListDTO employeeDropDownListDTO = new EmployeeDropDownListDTO();
//            BeanUtils.copyProperties(accountObject, employeeDropDownListDTO);
//            Optional<OrganizationUnit> organizationUnit = organizationUnitRepository.findById(accountObject.getOrganizationUnitId());
//            organizationUnit.ifPresent(orgUnit -> employeeDropDownListDTO.setOrganizationUnitName(orgUnit.getOrganizationUnitName()));
//            employeeDropDownListDTOList.add(employeeDropDownListDTO);
//        }
//        ObjectMapper objectMapper = new ObjectMapper();
//        String jsonDataString = objectMapper.writeValueAsString(employeeDropDownListDTOList);
//        return jsonDataString;
//    }

}
