package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.SysAutoId;
import com.example.form.SysAutoIdForm;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Bảng lưu cấu hình tự tăng của Số chứng từ.
 */
@Controller
public class SysAutoIdController {

    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    /**
     * Màn hình hiển thị Danh sách luật đánh số chứng từ tự động.
     *
     * @return
     */
    @RequestMapping(value = "/sys_auto_ids", method = RequestMethod.GET)
    public ModelAndView showListOfAutoId(){
        ModelAndView modelAndView = new ModelAndView("sys_auto_id/list");
        // Tạm thời hard-code branchId = 1.
        List<SysAutoId> sysAutoIdList = sysAutoIdRepository.findAllAutoIdOfCurrentBranch(1);
        modelAndView.addObject("sysAutoIdList", sysAutoIdList);
        modelAndView.getModel().put("page_title", "Danh sách Luật đánh số chứng từ tự động");
        return modelAndView;
    }

    /**
     * Show page for editing auto_id.
     *
     * @return
     */
    @RequestMapping(value = "/sys_auto_id/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEdit(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("sys_auto_id/edit");
        Optional<SysAutoId> sysAutoIdOptional = sysAutoIdRepository.findById(id);
        SysAutoId sysAutoId = sysAutoIdOptional.get();
        // FIXME: Phải cập nhật số chứng từ sẵn có, đẩy ra màn hình edit,
        //  không thể để tất cả bắt đầu bằng số 0 được.

        SysAutoIdForm sysAutoIdForm = new SysAutoIdForm();
        sysAutoIdForm.setRefTypeCategoryName(sysAutoId.getRefTypeCategoryName());
        sysAutoIdForm.setRefTypeCategory(sysAutoId.getRefTypeCategory());
        sysAutoIdForm.setId(id);
        sysAutoIdForm.setBranchId(sysAutoId.getBranchId());
        sysAutoIdForm.setLengthOfValue(sysAutoId.getLengthOfValue());
        sysAutoIdForm.setMaxExistValue(sysAutoId.getMaxExistValue());
        sysAutoIdForm.setPrefixString(sysAutoId.getPrefixString());
        sysAutoIdForm.setSuffix(sysAutoId.getSuffix());
        modelAndView.addObject("sysAutoIdForm", sysAutoIdForm);
        modelAndView.getModel().put("page_title", "Sửa Luật đánh số chứng từ tự động");
        return modelAndView;
    }

    /**
     * Submit data edit auto_id.
     *
     * @param sysAutoIdForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/edit_sys_auto_id", method = RequestMethod.POST)
    public String updateAutoId(@ModelAttribute("sysAutoId") @Valid SysAutoIdForm sysAutoIdForm, BindingResult bindingResult, Authentication authentication){
        if(bindingResult.hasErrors()){
            return "sys_auto_id/edit";
        }
        Optional<SysAutoId> sysAutoIdOptional = sysAutoIdRepository.findById(sysAutoIdForm.getId());
        SysAutoId sysAutoIdOld = sysAutoIdOptional.get();
        SysAutoId sysAutoId = new SysAutoId();

        sysAutoId.setRefTypeCategory(sysAutoIdOld.getRefTypeCategory());
        sysAutoId.setRefTypeCategoryName(sysAutoIdOld.getRefTypeCategoryName());
        sysAutoId.setDisplayOnBook(sysAutoIdOld.getDisplayOnBook());
        sysAutoId.setCreatedBy(sysAutoIdOld.getCreatedBy());

        sysAutoId.setId(sysAutoIdOld.getId());
        sysAutoId.setMaxExistValue(sysAutoIdForm.getMaxExistValue());
        sysAutoId.setPrefixString(sysAutoIdForm.getPrefixString());
        sysAutoId.setSuffix(sysAutoIdForm.getSuffix());
        sysAutoId.setLengthOfValue(sysAutoIdForm.getLengthOfValue());
        sysAutoId.setBranchId(sysAutoIdOld.getBranchId());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        sysAutoId.setModifiedDate(now);

        sysAutoId.setModifiedBy(authentication.getName());
        sysAutoIdRepository.save(sysAutoId);
        return "redirect:/sys_auto_ids";
    }

    /**
     * Show page for editing auto_id.
     *
     * @return
     */
    @RequestMapping(value = "/sys_auto_id/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewSysAutoID(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("sys_auto_id/view");
        Optional<SysAutoId> sysAutoIdOptional = sysAutoIdRepository.findById(id);
        SysAutoId sysAutoId = sysAutoIdOptional.get();
        SysAutoIdForm sysAutoIdForm = new SysAutoIdForm();
        sysAutoIdForm.setRefTypeCategory(sysAutoId.getRefTypeCategory());
        sysAutoIdForm.setRefTypeCategoryName(sysAutoId.getRefTypeCategoryName());
        sysAutoIdForm.setBranchId(sysAutoId.getBranchId());
        sysAutoIdForm.setLengthOfValue(sysAutoId.getLengthOfValue());
        sysAutoIdForm.setMaxExistValue(sysAutoId.getMaxExistValue());
        sysAutoIdForm.setPrefixString(sysAutoId.getPrefixString());
        sysAutoIdForm.setSuffix(sysAutoId.getSuffix());
        modelAndView.addObject("sysAutoIdForm", sysAutoIdForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết Luật đánh số chứng từ tự động");
        return modelAndView;
    }

    /**
     * Sinh số chứng từ tự động.
     *
     * @param refTypeCategory
     * @return
     */
    public String generateVoucherNumber(Integer refTypeCategory, SysAutoIdRepository repo){
        Integer branchId = 1;
        String voucherNumber = "";
        //List<SysAutoId> sysAutoIdList = repo.findSpecific(refTypeCategory, branchId);
        //SysAutoId sysAutoId = sysAutoIdList.get(0);
        Optional<SysAutoId> sysAutoIdOptional = repo.findSpecific(refTypeCategory, branchId);
        SysAutoId sysAutoId = sysAutoIdOptional.get();
        Integer currentVoucherNumber = sysAutoId.getMaxExistValue() + 1;
        String currentVoucherNumberString = String.valueOf(currentVoucherNumber);
        Integer currentVoucherNumberLength = currentVoucherNumberString.length();
        Integer lengthOfNumberSection = sysAutoId.getLengthOfValue();
        Integer zeroLengths = lengthOfNumberSection - currentVoucherNumber;
        String zeroString = "";
        for (int i = 0; i < zeroLengths; i++){
            zeroString.concat("0");
        }
        voucherNumber = sysAutoId.getPrefixString() + zeroString + currentVoucherNumberString;
        return voucherNumber;
    }

}
