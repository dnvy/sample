package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.BaWithDrawDetail;
import com.example.form.BaWithDrawForm;
import com.example.repository.BaWithDrawRepository;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * BaWithDraw là bảng master của chứng từ chi tiền gửi. Là Master của các bảng detail sau
 * <p>
 * - BAWithDrawDetail
 * - BAWithDrawDetailPurchase
 * - BAWithDrawDetaillFixedAsset
 */
@Controller
public class BaWithDrawController {

    @Autowired
    BaWithDrawRepository baWithDrawRepository;

    /**
     * Để generate số chứng từ tự động.
     */
    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    /**
     * RefType của Ủy nhiệm chi là 1510.
     */
    private static final Integer REF_TYPE_STANDING_ORDER = 1510;


    /**
     * Mã tham chiếu chứng từ của Ủy nhiệm chi là 151.
     */
    private static final Integer VOUCHER_TYPE = 151;

    /**
     * Not finished.
     *
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/add_ba_withdraw", method = RequestMethod.GET)
    public ModelAndView addBAWithdraw(){
        ModelAndView modelAndView = new ModelAndView("");
        return modelAndView;
    }

    /**
     * Payment from bank accounts. Uỷ nhiệm chi.
     *
     * @return
     */
    @RequestMapping(value = "/add_ba_withdraw_standing_order", method = RequestMethod.GET)
    public ModelAndView addBankWithdrawStandingOrder() {
        ModelAndView modelAndView = new ModelAndView("ba_withdraw/add_standing_order");
        BaWithDrawForm baWithDrawForm = new BaWithDrawForm();
        List<BaWithDrawDetail> baWithDrawDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            BaWithDrawDetail baWithDrawDetail = new BaWithDrawDetail();
            baWithDrawDetailList.add(baWithDrawDetail);
        }
        baWithDrawForm.setBaWithDrawDetailList(baWithDrawDetailList);
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        String voucherId = sysAutoIdController.generateVoucherNumber(VOUCHER_TYPE, sysAutoIdRepository);
        baWithDrawForm.setRefNoCommon(voucherId);
        modelAndView.addObject("baWithDrawForm", baWithDrawForm);
        modelAndView.getModel().put("page_title", "Ủy nhiệm chi");
        return modelAndView;
    }

    /**
     * Submit data.
     *
     * @param baWithDrawForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_ba_withdraw", method = RequestMethod.POST)
    public String submitData(@ModelAttribute("baWithDrawForm") @Valid BaWithDrawForm baWithDrawForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "ba_withdraw/add";
        }

        return "redirect:/desktop";
    }


    /**
     * Nộp tiền bảo hiểm bằng tài khoản ngân hàng.
     *
     * @return
     */
    @RequestMapping(value = "/add_bank_withdraw_for_insurance", method = RequestMethod.GET)
    public ModelAndView bankWithdrawInsurance(){
        ModelAndView modelAndView = new ModelAndView("ba_withdraw/insurance");
        BaWithDrawForm baWithDrawForm = new BaWithDrawForm();
        List<BaWithDrawDetail> baWithDrawDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            BaWithDrawDetail baWithDrawDetail = new BaWithDrawDetail();
            baWithDrawDetailList.add(baWithDrawDetail);
        }
        baWithDrawForm.setBaWithDrawDetailList(baWithDrawDetailList);
        modelAndView.addObject("baWithDrawForm", baWithDrawForm);
        modelAndView.getModel().put("page_title", "Nộp tiền bảo hiểm bằng Tài khoản ngân hàng");
        return modelAndView;
    }


}






















