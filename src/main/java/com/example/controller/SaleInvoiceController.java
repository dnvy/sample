package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.AddSaleInvoiceForm;
import com.example.entity.SaInvoice;
import com.example.entity.SaInvoiceDetail;
import com.example.repository.AccountObjectLedgerRepository;
import com.example.repository.GlVoucherRepository;
import com.example.repository.SAInvoiceRepository;
import com.example.repository.SaInvoiceDetailRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Hóa đơn bán hàng.
 */
@Controller
public class SaleInvoiceController {

    @Autowired
    AccountObjectLedgerRepository accountObjectLedgerRepository; // Tăng giảm công nợ khách hàng.

    @Autowired
    GlVoucherRepository glVoucherRepository; // Ghi vào bảng chứng từ.

    @Autowired
    SAInvoiceRepository saInvoiceRepository; // Ghi vào bảng Hóa đơn.

    @Autowired
    SaInvoiceDetailRepository saInvoiceDetailRepository; // Ghi vào bảng Chi tiết hóa đơn.

    /**
     * Show page for adding sale invoice.
     *
     * @return
     */
    @RequestMapping(value = "/add_sale_invoice", method = RequestMethod.GET)
    public ModelAndView showPageAddSaleInvoice() {
        ModelAndView modelAndView = new ModelAndView("sa_invoice/add");
        List<SaInvoiceDetail> saInvoiceDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SaInvoiceDetail saInvoiceDetail = new SaInvoiceDetail();
            saInvoiceDetailList.add(saInvoiceDetail);
        }
        AddSaleInvoiceForm addSaleInvoiceForm = new AddSaleInvoiceForm();
        addSaleInvoiceForm.setSaInvoiceDetailList(saInvoiceDetailList);
        modelAndView.addObject("addSaleInvoiceForm", addSaleInvoiceForm);
        modelAndView.getModel().put("page_title", "Thêm Hóa đơn Bán hàng");
        return modelAndView;
    }

    /**
     * Submit data of Sale Invoice.
     *
     * @return
     */
    @RequestMapping(value = "/add_sale_invoice", method = RequestMethod.POST)
    public String addSaleInvoice(@ModelAttribute("addSaleInvoiceForm") AddSaleInvoiceForm addSaleInvoiceForm, Authentication authentication) {
        // Ghi vào Bảng Hóa đơn.
        SaInvoice saInvoice = new SaInvoice();
        saInvoice.setAccountObjectId(addSaleInvoiceForm.getAccountObjectId()); // ID của khách hàng.
        // Một khách hàng có thể có nhiều Tài khoản Ngân hàng.
        //TODO: Tạo Entity AccountObjectBankAccount
        saInvoice.setEmployeeId(addSaleInvoiceForm.getEmployeeId()); // Nhân viên bán hàng.
        saInvoice.setIsPosted(addSaleInvoiceForm.getIsPosted()); // Trạng thái Hạch toán.
        saInvoice.setPaymentMethod(addSaleInvoiceForm.getPaymentMethod()); // Hình thức thanh toán.
        saInvoice.setInvTemplateNo(addSaleInvoiceForm.getInvTemplateNo()); // Mẫu số Hóa đơn.
        saInvoice.setInvSeries(addSaleInvoiceForm.getInvSeries()); // Ký hiệu hóa đơn
        saInvoice.setInvNo(addSaleInvoiceForm.getInvNo()); // Số hóa đơn
        saInvoice.setInvDate(addSaleInvoiceForm.getInvDate()); // Ngày hóa đơn
        saInvoice.setCurrencyId(addSaleInvoiceForm.getCurrencyId()); // Loại tiền.
        saInvoice.setExchangeRate(addSaleInvoiceForm.getExchangeRate()); // Tỷ giá.
        saInvoice.setDisplayOnBook(addSaleInvoiceForm.getDisplayOnBook()); // Sổ Tài chính hay Sổ quản trị, hay cả 2.
        saInvoice.setIsPostedLastYear(addSaleInvoiceForm.getIsPostedLastYear()); // Có phải từ năm ngoái không.
        saInvoice.setEditVersion(1); // Phiên bản của Hóa đơn.
        java.sql.Timestamp timestamp_now = new java.sql.Timestamp(new java.util.Date().getTime());
        saInvoice.setCreatedDate(timestamp_now); // Thời gian tạo.
        saInvoice.setCreatedBy(authentication.getName()); // Người tạo
        saInvoiceRepository.save(saInvoice); // Lưu vào bảng Hóa đơn.
        Integer saInvoiceId = saInvoice.getId();

        // 2. Ghi vào bảng SAInvoiceDetail.
        for(int i = 0; i < 10; i++){
            String description = addSaleInvoiceForm.getSaInvoiceDetailList().get(i).getDescription(); // gây ra bug
            if(!description.isBlank() && !description.isEmpty()){
                SaInvoiceDetail submitted = addSaleInvoiceForm.getSaInvoiceDetailList().get(i);
                SaInvoiceDetail saInvoiceDetail = new SaInvoiceDetail(); //FIXME: Không cần khởi tạo object mới ở chỗ này, dùng luôn submitted object , set luôn giá trị field khác vào.
                saInvoiceDetail.setRefId(saInvoiceId); // ID tham chiếu.
                saInvoiceDetail.setAmount(new BigDecimal(1)); // Số lượng
                saInvoiceDetail.setAmountOc(new BigDecimal(2));
                saInvoiceDetail.setAmountAfterTax(new BigDecimal(3));
                saInvoiceDetail.setVatAccount(submitted.getVatAccount());
                saInvoiceDetail.setAmountAfterTax(submitted.getAmountAfterTax());
                saInvoiceDetail.setDiscountAmount(submitted.getDiscountAmount());
                saInvoiceDetail.setUnitPriceAfterTax(submitted.getUnitPriceAfterTax());
                saInvoiceDetail.setUnitId(submitted.getUnitId());
                saInvoiceDetail.setSortOrder(submitted.getSortOrder());
                saInvoiceDetail.setPromotion(submitted.getPromotion());
                saInvoiceDetail.setQuantity(submitted.getQuantity());
                saInvoiceDetail.setNotInVatDeclaration(submitted.getNotInVatDeclaration());

                saInvoiceDetail.setVatRate(submitted.getVatRate());
                saInvoiceDetail.setCustomField1(submitted.getCustomField1());
                saInvoiceDetail.setCustomField2(submitted.getCustomField2());
                saInvoiceDetail.setCustomField3(submitted.getCustomField3());
                saInvoiceDetail.setCustomField4(submitted.getCustomField4());
                saInvoiceDetail.setCustomField5(submitted.getCustomField5());
                saInvoiceDetail.setCustomField6(submitted.getCustomField6());
                saInvoiceDetail.setCustomField7(submitted.getCustomField7());
                saInvoiceDetail.setCustomField8(submitted.getCustomField8());
                saInvoiceDetail.setCustomField9(submitted.getCustomField9());
                saInvoiceDetail.setCustomField10(submitted.getCustomField10());
                saInvoiceDetailRepository.save(saInvoiceDetail);
            }
        }


        // Sau khi thêm Hóa đơn bán hàng thành công, chuyển hướng về trang Danh sách Hóa đơn bán hàng.
        return "sale_invoices";
    }


}
