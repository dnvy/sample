package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.AutoBusiness;
import com.example.repository.AutoBusinessRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class AutoBusinessController {

    @Autowired
    AutoBusinessRepository autoBusinessRepository;

    /**
     * Show list of accounts transfer.
     *
     * @return
     */
    @RequestMapping(value = "/auto_business", method = RequestMethod.GET)
    public ModelAndView showAllAutoBusinessItems() {
        ModelAndView modelAndView = new ModelAndView("auto_business/list");
        List<AutoBusiness> autoBusinessList = autoBusinessRepository.findAll();
        modelAndView.addObject("autoBusinessList", autoBusinessList);
        modelAndView.getModel().put("page_title", "Định khoản Tự động");
        return modelAndView;
    }

    /**
     * Delete an auto_business item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/auto_business/delete/{id}", method = RequestMethod.POST)
    public String deleteAutoBusiness(@PathVariable("id") Integer id, HttpServletRequest request) {
        AutoBusiness autoBusiness = autoBusinessRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid auto_business id: " + id));
        autoBusinessRepository.delete(autoBusiness);
        return "redirect:" + request.getHeader("Referer");
    }

}
