package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dropdownlist.JobDropDownItem;
import com.example.entity.Job;
import com.example.entity.JobProduct;
import com.example.form.JobForm;
import com.example.repository.InventoryItemRepository;
import com.example.repository.JobProductRepository;
import com.example.repository.JobRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Job controller - Công đoạn tính giá thành.
 */
@Controller
public class JobController {

    @Autowired
    InventoryItemRepository inventoryItemRepository;

    @Autowired
    JobProductRepository jobProductRepository;

    @Autowired
    JobRepository jobRepository;

    private static final Integer JOB_TYPE_WORKSHOP = 0; // Phân xưởng.
    private static final Integer JOB_TYPE_PRODUCT = 1; // Sản phẩm.
    private static final Integer JOB_TYPE_PRODUCTION_PROCESS = 2; // Quy trình sản xuất.
    private static final Integer JOB_TYPE_STAGE = 3; // Công đoạn.

    /**
     * Show list of jobs.
     *
     * @return
     */
    @RequestMapping(value = "/jobs", method = RequestMethod.GET)
    public ModelAndView inventoryItems() {
        ModelAndView modelAndView = new ModelAndView("job/list");
        List<Job> jobList = jobRepository.findAll();
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("jobList", jobList);
        Map<Integer, String> jobTypeList = new HashMap<>();
        jobTypeList.put(0, "Phân xưởng");
        jobTypeList.put(1, "Sản phẩm");
        jobTypeList.put(2, "Quy trình sản xuất");
        jobTypeList.put(3, "Công đoạn");
        modelAndView.addObject("jobTypeList", jobTypeList);
        modelAndView.getModel().put("page_title", "Danh mục công đoạn");
        return modelAndView;
    }

    /**
     * Delete a job item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/job/delete/{id}", method = RequestMethod.POST)
    public String deleteJob(@PathVariable("id") Integer id, HttpServletRequest request) {
        Job job = jobRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid job id: " + id));
        jobRepository.delete(job);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Hiển thị trang để thêm Đối tượng tập hợp chi phí.
     *
     * @return
     */
    @RequestMapping(value = "/add_job", method = RequestMethod.GET)
    public ModelAndView showPageForAddJob(){
        ModelAndView modelAndView = new ModelAndView("job/add");
        List<JobProduct> jobProductList = new ArrayList<>();
        for(int i = 0; i < 20; i++){
            JobProduct jobProduct = new JobProduct();
            jobProductList.add(jobProduct);
        }
        JobForm jobForm = new JobForm();
        jobForm.setJobProductList(jobProductList);
        modelAndView.addObject("jobForm", jobForm);
        modelAndView.getModel().put("page_title", "Thêm Đối tượng tập hợp chi phí");
        return modelAndView;
    }

    /**
     * Submit data add job.
     *
     * @return
     */
    @RequestMapping(value = "/add_job", method = RequestMethod.POST)
    public String submitDataAddJob(@ModelAttribute("jobForm") JobForm jobForm, BindingResult bindingResult, Authentication authentication){
        if (bindingResult.hasErrors()){
            return "job/add";
        }
        Job job = new Job();
        job.setJobType(jobForm.getJobType());
        job.setJobCode(jobForm.getJobCode());
        job.setJobName(jobForm.getJobName());
        job.setDescription(jobForm.getDescription());
        job.setActiveStatus(Boolean.TRUE);
        job.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        job.setCreatedDate(now);
        job.setIsSystem(Boolean.FALSE); // IsSystem = TRUE khi thêm vào lúc khởi tạo hệ thống.
        // Quy trình sản xuất.
        if(jobForm.getJobType() == JOB_TYPE_PRODUCTION_PROCESS){
            job.setProductionProcessType(jobForm.getProductionProcessType());
        }
        jobRepository.save(job);
        Integer jobId = job.getId();
        List<JobProduct> jobFormJobProductList = jobForm.getJobProductList();
        for(int i = 0; i < 20; i++){
            // Kiểm tra xem người dùng có chọn sản phẩm nào không.
            if(jobFormJobProductList.get(i).getId() > 0){
                JobProduct jobProduct = jobFormJobProductList.get(i);
                jobProduct.setJobId(jobId);
                jobProductRepository.save(jobProduct);
            }
        }
        return "redirect:/jobs";
    }

    /**
     * Get active jobs, 2 columns.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/jobs_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getActiveJobsJSON() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Job> jobList = jobRepository.getActiveJobs();
        List<JobDropDownItem> jobDropDownItemList = new ArrayList<>();
        for (Job job : jobList){
            JobDropDownItem jobDropDownItem = new JobDropDownItem();
            jobDropDownItem.setId(job.getId());
            jobDropDownItem.setJobCode(job.getJobCode());
            jobDropDownItem.setJobName(job.getJobName());
            jobDropDownItemList.add(jobDropDownItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(jobDropDownItemList);
        return jsonDataString;
    }

}
