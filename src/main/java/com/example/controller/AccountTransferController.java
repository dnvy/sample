package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.AccountTransfer;
import com.example.repository.AccountTransferRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class AccountTransferController {

    @Autowired
    AccountTransferRepository accountTransferRepository;

    /**
     * Show list of accounts transfer.
     *
     * @return
     */
    @RequestMapping(value = "/account_transfers", method = RequestMethod.GET)
    public ModelAndView showAllAccountTransferItems() {
        ModelAndView modelAndView = new ModelAndView("account_transfer/list");
        List<AccountTransfer> accountTranfers = accountTransferRepository.findAll();
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("accountTranfers", accountTranfers);
        modelAndView.getModel().put("page_title", "Cặp Tài khoản Kế toán Kết chuyển");
        return modelAndView;
    }

    /**
     * Delete an account_transfer item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/account_transfer/delete/{id}", method = RequestMethod.POST)
    public String deleteAccountTransfer(@PathVariable("id") Integer id, HttpServletRequest request) {
        AccountTransfer accountTransfer = accountTransferRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account_transfer id: " + id));
        accountTransferRepository.delete(accountTransfer);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Show page for adding new account_transfer.
     *
     * @return
     */
    @RequestMapping(value = "/add_account_transfer", method = RequestMethod.GET)
    public ModelAndView showPageAddAccountTransfer() {
        AccountTransfer accountTransfer = new AccountTransfer();
        ModelAndView modelAndView = new ModelAndView("account_transfer/add");
        modelAndView.addObject("accountTransfer", accountTransfer);
        return modelAndView;
    }

    /**
     * Show page for edit an account_transfer.
     *
     * @return
     */
    @RequestMapping(value = "/account_transfer/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageEditAccountTransfer(@PathVariable("id") Integer id) {
        Optional<AccountTransfer> accountTransferOptional = accountTransferRepository.findById(id);
        AccountTransfer accountTransfer = accountTransferOptional.get();
        ModelAndView modelAndView = new ModelAndView("account_transfer/edit");
        modelAndView.addObject("accountTransfer", accountTransfer);
        return modelAndView;
    }

    /**
     * Show page for view detail an account_transfer.
     *
     * @return
     */
    @RequestMapping(value = "/account_transfer/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewDetailAccountTransfer(@PathVariable("id") Integer id) {
        Optional<AccountTransfer> accountTransferOptional = accountTransferRepository.findById(id);
        AccountTransfer accountTransfer = accountTransferOptional.get();
        ModelAndView modelAndView = new ModelAndView("account_transfer/view");
        modelAndView.addObject("accountTransfer", accountTransfer);
        return modelAndView;
    }

    /**
     * Submit data add accountTransfer.
     *
     * @param accountTransfer
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/submit_add_account_transfer", method = RequestMethod.POST)
    public String submitDataAddAccountTransfer(@ModelAttribute("accountTransfer") @Valid AccountTransfer accountTransfer, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "account_transfer/add";
        }
        accountTransfer.setActiveStatus(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountTransfer.setCreatedDate(now);
        accountTransfer.setCreatedBy(authentication.getName());
        accountTransferRepository.save(accountTransfer);
        return "redirect:/account_transfers";
    }

    /**
     * Submit data when edit accountTransfer.
     *
     * @param accountTransfer
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/submit_edit_account_transfer", method = RequestMethod.POST)
    public String submitDataEditAccountTransfer(@ModelAttribute("accountTransfer") @Valid AccountTransfer accountTransfer, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "account_transfer/edit";
        }
        AccountTransfer accountTransferOld = accountTransferRepository.findById(accountTransfer.getId()).get();
        accountTransfer.setCreatedBy(accountTransferOld.getCreatedBy());
        accountTransfer.setCreatedDate(accountTransferOld.getCreatedDate());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        accountTransfer.setModifiedDate(now);
        accountTransfer.setModifiedBy(authentication.getName());
        accountTransferRepository.save(accountTransfer);
        return "redirect:/account_transfers";
    }

}
