package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.SupplyCategory;
import com.example.repository.SupplyCategoryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class SupplyCategoryController {

    @Autowired
    SupplyCategoryRepository supplyCategoryRepository;

    /**
     * List of supply categories.
     *
     * @return
     */
    @RequestMapping(value = "/supply_categories", method = RequestMethod.GET)
    public ModelAndView supplyCategoryList() {
        ModelAndView modelAndView = new ModelAndView("supply_category/list");
        List<SupplyCategory> supplyCategories = supplyCategoryRepository.findAll();
        modelAndView.addObject("supplyCategories", supplyCategories);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Loại Công cụ dụng cụ");
        return modelAndView;
    }

    /**
     * Delete a supply category item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/supply_category/delete/{id}", method = RequestMethod.POST)
    public String deleteSupplyCategory(@PathVariable("id") Integer id, HttpServletRequest request) {
        SupplyCategory supplyCategory = supplyCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid supply category id: " + id));
        //TODO: Check related FK.
        supplyCategoryRepository.delete(supplyCategory);
        return "redirect:" + request.getHeader("Referer");
    }

}
