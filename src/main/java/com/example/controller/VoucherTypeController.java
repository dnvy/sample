package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.VoucherType;
import com.example.repository.VoucherTypeRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class VoucherTypeController {

    @Autowired
    VoucherTypeRepository voucherTypeRepository;

    /**
     * Show list of all voucher types.
     *
     * @return
     */
    @RequestMapping(value = "/voucher_types", method = RequestMethod.GET)
    public ModelAndView showUnits() {
        ModelAndView modelAndView = new ModelAndView("voucher_type/list");
        List<VoucherType> voucherTypeList = voucherTypeRepository.findAll();
        modelAndView.addObject("voucherTypeList", voucherTypeList);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Đơn vị tính");
        return modelAndView;
    }

    /**
     * Delete a voucher type.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/voucher_type/delete/{id}", method = RequestMethod.POST)
    public String deleteVoucherType(@PathVariable("id") Integer id, HttpServletRequest request) {
        VoucherType voucherType = voucherTypeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        voucherTypeRepository.delete(voucherType);
        return "redirect:" + request.getHeader("Referer");
    }

}
