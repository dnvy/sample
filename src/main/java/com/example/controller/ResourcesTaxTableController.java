package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.ResourcesTaxTable;
import com.example.repository.ResourcesTaxTableRepository;

import java.util.List;

/**
 * Danh mục Biểu thuế tài nguyên.
 */
@Controller
public class ResourcesTaxTableController {

    @Autowired
    ResourcesTaxTableRepository resourcesTaxTableRepository;

    /**
     * Show list all resources taxes.
     *
     * @return
     */
    @RequestMapping(value = "/resources_taxes", method = RequestMethod.GET)
    public ModelAndView showAllProjectWorks() {
        ModelAndView modelAndView = new ModelAndView("resources_tax_table/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<ResourcesTaxTable> resourcesTaxTableList = resourcesTaxTableRepository.findAll();
        modelAndView.addObject("resourcesTaxTableList", resourcesTaxTableList);
        modelAndView.getModel().put("page_title", "Biểu thuế tài nguyên");
        return modelAndView;
    }

}
