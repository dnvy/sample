package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.TaTaxAgentInfo;
import com.example.form.TaTaxAgentInfoForm;
import com.example.repository.TaTaxAgentInfoRepository;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Thiết lập thông tin đại lý thuế.
 */
@Controller
public class TaTaxAgentInfoController {

    @Autowired
    TaTaxAgentInfoRepository taTaxAgentInfoRepository;

    /**
     * Hiển thị trang để edit thông tin đại lý thuế
     *
     * @return
     */
    @RequestMapping(value = "/edit_ta_tax_agent_info", method = RequestMethod.GET)
    public ModelAndView showPageEditTATaxAgentInfo() {
        // Do table này chỉ có 1 bản ghi, và chỉ cho edit, không có add, list, delete.
        Integer id = 1;
        ModelAndView modelAndView = new ModelAndView("ta_tax_agent_info/edit");
        Optional<TaTaxAgentInfo> taTaxAgentInfoOptional = taTaxAgentInfoRepository.findById(id);
        TaTaxAgentInfo taTaxAgentInfo = taTaxAgentInfoOptional.get();
        TaTaxAgentInfoForm taTaxAgentInfoForm = new TaTaxAgentInfoForm();
        taTaxAgentInfoForm.setId(id);
        taTaxAgentInfoForm.setTaxAgentName(taTaxAgentInfo.getTaxAgentName());
        taTaxAgentInfoForm.setTaxCode(taTaxAgentInfo.getTaxCode());
        taTaxAgentInfoForm.setAddress(taTaxAgentInfo.getAddress());
        taTaxAgentInfoForm.setDistrict(taTaxAgentInfo.getDistrict());
        taTaxAgentInfoForm.setProvinceOrCity(taTaxAgentInfo.getProvinceOrCity());
        taTaxAgentInfoForm.setTel(taTaxAgentInfo.getTel());
        taTaxAgentInfoForm.setFax(taTaxAgentInfo.getFax());
        taTaxAgentInfoForm.setEmail(taTaxAgentInfo.getEmail());
        taTaxAgentInfoForm.setContractCode(taTaxAgentInfo.getContractCode());
        taTaxAgentInfoForm.setContractDate(taTaxAgentInfo.getContractDate());
        taTaxAgentInfoForm.setEmployeeName(taTaxAgentInfo.getEmployeeName());
        taTaxAgentInfoForm.setCertificateNo(taTaxAgentInfo.getCertificateNo());
        taTaxAgentInfoForm.setIsDisplayOnDeclaration(taTaxAgentInfo.getIsDisplayOnDeclaration());
        taTaxAgentInfoForm.setIsDisplayOnDeclaration(taTaxAgentInfo.getIsDisplayOnDeclaration());
        taTaxAgentInfoForm.setTaxationBureauCode(taTaxAgentInfo.getTaxationBureauCode());
        taTaxAgentInfoForm.setTaxOrganManagementCode(taTaxAgentInfo.getTaxOrganManagementCode());
        taTaxAgentInfoForm.setProvideUnit(taTaxAgentInfo.getProvideUnit());
        taTaxAgentInfoForm.setCertificateNoUnit(taTaxAgentInfo.getCertificateNoUnit());
        modelAndView.addObject("taTaxAgentInfoForm", taTaxAgentInfoForm);
        modelAndView.getModel().put("page_title", "Thiết lập thông tin cơ quan thuế; đại lý thuế; đơn vị cung cấp DV kế toán");
        return modelAndView;
    }

    /**
     * Submit data edit TATaxAgentInfo.
     *
     * @param taTaxAgentInfoForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/update_ta_tax_agent_info", method = RequestMethod.POST)
    public String submitDataEditTATaxAgentInfo(@ModelAttribute("taTaxAgentInfoForm") @Valid TaTaxAgentInfoForm taTaxAgentInfoForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "ta_tax_agent_info/edit";
        }
        Optional<TaTaxAgentInfo> taTaxAgentInfoOptional = taTaxAgentInfoRepository.findById(1);
        TaTaxAgentInfo taTaxAgentInfoOld = taTaxAgentInfoOptional.get();
        TaTaxAgentInfo taTaxAgentInfo = new TaTaxAgentInfo();
        taTaxAgentInfo.setId(taTaxAgentInfoOld.getId());
        taTaxAgentInfo.setTaxAgentName(taTaxAgentInfoForm.getTaxAgentName());
        taTaxAgentInfo.setTaxCode(taTaxAgentInfoForm.getTaxCode());
        taTaxAgentInfo.setAddress(taTaxAgentInfoForm.getAddress());
        taTaxAgentInfo.setDistrict(taTaxAgentInfoForm.getDistrict());
        taTaxAgentInfo.setProvinceOrCity(taTaxAgentInfoForm.getProvinceOrCity());
        taTaxAgentInfo.setTel(taTaxAgentInfoForm.getTel());
        taTaxAgentInfo.setFax(taTaxAgentInfoForm.getFax());
        taTaxAgentInfo.setEmail(taTaxAgentInfoForm.getEmail());
        taTaxAgentInfo.setContractCode(taTaxAgentInfoForm.getContractCode());
        taTaxAgentInfo.setContractDate(taTaxAgentInfoForm.getContractDate());
        taTaxAgentInfo.setEmployeeName(taTaxAgentInfoForm.getEmployeeName());
        taTaxAgentInfo.setCertificateNo(taTaxAgentInfoForm.getCertificateNo());
        if (taTaxAgentInfo.getIsDisplayOnDeclaration() != null) {
            taTaxAgentInfo.setIsDisplayOnDeclaration(Boolean.TRUE);
        } else {
            taTaxAgentInfo.setIsDisplayOnDeclaration(Boolean.FALSE);
        }
        taTaxAgentInfo.setCreatedDate(taTaxAgentInfoOld.getCreatedDate());
        taTaxAgentInfo.setCreatedBy(taTaxAgentInfoOld.getCreatedBy());
        taTaxAgentInfo.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        taTaxAgentInfo.setModifiedDate(now);
        taTaxAgentInfo.setTaxationBureauCode(taTaxAgentInfoForm.getTaxationBureauCode());
        taTaxAgentInfo.setTaxOrganManagementCode(taTaxAgentInfoForm.getTaxOrganManagementCode());
        taTaxAgentInfo.setBranchId(taTaxAgentInfoOld.getBranchId());
        taTaxAgentInfo.setProvideUnit(taTaxAgentInfoForm.getProvideUnit());
        taTaxAgentInfo.setCertificateNoUnit(taTaxAgentInfoForm.getCertificateNoUnit());
        taTaxAgentInfoRepository.save(taTaxAgentInfo);
        return "redirect:/edit_ta_tax_agent_info";
    }

}
