package com.example.controller;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.CaAudit;
import com.example.entity.CaAuditDetail;
import com.example.entity.CaAuditMemberDetail;
import com.example.form.CaAuditForm;
import com.example.repository.CaAuditDetailRepository;
import com.example.repository.CaAuditMemberDetailRepository;
import com.example.repository.CaAuditRepository;
import com.example.repository.OrganizationUnitRepository;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Kiểm kê quỹ.
 */
@Controller
public class CaAuditController {

    @Autowired
    CaAuditRepository caAuditRepository;

    @Autowired
    CaAuditDetailRepository caAuditDetailRepository;

    @Autowired
    CaAuditMemberDetailRepository caAuditMemberDetailRepository;

    /**
     * Để tìm tên công ty từ branchId.
     */
    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    /**
     * Mã nghiệp vụ, Kiểm kê tiền mặt = 1030.
     */
    private static final Integer REF_TYPE_CASH_AUDIT = 1030;

    /**
     * Add Cash auditing.
     *
     * @return
     */
    @RequestMapping(value = "/add_ca_audit", method = RequestMethod.GET)
    public ModelAndView showPageForAddCaAudit() {
        ModelAndView modelAndView = new ModelAndView("ca_audit/add");
        CaAuditForm caAuditForm = new CaAuditForm();
        List<CaAuditDetail> caAuditDetailList = new ArrayList<>();
        // Có 12 loại mệnh giá tiền VND.
        for (int i = 0; i < 12; i++) {
            CaAuditDetail caAuditDetail = new CaAuditDetail();
            caAuditDetailList.add(caAuditDetail);
        }
        caAuditForm.setCaAuditDetailList(caAuditDetailList);
        List<CaAuditMemberDetail> caAuditMemberDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CaAuditMemberDetail caAuditMemberDetail = new CaAuditMemberDetail();
            caAuditMemberDetailList.add(caAuditMemberDetail);
        }
        caAuditForm.setCaAuditMemberDetailList(caAuditMemberDetailList);
        modelAndView.addObject("caAuditForm", caAuditForm);
        modelAndView.getModel().put("page_title", "Thêm Đợt kiểm kê quỹ");
        return modelAndView;
    }

    /**
     * Submit data add Cash auditing.
     *
     * @param caAuditForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_ca_audit", method = RequestMethod.POST)
    public String submitDataAddCaAudit(@ModelAttribute("caAuditForm") CaAuditForm caAuditForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "ca_audit/add";
        }
        CaAudit caAudit = new CaAudit();
        caAudit.setRefNo(caAuditForm.getRefNo());
        caAudit.setRefType(REF_TYPE_CASH_AUDIT);
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date parsedDate = dateFormat.parse(caAuditForm.getRefTimeString());
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            caAudit.setRefTime(timestamp);
        } catch (Exception e) {

        }
        caAudit.setJournalMemo(caAuditForm.getJournalMemo());
        caAudit.setAuditDate(caAuditForm.getAuditDate());
        caAudit.setTotalAuditAmount(caAuditForm.getTotalAuditAmount());
        caAudit.setTotalBalanceAmount(caAuditForm.getTotalBalanceAmount()); // NOT NULL.
        caAudit.setReason(caAuditForm.getReason());
        caAudit.setConclusion(caAuditForm.getConclusion());
        if (caAuditForm.getIsExecuted() != null) {
            caAudit.setIsExecuted(Boolean.TRUE);
        } else {
            caAudit.setIsExecuted(Boolean.FALSE);
        }
        // Ghi nhận trên cả sổ Tài chính và Sổ quản trị.
        caAudit.setDisplayOnBook(2);
        // Lần đầu thêm mới, có edit version = 1.
        caAudit.setEditVersion(1);
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < 12; i++) {
            if (!caAuditForm.getCaAuditDetailList().get(i).getDescription().isEmpty() && !caAuditForm.getCaAuditDetailList().get(i).getDescription().isBlank()
                    && caAuditForm.getCaAuditDetailList().get(i).getDescription() != null) {
                BigDecimal temp = BigDecimal.valueOf(caAuditForm.getCaAuditDetailList().get(i).getValueOfMoney() * caAuditForm.getCaAuditDetailList().get(i).getQuantity());
                sum = sum.add(temp);
            }
        }
        caAudit.setTotalAuditAmount(sum);
        caAuditRepository.save(caAudit);
        Integer caAuditId = caAudit.getId();
        // Kiểm đếm tiền.
        for (int i = 0; i < 12; i++) {
            if (!caAuditForm.getCaAuditDetailList().get(i).getDescription().isEmpty() && !caAuditForm.getCaAuditDetailList().get(i).getDescription().isBlank()
                    && caAuditForm.getCaAuditDetailList().get(i).getDescription() != null) {
                CaAuditDetail caAuditDetail = new CaAuditDetail();
                caAuditDetail.setValueOfMoney(caAuditForm.getCaAuditDetailList().get(i).getValueOfMoney());
                caAuditDetail.setDescription(caAuditForm.getCaAuditDetailList().get(i).getDescription());
                caAuditDetail.setQuantity(caAuditForm.getCaAuditDetailList().get(i).getQuantity());
                caAuditDetail.setRefId(caAuditId);
                caAuditDetail.setSortOrder(caAuditId); // NOT NULL. Chưa rõ tác dụng.
                BigDecimal temp = BigDecimal.valueOf(caAuditForm.getCaAuditDetailList().get(i).getValueOfMoney() * caAuditForm.getCaAuditDetailList().get(i).getQuantity());
                caAuditDetail.setAmount(temp);
                caAuditDetailRepository.save(caAuditDetail);
            }
        }
        // Thành viên kiểm kê.
        for (int i = 0; i < 10; i++) {
            if (!caAuditForm.getCaAuditMemberDetailList().get(i).getAccountObjectName().isEmpty() && !caAuditForm.getCaAuditMemberDetailList().get(i).getAccountObjectName().isBlank()
                    && caAuditForm.getCaAuditMemberDetailList().get(i).getAccountObjectName() != null) {
                CaAuditMemberDetail caAuditMemberDetail = new CaAuditMemberDetail();
                caAuditMemberDetail.setAccountObjectName(caAuditForm.getCaAuditMemberDetailList().get(i).getAccountObjectName());
                caAuditMemberDetail.setPosition(caAuditForm.getCaAuditMemberDetailList().get(i).getPosition());
                caAuditMemberDetail.setRefId(caAuditId);
                caAuditMemberDetail.setSortOrder(caAuditId); // NOT NULL. Chưa rõ tác dụng
                caAuditMemberDetail.setRepresentative(caAuditForm.getCaAuditMemberDetailList().get(i).getRepresentative());
                caAuditMemberDetailRepository.save(caAuditMemberDetail);
            }
        }
        //return "redirect:/ca_audits";
        return "redirect:/desktop";
    }

    /**
     * Show list of cash audits.
     *
     * @return
     */
    @RequestMapping(value = "/ca_audits", method = RequestMethod.GET)
    public ModelAndView showListOfCashAudit(){
        ModelAndView modelAndView = new ModelAndView("ca_audit/list");

        return modelAndView;
    }



    /**
     * Sinh ra file Biên bản kiểm kê tiền mặt.
     *
     * @throws JRException
     */
    @RequestMapping(value = "/ca_audit/minute_pdf/{id}", method = RequestMethod.GET)
    public void generatePDFCashAuditMinute(@PathVariable("id") Integer id) throws JRException {
        Optional<CaAudit> caAuditOptional = caAuditRepository.findById(id);
        CaAudit caAudit = caAuditOptional.get();
        Integer branchId = caAudit.getBranchId();
        String companyName = organizationUnitRepository.findById(branchId).get().getOrganizationUnitName();
        Connection connection = null;
        try {
            String source = "E:\\source_code\\github.com\\acc133\\src\\main\\resources\\report\\ca_audit.jrxml";
            JasperReport jasperReport = JasperCompileManager.compileReport(source);
            Map<String, Object> params = new HashMap<>();
            params.put("companyName", companyName); // Tên công ty. Company name.
            params.put("minu", caAudit.getRefNo()); // Số biên bản. Minute number.
            params.put("id", caAudit.getId()); // ID
            Timestamp timestampAudit = caAudit.getRefTime();
            params.put("dateString", "Hôm nay, vào " + timestampAudit.getHours() + " giờ " + timestampAudit.getHours() + " ngày " + timestampAudit.getDay() + " tháng " + timestampAudit.getMonth() + " năm " + timestampAudit.getYear()); // Ngày
            connection = getConnection();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
            OutputStream outputStream = new FileOutputStream("E:\\source_code\\github.com\\acc133\\src\\main\\resources\\report\\ca_audit.pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * For real application and sample.
     *
     * @return
     */
    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection("jdbc:sqlserver://localhost\\MSSQLSERVER;database=accounting133", "sa", "12345678");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * Sample.
     *
     * @param classId
     * @param className
     * @param targetFolder
     */
    public static void exportToPdfSample(String classId, String className, String targetFolder) {
        Connection connection = null;
        try {
            String source = "E:\\source_code\\github.com\\acc133\\src\\main\\resources\\report\\foo.jrxml";
            JasperReport jasperReport = JasperCompileManager.compileReport(source);
            Map<String, Object> params = new HashMap<>();
            params.put("classId", classId);
            params.put("className", className);
            connection = getConnection();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
            OutputStream outputStream = new FileOutputStream(targetFolder + "_STUDENT_MARK_" + classId + ".pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sample.
     */
    @RequestMapping(value = "/ca_audit/minute_pdf2", method = RequestMethod.GET)
    public void generatePDFCashAuditMinuteSample() {
        String targetFolder = "D:\\StudentMarks";
        Connection connection = null;
        try {
            String query = "SELECT * FROM CLASS";
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String classId = resultSet.getString("CLASS_ID");
                String className = resultSet.getString("CLASS_NAME");
                exportToPdfSample(classId, className, targetFolder);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
