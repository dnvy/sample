package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dto.PuContractDTO;
import com.example.entity.AccountObject;
import com.example.entity.PuContract;
import com.example.entity.PuContractDetailInventoryItem;
import com.example.form.PuContractForm;
import com.example.repository.AccountObjectRepository;
import com.example.repository.PuContractRepository;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Hợp đồng mua hàng.
 */
@Controller
public class PuContractController {

    @Autowired
    AccountObjectRepository accountObjectRepository;

    @Autowired
    PuContractRepository puContractRepository;

    private static final Integer ADD_PU_CONTRACT_REF_TYPE = 9040;

    /**
     * Show list all project works.
     *
     * @return
     */
    @RequestMapping(value = "/pu_contracts", method = RequestMethod.GET)
    public ModelAndView showAllPuContracts() {
        ModelAndView modelAndView = new ModelAndView("pu_contract/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<PuContract> puContractList = puContractRepository.findAll();
        List<PuContractDTO> puContractDTOList = new ArrayList<>();
        for (PuContract puContract : puContractList){
            PuContractDTO puContractDTO = new PuContractDTO();
            puContractDTO.setId(puContract.getId());
            puContractDTO.setStatus(puContract.getStatus());
            puContractDTO.setContractCode(puContract.getContractCode());
            puContractDTO.setSignDate(puContract.getSignDate());
            puContractDTO.setSubject(puContract.getSubject());
            AccountObject vendor = accountObjectRepository.findById(puContract.getAccountObjectId()).get();
            puContractDTO.setAccountObjectName(vendor.getAccountObjectName());
            puContractDTO.setAccountObjectAddress(vendor.getAddress());
            puContractDTO.setAccountObjectContactName(vendor.getContactName());
            puContractDTO.setAmount(puContract.getAmount());
            puContractDTO.setCloseAmount(puContract.getCloseAmount());
            puContractDTO.setPaidAmount(puContract.getPaidAmount());
            puContractDTO.setPayableAmount(puContract.getPayableAmount());
            puContractDTO.setPaidAmount(puContract.getPaidAmount());
            puContractDTO.setPayableAmount(puContract.getPayableAmount());
            puContractDTO.setPaidAmountFinance(puContract.getPaidAmountFinance());
            puContractDTO.setEmployeeId(puContract.getEmployeeId());
            puContractDTO.setCloseDate(puContract.getCloseDate());
            puContractDTO.setCloseReason(puContract.getCloseReason());
            puContractDTOList.add(puContractDTO);
        }
        modelAndView.addObject("puContractDTOList", puContractDTOList);
        modelAndView.getModel().put("page_title", "Danh sách Hợp đồng mua hàng");
        return modelAndView;
    }

    /**
     * Delete pu_contract item.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/pu_contract/delete/{id}", method = RequestMethod.POST)
    public String deletePuContract(@PathVariable("id") Integer id) {
        PuContract puContract = puContractRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exepense_item id: " + id));
        //TODO: Check related FK.
        puContractRepository.delete(puContract);
        return "redirect:/pu_contracts";
    }

    /**
     * Show page for add project_work.
     *
     * @return
     */
    @RequestMapping(value = "/add_pu_contract", method = RequestMethod.GET)
    public ModelAndView showPageForAddPuContract() {
        PuContractForm puContractForm = new PuContractForm();
        ModelAndView modelAndView = new ModelAndView("pu_contract/add");
        List<PuContractDetailInventoryItem> puContractDetailInventoryItemList = new ArrayList<>();
        for (int i = 0; i< 20; i++){
            PuContractDetailInventoryItem puContractDetailInventoryItem = new PuContractDetailInventoryItem();
            puContractDetailInventoryItemList.add(puContractDetailInventoryItem);
        }
        puContractForm.setPuContractDetailInventoryItemList(puContractDetailInventoryItemList);
        modelAndView.addObject("puContractForm", puContractForm);
        return modelAndView;
    }

    /**
     * View detail a purchase contract.
     *
     * @return
     */
    @RequestMapping(value = "/pu_contract/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewPuContract(@PathVariable("id") Integer id) {
        Optional<PuContract> puContractOptional = puContractRepository.findById(id);
        PuContract puContract = puContractOptional.get();
        ModelAndView modelAndView = new ModelAndView("pu_contract/view");
        modelAndView.addObject("puContract", puContract);
        return modelAndView;
    }


    /**
     * Show page for editing purchase contract.
     *
     * @return
     */
    @RequestMapping(value = "/pu_contract/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageEditPuContract(@PathVariable("id") Integer id) {
        Optional<PuContract> puContractOptional = puContractRepository.findById(id);
        PuContract puContract = puContractOptional.get();
        ModelAndView modelAndView = new ModelAndView("pu_contract/edit");
        modelAndView.addObject("puContract", puContract);
        return modelAndView;
    }

    /**
     * Submit data for adding pu_contract.
     *
     * @return
     */
    @RequestMapping(value = "/edit_pu_contract", method = RequestMethod.POST)
    public String updatePuContract(@ModelAttribute("puContract") @Valid PuContract puContract, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "pu_contract/add";
        }
        // Optional<PuContract> puContractOptional = puContractRepository.findById(puContract.getId());
        // PuContract puContractOld = puContractOptional.get();
        puContract.setRefType(ADD_PU_CONTRACT_REF_TYPE); // NOT NULL.
        puContract.setAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setCloseAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setCloseAmountOc(puContract.getAmountOc()); // NOT NULL.
        puContract.setPaidAmountOc(new BigDecimal(0)); // NOT NULL.
        puContract.setPaidAmount(new BigDecimal(0)); // NOT NULL.
        puContract.setPayableAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setPayableAmountOc(puContract.getAmountOc()); // NOT NULL.
        puContractRepository.save(puContract);
        return "redirect:/pu_contracts";
    }

    /**
     * Submit data for adding pu_contract.
     *
     * @return
     */
    @RequestMapping(value = "/add_pu_contract", method = RequestMethod.POST)
    public String submitAddPuContract(@ModelAttribute("puContract") @Valid PuContract puContract, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "pu_contract/add";
        }
        puContract.setRefType(ADD_PU_CONTRACT_REF_TYPE); // NOT NULL.
        puContract.setAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setCloseAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setCloseAmountOc(puContract.getAmountOc()); // NOT NULL.
        puContract.setPaidAmountOc(new BigDecimal(0)); // NOT NULL.
        puContract.setPaidAmount(new BigDecimal(0)); // NOT NULL.
        puContract.setPayableAmount(puContract.getAmountOc()); // NOT NULL.
        puContract.setPayableAmountOc(puContract.getAmountOc()); // NOT NULL.
        puContractRepository.save(puContract);
        return "redirect:/pu_contracts";
    }

    /**
     * List all project works has IsActive = 1, in JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/pu_contracts_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getActivePuContractJSON() throws JsonProcessingException {
        List<PuContract> puContractList = puContractRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(puContractList);
        return jsonDataString;
    }

}
