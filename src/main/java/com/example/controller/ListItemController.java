package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.ListItem;
import com.example.repository.ListItemRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Danh mục mã thống kê.
 */
@Controller
public class ListItemController {

    @Autowired
    ListItemRepository listItemRepository;

    /**
     * Show all list_item.
     *
     * @return
     */
    @RequestMapping(value = "/list_item", method = RequestMethod.GET)
    public ModelAndView allListItem() {
        ModelAndView modelAndView = new ModelAndView("list_item/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<ListItem> listItems = listItemRepository.findAll();
        modelAndView.addObject("listItems", listItems);
        modelAndView.getModel().put("page_title", "Danh mục mã thống kê");
        return modelAndView;
    }

    /**
     * Delete a list_item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/list_item/delete/{id}", method = RequestMethod.POST)
    public String deleteListItem(@PathVariable("id") Integer id, HttpServletRequest request) {
        ListItem listItem = listItemRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid list_item id: " + id));
        //TODO: Check related FK.
        listItemRepository.delete(listItem);
        return "redirect:" + request.getHeader("Referer");
    }

}
