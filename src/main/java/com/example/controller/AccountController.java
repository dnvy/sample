package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.dto.AccountDTO;
import com.example.dto.SimpleAccount;
import com.example.entity.Account;
import com.example.repository.AccountRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Danh mục tài khoản. Liên quan đến Tài khoản 3 ký tự, Tài khoản 4 ký tự.
 */
@Controller
public class AccountController {

    @Autowired
    AccountRepository accountRepository;

    /**
     * Trả về JSON các tài khoản có 4 ký tự. Gồm các cột: Id, Số tài khoản 4 ký tự, Tên tài khoản.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/accounts_mulcol_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accountsMulColJSON() throws JsonProcessingException {
        // Lấy các tài khoản có 4 ký tự và ActiveStatus = 1.
        List<SimpleAccount> simpleAccountList = accountRepository.findAllAccount(Sort.by("accountNumber")).stream().map(SimpleAccount::new).collect(Collectors.toList());
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(simpleAccountList);
        return jsonDataString;
    }

    /**
     * Trả về JSON các tài khoản (3,4,5 ký tự). Gồm các cột: Id, Số tài khoản 4 ký tự, Tên tài khoản.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/accounts345_mulcol_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accounts345MulColJSON() throws JsonProcessingException {
        // Lấy các tài khoản có 4 ký tự và ActiveStatus = 1.
        List<SimpleAccount> simpleAccountList = accountRepository.findActiveAccount345().stream().map(SimpleAccount::new).collect(Collectors.toList());
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(simpleAccountList);
        return jsonDataString;
    }


    /**
     * Show list of accounts. Show all accounts in a data-grid. Use /accounts
     * for alternative.
     *
     * @return
     * @see #showAllAccounts()
     */
    @Deprecated
    @RequestMapping(value = "/accounts_flat", method = RequestMethod.GET)
    public ModelAndView accountsFlat() {
        ModelAndView modelAndView = new ModelAndView("account/list");
        List<Account> accounts = accountRepository.findAll();
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("accounts", accounts);
        return modelAndView;
    }

    /**
     * Show all accounts in TreeList.
     *
     * @return
     */
    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public ModelAndView showAllAccounts() {
        ModelAndView modelAndView = new ModelAndView("account/list");
        modelAndView.getModel().put("page_title", "Danh mục tài khoản");
        return modelAndView;
    }

    /**
     * JSON for show list all account_object_group in TreeList.
     *
     * @return
     * @throws JsonProcessingException
     * @see #showAllAccounts()
     */
    @RequestMapping(value = "/account_dto_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String accountDtoJSON() throws JsonProcessingException {
        List<Account> accountList = accountRepository.findAll();
        List<AccountDTO> accountDTOList = new ArrayList<>();
        for (Account account : accountList) {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setAccountNumber(account.getAccountNumber());
            accountDTO.setAccountName(account.getAccountName());
            accountDTO.setAccountNameEnglish(account.getAccountNameEnglish());
            accountDTO.setParentId(account.getParentId());
            accountDTO.setDescription(account.getDescription());
            if (account.getActiveStatus() == Boolean.TRUE) {
                accountDTO.setActiveStatusString("Có");
            } else {
                accountDTO.setActiveStatusString("Không");
            }
            accountDTO.setAccountCategoryKind(account.getAccountCategoryKind());
            if (account.getAccountCategoryKind() == 0) {
                accountDTO.setAccountCategoryKindString("Dư nợ");
            } else if (account.getAccountCategoryKind() == 1) {
                accountDTO.setAccountCategoryKindString("Dư có");
            } else {
                accountDTO.setAccountCategoryKindString("Lưỡng tính");
            }
            accountDTOList.add(accountDTO);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(accountDTOList);
        return jsonDataString;
    }

    /**
     * Show page for adding a new account.
     *
     * @return
     */
    @RequestMapping(value = "/add_account", method = RequestMethod.GET)
    public ModelAndView addAccount() {
        ModelAndView modelAndView = new ModelAndView("account/add");
        return modelAndView;
    }

    /**
     * Submit form add account.
     *
     * @return
     */
    @RequestMapping(value = "/add_account", method = RequestMethod.POST)
    public String addAccountSubmit(@ModelAttribute("addAccountForm") Account account, Authentication authentication) {
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        account.setCreatedDate(now);
        account.setCreatedBy(authentication.getName());
        // Tài khoản có 3 ký tự thì isParent = 1. Các tài khoản thêm mới luôn là tài khoản con.
        account.setIsParent(Boolean.FALSE);
        if (account.getDetailByAccountObject() == null) {
            account.setDetailByAccountObject(Boolean.FALSE);
        }
        if (account.getDetailByBankAccount() == null) {
            account.setDetailByBankAccount(Boolean.FALSE);
        }
        if (account.getDetailByJob() == null) {
            account.setDetailByJob(Boolean.FALSE);
        }
        if (account.getDetailByProjectWork() == null) {
            account.setDetailByProjectWork(Boolean.FALSE);
        }
        if (account.getDetailByOrder() == null) {
            account.setDetailByOrder(Boolean.FALSE);
        }
        if (account.getDetailByContract() == null) {
            account.setDetailByContract(Boolean.FALSE);
        }
        if (account.getDetailByExpenseItem() == null) {
            account.setDetailByExpenseItem(Boolean.FALSE);
        }
        if (account.getDetailByDepartment() == null) {
            account.setDetailByDepartment(Boolean.FALSE);
        }
        if (account.getDetailByListItem() == null) {
            account.setDetailByListItem(Boolean.FALSE);
        }
        if (account.getDetailByPuContract() == null) {
            account.setDetailByPuContract(Boolean.FALSE);
        }
        if (account.getActiveStatus() == null) {
            //FIXME: Kiểm tra lại, nếu không check thì là  NULL hay là FASLE?
            account.setActiveStatus(Boolean.FALSE);
        }
        if (account.getIsPostableInForeignCurrency() == null) {
            account.setIsPostableInForeignCurrency(Boolean.FALSE);
        }
        accountRepository.save(account);
        return "redirect:/accounts";
    }

    /**
     * Delete an account.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/account/delete/{id}", method = RequestMethod.POST)
    public String deleteAccount(@PathVariable("id") Integer id, HttpServletRequest request) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account id: " + id));
        accountRepository.delete(account);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * View detail of an account. For also 3-characters-account-numbersArray
     * and 4-characters-account-numbersArray.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/account/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewAccountDetail(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account/view");
        Optional<Account> account = accountRepository.findById(id);
        account.ifPresent(obj -> modelAndView.addObject("account", obj));
        Map<Integer, String> accountObjectTypeList = new HashMap<>();
        accountObjectTypeList.put(0, "Nhà cung cấp");
        accountObjectTypeList.put(1, "Khách hàng");
        accountObjectTypeList.put(2, "Nhân viên");
        modelAndView.addObject("accountObjectTypeList", accountObjectTypeList);
        UtilityList.setActiveStatusList(modelAndView);
        UtilityList.setTrueOrFalseList(modelAndView);
        UtilityList.setAlertOrRequireList(modelAndView);
        UtilityList.setAccountCategoryKindList(modelAndView);
        return modelAndView;
    }

    /**
     * Show page for editing an account.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/account/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditAcccount(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("account/edit");
        Optional<Account> account = accountRepository.findById(id);
        account.ifPresent(obj -> modelAndView.addObject("account", obj));
        Map<Integer, String> accountObjectTypeList = new HashMap<>();
        accountObjectTypeList.put(0, "Nhà cung cấp");
        accountObjectTypeList.put(1, "Khách hàng");
        accountObjectTypeList.put(2, "Nhân viên");
        modelAndView.addObject("accountObjectTypeList", accountObjectTypeList);
        UtilityList.setActiveStatusList(modelAndView);
        UtilityList.setTrueOrFalseList(modelAndView);
        UtilityList.setAlertOrRequireList(modelAndView);
        UtilityList.setAccountCategoryKindList(modelAndView);
        return modelAndView;
    }

    /**
     * Submit when update accout.
     *
     * @param account
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/update_account", method = RequestMethod.POST)
    public String updateAccountSubmit(@ModelAttribute("account") Account account, Authentication authentication) {
        Optional<Account> accountOptional = accountRepository.findById(account.getId());
        Account accountOld = accountOptional.get();
        account.setCreatedBy(accountOld.getCreatedBy());
        account.setCreatedDate(accountOld.getCreatedDate());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        account.setModifiedDate(now);
        account.setModifiedBy(authentication.getName());
        // Tài khoản có 3 ký tự thì isParent = 1. Các tài khoản thêm mới luôn là tài khoản con.
        account.setIsParent(Boolean.FALSE);
        if (account.getDetailByAccountObject() == null) {
            account.setDetailByAccountObject(Boolean.FALSE);
        }
        if (account.getDetailByBankAccount() == null) {
            account.setDetailByBankAccount(Boolean.FALSE);
        }
        if (account.getDetailByJob() == null) {
            account.setDetailByJob(Boolean.FALSE);
        }
        if (account.getDetailByProjectWork() == null) {
            account.setDetailByProjectWork(Boolean.FALSE);
        }
        if (account.getDetailByOrder() == null) {
            account.setDetailByOrder(Boolean.FALSE);
        }
        if (account.getDetailByContract() == null) {
            account.setDetailByContract(Boolean.FALSE);
        }
        if (account.getDetailByExpenseItem() == null) {
            account.setDetailByExpenseItem(Boolean.FALSE);
        }
        if (account.getDetailByDepartment() == null) {
            account.setDetailByDepartment(Boolean.FALSE);
        }
        if (account.getDetailByListItem() == null) {
            account.setDetailByListItem(Boolean.FALSE);
        }
        if (account.getDetailByPuContract() == null) {
            account.setDetailByPuContract(Boolean.FALSE);
        }
        if (account.getActiveStatus() == null) {
            //FIXME: Kiểm tra là NULL hay FALSE.
            account.setActiveStatus(Boolean.FALSE);
        }
        if (account.getIsPostableInForeignCurrency() == null) {
            account.setIsPostableInForeignCurrency(Boolean.FALSE);
        }
        accountRepository.save(account);
        return "redirect:/accounts";
    }

    /**
     * Get base URL.
     *
     * @param request
     * @return
     * @throws MalformedURLException
     */
    public String getURLBase(HttpServletRequest request) throws MalformedURLException {
        URL requestURL = new URL(request.getRequestURL().toString());
        String port = requestURL.getPort() == -1 ? "" : ":" + requestURL.getPort();
        return requestURL.getProtocol() + "://" + requestURL.getHost() + port;
    }

    /**
     * Export Excel all customers.
     *
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/Danh_muc_tai_khoan.xlsx", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportAllAccountsToExcel() throws IOException {
        List<Account> accounts = accountRepository.findAll();
        ByteArrayInputStream byteArrayInputStream = accountsToExcel(accounts);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=Danh_muc_tai_khoan.xlsx");
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(byteArrayInputStream));
    }

    /**
     * Support export to Excel.
     *
     * @param accounts
     * @return
     * @throws IOException
     * @see #exportAllAccountsToExcel()
     */
    public static ByteArrayInputStream accountsToExcel(List<Account> accounts) throws IOException {
        String[] COLUMNs = {"Số tài khoản", "Tên tài khoản", "Tính chất", "Tên tiếng Anh", "Diễn giải", "Trạng thái theo dõi"};
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Danh mục tài khoản");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Row for Header
            Row headerRow = sheet.createRow(3);
            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }
            // CellStyle for Age
            CellStyle ageCellStyle = workbook.createCellStyle();
            ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
            int rowIdx = 4;
            for (Account account : accounts) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(account.getAccountNumber());
                row.createCell(1).setCellValue(account.getAccountName());
                String accountCategoryKindString = "";
                if (account.getAccountCategoryKind() == 0) {
                    accountCategoryKindString = "Dư nợ";
                } else if (account.getAccountCategoryKind() == 1) {
                    accountCategoryKindString = "Dư có";
                } else {
                    accountCategoryKindString = "Lưỡng tính";
                }
                row.createCell(2).setCellValue(accountCategoryKindString);
                row.createCell(3).setCellValue(account.getAccountNameEnglish());
                row.createCell(4).setCellValue(account.getDescription());
                row.createCell(5).setCellValue(account.getActiveStatus() == Boolean.TRUE ? "Có" : "Không");
                //Cell ageCell = row.createCell(3);
                //ageCell.setCellValue(customer.getCompanyTaxCode());
                //ageCell.setCellStyle(ageCellStyle);
            }
            // Auto setActiveStatusList corresponding width of columns.
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

}
