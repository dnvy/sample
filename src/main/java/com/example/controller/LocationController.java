package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.common.DropDownListItem;
import com.example.entity.Location;
import com.example.repository.LocationRepository;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LocationController {

    @Autowired
    LocationRepository locationRepository;

    /**
     * Generate JSON for countries drop-down list.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String countriesJSON() throws JsonProcessingException {
        List<Location> locationList = locationRepository.getCountries();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> objList = new ArrayList<>();
        for (Location location : locationList) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setValue(location.getLocationName());
            dropDownListItem.setText(location.getLocationName());
            objList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(objList);
        return jsonDataString;
    }

    /**
     * Generate JSON for provinces drop-down list.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/provinces", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String provincesJSON() throws JsonProcessingException {
        List<Location> locationList = locationRepository.getProvinces();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> objList = new ArrayList<>();
        for (Location location : locationList) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setValue(location.getLocationName());
            dropDownListItem.setText(location.getLocationName());
            objList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(objList);
        return jsonDataString;
    }

    /**
     * Generate JSON for provinces drop-down list.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/districts", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String districtsJSON() throws JsonProcessingException {
        List<Location> locationList = locationRepository.getDistricts();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> objList = new ArrayList<>();
        for (Location location : locationList) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setValue(location.getLocationName());
            dropDownListItem.setText(location.getLocationName());
            objList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(objList);
        return jsonDataString;
    }

    /**
     * Generate JSON for wards drop-down list.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/wards", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String wardsJSON() throws JsonProcessingException {
        List<Location> locationList = locationRepository.getWards();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> objList = new ArrayList<>();
        for (Location location : locationList) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setValue(location.getLocationName());
            dropDownListItem.setText(location.getLocationName());
            objList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(objList);
        return jsonDataString;
    }

    /**
     * Get list of wards by district name.
     *
     * @param districtName
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/wards_by_district_name", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String wardsByDistrictJSON(@RequestParam(value = "district_name", required = true) String districtName) throws JsonProcessingException {
        List<Location> locationList = locationRepository.getWardsByDistrict(districtName);
        try {
            URLEncoder.encode(districtName, "UTF-8");
            System.out.println("districtName = " + districtName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> objList = new ArrayList<>();
        for (Location location : locationList) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setValue(location.getLocationName());
            dropDownListItem.setText(location.getLocationName());
            objList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(objList);
        return jsonDataString;
    }

}
