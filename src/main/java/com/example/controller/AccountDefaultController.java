package com.example.controller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.AccountDefaultForm;
import com.example.entity.Account;
import com.example.entity.AccountDefault;
import com.example.repository.AccountDefaultRepository;
import com.example.repository.AccountRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Tài khoản kế toán mặc định.
 */
@Controller
public class AccountDefaultController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountDefaultRepository accountDefaultRepository;

    /**
     * Show list of account default items.
     *
     * @return
     */
    @RequestMapping(value = "/account_default", method = RequestMethod.GET)
    public ModelAndView listAllAccountDefault() {
        ModelAndView modelAndView = new ModelAndView("account_default/list");
        // List<AccountDefault> accountDefaults = accountDefaultRepository.findAll(new Sort("refType")); // Old way.
        List<AccountDefault> accountDefaults = accountDefaultRepository.findAll(Sort.by("refType")); // New way.
        modelAndView.addObject("accountDefaults", accountDefaults);
        modelAndView.getModel().put("page_title", "Tài khoản ngầm định");
        return modelAndView;
    }

    /**
     * Delete an account_default item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/account_default/delete/{id}", method = RequestMethod.POST)
    public String deleteAccountDefault(@PathVariable("id") Integer id, HttpServletRequest request) {
        AccountDefault accountDefault = accountDefaultRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account_default id: " + id));
        accountDefaultRepository.delete(accountDefault);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Export Excel all account_default.
     *
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/Tai_khoan_ngam_dinh.xlsx", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportAllAccountDefaultsToExcel() throws IOException {
        // List<AccountDefault> accountDefaults = accountDefaultRepository.findAll(new Sort("refType")); // Old way, deprecated.
        List<AccountDefault> accountDefaults = accountDefaultRepository.findAll(Sort.by("refType")); // New way.
        ByteArrayInputStream byteArrayInputStream = accountDefaultsToExcel(accountDefaults);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Disposition", "attachment; filename=Tai_khoan_ngam_dinh.xlsx");
        return ResponseEntity.ok().headers(httpHeaders).body(new InputStreamResource(byteArrayInputStream));
    }

    /**
     * Support export to Excel.
     *
     * @param accountDefaults
     * @return
     * @throws IOException
     * @see #exportAllAccountDefaultsToExcel()
     */
    public static ByteArrayInputStream accountDefaultsToExcel(List<AccountDefault> accountDefaults) throws IOException {
        String[] COLUMNs = {"Tên loại chứng từ", "Tên cột", "Lọc tài khoản", "Tài khoản mặc định"};
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            Font font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short)13);
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Danh mục tài khoản mặc định");

            CellStyle titleCellStyle = workbook.createCellStyle();
            Font titleFont = workbook.createFont();
            titleFont.setFontName("Arial");
            titleFont.setFontHeightInPoints((short) 15);
            titleFont.setColor(IndexedColors.BLUE.getIndex());
            titleFont.setBold(true);
            titleCellStyle.setFont(titleFont);
            // Row titleRow = sheet.createRow(1);
            //Cell titleCell = titleRow.createCell(1);
            Row titleRow = sheet.createRow(1);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("DANH MỤC TÀI KHOẢN NGẦM ĐỊNH");
            titleCell.setCellStyle(titleCellStyle);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 3));
            CellUtil.setAlignment(titleCell, HorizontalAlignment.CENTER);

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            CellStyle normallCellStyle = workbook.createCellStyle();
            normallCellStyle.setFont(font);

            // Row for Header
            Row headerRow = sheet.createRow(3);
            // Header
            for (int col = 1; col <COLUMNs.length; col++) {
                int realCol = col + 1;
                Cell cell = headerRow.createCell(realCol);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }
            // CellStyle for Age
            CellStyle ageCellStyle = workbook.createCellStyle();
            ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
            int rowIdx = 4;
            for (AccountDefault accountDefault : accountDefaults) {
                Row row = sheet.createRow(rowIdx++);
                //Cell refTypeNameCell = row.createCell(0);
                //refTypeNameCell.setCellValue(accountDefault.getRefTypeName());
                //refTypeNameCell.setCellStyle(normallCellStyle);
                row.createCell(1).setCellValue(accountDefault.getRefTypeName());
                row.createCell(2).setCellValue(accountDefault.getColumnCaption());
                row.createCell(3).setCellValue(accountDefault.getFilterCondition());
                row.createCell(4).setCellValue(accountDefault.getDefaultValue());
                //Cell ageCell = row.createCell(3);
                //ageCell.setCellValue(customer.getCompanyTaxCode());
                //ageCell.setCellStyle(ageCellStyle);
            }
            // Auto setActiveStatusList corresponding width of columns.
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            workbook.write(byteArrayOutputStream);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        }
    }

    /**
     * Show page for editing Account.
     *
     * @return
     */
    @RequestMapping(value = "/account_default/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditAccount(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("account_default/edit");
        Optional<AccountDefault> accountDefaultOptional = accountDefaultRepository.findById(id);
        AccountDefault accountDefault = accountDefaultOptional.get();
        AccountDefaultForm accountDefaultForm = new AccountDefaultForm();
        accountDefault.setId(accountDefault.getId());
        accountDefaultForm.setRefType(accountDefault.getRefType());
        accountDefaultForm.setRefTypeName(accountDefault.getRefTypeName());
        accountDefaultForm.setVoucherType(accountDefault.getVoucherType());
        accountDefaultForm.setColumnName(accountDefault.getColumnName());
        accountDefaultForm.setColumnCaption(accountDefault.getColumnCaption());
        accountDefaultForm.setFilterCondition(accountDefault.getFilterCondition());
        accountDefaultForm.setDefaultValue(accountDefault.getDefaultValue());

        List<Account> accounts = accountRepository.findAllActiveAccount();
        accountDefaultForm.setAccountList(accounts);

        modelAndView.addObject("accountDefaultForm", accountDefaultForm);
        return modelAndView;
    }

    /**
     * Submit data for updating Account_default.
     *
     * @return
     */
    @RequestMapping(value = "/update_account_default", method = RequestMethod.GET)
    public String submitDataForUpdateAccountDefault(@ModelAttribute("accountDefaultForm") AccountDefaultForm accountDefaultForm, BindingResult bindingResult, Authentication authentication){
        if (bindingResult.hasErrors()) {
            return "account_default/edit";
        }
        //TODO:

        return "redirect:/account_defaults";
    }


}
