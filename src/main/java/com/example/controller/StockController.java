package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.Stock;
import com.example.repository.StockRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Kho hàng.
 */
@Controller
public class StockController {

    @Autowired
    StockRepository stockRepository;

    /**
     * List of stocks.
     *
     * @return
     */
    @RequestMapping(value = "/stocks", method = RequestMethod.GET)
    public ModelAndView showUnits() {
        ModelAndView modelAndView = new ModelAndView("stock/list");
        List<Stock> stocks = stockRepository.findAll();
        modelAndView.addObject("stocks", stocks);
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.getModel().put("page_title", "Kho");
        return modelAndView;
    }

    /**
     * Delete a stock item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/stock/delete/{id}", method = RequestMethod.POST)
    public String deleteStock(@PathVariable("id") Integer id, HttpServletRequest request) {
        Stock stock = stockRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        //TODO: Kiểm tra các FK liên quan.
        stockRepository.delete(stock);
        return "redirect:" + request.getHeader("Referer");
    }


    /**
     * JSON has multi-column of stock. Chỉ trả về các kho đang theo dõi.
     * Dùng cho drop-down list ở màn hình Thêm mới Hàng hóa, vật tư.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/stock_mul_col_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String stockMultiColumnJSON() throws JsonProcessingException {
        List<Stock> stockList = stockRepository.findAllActiveStocks();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(stockList);
        return jsonDataString;
    }

}
