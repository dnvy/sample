package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.CaPaymentForm;
import com.example.entity.CaPayment;
import com.example.entity.CaPaymentDetail;
import com.example.repository.CaPaymentDetailRepository;
import com.example.repository.CaPaymentRepository;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Phiếu chi tiền mặt.
 */
@Controller
public class CaPaymentController {

    @Autowired
    CaPaymentRepository caPaymentRepository;

    @Autowired
    CaPaymentDetailRepository caPaymentDetailRepository;

    /**
     * Phục vụ cho việc generate số chứng từ Phiếu chi tiền mặt.
     */
    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    /**
     * Phiếu thu có mã loại = 1020. Xem table SYSAddNewDefaultValue .
     */
    private static final Integer REF_TYPE_CA_PAYMENT = 1020;

    /**
     * SYSAutoID.RefTypeCategory
     */
    private static final Integer REF_TYPE_CATEGORY_CASH_PAYMENT_VOUCHER = 102;

    /**
     * Show page for add new cash payment. Phiếu chi.
     *
     * @return
     */
    @RequestMapping(value = "/add_cash_payment", method = RequestMethod.GET)
    public ModelAndView showPageForAddCastReceipt() {
        ModelAndView modelAndView = new ModelAndView("ca_payment/add");
        List<CaPaymentDetail> caPaymentDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CaPaymentDetail caPaymentDetail = new CaPaymentDetail();
            caPaymentDetailList.add(caPaymentDetail);
        }
        CaPaymentForm caPaymentForm = new CaPaymentForm();
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        caPaymentForm.setRefNoFinance(sysAutoIdController.generateVoucherNumber(REF_TYPE_CATEGORY_CASH_PAYMENT_VOUCHER, sysAutoIdRepository));
        caPaymentForm.setCaPaymentDetailList(caPaymentDetailList);
        modelAndView.addObject("caPaymentForm", caPaymentForm);
        modelAndView.getModel().put("page_title", "Thêm phiếu chi");
        return modelAndView;
    }

    /**
     * Submit data màn hình Thêm phiếu thu.
     *
     * @param caPaymentForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_cash_payment", method = RequestMethod.POST)
    public String submitDataAddCashPaymnet(@ModelAttribute("caPaymentForm") @Valid CaPaymentForm caPaymentForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "ca_payment/add";
        }
        CaPayment caPayment = new CaPayment();
        caPayment.setAccountObjectId(caPaymentForm.getAccountObjectId());
        caPayment.setAccountObjectName(caPaymentForm.getAccountObjectName());
        caPayment.setAccountObjectContactName(caPaymentForm.getAccountObjectContactName());
        caPayment.setAccountObjectAddress(caPaymentForm.getAccountObjectAddress());
        caPayment.setReasonTypeId(caPaymentForm.getReasonTypeId());
        caPayment.setJournalMemo(caPaymentForm.getJournalMemo());
        caPayment.setCurrencyId(caPaymentForm.getCurrencyId());
        // Phải lấy tỷ giá hối đoái từ màn hình nhập, vì tỷ giá biến động, và có thể khác trong dữ liệu đã có.
        caPayment.setExchangeRate(caPaymentForm.getExchangeRate());
        // Cần lấy trong session, xem người dùng đang làm việc với sổ nào.
        caPayment.setIsPostedCashBookFinance(Boolean.TRUE); // Kiểm tra lại chỗ này.
        caPayment.setIsPostedCashBookManagement(Boolean.TRUE); // Kiểm tra lại chỗ này.
        caPayment.setRefNoFinance(caPaymentForm.getRefNoFinance());
        caPayment.setRefNoManagement(caPaymentForm.getRefNoFinance()); // Kiểm tra lại chỗ này.
        caPayment.setRefDate(caPaymentForm.getRefDate()); // Ngày chứng từ.
        caPayment.setPostedDate(caPaymentForm.getPostedDate()); // Ngày hạch toán.
        // select * from SYSRefType WHERE RefType = 1020;
        caPayment.setRefType(REF_TYPE_CA_PAYMENT); // Loại chứng từ. 1020
        caPaymentRepository.save(caPayment);
        Integer caPaymentId = caPayment.getId();
        for (int i = 0; i < 10; i++) {
            if (!caPaymentForm.getCaPaymentDetailList().get(i).getDescription().isBlank() && !caPaymentForm.getCaPaymentDetailList().get(i).getDescription().isEmpty() && caPaymentForm.getCaPaymentDetailList().get(i).getDescription() != null) {
                CaPaymentDetail caPaymentDetail = new CaPaymentDetail();
                CaPaymentDetail subForm = caPaymentForm.getCaPaymentDetailList().get(i);
                caPaymentDetail.setRefId(caPaymentId);
                caPaymentDetail.setDescription(subForm.getDescription());
                caPaymentDetail.setDebitAccount(subForm.getDebitAccount());
                caPaymentDetail.setCreditAccount(subForm.getCreditAccount());
                caPaymentDetail.setAmountOc(subForm.getAmountOc());
                caPaymentDetail.setAmount(subForm.getAmount());
                caPaymentDetail.setBudgetItemId(subForm.getBudgetItemId());
                caPaymentDetail.setExpenseItemId(subForm.getExpenseItemId());
                caPaymentDetail.setProjectWorkId(subForm.getProjectWorkId());
                caPaymentDetail.setJobId(subForm.getJobId());
                caPaymentDetail.setOrderId(subForm.getOrderId());
                caPaymentDetail.setContractId(subForm.getContractId());
                caPaymentDetail.setAccountObjectId(subForm.getAccountObjectId());
                caPaymentDetail.setOrganizationUnitId(subForm.getOrganizationUnitId());
                caPaymentDetail.setListItemId(subForm.getListItemId());
                caPaymentDetail.setDebtAgreementId(subForm.getDebtAgreementId());
                caPaymentDetail.setUnResonableCost(subForm.getUnResonableCost());
                caPaymentDetail.setBusinessType(subForm.getBusinessType());
                caPaymentDetail.setPuOrderRefId(subForm.getPuOrderRefId());
                caPaymentDetail.setCustomField1(subForm.getCustomField1());
                caPaymentDetail.setCustomField2(subForm.getCustomField2());
                caPaymentDetail.setCustomField3(subForm.getCustomField3());
                caPaymentDetail.setCustomField4(subForm.getCustomField4());
                caPaymentDetail.setCustomField5(subForm.getCustomField5());
                caPaymentDetail.setCustomField6(subForm.getCustomField6());
                caPaymentDetail.setCustomField7(subForm.getCustomField7());
                caPaymentDetail.setCustomField8(subForm.getCustomField8());
                caPaymentDetail.setCustomField9(subForm.getCustomField9());
                caPaymentDetail.setCustomField10(subForm.getCustomField10());
                caPaymentDetailRepository.save(caPaymentDetail);
            }
        }
        return "redirect:/desktop";
    }

    /**
     * Hiển thị trang nộp tiền bảo hiểm.
     *
     * @return
     */
    @RequestMapping(value = "/insurance_payment", method = RequestMethod.GET)
    public ModelAndView showPageForInsurancePayment() {
        ModelAndView modelAndView = new ModelAndView("ca_payment/insurance_payment");
        List<CaPaymentDetail> caPaymentDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CaPaymentDetail caPaymentDetail = new CaPaymentDetail();
            caPaymentDetailList.add(caPaymentDetail);
        }
        CaPaymentForm caPaymentForm = new CaPaymentForm();
        caPaymentForm.setCaPaymentDetailList(caPaymentDetailList);
        modelAndView.addObject("caPaymentDetailList", caPaymentDetailList);
        return modelAndView;
    }

}
