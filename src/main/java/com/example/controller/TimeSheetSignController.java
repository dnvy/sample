package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.TimeSheetSign;
import com.example.repository.TimeSheetSignRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class TimeSheetSignController {

    @Autowired
    TimeSheetSignRepository timeSheetSignRepository;

    /**
     * Show list of timesheet signs.
     *
     * @return
     */
    @RequestMapping(value = "/timesheet_signs", method = RequestMethod.GET)
    public ModelAndView timesheetSignList() {
        ModelAndView modelAndView = new ModelAndView("timesheet_sign/list");
        List<TimeSheetSign> timeSheetSignList = timeSheetSignRepository.findAll();
        modelAndView.addObject("timeSheetSignList", timeSheetSignList);
        UtilityList.setActiveStatusList(modelAndView);
        UtilityList.setTrueOrFalseList(modelAndView);
        modelAndView.getModel().put("page_title", "Ký hiệu chấm công");
        return modelAndView;
    }

    /**
     * Delete a timesheet sign item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/timesheet_sign/delete/{id}", method = RequestMethod.POST)
    public String deleteTimesheetSign(@PathVariable("id") Integer id, HttpServletRequest request) {
        TimeSheetSign timeSheetSign = timeSheetSignRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid timesheet_sign id: " + id));
        //TODO: Check related FK.
        timeSheetSignRepository.delete(timeSheetSign);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * Show page for adding new timesheet_sign.
     *
     * @return
     */
    @RequestMapping(value = "/add_timesheet_sign", method = RequestMethod.GET)
    public ModelAndView showPageAddTimesheetSign() {
        TimeSheetSign timeSheetSign = new TimeSheetSign();
        ModelAndView modelAndView = new ModelAndView("timesheet_sign/add");
        modelAndView.addObject("timesheetSign", timeSheetSign);
        return modelAndView;
    }

    /**
     * Submit data add timesheet sign.
     *
     * @param timeSheetSign
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/submit_add_timesheet_sign", method = RequestMethod.POST)
    public String submitDataAddTimesheetSign(@ModelAttribute("timesheetSign") @Valid TimeSheetSign timeSheetSign, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "timesheet_sign/add";
        }
        timeSheetSign.setActiveStatus(Boolean.TRUE);
        timeSheetSign.setIsSystem(Boolean.FALSE);
        timeSheetSignRepository.save(timeSheetSign);
        return "redirect:/timesheet_signs";
    }


    /**
     * View detail a timesheet_sign.
     *
     * @param id
     * @return
     */
    //FIXME: đang lỗi vẫn commit.
    @RequestMapping(value = "/timesheet_sign/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewTimesheetSign(@PathVariable("id") Integer id) {
        Optional<TimeSheetSign> timeSheetSignOptional = timeSheetSignRepository.findById(id);
        TimeSheetSign timeSheetSign = timeSheetSignOptional.get();
        ModelAndView modelAndView = new ModelAndView("timesheet_sign/view");
        modelAndView.addObject("timeSheetSign", timeSheetSign);
        return modelAndView;
    }

}
