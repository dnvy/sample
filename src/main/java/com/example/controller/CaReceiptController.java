package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.NumberReading;
import com.example.common.UtilityList;
import com.example.dto.AddCashReceiptForm;
import com.example.dto.CaReceiptDTO;
import com.example.dto.CaReceiptForm;
import com.example.entity.AccountObject;
import com.example.entity.CAReceipt;
import com.example.entity.CaReceiptDetail;
import com.example.entity.GlVoucher;
import com.example.generateFile.GenerateDocuments;
import com.example.repository.AccountObjectRepository;
import com.example.repository.CaReceiptDetailRepository;
import com.example.repository.CaReceiptRepository;
import com.example.repository.GlVoucherRepository;
import com.example.repository.OrganizationUnitRepository;
import com.example.repository.SysAutoIdRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Đây là chức năng thu tiền (nói chung). Ví dụ rút tiền từ ngân hàng về quỹ tiền mặt,
 * thu hoàn ứng, Thu hoàn thuế GTGT, Thu khác. Thu tiền từ khách hàng sử dụng Controller khác
 * (đó là /collect_money )
 */
@Controller
public class CaReceiptController {

    @Autowired
    CaReceiptRepository caReceiptRepository;

    @Autowired
    CaReceiptDetailRepository caReceiptDetailRepository;

    @Autowired
    AccountObjectRepository accountObjectRepository;

    @Autowired
    GlVoucherRepository glVoucherRepository;

    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    // Phiếu thu có mã loại = 1010. Xem table SYSAddNewDefaultValue .
    private static final Integer REF_TYPE_CASH_RECEIPT = 1010;

    // select * from SYSRefTypeCategory WHERE RefTypeCategory = 101;
    // Phiếu thu.
    private static final Integer REF_TYPE_CATEGORY = 101;

    /**
     * Show page for add new cash receipt. Phiếu thu.
     *
     * @return
     */
    @RequestMapping(value = "/add_cash_receipt", method = RequestMethod.GET)
    public ModelAndView showPageForAddCastReceipt() {
        ModelAndView modelAndView = new ModelAndView("ca_receipt/add");
        List<CaReceiptDetail> caReceiptDetailList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CaReceiptDetail caReceiptDetail = new CaReceiptDetail();
            caReceiptDetailList.add(caReceiptDetail);
        }
        AddCashReceiptForm addCashReceiptForm = new AddCashReceiptForm();
        addCashReceiptForm.setExchangeRate(new BigDecimal(1));
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        addCashReceiptForm.setVoucherNumber(sysAutoIdController.generateVoucherNumber(REF_TYPE_CATEGORY, sysAutoIdRepository));
        addCashReceiptForm.setCaReceiptDetailList(caReceiptDetailList);
        modelAndView.addObject("addCashReceiptForm", addCashReceiptForm);
        return modelAndView;
    }

    /**
     * List of all cash receipts.
     *
     * @return
     */
    @RequestMapping(value = "/cash_receipts", method = RequestMethod.GET)
    public ModelAndView showListCashReceipts() {
        ModelAndView modelAndView = new ModelAndView("list/banks");
        UtilityList.setActiveStatusList(modelAndView);
        List<CAReceipt> caReceiptList = caReceiptRepository.findAll();
        modelAndView.addObject("caReceiptList", caReceiptList);
        modelAndView.getModel().put("page_title", "Ngân hàng");
        return modelAndView;
    }

    /**
     * Submit form Add cash receipt.
     *
     * @return
     */
    @RequestMapping(value = "/add_cash_receipt", method = RequestMethod.POST)
    public String addCashReceipt(@ModelAttribute("addCashReceiptForm") AddCashReceiptForm addCashReceiptForm, Authentication authentication) {
        // Luồng: Có chứng từ --> Lập phiếu thu --> Sổ hạch toán.
        CAReceipt caReceipt = new CAReceipt();
        // Tất cả các giao dịch từ màn hình Thêm Phiếu thu sẽ có RefType là 1010.
        caReceipt.setRefType(REF_TYPE_CASH_RECEIPT);
        Date now = new Date();
        caReceipt.setRefDate(now); // not null. Ngày phiếu thu.
        caReceipt.setPostedDate(addCashReceiptForm.getPostedDate()); // not null. Ngày hạch toán.
        caReceipt.setRefNoFinance(addCashReceiptForm.getVoucherNumber()); // Số chứng từ.
        caReceipt.setRefNoManagement(addCashReceiptForm.getRefNoManagement()); // Số chứng từ.
        Integer accountObjectId = accountObjectRepository.findAccountObjectByAccountObjectCode(addCashReceiptForm.getAccountObjectCode()).get(0).getId();
        caReceipt.setAccountObjectId(accountObjectId);
        caReceipt.setAccountObjectName(addCashReceiptForm.getAccountObjectName());
        caReceipt.setAccountObjectContactName(addCashReceiptForm.getAccountObjectContactName());
        caReceipt.setReasonTypeId(addCashReceiptForm.getReasonTypeId()); // Lý do nộp.
        caReceipt.setJournalMemo(addCashReceiptForm.getJournalMemo()); // Diễn giải lý do nộp.
        caReceipt.setDocumentIncluded(addCashReceiptForm.getDocumentIncluded()); // Mô tả chứng từ kèm theo.
        caReceipt.setCurrencyId(addCashReceiptForm.getCurrencyId()); // Tiền tệ
        caReceipt.setExchangeRate(addCashReceiptForm.getExchangeRate()); // Tỷ giá tiền tệ.
        caReceipt.setTotalAmountOc(addCashReceiptForm.getTotalAmountOc()); // not null. số tiền.
        caReceipt.setTotalAmountOc(addCashReceiptForm.getTotalAmountOc()); // not null. Số tiền quy đổi. //TODO: Get exchange rate.
        // Chọn chi nhánh làm việc.
        Integer branchId = organizationUnitRepository.findOneAtTopLevel();
        caReceipt.setBranchId(branchId);
        caReceipt.setEmployeeId(addCashReceiptForm.getEmployeeId()); // Nhân viên thu. = ID của object ID có IsEmployee = 1.
        caReceipt.setDisplayOnBook(addCashReceiptForm.getDisplayOnBook());
        // Trường IsPostedFinance không được NULL.
        // displayOnBook = 0, Sổ Tài chính
        // displayOnBook = 1, Sổ Quản trị
        // displayOnBook = 2, Sổ Tài chính + Sổ Quản trị
        if (addCashReceiptForm.getDisplayOnBook() == 0) {
            caReceipt.setIsPostedFinance(Boolean.TRUE);
            caReceipt.setIsPostedManagement(Boolean.FALSE);
        }
        if (addCashReceiptForm.getDisplayOnBook() == 1) {
            caReceipt.setIsPostedFinance(Boolean.FALSE);
            caReceipt.setIsPostedManagement(Boolean.TRUE);
        }
        if (addCashReceiptForm.getDisplayOnBook() == 2) {
            caReceipt.setIsPostedFinance(Boolean.TRUE);
            caReceipt.setIsPostedManagement(Boolean.TRUE);
        }
        caReceipt.setCashBookPostedDate(addCashReceiptForm.getCashBookPostedDate()); // Ngày chứng từ
        caReceipt.setPostedDate(addCashReceiptForm.getPostedDate()); // Ngày hạch toán
        caReceipt.setEditVersion(1); // First version.
        caReceipt.setRefOrder(1); // Not null. Chưa rõ tác dụng.
        java.sql.Timestamp timestamp_now = new java.sql.Timestamp(new java.util.Date().getTime());
        caReceipt.setCreatedDate(timestamp_now); // Thời gian tạo.
        caReceipt.setCreatedBy(authentication.getName()); // Người tạo
        // Cannot insert the value NULL into column 'TotalAmount', table 'accounting.dbo.CAReceipt'; column does not allow nulls. INSERT fails.
        caReceipt.setTotalAmount(new BigDecimal(1000)); // not null
        caReceipt.setTotalAmountOc(new BigDecimal(2000)); // not null.
        // Lưu vào bảng chứng từ
        caReceiptRepository.save(caReceipt);
        Integer caReceiptId = caReceipt.getId(); // Lấy ID của bản ghi vừa được insert thành công.
        System.out.println("Id = " + caReceiptId);
        // Insert into table CAReceiptDetail .
        // Đừng cải thiện performance, gây ra bug NullPointerException.
        for (int i = 0; i < 10; i++) {
            String description = addCashReceiptForm.getCaReceiptDetailList().get(i).getDescription();
            CaReceiptDetail caReceiptDetail = new CaReceiptDetail();
            if (!addCashReceiptForm.getCaReceiptDetailList().get(i).getDescription().isBlank() && !addCashReceiptForm.getCaReceiptDetailList().get(i).getDescription().isEmpty()) {
                CaReceiptDetail submitted = addCashReceiptForm.getCaReceiptDetailList().get(i);
                caReceiptDetail.setRefId(caReceiptId); // not null.
                caReceiptDetail.setDescription(submitted.getDescription());
                caReceiptDetail.setDebitAccount(submitted.getDebitAccount());
                caReceiptDetail.setCreditAccount(submitted.getCreditAccount());
                caReceiptDetail.setBankAccountId(submitted.getBankAccountId()); // Tài khoản ngân hàng.
                caReceiptDetail.setAccountObjectId(submitted.getAccountObjectId()); // Đối tượng
                caReceiptDetail.setBudgetItemId(submitted.getBudgetItemId()); // Mục chi của kế hoạch chi tiêu.
                caReceiptDetail.setOrderId(submitted.getOrderId()); // Đơn hàng.
                caReceiptDetail.setProjectWorkId(submitted.getProjectWorkId()); // Công trình, hạng mục, vụ việc.
                caReceiptDetail.setContractId(submitted.getContractId()); // Hợp đồng bán hàng.
                caReceiptDetail.setPuContractId(submitted.getPuContractId()); // Hợp đồng mua hàng.
                caReceiptDetail.setExpenseItemId(submitted.getExpenseItemId()); // Khoản mục chi phí.
                caReceiptDetail.setAmountOc(submitted.getAmountOc()); // not null.
                caReceiptDetail.setAmount(new BigDecimal(1000)); // not null.
                caReceiptDetail.setAmountOc(new BigDecimal(1000)); // not null.
                caReceiptDetail.setUnResonableCost(Boolean.FALSE); // not null.
                caReceiptDetail.setCashOutAmountFinance(new BigDecimal(1000)); // not null.
                caReceiptDetail.setCashOutDiffAmountFinance(new BigDecimal(1000)); // not null
                caReceiptDetail.setCashOutDiffAmountManagement(new BigDecimal(200)); // not null
                caReceiptDetail.setCashOutAmountManagement(new BigDecimal(1000)); // not null.
                caReceiptDetail.setCashOutExchangeRateFinance(new BigDecimal(1000)); // not null.
                caReceiptDetail.setCashOutExchangeRateManagement(new BigDecimal(1000)); // not null.
                //caReceiptDetail.setAmount(submitted.getAmount()); // not null.
                //caReceiptDetail.setUnResonableCost(submitted.getUnResonableCost()); // not null.
                //caReceiptDetail.setCashOutAmountFinance(submitted.getCashOutAmountFinance()); // not null.
                // caReceiptDetail.setCashOutDiffAmountFinance(new BigDecimal(1000)); // not null
                // caReceiptDetail.setCashOutDiffAmountManagement(new BigDecimal(200)); // not null
                //caReceiptDetail.setCashOutAmountManagement(submitted.getCashOutAmountManagement()); // not null.
                //caReceiptDetail.setCashOutExchangeRateFinance(submitted.getCashOutExchangeRateFinance()); // not null.
                //caReceiptDetail.setCashOutExchangeRateManagement(submitted.getCashOutExchangeRateManagement()); // not null.
                //caReceiptDetail.setAmountOc(submitted.getAmountOc());
                caReceiptDetailRepository.save(caReceiptDetail);
            }
        }
        // Insert into table GLVoucher .
        GlVoucher glVoucher = new GlVoucher();
        glVoucher.setDisplayOnBook(addCashReceiptForm.getDisplayOnBook()); // not null.
        glVoucher.setRefType(REF_TYPE_CASH_RECEIPT); // not null.
        glVoucher.setPostedDate(addCashReceiptForm.getPostedDate()); // not null.
        glVoucher.setRefNoFinance(addCashReceiptForm.getRefNoFinance());
        glVoucher.setRefNoManagement(addCashReceiptForm.getRefNoManagement());
        if (addCashReceiptForm.getDisplayOnBook() == 0) {
            glVoucher.setIsPostedFinance(Boolean.TRUE);
            glVoucher.setRefNoFinance(addCashReceiptForm.getVoucherNumber());
            glVoucher.setIsPostedManagement(Boolean.FALSE);
        }
        if (addCashReceiptForm.getDisplayOnBook() == 1) {
            glVoucher.setIsPostedFinance(Boolean.FALSE);
            glVoucher.setIsPostedManagement(Boolean.TRUE);
            glVoucher.setRefNoManagement(addCashReceiptForm.getVoucherNumber());
        }
        if (addCashReceiptForm.getDisplayOnBook() == 2) {
            glVoucher.setIsPostedFinance(Boolean.TRUE);
            glVoucher.setRefNoFinance(addCashReceiptForm.getVoucherNumber());
            glVoucher.setIsPostedManagement(Boolean.TRUE);
            glVoucher.setRefNoManagement(addCashReceiptForm.getVoucherNumber());
        }
        glVoucher.setJournalMemo(addCashReceiptForm.getJournalMemo());
        glVoucher.setTotalAmount(new BigDecimal(1000));
        glVoucher.setTotalAmountOc(new BigDecimal(1000));
        glVoucher.setDeptStatus(1);
        glVoucher.setEditVersion(1);
        glVoucher.setBranchId(1);
        glVoucher.setCurrencyId("VND"); // not null.
        glVoucher.setIsOnceSettlementAdvance(Boolean.TRUE);
        glVoucher.setAdvancedAmount(new BigDecimal(100));
        glVoucher.setDiffAmountOc(new BigDecimal(100));
        glVoucher.setAdvancedAmount(new BigDecimal(10)); // not null
        glVoucher.setAdvancedAmountOc(new BigDecimal(343)); //not null
        glVoucher.setDiffAmount(new BigDecimal(3)); // not null
        glVoucher.setIsExecuted(Boolean.TRUE); // not null
        glVoucher.setRefDate(new Date()); // not null
        glVoucherRepository.save(glVoucher);
        // Chuyển đến trang Danh sách thu chi, sổ chứng từ, trang cũ, hay Sổ quỹ tiền mặt?
        // Chuyển đến Sổ chứng từ, thì có thể in chứng từ từ trang này được.
        // Thêm argument HttpServletRequest request
        // return "redirect:" + request.getHeader("Referer");
        return "redirect:/list_ca_receipt_payments";
    }

    /**
     * Hiển thị danh sách phiếu thu, phiếu chi.
     *
     * @return
     */
    @RequestMapping(value = "/list_ca_receipt_payments", method = RequestMethod.GET)
    public ModelAndView showListOfCashReceipt() {
        List<CAReceipt> caReceiptList = caReceiptRepository.findAll();
        List<CaReceiptDTO> caReceiptDTOList = new ArrayList<>();
        for (CAReceipt caReceipt : caReceiptList) {
            CaReceiptDTO caReceiptDTO = new CaReceiptDTO();
            caReceiptDTO.setId(caReceipt.getId());
            caReceiptDTO.setRefType(caReceipt.getRefType());
            caReceiptDTO.setRefDate(caReceipt.getRefDate());
            caReceiptDTO.setPostedDate(caReceipt.getPostedDate());
            caReceiptDTO.setRefNoFinance(caReceipt.getRefNoFinance());
            caReceiptDTO.setRefNoManagement(caReceipt.getRefNoManagement());
            caReceiptDTO.setAccountObjectId(caReceipt.getAccountObjectId());
            caReceiptDTO.setAccountObjectName(caReceipt.getAccountObjectName());
            caReceiptDTO.setAccountObjectAddress(caReceipt.getAccountObjectAddress());
            caReceiptDTO.setAccountObjectContactName(caReceipt.getAccountObjectName());
            caReceiptDTO.setReasonTypeId(caReceipt.getReasonTypeId());
            caReceiptDTO.setJournalMemo(caReceipt.getJournalMemo());
            caReceiptDTO.setDocumentIncluded(caReceipt.getDocumentIncluded());
            caReceiptDTO.setCurrencyId(caReceipt.getCurrencyId());
            caReceiptDTO.setExchangeRate(caReceipt.getExchangeRate());
            caReceiptDTO.setTotalAmountOc(caReceipt.getTotalAmountOc());
            caReceiptDTO.setTotalAmount(caReceipt.getTotalAmount());
            caReceiptDTO.setBranchId(caReceipt.getBranchId());
            caReceiptDTO.setEmployeeId(caReceipt.getEmployeeId());
            caReceiptDTO.setDisplayOnBook(caReceipt.getDisplayOnBook());
            caReceiptDTO.setIsPostedCashBookFinance(caReceipt.getIsPostedCashBookFinance());
            caReceiptDTO.setIsPostedCashBookManagement(caReceipt.getIsPostedCashBookManagement());
            caReceiptDTO.setCashBookPostedDate(caReceipt.getCashBookPostedDate());
            caReceiptDTO.setEditVersion(caReceipt.getEditVersion());
            caReceiptDTO.setRefOrder(caReceipt.getRefOrder());
            caReceiptDTO.setGlVoucherRefId(caReceipt.getGlVoucherRefId());
            caReceiptDTOList.add(caReceiptDTO);
        }
        ModelAndView modelAndView = new ModelAndView("ca_receipt/list");
        modelAndView.addObject("caReceiptDTOList", caReceiptDTOList);
        modelAndView.getModel().put("page_title", "Danh sách thu, chi");
        return modelAndView;
    }

    /**
     * Tạo file docx phiếu thu để in.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/generate/ca_payment/{id}", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> generateFile(@PathVariable("id") Integer id) throws FileNotFoundException {
        Optional<CAReceipt> caReceiptOptional = caReceiptRepository.findById(id);
        CAReceipt caReceipt = caReceiptOptional.get();

        // Try with test/test.txt
        // File template = new File("D:\\TestTemplate.docx");
        File template = new File("E:\\source_code\\github.com\\acc133\\src\\main\\resources\\report\\phieu_thu.docx");
        //String var = "don_vi";
        String outputDocument = "E:\\source_code\\github.com\\acc133\\src\\main\\resources\\report\\phieu_thu_result.docx";
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("don_vi", caReceipt.getAccountObjectName());
        parameters.put("dia_chi", caReceipt.getAccountObjectAddress());
        parameters.put("ngay", String.valueOf(caReceipt.getCashBookPostedDate().getDay()));
        parameters.put("thang", String.valueOf(caReceipt.getCashBookPostedDate().getDate()));
        parameters.put("nam", String.valueOf(caReceipt.getCashBookPostedDate().getYear()));
        parameters.put("quyen_so", "1");
        parameters.put("so_phieu_thu", "1");
        parameters.put("tai_khoan_no", "1");
        parameters.put("tai_khoan_co", "1");
        parameters.put("ho_va_ten_nguoi_nop_tien", "1");
        parameters.put("dia_chi_nguoi_nop_tien", "1");
        parameters.put("ly_do_nop", "1");
        parameters.put("so_tien", String.valueOf(caReceipt.getTotalAmount()));
        parameters.put("viet_bang_chu", NumberReading.readMoneyNumber(125425000));
        parameters.put("so_luong_chung_tu_goc", "1");
        parameters.put("chung_tu_goc", "1");
        parameters.put("giam_doc", "1");
        parameters.put("ke_toan_truong", "1");
        parameters.put("nguoi_nop_tien", "1");
        parameters.put("nguoi_lap_phieu", "1");
        parameters.put("thu_quy", "1");
        parameters.put("ty_gia_ngoai_te", "1");
        parameters.put("so_tien_quy_doi", "1");
        GenerateDocuments.generateDocument(template, parameters, outputDocument);
        String mineType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        MediaType mediaType = MediaType.parseMediaType(mineType);
        File file = new File(outputDocument);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName()).contentType(mediaType).contentLength(file.length()).body(resource);
    }

    /**
     * Xem chi tiết phiếu thu.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/ca_receipt/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewDetail(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("ca_receipt/view");
        CaReceiptForm caReceiptForm = new CaReceiptForm();
        Optional<CAReceipt> caReceiptOptional = caReceiptRepository.findById(id);
        CAReceipt caReceipt = caReceiptOptional.get();
        caReceiptForm.setId(caReceipt.getId());
        caReceiptForm.setRefType(caReceipt.getRefType());
        caReceiptForm.setPostedDate(caReceipt.getPostedDate());
        caReceiptForm.setRefNoFinance(caReceipt.getRefNoFinance());
        caReceiptForm.setRefNoManagement(caReceipt.getRefNoManagement());
        caReceiptForm.setAccountObjectId(caReceipt.getAccountObjectId());
        caReceiptForm.setAccountObjectName(caReceipt.getAccountObjectName());
        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(caReceipt.getAccountObjectId());
        AccountObject accountObject = accountObjectOptional.get();
        caReceiptForm.setAccountObjectCode(accountObject.getAccountObjectCode());
        caReceiptForm.setAccountObjectAddress(caReceipt.getAccountObjectAddress());
        caReceiptForm.setReasonTypeId(caReceipt.getReasonTypeId());
        caReceiptForm.setJournalMemo(caReceipt.getJournalMemo());
        caReceiptForm.setDocumentIncluded(caReceipt.getDocumentIncluded());
        caReceiptForm.setCurrencyId(caReceipt.getCurrencyId());
        caReceiptForm.setExchangeRate(caReceipt.getExchangeRate());
        caReceiptForm.setTotalAmountOc(caReceipt.getTotalAmountOc());
        caReceiptForm.setTotalAmount(caReceipt.getTotalAmount());
        caReceiptForm.setBranchId(caReceipt.getBranchId());
        caReceiptForm.setEmployeeId(caReceipt.getEmployeeId());
        caReceiptForm.setDisplayOnBook(caReceipt.getDisplayOnBook());
        caReceiptForm.setIsPostedCashBookFinance(caReceipt.getIsPostedCashBookFinance());
        caReceiptForm.setIsPostedCashBookManagement(caReceipt.getIsPostedCashBookManagement());
        caReceiptForm.setCashBookPostedDate(caReceipt.getCashBookPostedDate());
        caReceiptForm.setEditVersion(caReceipt.getEditVersion());
        caReceiptForm.setRefOrder(caReceipt.getRefOrder());
        caReceiptForm.setGlVoucherRefId(caReceipt.getGlVoucherRefId());
        modelAndView.addObject("caReceiptForm", caReceiptForm);
        modelAndView.getModel().put("page_title", "Xem chi tiết phiếu thu");
        return modelAndView;
    }

}





















