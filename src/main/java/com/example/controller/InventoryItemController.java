package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.dropdownlist.InventoryItemDropDownItem;
import com.example.dropdownlist.InventoryItemForDetailRow;
import com.example.entity.InventoryItem;
import com.example.entity.InventoryItemCategory;
import com.example.form.InventoryItemForm;
import com.example.repository.InventoryItemCategoryRepository;
import com.example.repository.InventoryItemRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hàng hóa, vật tư, dịch vụ.
 */
@Controller
public class InventoryItemController {

    @Autowired
    InventoryItemRepository inventoryItemRepository;

    @Autowired
    InventoryItemCategoryRepository inventoryItemCategoryRepository;

    /**
     * Show page for adding inventory item.
     *
     * @return
     */
    @RequestMapping(value = "/add_inventory_item", method = RequestMethod.GET)
    public ModelAndView addInventoryItem() {
        ModelAndView modelAndView = new ModelAndView("inventory_item/add");
        List<InventoryItemCategory> inventoryItemCategoryList = inventoryItemCategoryRepository.getAllActiveInventoryItemCategories();
        InventoryItemForm inventoryItemForm = new InventoryItemForm();
        modelAndView.addObject("inventoryItemForm", inventoryItemForm);
        modelAndView.addObject("inventoryItemCategoryList", inventoryItemCategoryList);
        modelAndView.getModel().put("page_title", "Thêm Hàng hóa, Vật tư, Dịch vụ");
        return modelAndView;
    }

    /**
     * List of inventory items.
     *
     * @return
     */
    //FIXME: Chuyển  InventoryItem sang DTO vì còn query ở bảng khác nữa, lấy tồn kho.
    @RequestMapping(value = "/inventory_items", method = RequestMethod.GET)
    public ModelAndView inventoryItems() {
        // TODO: Lấy tồn kho ở bảng nào?
        ModelAndView modelAndView = new ModelAndView("inventory_item/list");
        List<InventoryItem> inventoryItemList = inventoryItemRepository.listInventoryItems();
        Map<Integer, String> inventoryItemTypeList = new HashMap<>();
        inventoryItemTypeList.put(0, "Vật tư hàng hóa");
        inventoryItemTypeList.put(1, "Thành phẩm");
        inventoryItemTypeList.put(2, "Dịch vụ");
        inventoryItemTypeList.put(3, "Diễn giải");
        modelAndView.addObject("inventoryItemTypeList", inventoryItemTypeList);
        modelAndView.addObject("inventoryItemList", inventoryItemList);
        modelAndView.getModel().put("page_title", "Danh mục vật tư, hàng hóa, dịch vụ");
        return modelAndView;
    }

    /**
     * Add an inventory item.
     *
     * @param inventoryItemForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_inventory_item", method = RequestMethod.POST)
    public String submitDataAddInventoryItem(@ModelAttribute("inventoryItemForm") InventoryItemForm inventoryItemForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "inventory_item/add";
        }
        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.setInventoryItemCode(inventoryItemForm.getInventoryItemCode());
        inventoryItem.setInventoryItemName(inventoryItemForm.getInventoryItemName());
        inventoryItem.setInventoryItemType(inventoryItemForm.getInventoryItemType());
        inventoryItem.setInventoryItemCategoryList(inventoryItemForm.getInventoryItemCategoryList());
        inventoryItem.setInventoryItemCategoryCode(inventoryItemForm.getInventoryItemCategoryCode());
        inventoryItem.setDescription(inventoryItemForm.getDescription());
        inventoryItem.setUnitId(inventoryItemForm.getUnitId());
        inventoryItem.setGuarantyPeriod(inventoryItemForm.getGuarantyPeriod());
        if (inventoryItemForm.getMinimumStock() == null) {
            inventoryItem.setMinimumStock(BigDecimal.ZERO);
        } else {
            inventoryItem.setMinimumStock(inventoryItemForm.getMinimumStock());
        }
        inventoryItem.setInventoryItemSource(inventoryItemForm.getInventoryItemSource());
        inventoryItem.setDefaultStockId(inventoryItemForm.getDefaultStockId());
        inventoryItem.setInventoryAccount(inventoryItemForm.getInventoryAccount());
        inventoryItem.setCogsAccount(inventoryItemForm.getCogsAccount());
        inventoryItem.setSaleAccount(inventoryItemForm.getSaleAccount());
        // Bởi vì field PurchaseDiscountRate là NOT NULL.
        if (inventoryItemForm.getPurchaseDiscountRate() == null) {
            inventoryItem.setPurchaseDiscountRate(BigDecimal.ZERO);
        } else {
            inventoryItem.setPurchaseDiscountRate(inventoryItemForm.getPurchaseDiscountRate());
        }
        if (inventoryItemForm.getUnitPrice() == null) {
            inventoryItem.setUnitPrice(BigDecimal.ZERO);
        } else {
            inventoryItem.setUnitPrice(inventoryItemForm.getUnitPrice());
        }
        if (inventoryItemForm.getSalePrice1() == null) {
            inventoryItem.setSalePrice1(BigDecimal.ZERO);
        } else {
            inventoryItem.setSalePrice1(inventoryItemForm.getSalePrice1());
        }
        if (inventoryItemForm.getSalePrice2() == null) {
            inventoryItem.setSalePrice2(BigDecimal.ZERO);
        } else {
            inventoryItem.setSalePrice2(inventoryItemForm.getSalePrice2());
        }
        if (inventoryItemForm.getSalePrice3() == null) {
            inventoryItem.setSalePrice3(BigDecimal.ZERO);
        } else {
            inventoryItem.setSalePrice3(inventoryItemForm.getSalePrice3());
        }
        if (inventoryItemForm.getFixedSalePrice() == null) {
            inventoryItem.setFixedSalePrice(BigDecimal.ZERO);
        } else {
            inventoryItem.setFixedSalePrice(inventoryItemForm.getFixedSalePrice());
        }
        inventoryItem.setIsUnitPriceAfterTax(Boolean.TRUE); // NOT NULL. Chưa rõ. Trên màn hình không thấy chỗ để nhập.
        inventoryItem.setTaxRate(inventoryItemForm.getTaxRate());
        inventoryItem.setImportTaxRate(inventoryItemForm.getImportTaxRate());
        inventoryItem.setInventoryCategorySpecialTaxId(inventoryItemForm.getInventoryCategorySpecialTaxId());
        inventoryItem.setIsSystem(Boolean.FALSE); // Thêm bởi người dùng.
        inventoryItem.setActiveStatus(Boolean.TRUE); // Khi thêm vào thì ActiveStatus = TRUE.
        inventoryItem.setIsSaleDiscount(inventoryItemForm.getIsSaleDiscount());
        inventoryItem.setDiscountType(inventoryItemForm.getDiscountType());
        inventoryItem.setIsFollowSerialNumber(inventoryItemForm.getIsFollowSerialNumber());
        inventoryItem.setCostMethod(inventoryItemForm.getCostMethod());
        inventoryItem.setInventoryItemCategoryName(inventoryItemForm.getInventoryItemCategoryName());
        inventoryItem.setSpecificity(inventoryItemForm.getSpecificity());
        inventoryItem.setPurchaseDescription(inventoryItemForm.getPurchaseDescription());
        inventoryItem.setSaleDescription(inventoryItemForm.getSaleDescription());
        inventoryItem.setTaCareerGroupId(inventoryItemForm.getTaCareerGroupId());
        inventoryItem.setImage(inventoryItemForm.getImage());
        if (inventoryItemForm.getFixedUnitPrice() == null) {
            inventoryItem.setFixedUnitPrice(BigDecimal.ZERO);
        } else {
            inventoryItem.setFixedUnitPrice(inventoryItemForm.getFixedUnitPrice());
        }
        inventoryItem.setFrontEndFormula(inventoryItemForm.getFrontEndFormula());
        inventoryItem.setBackEndFormula(inventoryItemForm.getBackEndFormula());
        inventoryItem.setBaseOnFormula(inventoryItemForm.getBaseOnFormula());
        inventoryItem.setDiscountAccount(inventoryItemForm.getDiscountAccount());
        inventoryItem.setSaleOffAccount(inventoryItemForm.getSaleOffAccount());
        inventoryItem.setReturnAccount(inventoryItemForm.getReturnAccount());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        inventoryItem.setCreatedDate(now);
        inventoryItem.setCreatedBy(authentication.getName());
        inventoryItemRepository.save(inventoryItem);
        return "redirect:/inventory_items";
    }

    /**
     * Delete an inventory_item.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/inventory_item/delete/{id}", method = RequestMethod.POST)
    public String deleteInventoryItem(@PathVariable("id") Integer id) {
        InventoryItem inventoryItem = inventoryItemRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid job id: " + id));
        inventoryItemRepository.delete(inventoryItem);
        return "redirect:/inventory_items";
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã hàng
     * 2. Tên hàng
     * 3. Số lượng tồn
     * 4. Giá bán 1
     * 5. Giá bán 2
     * 6. Giá bán 3
     * 7. Giá bán cố định
     * <p>
     * Dùng trong form Add Contract.
     *
     * @return
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     * @see ContractController#showPageAddContract()
     */
    @RequestMapping(value = "/all_active_inventory_item_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String allActiveContractsJSON() throws JsonProcessingException {
        // Chỉ lấy các hàng hóa, vật tư có ActiveStatus = 1.
        List<InventoryItem> inventoryItemList = inventoryItemRepository.findAllActiveInventoryItems();
        List<InventoryItemDropDownItem> inventoryItemDropDownItemList = new ArrayList<>();
        if (inventoryItemList.size() > 0) {
            for (InventoryItem inventoryItem : inventoryItemList) {
                InventoryItemDropDownItem inventoryItemDropDownItem = new InventoryItemDropDownItem();
                inventoryItemDropDownItem.setId(inventoryItem.getId());
                inventoryItemDropDownItem.setAvailableQuantity(1); //FIXME: Chưa rõ lấy ở table nào.
                inventoryItemDropDownItem.setSalePrice1(inventoryItem.getSalePrice1());
                inventoryItemDropDownItem.setSalePrice2(inventoryItem.getSalePrice2());
                inventoryItemDropDownItem.setSalePrice3(inventoryItem.getSalePrice3());
                inventoryItemDropDownItem.setFixedSalePrice(inventoryItem.getFixedSalePrice());
                inventoryItemDropDownItem.setInventoryItemCode(inventoryItem.getInventoryItemCode());
                inventoryItemDropDownItem.setInventoryItemName(inventoryItem.getInventoryItemName());
                inventoryItemDropDownItemList.add(inventoryItemDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(inventoryItemDropDownItemList);
        return jsonDataString;
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã hàng
     * 2. Tên hàng
     * 3. Số lượng tồn
     * 4. Giá bán 1
     * 5. Giá bán 2
     * 6. Giá bán 3
     * 7. Giá bán cố định
     * <p>
     * Dùng trong form Add Contract.
     *
     * @return
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     * @see ContractController#showPageAddContract()
     */
    @RequestMapping(value = "/active_inventory_items_for_detail_row_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getActiveInventoryItemsForDetailRowJSON() throws JsonProcessingException {
        // Chỉ lấy các hàng hóa, vật tư có ActiveStatus = 1.
        List<InventoryItem> inventoryItemList = inventoryItemRepository.findAllActiveInventoryItems();
        List<InventoryItemForDetailRow> inventoryItemDropDownItemList = new ArrayList<>();
        if (inventoryItemList.size() > 0) {
            for (InventoryItem inventoryItem : inventoryItemList) {
                InventoryItemForDetailRow inventoryItemForDetailRow = new InventoryItemForDetailRow();
                inventoryItemForDetailRow.setId(inventoryItem.getId());
                inventoryItemForDetailRow.setAvailableQuantity(1); //FIXME: Chưa rõ lấy ở table nào.
                inventoryItemForDetailRow.setSalePrice1(inventoryItem.getSalePrice1());
                inventoryItemForDetailRow.setSalePrice2(inventoryItem.getSalePrice2());
                inventoryItemForDetailRow.setSalePrice3(inventoryItem.getSalePrice3());
                inventoryItemForDetailRow.setFixedSalePrice(inventoryItem.getFixedSalePrice());
                inventoryItemForDetailRow.setInventoryItemCode(inventoryItem.getInventoryItemCode());
                inventoryItemForDetailRow.setInventoryItemName(inventoryItem.getInventoryItemName());
                inventoryItemForDetailRow.setUnitId(inventoryItem.getUnitId());
                inventoryItemDropDownItemList.add(inventoryItemForDetailRow);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(inventoryItemDropDownItemList);
        return jsonDataString;
    }
}
