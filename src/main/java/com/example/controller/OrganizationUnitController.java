package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.DropDownListItem;
import com.example.common.UtilityList;
import com.example.dropdownlist.OrganizationUnitDropDownItem;
import com.example.entity.OrganizationUnit;
import com.example.entity.OrganizationUnitInfo;
import com.example.form.OrganizationUnitForm;
import com.example.repository.OrganizationUnitInfoRepository;
import com.example.repository.OrganizationUnitRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Đơn vị, phòng ban.
 */
@Controller
public class OrganizationUnitController {

    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    @Autowired
    OrganizationUnitInfoRepository organizationUnitInfoRepository;

    /**
     * Show list organization units.
     *
     * @return
     */
    @RequestMapping(value = "/organization_units", method = RequestMethod.GET)
    public ModelAndView showAllOrganizationUnits() {
        ModelAndView modelAndView = new ModelAndView("organization_unit/list");
        List<OrganizationUnit> organizationUnitList = organizationUnitRepository.findAll();
        Map<Integer, String> orgUnitTypeList = new HashMap<>();
        orgUnitTypeList.put(1, "Tổng công ty");
        orgUnitTypeList.put(2, "Chi nhánh");
        orgUnitTypeList.put(3, "VP/TT");
        orgUnitTypeList.put(4, "Phòng ban");
        orgUnitTypeList.put(5, "Phân xưởng");
        orgUnitTypeList.put(6, "Nhóm/Tổ,hội");
        UtilityList.setActiveStatusList(modelAndView);
        modelAndView.addObject("orgUnitTypeList", orgUnitTypeList);
        modelAndView.addObject("organizationUnitList", organizationUnitList);
        modelAndView.getModel().put("page_title", "Danh sách Đơn vị, phòng ban");
        return modelAndView;
    }

    /**
     * Show page for adding new organization unit.
     *
     * @return
     */
    @RequestMapping(value = "/add_organization_unit", method = RequestMethod.GET)
    public ModelAndView addOrganizationUnit() {
        ModelAndView modelAndView = new ModelAndView("organization_unit/add");
        OrganizationUnit organizationUnit = new OrganizationUnit();
        modelAndView.addObject("organizationUnit", organizationUnit);
        modelAndView.getModel().put("page_title", "Thêm Đơn vị");
        return modelAndView;
    }

    /**
     * Submit data add organization unit.
     *
     * @return
     */
    @RequestMapping(value = "/add_organization_unit", method = RequestMethod.POST)
    public String submitAddOrganizationUnit(@ModelAttribute("organizationUnit") @Valid OrganizationUnit organizationUnit, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "organization_unit/add";
        }
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        organizationUnit.setCreatedDate(now);
        organizationUnit.setCreatedBy(authentication.getName());
        organizationUnit.setIsSystem(Boolean.FALSE);
        organizationUnit.setIsParent(Boolean.FALSE);
        organizationUnit.setIsPrivateVatDeclaration(Boolean.FALSE);
        organizationUnit.setActiveStatus(Boolean.TRUE);
        organizationUnit.setIsPrintSigner(Boolean.TRUE);
        organizationUnit.setIsGetReporterNameByUserLogIn(Boolean.FALSE);
        organizationUnitRepository.save(organizationUnit);
        return "redirect:/organization_units";
    }

    /**
     * Xem chi tiết, dành cho cấp Công ty.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/organization_unit/view/1", method = RequestMethod.GET)
    public ModelAndView viewOrganizationUnitCompany(@PathVariable("id") Integer id) {
        Optional<OrganizationUnit> organizationUnitOptional = organizationUnitRepository.findById(id);
        OrganizationUnit organizationUnit = organizationUnitOptional.get();
        ModelAndView modelAndView = new ModelAndView("organization_unit/view");
        modelAndView.addObject("organizationUnit", organizationUnit);
        modelAndView.getModel().put("page_title", "Xem chi tiết Đơn vị, phòng ban");
        return modelAndView;
    }

    /**
     * View an organization unit. Xem chi tiết, dành cho cấp Đơn vị, phòng ban.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/organization_unit/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewOrganizationUnit(@PathVariable("id") Integer id) {
        Optional<OrganizationUnit> organizationUnitOptional = organizationUnitRepository.findById(id);
        OrganizationUnit organizationUnit = organizationUnitOptional.get();
        ModelAndView modelAndView = new ModelAndView("organization_unit/view");
        modelAndView.addObject("organizationUnit", organizationUnit);
        modelAndView.getModel().put("page_title", "Xem chi tiết Đơn vị, phòng ban");
        return modelAndView;
    }

    /**
     * Hiển thị trang edit thông tin công ty. Cấp công ty.
     *
     * @return
     */
    @RequestMapping(value = "/organization_unit/edit/1", method = RequestMethod.GET)
    public ModelAndView editAccountObject() {
        ModelAndView modelAndView = new ModelAndView("organization_unit/edit_company");
        OrganizationUnit organizationUnit = organizationUnitRepository.findById(1).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + 1));
        OrganizationUnitForm organizationUnitForm = new OrganizationUnitForm();
        organizationUnitForm.setId(organizationUnit.getId());
        organizationUnitForm.setBranchId(organizationUnit.getBranchId());
        organizationUnitForm.setOrganizationUnitCode(organizationUnit.getOrganizationUnitCode());
        organizationUnitForm.setIsSystem(organizationUnit.getIsSystem());
        organizationUnitForm.setVyCodeId(organizationUnit.getVyCodeId());
        organizationUnitForm.setGrade(organizationUnit.getGrade());
        organizationUnitForm.setIsParent(organizationUnit.getIsParent());
        organizationUnitForm.setAddress(organizationUnit.getAddress());
        organizationUnitForm.setOrganizationUnitTypeId(organizationUnit.getOrganizationUnitTypeId());
        organizationUnitForm.setBusinessRegistrationNumber(organizationUnit.getBusinessRegistrationNumber());
        organizationUnitForm.setBusinessRegistrationNumberIssuedDate(organizationUnit.getBusinessRegistrationNumberIssuedDate());
        organizationUnitForm.setBusinessRegistrationNumber(organizationUnit.getBusinessRegistrationNumber());
        organizationUnitForm.setBusinessRegistrationNumberIssuedPlace(organizationUnit.getBusinessRegistrationNumberIssuedPlace());
        organizationUnitForm.setIsDependent(organizationUnit.getIsDependent());
        organizationUnitForm.setIsPrivateVatDeclaration(organizationUnit.getIsPrivateVatDeclaration());
        organizationUnitForm.setCostAccount(organizationUnit.getCostAccount());
        organizationUnitForm.setActiveStatus(Boolean.TRUE);
        organizationUnitForm.setCompanyTaxCode(organizationUnit.getCompanyTaxCode());
        organizationUnitForm.setCompanyTel(organizationUnit.getCompanyTel());
        organizationUnitForm.setCompanyFax(organizationUnit.getCompanyFax());
        organizationUnitForm.setCompanyEmail(organizationUnit.getCompanyEmail());
        organizationUnitForm.setCompanyWebsite(organizationUnit.getCompanyWebsite());
        organizationUnitForm.setCompanyBankAccountId(organizationUnit.getCompanyBankAccountId());
        organizationUnitForm.setCompanyOwnerName(organizationUnit.getCompanyOwnerName());
        organizationUnitForm.setCompanyTaxCode(organizationUnit.getCompanyTaxCode());
        organizationUnitForm.setDirectorTitle(organizationUnit.getDirectorTitle());
        organizationUnitForm.setDirectorName(organizationUnit.getDirectorName());
        organizationUnitForm.setChiefOfAccountingTitle(organizationUnit.getChiefOfAccountingTitle());
        organizationUnitForm.setChiefOfAccountingName(organizationUnit.getChiefOfAccountingName());
        organizationUnitForm.setStoreKeeperTitle(organizationUnit.getStoreKeeperTitle());
        organizationUnitForm.setStoreKeeperName(organizationUnit.getStoreKeeperName());
        organizationUnitForm.setCashierTitle(organizationUnit.getCashierTitle());
        organizationUnitForm.setCashierName(organizationUnit.getCashierName());
        organizationUnitForm.setReporterTitle(organizationUnit.getReporterTitle());
        organizationUnitForm.setReporterName(organizationUnit.getReporterName());
        organizationUnitForm.setIsPrintSigner(organizationUnit.getIsPrintSigner());
        organizationUnitForm.setIsGetReporterNameByUserLogIn(organizationUnit.getIsGetReporterNameByUserLogIn());
        organizationUnitForm.setCompanyDistrict(organizationUnit.getCompanyDistrict());
        organizationUnitForm.setCompanyCity(organizationUnit.getCompanyCity());

        // OrganizationUnitInfo
        Optional<OrganizationUnitInfo> organizationUnitInfoOptional = organizationUnitInfoRepository.findById(1);
        OrganizationUnitInfo organizationUnitInfo = organizationUnitInfoOptional.get();
        organizationUnitForm.setI(organizationUnitInfo.getDescription());

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(2);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(3);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI2(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(4);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI3(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(5);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI4(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(6);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI5(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(7);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setI6(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(8);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIi(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(9);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIi1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(10);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIi2(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(11);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIii(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(12);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIii1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(13);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIii2(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(14);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(15);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(16);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv2(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(17);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv3(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(18);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv4(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(19);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv41(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(20);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv42(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(21);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv5(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(22);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv6(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(23);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv7(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(24);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv71(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(25);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv72(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(26);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv73(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(27);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv8(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(28);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv9(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(29);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv10(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(30);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv11(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(31);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv111(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(32);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv112(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(33);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv113(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(34);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv114(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(35);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setIv12(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(36);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setV15(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(37);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setV16(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(38);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setVii(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(39);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setVii1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(40);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(41);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii1(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(42);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii2(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(43);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii3(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(44);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii4(organizationUnitInfo.getDescription());
        }

        organizationUnitInfoOptional = organizationUnitInfoRepository.findById(45);
        organizationUnitInfo = organizationUnitInfoOptional.get();
        if (organizationUnitInfo.getDescription() != null) {
            organizationUnitForm.setViii5(organizationUnitInfo.getDescription());
        }

        modelAndView.addObject("organizationUnitForm", organizationUnitForm);
        modelAndView.getModel().put("page_title", "Sửa thông tin Đơn vị");
        return modelAndView;
    }

    /**
     * Hiển thị trang edit (dành cho cấp phòng ban)
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/organization_unit/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editAccountObject(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("organization_unit/edit");
        OrganizationUnit organizationUnit = organizationUnitRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        OrganizationUnitForm organizationUnitForm = new OrganizationUnitForm();
        organizationUnitForm.setId(organizationUnit.getId());
        organizationUnitForm.setBranchId(organizationUnit.getBranchId());
        organizationUnitForm.setOrganizationUnitCode(organizationUnit.getOrganizationUnitCode());
        organizationUnitForm.setIsSystem(organizationUnit.getIsSystem());
        organizationUnitForm.setVyCodeId(organizationUnit.getVyCodeId());
        organizationUnitForm.setGrade(organizationUnit.getGrade());
        organizationUnitForm.setIsParent(organizationUnit.getIsParent());
        organizationUnitForm.setAddress(organizationUnit.getAddress());
        organizationUnitForm.setOrganizationUnitTypeId(organizationUnit.getOrganizationUnitTypeId());
        organizationUnitForm.setBusinessRegistrationNumber(organizationUnit.getBusinessRegistrationNumber());
        organizationUnitForm.setBusinessRegistrationNumberIssuedDate(organizationUnit.getBusinessRegistrationNumberIssuedDate());
        organizationUnitForm.setBusinessRegistrationNumber(organizationUnit.getBusinessRegistrationNumber());
        organizationUnitForm.setBusinessRegistrationNumberIssuedPlace(organizationUnit.getBusinessRegistrationNumberIssuedPlace());
        organizationUnitForm.setIsDependent(organizationUnit.getIsDependent());
        organizationUnitForm.setIsPrivateVatDeclaration(organizationUnit.getIsPrivateVatDeclaration());
        organizationUnitForm.setCostAccount(organizationUnit.getCostAccount());
        organizationUnitForm.setActiveStatus(organizationUnit.getActiveStatus());
        organizationUnitForm.setCompanyTaxCode(organizationUnit.getCompanyTaxCode());
        organizationUnitForm.setCompanyTel(organizationUnit.getCompanyTel());
        organizationUnitForm.setCompanyFax(organizationUnit.getCompanyFax());
        organizationUnitForm.setCompanyEmail(organizationUnit.getCompanyEmail());
        organizationUnitForm.setCompanyWebsite(organizationUnit.getCompanyWebsite());
        organizationUnitForm.setCompanyBankAccountId(organizationUnit.getCompanyBankAccountId());
        organizationUnitForm.setCompanyOwnerName(organizationUnit.getCompanyOwnerName());
        organizationUnitForm.setCompanyTaxCode(organizationUnit.getCompanyTaxCode());
        organizationUnitForm.setDirectorTitle(organizationUnit.getDirectorTitle());
        organizationUnitForm.setDirectorName(organizationUnit.getDirectorName());
        organizationUnitForm.setChiefOfAccountingTitle(organizationUnit.getChiefOfAccountingTitle());
        organizationUnitForm.setChiefOfAccountingName(organizationUnit.getChiefOfAccountingName());
        organizationUnitForm.setStoreKeeperTitle(organizationUnit.getStoreKeeperTitle());
        organizationUnitForm.setStoreKeeperName(organizationUnit.getStoreKeeperName());
        organizationUnitForm.setCashierTitle(organizationUnit.getCashierTitle());
        organizationUnitForm.setCashierName(organizationUnit.getCashierName());
        organizationUnitForm.setReporterTitle(organizationUnit.getReporterTitle());
        organizationUnitForm.setReporterName(organizationUnit.getReporterName());
        organizationUnitForm.setIsPrintSigner(organizationUnit.getIsPrintSigner());
        organizationUnitForm.setIsGetReporterNameByUserLogIn(organizationUnit.getIsGetReporterNameByUserLogIn());
        organizationUnitForm.setCompanyDistrict(organizationUnit.getCompanyDistrict());
        organizationUnitForm.setCompanyCity(organizationUnit.getCompanyCity());
        modelAndView.addObject("organizationUnitForm", organizationUnitForm);
        modelAndView.getModel().put("page_title", "Sửa thông tin Đơn vị");
        return modelAndView;
    }

    /**
     * Submit change for organization unit. Cấp công ty.
     *
     * @param organizationUnitForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/edit_organization_unit_company", method = RequestMethod.POST)
    public String updateOrganizationUnitCompany(@ModelAttribute("organizationUnitForm") OrganizationUnitForm organizationUnitForm, Authentication authentication) {
        Optional<OrganizationUnit> organizationUnitOptional = organizationUnitRepository.findById(organizationUnitForm.getId());
        OrganizationUnit organizationUnitOld = organizationUnitOptional.get();
        OrganizationUnit organizationUnit = new OrganizationUnit();
        organizationUnit.setId(organizationUnitOld.getId());
        organizationUnit.setOrganizationUnitTypeId(organizationUnitOld.getOrganizationUnitTypeId());
        organizationUnit.setOrganizationUnitCode(organizationUnitForm.getOrganizationUnitCode());
        organizationUnit.setOrganizationUnitName(organizationUnitForm.getOrganizationUnitName());
        organizationUnit.setParentId(organizationUnitForm.getParentId());
        organizationUnit.setGrade(organizationUnitForm.getGrade());
        organizationUnit.setAddress(organizationUnitForm.getAddress());
        organizationUnit.setBusinessRegistrationNumber(organizationUnitForm.getBusinessRegistrationNumber());
        organizationUnit.setBusinessRegistrationNumberIssuedDate(organizationUnitForm.getBusinessRegistrationNumberIssuedDate());
        organizationUnit.setBusinessRegistrationNumberIssuedPlace(organizationUnitForm.getBusinessRegistrationNumberIssuedPlace());
        if (organizationUnitForm.getActiveStatus() == null) {
            organizationUnit.setActiveStatus(Boolean.FALSE);
        } else {
            organizationUnit.setActiveStatus(Boolean.TRUE);
        }
        organizationUnit.setCreatedDate(organizationUnitOld.getCreatedDate());
        organizationUnit.setCreatedBy(organizationUnitOld.getCreatedBy());
        organizationUnit.setIsSystem(Boolean.FALSE);
        organizationUnit.setIsParent(Boolean.FALSE);
        organizationUnit.setIsPrivateVatDeclaration(Boolean.FALSE);
        organizationUnit.setIsPrintSigner(Boolean.TRUE);
        organizationUnit.setIsGetReporterNameByUserLogIn(Boolean.FALSE);
        organizationUnit.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        organizationUnit.setModifiedDate(now);

        // Chữ ký.
        organizationUnit.setCompanyTaxCode(organizationUnitForm.getCompanyTaxCode());
        organizationUnit.setCompanyDistrict(organizationUnitForm.getCompanyDistrict());
        organizationUnit.setCompanyCity(organizationUnitForm.getCompanyCity());
        organizationUnit.setCompanyTel(organizationUnitForm.getCompanyTel());
        organizationUnit.setCompanyFax(organizationUnitForm.getCompanyFax());
        organizationUnit.setCompanyEmail(organizationUnitForm.getCompanyEmail());
        organizationUnit.setCompanyWebsite(organizationUnitForm.getCompanyWebsite());
        organizationUnit.setCompanyBankAccountId(organizationUnitForm.getCompanyBankAccountId());
        organizationUnit.setParentId(0); // Do đây là cấp company.
        organizationUnit.setCompanyOwnerTaxCode("0"); // Do đây là cấp company.

        organizationUnit.setDirectorTitle(organizationUnitForm.getDirectorTitle());
        organizationUnit.setDirectorName(organizationUnitForm.getDirectorName());
        organizationUnit.setChiefOfAccountingTitle(organizationUnitForm.getChiefOfAccountingTitle());
        organizationUnit.setChiefOfAccountingName(organizationUnitForm.getChiefOfAccountingName());
        organizationUnit.setStoreKeeperTitle(organizationUnitForm.getStoreKeeperTitle());
        organizationUnit.setStoreKeeperName(organizationUnitForm.getStoreKeeperName());
        organizationUnit.setCashierTitle(organizationUnitForm.getCashierTitle());
        organizationUnit.setCashierName(organizationUnitForm.getCashierName());
        organizationUnit.setReporterTitle(organizationUnitForm.getReporterTitle());
        organizationUnit.setReporterName(organizationUnitForm.getReporterName());
        if(organizationUnitForm.getIsPrintSigner() != null){
            organizationUnit.setIsPrintSigner(Boolean.TRUE);
        }else {
            organizationUnit.setIsPrintSigner(Boolean.FALSE);
        }
        if(organizationUnitForm.getIsGetReporterNameByUserLogIn() != null){
            organizationUnit.setIsGetReporterNameByUserLogIn(Boolean.TRUE);
        }else {
            organizationUnit.setIsGetReporterNameByUserLogIn(Boolean.FALSE);
        }
        organizationUnitRepository.save(organizationUnit);

        OrganizationUnitInfo organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(1);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I");
        organizationUnitInfo.setItemName("I. Đặc điểm hoạt động của doanh nghiệp ");
        if (organizationUnitForm.getI() != null && !organizationUnitForm.getI().isEmpty() && !organizationUnitForm.getI().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(2);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.1");
        organizationUnitInfo.setItemName("1. Hình thức sở hữu vốn:");
        if (organizationUnitForm.getI1() != null && !organizationUnitForm.getI1().isEmpty() && !organizationUnitForm.getI1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(3);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.2");
        organizationUnitInfo.setItemName("2. Lĩnh vực kinh doanh:");
        if (organizationUnitForm.getI2() != null && !organizationUnitForm.getI2().isEmpty() && !organizationUnitForm.getI2().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI2());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(4);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.3");
        organizationUnitInfo.setItemName("3. Ngành nghề kinh doanh: ");
        if (organizationUnitForm.getI3() != null && !organizationUnitForm.getI3().isEmpty() && !organizationUnitForm.getI3().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI3());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(5);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.4");
        organizationUnitInfo.setItemName("4. Chu kỳ sản xuất, kinh doanh thông thường:");
        if (organizationUnitForm.getI4() != null && !organizationUnitForm.getI4().isEmpty() && !organizationUnitForm.getI4().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI4());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(6);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.5");
        organizationUnitInfo.setItemName("5. Đặc điểm hoạt động của doanh nghiệp trong năm tài chính có ảnh hưởng đến Báo cáo tài chính:");
        if (organizationUnitForm.getI5() != null && !organizationUnitForm.getI5().isEmpty() && !organizationUnitForm.getI5().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI5());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(7);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("I.6");
        organizationUnitInfo.setItemName("6. Tuyên bố về khả năng so sánh thông tin trên Báo cáo tài chính (có so sánh được hay không, nếu không so sánh được phải nêu rõ lý do như vì chuyển đổi hình thức sở hữu, chia tách, sáp nhập, nêu độ dài về kỳ so sánh…):");
        if (organizationUnitForm.getI6() != null && !organizationUnitForm.getI6().isEmpty() && !organizationUnitForm.getI6().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getI6());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(8);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("II");
        organizationUnitInfo.setItemName("II. Kỳ kế toán, đơn vị tiền tệ sử dụng trong kế toán");
        if (organizationUnitForm.getIi() != null && !organizationUnitForm.getIi().isEmpty() && !organizationUnitForm.getIi().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIi());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(9);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("II.1");
        organizationUnitInfo.setItemName("1. Kỳ kế toán năm:");
        if (organizationUnitForm.getIi1() != null && !organizationUnitForm.getIi1().isEmpty() && !organizationUnitForm.getIi1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIi1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(10);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("II.2");
        organizationUnitInfo.setItemName("2. Đơn vị tiền tệ sử dụng trong kế toán:");
        if (organizationUnitForm.getIi2() != null && !organizationUnitForm.getIi2().isEmpty() && !organizationUnitForm.getIi2().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIi2());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(11);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("III");
        organizationUnitInfo.setItemName("III. Chuẩn mực và Chế độ kế toán áp dụng");
        if (organizationUnitForm.getIii() != null && !organizationUnitForm.getIii().isEmpty() && !organizationUnitForm.getIii().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIii());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(12);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("III.1");
        organizationUnitInfo.setItemName("1- Chế độ kế toán áp dụng:");
        if (organizationUnitForm.getIii1() != null && !organizationUnitForm.getIii1().isEmpty() && !organizationUnitForm.getIii1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIii1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(13);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("III.2");
        organizationUnitInfo.setItemName("2- Tuyên bố về việc tuân thủ Chuẩn mực kế toán và Chế độ kế toán:");
        if (organizationUnitForm.getIii2() != null && !organizationUnitForm.getIii2().isEmpty() && !organizationUnitForm.getIii2().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIii2());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(14);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV");
        organizationUnitInfo.setItemName("IV. Các chính sách kế toán áp dụng ");
        if (organizationUnitForm.getIv() != null && !organizationUnitForm.getIv().isEmpty() && !organizationUnitForm.getIv().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(15);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.1");
        organizationUnitInfo.setItemName("- Tỷ giá hối đoái áp dụng trong kế toán:");
        if (organizationUnitForm.getIv1() != null && !organizationUnitForm.getIv1().isEmpty() && !organizationUnitForm.getIv1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(16);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.2");
        organizationUnitInfo.setItemName("- Nguyên tắc chuyển đổi BCTC lập bằng ngoại tệ sang Đồng Việt Nam:");
        if (organizationUnitForm.getIv2() != null && !organizationUnitForm.getIv2().isEmpty() && !organizationUnitForm.getIv2().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv2());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(17);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.3");
        organizationUnitInfo.setItemName("- Nguyên tắc ghi nhận các khoản tiền và các khoản tương đương tiền:");
        if (organizationUnitForm.getIv3() != null && !organizationUnitForm.getIv3().isEmpty() && !organizationUnitForm.getIv3().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv3());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(18);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.4");
        organizationUnitInfo.setItemName("- Nguyên tắc kế toán các khoản đầu tư tài chính:");
        if (organizationUnitForm.getIv4() != null && !organizationUnitForm.getIv4().isEmpty() && !organizationUnitForm.getIv4().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv4());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(19);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.4.1");
        organizationUnitInfo.setItemName("");
        if (organizationUnitForm.getIv41() != null && !organizationUnitForm.getIv41().isEmpty() && !organizationUnitForm.getIv41().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv41());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(20);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.4.2");
        organizationUnitInfo.setItemName("");
        if (organizationUnitForm.getIv42() != null && !organizationUnitForm.getIv42().isEmpty() && !organizationUnitForm.getIv42().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv42());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(21);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.5");
        organizationUnitInfo.setItemName("- Nguyên tắc kế toán nợ phải thu:");
        if (organizationUnitForm.getIv5() != null && !organizationUnitForm.getIv5().isEmpty() && !organizationUnitForm.getIv5().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv5());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(22);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.6");
        organizationUnitInfo.setItemName("- Nguyên tắc ghi nhận hàng tồn kho:");
        if (organizationUnitForm.getIv6() != null && !organizationUnitForm.getIv6().isEmpty() && !organizationUnitForm.getIv6().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv6());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(23);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.7");
        organizationUnitInfo.setItemName("- Nguyên tắc ghi nhận và các phương pháp khấu hao TSCĐ, TSCĐ thuê tài chính, bất động sản đầu tư:");
        if (organizationUnitForm.getIv7() != null && !organizationUnitForm.getIv7().isEmpty() && !organizationUnitForm.getIv7().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv7());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(24);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.7.1");
        organizationUnitInfo.setItemName("");
        if (organizationUnitForm.getIv71() != null && !organizationUnitForm.getIv71().isEmpty() && !organizationUnitForm.getIv71().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv71());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(25);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.7.2");
        organizationUnitInfo.setItemName("+ Nguyên tắc ghi nhận TSCĐ vô hình:");
        if (organizationUnitForm.getIv72() != null && !organizationUnitForm.getIv72().isEmpty() && !organizationUnitForm.getIv72().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv72());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(26);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.7.3");
        organizationUnitInfo.setItemName("+ Nguyên tắc ghi nhận bất động sản đầu tư:");
        if (organizationUnitForm.getIv73() != null && !organizationUnitForm.getIv73().isEmpty() && !organizationUnitForm.getIv73().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv73());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(27);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.8");
        organizationUnitInfo.setItemName("- Nguyên tắc kế toán nợ phải trả:");
        if (organizationUnitForm.getIv8() != null && !organizationUnitForm.getIv8().isEmpty() && !organizationUnitForm.getIv8().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv8());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(28);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.9");
        organizationUnitInfo.setItemName("- Nguyên tắc ghi nhận và vốn hóa các khoản chi phí đi vay:");
        if (organizationUnitForm.getIv9() != null && !organizationUnitForm.getIv9().isEmpty() && !organizationUnitForm.getIv9().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv9());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(29);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.10");
        organizationUnitInfo.setItemName("- Nguyên tắc ghi nhận vốn chủ sở hữu:");
        if (organizationUnitForm.getIv10() != null && !organizationUnitForm.getIv10().isEmpty() && !organizationUnitForm.getIv10().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv10());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(30);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.11");
        organizationUnitInfo.setItemName("- Nguyên tắc và phương pháp ghi nhận doanh thu:");
        if (organizationUnitForm.getIv11() != null && !organizationUnitForm.getIv11().isEmpty() && !organizationUnitForm.getIv11().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv11());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(31);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.11.1");
        organizationUnitInfo.setItemName("+ Doanh thu bán hàng:");
        if (organizationUnitForm.getIv111() != null && !organizationUnitForm.getIv111().isEmpty() && !organizationUnitForm.getIv111().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv111());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(32);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.11.2");
        organizationUnitInfo.setItemName("");
        if (organizationUnitForm.getIv112() != null && !organizationUnitForm.getIv112().isEmpty() && !organizationUnitForm.getIv112().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv112());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(33);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.11.3");
        organizationUnitInfo.setItemName("+ Doanh thu hoạt động tài chính:");
        if (organizationUnitForm.getIv113() != null && !organizationUnitForm.getIv113().isEmpty() && !organizationUnitForm.getIv113().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv113());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(34);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.11.4");
        organizationUnitInfo.setItemName("+  Doanh thu hợp đồng xây dựng:");
        if (organizationUnitForm.getIv114() != null && !organizationUnitForm.getIv114().isEmpty() && !organizationUnitForm.getIv114().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv114());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(35);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("IV.12");
        organizationUnitInfo.setItemName("- Nguyên tắc kế toán chi phí:");
        if (organizationUnitForm.getIv12() != null && !organizationUnitForm.getIv12().isEmpty() && !organizationUnitForm.getIv12().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getIv12());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(36);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("V.15");
        organizationUnitInfo.setItemName("15. Thuyết minh về các bên liên quan (danh sách các bên liên quan, giao dịch và các thông tin khác về các bên liên quan chưa được trình bày ở các nội dung nêu trên)");
        if (organizationUnitForm.getV15() != null && !organizationUnitForm.getV15().isEmpty() && !organizationUnitForm.getV15().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getV15());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(37);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("V.16");
        organizationUnitInfo.setItemName("16. Ngoài các nội dung đã trình bày trên, các doanh nghiệp được giải trình, thuyết minh các thông tin khác nếu thấy cần thiết");
        if (organizationUnitForm.getV16() != null && !organizationUnitForm.getV16().isEmpty() && !organizationUnitForm.getV16().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getV16());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(38);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VII");
        organizationUnitInfo.setItemName("VII. Thông tin bổ sung cho các khoản mục trình bày trong Báo cáo lưu chuyển tiền tệ");
        if (organizationUnitForm.getVii() != null && !organizationUnitForm.getVii().isEmpty() && !organizationUnitForm.getVii().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getVii());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(39);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VII.1");
        organizationUnitInfo.setItemName("Các khoản tiền do doanh nghiệp nắm giữ nhưng không được sử dụng: Trình bày giá trị và lý do của các khoản tiền và tương đương tiền lớn do doanh nghiệp nắm giữ nhưng không được sử dụng do có sự hạn chế của pháp luật hoặc các ràng buộc khác mà doanh nghiệp phải thực hiện");
        if (organizationUnitForm.getVii1() != null && !organizationUnitForm.getVii1().isEmpty() && !organizationUnitForm.getVii1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getVii1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(40);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII");
        organizationUnitInfo.setItemName("VIII. Những thông tin khác");
        if (organizationUnitForm.getViii() != null && !organizationUnitForm.getViii().isEmpty() && !organizationUnitForm.getViii().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(41);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII.1");
        organizationUnitInfo.setItemName("1. Những khoản nợ tiềm tàng, khoản cam kết và những thông tin tài chính khác:");
        if (organizationUnitForm.getViii1() != null && !organizationUnitForm.getViii1().isEmpty() && !organizationUnitForm.getViii1().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii1());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(42);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII.2");
        organizationUnitInfo.setItemName("2. Những sự kiện phát sinh sau ngày kết thúc kỳ kế toán năm:");
        if (organizationUnitForm.getViii2() != null && !organizationUnitForm.getViii2().isEmpty() && !organizationUnitForm.getViii2().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii2());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(43);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII.3");
        organizationUnitInfo.setItemName("3. Thông tin so sánh (những thay đổi về thông tin trong Báo cáo tài chính của các niên độ kế toán trước):");
        if (organizationUnitForm.getViii3() != null && !organizationUnitForm.getViii3().isEmpty() && !organizationUnitForm.getViii3().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii3());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(44);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII.4");
        organizationUnitInfo.setItemName("4. Thông tin về hoạt động liên tục:");
        if (organizationUnitForm.getViii4() != null && !organizationUnitForm.getViii4().isEmpty() && !organizationUnitForm.getViii4().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii4());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        organizationUnitInfo = new OrganizationUnitInfo();
        organizationUnitInfo.setId(45);
        organizationUnitInfo.setOrganizationUnitId(1);
        organizationUnitInfo.setItemCode("VIII.5");
        organizationUnitInfo.setItemName("5. Những thông tin khác.");
        if (organizationUnitForm.getViii5() != null && !organizationUnitForm.getViii5().isEmpty() && !organizationUnitForm.getViii5().isBlank()) {
            organizationUnitInfo.setDescription(organizationUnitForm.getViii5());
        }
        organizationUnitInfoRepository.save(organizationUnitInfo);

        return "redirect:/organization_units";
    }

    /**
     * Submit change for organization unit. Cấp đơn vị/phòng ban.
     *
     * @param organizationUnitForm
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/edit_organization_unit", method = RequestMethod.POST)
    public String updateOrganizationUnit(@ModelAttribute("organizationUnitForm") OrganizationUnitForm organizationUnitForm, Authentication authentication) {
        Optional<OrganizationUnit> organizationUnitOptional = organizationUnitRepository.findById(organizationUnitForm.getId());
        OrganizationUnit organizationUnitOld = organizationUnitOptional.get();
        OrganizationUnit organizationUnit = new OrganizationUnit();
        organizationUnit.setId(organizationUnitOld.getId());
        organizationUnit.setOrganizationUnitCode(organizationUnitForm.getOrganizationUnitCode());
        organizationUnit.setOrganizationUnitName(organizationUnitForm.getOrganizationUnitName());
        organizationUnit.setParentId(organizationUnitForm.getParentId());
        organizationUnit.setGrade(organizationUnitForm.getGrade());
        organizationUnit.setAddress(organizationUnitForm.getAddress());
        organizationUnit.setBusinessRegistrationNumber(organizationUnitForm.getBusinessRegistrationNumber());
        organizationUnit.setBusinessRegistrationNumberIssuedDate(organizationUnitForm.getBusinessRegistrationNumberIssuedDate());
        organizationUnit.setBusinessRegistrationNumberIssuedPlace(organizationUnitForm.getBusinessRegistrationNumberIssuedPlace());
        if (organizationUnitForm.getActiveStatus() == null) {
            organizationUnit.setActiveStatus(Boolean.FALSE);
        } else {
            organizationUnit.setActiveStatus(Boolean.TRUE);
        }
        organizationUnit.setCreatedDate(organizationUnitOld.getCreatedDate());
        organizationUnit.setCreatedBy(organizationUnitOld.getCreatedBy());
        organizationUnit.setIsSystem(Boolean.FALSE);
        organizationUnit.setIsParent(Boolean.FALSE);
        organizationUnit.setIsPrivateVatDeclaration(Boolean.FALSE);
        organizationUnit.setIsPrintSigner(Boolean.TRUE);
        organizationUnit.setIsGetReporterNameByUserLogIn(Boolean.FALSE);
        organizationUnit.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        organizationUnit.setModifiedDate(now);
        organizationUnitRepository.save(organizationUnit);
        return "redirect:/organization_units";
    }

    /**
     * Organization unit JSON 2 columns.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/org_units_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    @Deprecated
    public String organizationUnitJSON() throws JsonProcessingException {
        List<OrganizationUnit> organizationUnitList = organizationUnitRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> accObjList = new ArrayList<>();
        for (OrganizationUnit organizationUnit : organizationUnitList) {
            DropDownListItem accObj = new DropDownListItem();
            accObj.setValue(organizationUnit.getId().toString());
            accObj.setText(organizationUnit.getOrganizationUnitName());
            accObjList.add(accObj);
        }
        String jsonDataString = objectMapper.writeValueAsString(accObjList);
        return jsonDataString;
    }

    /**
     * Organization unit JSON 2 columns.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/organization_units_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    @Deprecated
    public String organizationUnitsJSON() throws JsonProcessingException {
        List<OrganizationUnit> organizationUnitList = organizationUnitRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        List<OrganizationUnitDropDownItem> organizationUnitDropDownItemList = new ArrayList<>();
        for (OrganizationUnit organizationUnit : organizationUnitList) {
            OrganizationUnitDropDownItem organizationUnitDropDownItem = new OrganizationUnitDropDownItem();
            organizationUnitDropDownItem.setId(organizationUnit.getId());
            organizationUnitDropDownItem.setOrganizationUnitCode(organizationUnit.getOrganizationUnitCode());
            organizationUnitDropDownItem.setOrganizationUnitName(organizationUnit.getOrganizationUnitName());
            organizationUnitDropDownItemList.add(organizationUnitDropDownItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(organizationUnitDropDownItemList);
        return jsonDataString;
    }


    /**
     * Delete an organization unit item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/organization_unit/delete/{id}", method = RequestMethod.POST)
    public String deleteOrganizationUnit(@PathVariable("id") Integer id, HttpServletRequest request) {
        OrganizationUnit organizationUnit = organizationUnitRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        //TODO: Kiểm tra các khóa ngoại đang dùng đến.
        organizationUnitRepository.delete(organizationUnit);
        return "redirect:" + request.getHeader("Referer");
    }

}
