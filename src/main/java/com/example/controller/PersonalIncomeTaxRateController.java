package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.PersonalIncomeTaxRate;
import com.example.repository.PersonalIncomeTaxRateRepository;

import java.util.List;

@Controller
public class PersonalIncomeTaxRateController {

    @Autowired
    PersonalIncomeTaxRateRepository personalIncomeTaxRateRepository;

    /**
     * Show list of Personal Income Tax rates.
     *
     * @return
     */
    @RequestMapping(value = "/pit_rates", method = RequestMethod.GET)
    public ModelAndView accounts() {
        ModelAndView modelAndView = new ModelAndView("personal_income_tax_rate/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<PersonalIncomeTaxRate> personalIncomeTaxRateList = personalIncomeTaxRateRepository.findAllInOrder();
        modelAndView.addObject("personalIncomeTaxRateList", personalIncomeTaxRateList);
        modelAndView.getModel().put("page_title", "Bậc thang Thuế thu nhập cá nhân");
        return modelAndView;
    }

}
