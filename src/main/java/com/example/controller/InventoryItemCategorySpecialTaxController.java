package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.InventoryItemCategorySpecialTax;
import com.example.repository.InventoryItemCategorySpecialTaxRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Bảng nhóm Hàng hóa dịch vụ chịu Thuế tiêu thụ đặc biệt.
 */
@Controller
public class InventoryItemCategorySpecialTaxController {

    @Autowired
    InventoryItemCategorySpecialTaxRepository inventoryItemCategorySpecialTaxRepository;

    /**
     * Show list all special taxes.
     *
     * @return
     */
    @RequestMapping(value = "/special_taxes", method = RequestMethod.GET)
    public ModelAndView listAllSpecialTaxes() {
        ModelAndView modelAndView = new ModelAndView("inventory_item_category_special_tax/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<InventoryItemCategorySpecialTax> inventoryItemCategorySpecialTaxList = inventoryItemCategorySpecialTaxRepository.findAll();
        modelAndView.addObject("inventoryItemCategorySpecialTaxList", inventoryItemCategorySpecialTaxList);
        modelAndView.getModel().put("page_title", "Danh sách Nhóm HHDV chịu thuế Tiêu thụ đặc biệt");
        return modelAndView;
    }

    /**
     * Delete a special tax.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/special_tax/delete/{id}", method = RequestMethod.POST)
    public String deleteSpecialTax(@PathVariable("id") Integer id, HttpServletRequest request) {
        InventoryItemCategorySpecialTax inventoryItemCategorySpecialTax = inventoryItemCategorySpecialTaxRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exepense_item id: " + id));
        //TODO: Check related FK.
        inventoryItemCategorySpecialTaxRepository.delete(inventoryItemCategorySpecialTax);
        return "redirect:" + request.getHeader("Referer");
    }


    /**
     * JSON has multi-column of Special tax. Chỉ trả về mức thuế suất đang theo dõi.
     * Dùng cho drop-down list ở màn hình Thêm mới Hàng hóa, vật tư.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/special_tax_mul_col_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String stockMultiColumnJSON() throws JsonProcessingException {
        List<InventoryItemCategorySpecialTax> inventoryItemCategorySpecialTaxList = inventoryItemCategorySpecialTaxRepository.findAllActiveSpecialTaxRates();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(inventoryItemCategorySpecialTaxList);
        return jsonDataString;
    }

}
