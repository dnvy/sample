package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.DropDownListItem;
import com.example.common.NumberStringDropDownListItem;
import com.example.common.UtilityList;
import com.example.dto.SimpleCurrency;
import com.example.entity.Ccy;
import com.example.repository.CcyRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CcyController {

    @Autowired
    CcyRepository ccyRepository;

    /**
     * Màn hình danh sách tiền tệ.
     *
     * @return
     */
    @RequestMapping(value = "/currencies", method = RequestMethod.GET)
    public ModelAndView accounts() {
        ModelAndView modelAndView = new ModelAndView("ccy/list");
        UtilityList.setActiveStatusList(modelAndView);
        List<Ccy> currencies = ccyRepository.findAll();
        modelAndView.addObject("currencies", currencies);
        modelAndView.getModel().put("page_title", "Đơn vị tiền tệ");
        return modelAndView;
    }

    /**
     * For drop-down list. Used in /add_cash_receipt
     *
     * @return
     * @throws JsonProcessingException
     */
    @Deprecated
    @RequestMapping(value = "/currency_id_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String currencyIdJSON() throws JsonProcessingException {
        List<Ccy> currencies = ccyRepository.findAllByOrderBySortOrder();
        ObjectMapper objectMapper = new ObjectMapper();
        List<NumberStringDropDownListItem> currencyList = new ArrayList<>();
        for (Ccy ccy : currencies) {
            NumberStringDropDownListItem ccyObj = new NumberStringDropDownListItem();
            ccyObj.setNum(ccy.getId());
            ccyObj.setText(ccy.getCurrencyName());
            currencyList.add(ccyObj);
        }
        String jsonDataString = objectMapper.writeValueAsString(currencyList);
        return jsonDataString;
    }

    /**
     * Trả về 2 trường theo định dạng của Single currency JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @Deprecated
    @RequestMapping(value = "/currency_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String currencyJSON() throws JsonProcessingException {
        List<Ccy> currencies = ccyRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        List<DropDownListItem> currencyList = new ArrayList<>();
        for (Ccy ccy : currencies) {
            DropDownListItem dropDownListItem = new DropDownListItem();
            dropDownListItem.setText(ccy.getCurrencyCode());
            dropDownListItem.setValue(ccy.getCurrencyCode());
            currencyList.add(dropDownListItem);
        }
        String jsonDataString = objectMapper.writeValueAsString(currencyList);
        return jsonDataString;
    }

    /**
     * JSON has multi-column of currencies.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/ccy_mul_col_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String currencyMultiColumnJSON() throws JsonProcessingException {
        List<Ccy> currencies = ccyRepository.findAllByOrderBySortOrder();
        List<SimpleCurrency> simpleCurrencyList = new ArrayList<>();
        for (Ccy currency : currencies) {
            SimpleCurrency simpleCurrency = new SimpleCurrency();
            simpleCurrency.setId(currency.getId());
            simpleCurrency.setCurrencyCode(currency.getCurrencyCode());
            simpleCurrency.setCurrencyName(currency.getCurrencyName());
            simpleCurrency.setExchangeRate(currency.getExchangeRate());
            simpleCurrencyList.add(simpleCurrency);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(simpleCurrencyList);
        return jsonDataString;
    }

    /**
     * Delete a currency unit.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/currency/delete/{id}", method = RequestMethod.POST)
    public String deleteCurrency(@PathVariable("id") Integer id, HttpServletRequest request) {
        Ccy ccy = ccyRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        ccyRepository.delete(ccy);
        return "redirect:" + request.getHeader("Referer");
    }

}
