package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.OpeningAccountEntry;
import com.example.entity.OpeningAccountEntryDetail;
import com.example.entity.OpeningAccountEntryDetailInvoice;
import com.example.form.OpeningAccountEntryForm;
import com.example.repository.OpeningAccountEntryDetailInvoiceRepository;
import com.example.repository.OpeningAccountEntryDetailRepository;
import com.example.repository.OpeningAccountEntryRepository;
import com.example.view.OpeningAccountEntryView;
import com.example.view.OpeningAccountEntryViewRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nhập số dư ban đầu
 */
@Controller
public class OpeningAccountEntryController {

    @Autowired
    OpeningAccountEntryRepository openingAccountEntryRepository;

    @Autowired
    OpeningAccountEntryDetailRepository openingAccountEntryDetailRepository;

    @Autowired
    OpeningAccountEntryDetailInvoiceRepository detailInvoiceRepository;

    @Autowired
    OpeningAccountEntryViewRepository openingAccountEntryViewRepository;

    private static final Integer FIRST_VERSION = 1;

    ///**
    // * Show OpeningAccountEntry list.
    // *
    // * @return
    // */
    //@Deprecated
    //@RequestMapping(value = "/show_opening_account_entry_editable", method = RequestMethod.GET)
    //public ModelAndView showOpeningAccountEntryListEditable() {
    //    ModelAndView modelAndView = new ModelAndView("opening_account_entry/list_editable");
    //    modelAndView.getModel().put("page_title", "Nhập số dư ban đầu");
    //    return modelAndView;
    //}

    /**
     * Show OpeningAccountEntry list.
     *
     * @return
     */
    @RequestMapping(value = "/opening_account_entries", method = RequestMethod.GET)
    public ModelAndView showOpeningAccountEntryList() {
        ModelAndView modelAndView = new ModelAndView("opening_account_entry/list");
        modelAndView.getModel().put("page_title", "Nhập số dư ban đầu");
        return modelAndView;
    }

    /**
     * JSON for show list all opening_accoun_entry_view in TreeList.
     * Item is OpeningAccountEntryView.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/opening_account_entry_view_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String openingAccountEntryViewJSON() throws JsonProcessingException {
        List<OpeningAccountEntryView> openingAccountEntryViewList = openingAccountEntryViewRepository.findAll();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(openingAccountEntryViewList);
        return jsonDataString;
    }

    /**
     * Hiển thị trang để nhập số dư ban đầu từng tài khoản. Kiểu màn hình Type_normal.
     *
     * @return
     */
    @RequestMapping(value = "/add_opening_account_entry_type_normal", method = RequestMethod.GET)
    public ModelAndView showPageForAddOpenAccountEntryTypeNormal() {
        ModelAndView modelAndView = new ModelAndView("opening_account_entry/add_type_normal");
        OpeningAccountEntryForm openingAccountEntryForm = new OpeningAccountEntryForm();
        List<OpeningAccountEntryDetail> openingAccountEntryDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++){
            OpeningAccountEntryDetail openingAccountEntryDetail = new OpeningAccountEntryDetail();
            openingAccountEntryDetailList.add(openingAccountEntryDetail);
        }
        openingAccountEntryForm.setOpeningAccountEntryDetailList(openingAccountEntryDetailList);
        //List<OpeningAccountEntryDetailInvoice> openingAccountEntryDetailInvoiceList = new ArrayList<>();
        //for (int i = 0; i < 20; i++){
        //    OpeningAccountEntryDetailInvoice openingAccountEntryDetailInvoice = new OpeningAccountEntryDetailInvoice();
        //    openingAccountEntryDetailInvoiceList.add(openingAccountEntryDetailInvoice);
        //}
        //openingAccountEntryForm.setOpeningAccountEntryDetailInvoiceList(openingAccountEntryDetailInvoiceList);
        modelAndView.addObject("openingAccountEntryForm", openingAccountEntryForm);
        modelAndView.getModel().put("page_title", "Nhập số dư ban đầu của tài khoản");
        return modelAndView;
    }

    /**
     * Hiển thị trang để nhập số dư ban đầu tài khoản 131, 331. Kiểu màn hình Type_acc_obj.
     * Một tài khoản liên quan đến nhiều khách hàng, nhiều nhà cung cấp. Mỗi
     * khác hàng hoặc Nhà cung cấp lại có nhiều Dự án, Khoản mục chi phí, Hóa đơn liên quan. Nút submit
     * sẽ là nút Thêm, Thêm và Tiếp tục, Thôi.
     *
     * @return
     */
    @RequestMapping(value = "/add_opening_account_entry_type_acc_obj", method = RequestMethod.GET)
    public ModelAndView showPageForAddOpenAccountEntryTypeAccountObject() {
        ModelAndView modelAndView = new ModelAndView("opening_account_entry/add_type_acc_obj");
        OpeningAccountEntryForm openingAccountEntryForm = new OpeningAccountEntryForm();
        List<OpeningAccountEntryDetail> openingAccountEntryDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++){
            OpeningAccountEntryDetail openingAccountEntryDetail = new OpeningAccountEntryDetail();
            openingAccountEntryDetailList.add(openingAccountEntryDetail);
        }
        openingAccountEntryForm.setOpeningAccountEntryDetailList(openingAccountEntryDetailList);
        List<OpeningAccountEntryDetailInvoice> openingAccountEntryDetailInvoiceList = new ArrayList<>();
        for (int i = 0; i < 20; i++){
            OpeningAccountEntryDetailInvoice openingAccountEntryDetailInvoice = new OpeningAccountEntryDetailInvoice();
            openingAccountEntryDetailInvoiceList.add(openingAccountEntryDetailInvoice);
        }
        openingAccountEntryForm.setOpeningAccountEntryDetailInvoiceList(openingAccountEntryDetailInvoiceList);
        modelAndView.addObject("openingAccountEntryForm", openingAccountEntryForm);
        modelAndView.getModel().put("page_title", "Nhập số dư ban đầu của tài khoản");
        return modelAndView;
    }

    /**
     * Hiển thị trang để nhập số dư ban đầu tài khoản 1121, 1122. Kiểu màn hình
     * Type_bank. Một tài khoản liên quan đến nhiều Tài khoản ngân hàng
     * (BankAccount). Có các nút bấm Lưu, Thôi.
     *
     * @return
     */
    @RequestMapping(value = "/add_opening_account_entry_type_bank", method = RequestMethod.GET)
    public ModelAndView showPageForAddOpenAccountEntryTypeBank() {
        ModelAndView modelAndView = new ModelAndView("opening_account_entry/add_type_bank");
        OpeningAccountEntryForm openingAccountEntryForm = new OpeningAccountEntryForm();
        modelAndView.addObject("openingAccountEntryForm", openingAccountEntryForm);
        modelAndView.getModel().put("page_title", "Nhập số dư ban đầu của tài khoản");
        return modelAndView;
    }

    /**
     * Submit dữ liệu Khai báo số dư đầu kỳ
     *
     * @param openAccountEntryForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_opening_account_entry", method = RequestMethod.POST)
    public String submitAddOpenAccountEntry(@ModelAttribute("openAccountEntryForm") OpeningAccountEntryForm openAccountEntryForm, BindingResult bindingResult, Authentication authentication) {
        // Validate.
        if (bindingResult.hasErrors()) {
            return "opening_account_entry/add";
        }
        // Ghi vào table OpeningAccountEntry.
        OpeningAccountEntry openingAccountEntry = new OpeningAccountEntry();
        openingAccountEntry.setAccountNumber(openAccountEntryForm.getAccountNumber());
        openingAccountEntry.setAccountObjectId(openAccountEntryForm.getAccountObjectId());
        openingAccountEntry.setCurrencyId(openAccountEntryForm.getCurrencyId());
        openingAccountEntry.setExchangeRate(openAccountEntryForm.getExchangeRate());
        openingAccountEntry.setCreditAmountOc(openAccountEntryForm.getCreditAmountOc());
        openingAccountEntry.setCreditAmount(openAccountEntryForm.getCreditAmount());
        openingAccountEntry.setDebitAmountOc(openAccountEntryForm.getDebitAmountOc());
        openingAccountEntry.setCreatedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        openingAccountEntry.setCreatedDate(now);
        openingAccountEntry.setEditVersion(FIRST_VERSION);
        openingAccountEntryRepository.save(openingAccountEntry);
        Integer openingAccountEntryId = openingAccountEntry.getId();

        // Ghi vào table OpeningAccountEntryDetail.
        List<OpeningAccountEntryDetail> openingAccountEntryDetailList = openAccountEntryForm.getOpeningAccountEntryDetailList();
        for (OpeningAccountEntryDetail openingAccountEntryDetail : openingAccountEntryDetailList) {
            // Nếu tồn tại giá trị dư nợ hoặc dư có
            if (openingAccountEntryDetail.getCreditAmountOc().compareTo(BigDecimal.ZERO) != 0 || openingAccountEntryDetail.getDebitAmountOc().compareTo(BigDecimal.ZERO) != 0) {
                openingAccountEntryDetail.setRefId(openingAccountEntryId);
                openingAccountEntryDetailRepository.save(openingAccountEntryDetail);
            }
        }
        // Ghi vào table OpeningAccountEntryDetailInvoice.
        List<OpeningAccountEntryDetailInvoice> openingAccountEntryDetailInvoiceList = openAccountEntryForm.getOpeningAccountEntryDetailInvoiceList();
        for (OpeningAccountEntryDetailInvoice detail : openingAccountEntryDetailInvoiceList) {
            if (!detail.getInvNo().isBlank() && !detail.getInvNo().isEmpty()) {
                detail.setRefId(openingAccountEntryId);
                detailInvoiceRepository.save(detail);
            }
        }
        return "redirect:/opening_account_entries";
    }

}
