package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.PuOrder;
import com.example.entity.PuOrderDetail;
import com.example.form.PUOrderForm;
import com.example.repository.PuOrderDetailRepository;
import com.example.repository.PuOrderRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Đơn mua hàng.
 */
@Controller
public class PuOrderController {

    @Autowired
    PuOrderRepository puOrderRepository;

    @Autowired
    PuOrderDetailRepository puOrderDetailRepository;

    /**
     * Hiển thị trang thêm mới đơn đặt hàng.
     *
     * @return
     */
    public ModelAndView showPageAddPurchaseOrder() {
        ModelAndView modelAndView = new ModelAndView("pu_order/add");
        PUOrderForm puOrderForm = new PUOrderForm();
        List<PuOrderDetail> puOrderDetailList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            PuOrderDetail puOrderDetail = new PuOrderDetail();
            puOrderDetailList.add(puOrderDetail);
        }
        puOrderForm.setPuOrderDetailList(puOrderDetailList);
        modelAndView.addObject("puOrderDetailList", puOrderDetailList);
        modelAndView.getModel().put("page_title", "Thêm mới đơn đặt hàng");
        return modelAndView;
    }


    /**
     * Submit data add purchase order. Đơn mua hàng.
     *
     * @param puOrderForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    public String submitDataAddPurchaseOrder(@ModelAttribute("puOrderForm") @Valid PUOrderForm puOrderForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "pu_order/add";
        }
        PuOrder puOrder = new PuOrder();
        puOrder.setBranchId(1);
        puOrder.setRefDate(puOrderForm.getRefDate());
        puOrder.setRefType(puOrderForm.getRefType());
        puOrder.setRefNo(puOrderForm.getRefNo());
        puOrder.setStatus(puOrderForm.getStatus());
        puOrder.setAccountObjectId(puOrderForm.getAccountObjectId());
        puOrder.setAccountObjectName(puOrderForm.getAccountObjectName());
        puOrder.setAccountObjectAddress(puOrderForm.getAccountObjectAddress());
        puOrder.setAccountObjectTaxCode(puOrderForm.getAccountObjectTaxCode());
        puOrder.setJournalMemo(puOrderForm.getJournalMemo());
        puOrder.setCurrencyId(puOrderForm.getCurrencyId());
        puOrder.setExchangeRate(puOrderForm.getExchangeRate());
        puOrder.setReceiveDate(puOrderForm.getReceiveDate());
        puOrder.setReceiveAddress(puOrderForm.getReceiveAddress());
        puOrder.setPaymentTermId(puOrderForm.getPaymentTermId());
        puOrder.setDueDay(puOrderForm.getDueDay());
        puOrder.setOtherTerm(puOrderForm.getOtherTerm());
        puOrder.setEmployeeId(puOrderForm.getEmployeeId());
        puOrder.setTotalAmountOc(puOrderForm.getTotalAmountOc());
        puOrder.setTotalAmount(puOrderForm.getTotalAmount());
        puOrder.setTotalVatAmountOc(puOrderForm.getTotalVatAmountOc());
        puOrder.setTotalVatAmount(puOrderForm.getTotalVatAmount());
        puOrder.setTotalDiscountAmountOc(puOrderForm.getTotalDiscountAmountOc());
        puOrder.setTotalDiscountAmount(puOrderForm.getTotalDiscountAmount());
        puOrder.setEditVersion(1);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        puOrder.setCreatedBy(authentication.getName());
        puOrder.setCreatedDate(now);
        puOrder.setCustomField1(puOrderForm.getCustomField1());
        puOrder.setCustomField2(puOrderForm.getCustomField2());
        puOrder.setCustomField3(puOrderForm.getCustomField3());
        puOrder.setCustomField4(puOrderForm.getCustomField4());
        puOrder.setCustomField5(puOrderForm.getCustomField5());
        puOrder.setCustomField6(puOrderForm.getCustomField6());
        puOrder.setCustomField7(puOrderForm.getCustomField7());
        puOrder.setCustomField8(puOrderForm.getCustomField8());
        puOrder.setCustomField9(puOrderForm.getCustomField9());
        puOrder.setCustomField10(puOrderForm.getCustomField10());
        puOrderRepository.save(puOrder);
        Integer puOrderId = puOrder.getId();
        for (int i = 0; i < 20; i++) {
            if (!puOrderForm.getPuOrderDetailList().get(i).getDescription().isBlank() &&
                    !puOrderForm.getPuOrderDetailList().get(i).getDescription().isEmpty() &&
                    puOrderForm.getPuOrderDetailList().get(i).getDescription() != null) {
                PuOrderDetail subForm = puOrderForm.getPuOrderDetailList().get(i);
                PuOrderDetail puOrderDetail = new PuOrderDetail();
                puOrderDetail.setRefId(puOrderId);
                puOrderDetail.setInventoryItemId(subForm.getInventoryItemId());
                puOrderDetail.setDescription(subForm.getDescription());
                puOrderDetail.setUnitId(subForm.getUnitId());
                puOrderDetail.setQuantity(subForm.getQuantity());
                puOrderDetail.setQuantityReceipt(subForm.getQuantityReceipt());
                puOrderDetail.setUnitPrice(subForm.getUnitPrice());
                puOrderDetail.setAmount(subForm.getAmount());
                puOrderDetail.setDiscountRate(subForm.getDiscountRate());
                puOrderDetail.setDiscountAmount(subForm.getDiscountAmount());
                puOrderDetail.setVatRate(subForm.getVatRate());
                puOrderDetail.setVatAmount(subForm.getVatAmount());
                puOrderDetail.setSortOrder(subForm.getSortOrder());
                puOrderDetail.setAmountOc(subForm.getAmountOc());
                puOrderDetail.setDiscountAmountOc(subForm.getDiscountAmountOc());
                puOrderDetail.setVatAmountOc(subForm.getVatAmountOc());
                puOrderDetail.setCustomField1(subForm.getCustomField1());
                puOrderDetail.setCustomField2(subForm.getCustomField2());
                puOrderDetail.setCustomField3(subForm.getCustomField3());
                puOrderDetail.setCustomField4(subForm.getCustomField4());
                puOrderDetail.setCustomField5(subForm.getCustomField5());
                puOrderDetail.setCustomField6(subForm.getCustomField6());
                puOrderDetail.setCustomField7(subForm.getCustomField7());
                puOrderDetail.setCustomField8(subForm.getCustomField8());
                puOrderDetail.setCustomField9(subForm.getCustomField9());
                puOrderDetail.setCustomField10(subForm.getCustomField10());
                puOrderDetail.setMainUnitId(subForm.getMainUnitId());
                puOrderDetail.setMainUnitPrice(subForm.getMainUnitPrice());
                puOrderDetail.setMainConvertRate(subForm.getMainConvertRate());
                puOrderDetail.setExchangeRateOperator(subForm.getExchangeRateOperator());
                puOrderDetail.setMainQuantity(subForm.getMainQuantity());
                puOrderDetail.setExpenseItemId(subForm.getExpenseItemId());
                puOrderDetail.setOrganizationUnitId(subForm.getOrganizationUnitId());
                puOrderDetail.setJobId(subForm.getJobId());
                puOrderDetail.setProjectWorkId(subForm.getProjectWorkId());
                puOrderDetail.setOrderId(subForm.getOrderId());
                puOrderDetail.setContractId(subForm.getContractId());
                puOrderDetail.setListItemId(subForm.getListItemId());
                puOrderDetail.setQuantityReceiptLastYear(subForm.getQuantityReceiptLastYear());
                puOrderDetail.setSaOrderRefDetailId(subForm.getSaOrderRefDetailId());
                puOrderDetail.setStockId(subForm.getStockId());
                puOrderDetail.setInProductionOrderRefId(subForm.getInProductionOrderRefId());
                puOrderDetail.setProductionId(subForm.getProductionId());
                puOrderDetail.setUnitPriceAfterTax(subForm.getUnitPriceAfterTax());
                puOrderDetailRepository.save(puOrderDetail);
            }
        }
        return "redirect:/desktop";
    }

}
