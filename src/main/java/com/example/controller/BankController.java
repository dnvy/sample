package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.common.UtilityList;
import com.example.entity.Bank;
import com.example.repository.BankRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Ngân hàng (Ví dụ VietComBank, BIDV, ACB, v.v..)
 */
@Controller
public class BankController {

    @Autowired
    BankRepository bankRepository;

    /**
     * Show list of banks.
     *
     * @return
     */
    @RequestMapping(value = "/banks", method = RequestMethod.GET)
    public ModelAndView showAllBanks() {
        ModelAndView modelAndView = new ModelAndView("bank/list");
        UtilityList.setActiveStatusList(modelAndView);
        // Tìm tất cả các ngân hàng, kể cả có ActiveStatus = 0.
        List<Bank> banks = bankRepository.findAll();
        modelAndView.addObject("banks", banks);
        modelAndView.getModel().put("page_title", "Ngân hàng");
        return modelAndView;
    }

    /**
     * Delete a bank.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/bank/delete/{id}", method = RequestMethod.POST)
    public String deleteABank(@PathVariable("id") Integer id, HttpServletRequest request) {
        Bank bank = bankRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account object id: " + id));
        //TODO: Check related FK.
        bankRepository.delete(bank);
        return "redirect:" + request.getHeader("Referer");
    }

    /**
     * All Banks has active_status = TRUE in JSON.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/banks_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String bankJSON() throws JsonProcessingException {
        List<Bank> bankList = bankRepository.findByActiveStatus(Boolean.TRUE);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(bankList);
        return jsonDataString;
    }

    /**
     * Show page for adding bank.
     *
     * @return
     */
    @RequestMapping(value = "/add_bank", method = RequestMethod.GET)
    public ModelAndView showPageForAddBank() {
        Bank bank = new Bank();
        ModelAndView modelAndView = new ModelAndView("bank/add");
        modelAndView.addObject("bank", bank);
        modelAndView.getModel().put("page_title", "Thêm Ngân hàng");
        return modelAndView;
    }

    /**
     * View detail a bank.
     *
     * @return
     */
    @RequestMapping(value = "/bank/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewBank(@PathVariable("id") Integer id) {
        Bank bank = bankRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("bank/view");
        modelAndView.addObject("bank", bank);
        UtilityList.setActiveStatusList(modelAndView);
        return modelAndView;
    }

    /**
     * Show page for editing a bank.
     *
     * @return
     */
    @RequestMapping(value = "/bank/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditBank(@PathVariable("id") Integer id) {
        Bank bank = bankRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("bank/edit");
        modelAndView.addObject("bank", bank);
        return modelAndView;
    }

    /**
     * Submit data when edit a bank.
     *
     * @return
     */
    @RequestMapping(value = "/submit_update_bank", method = RequestMethod.POST)
    public String submitDataWhenEditBank(@ModelAttribute("accountObject") @Valid Bank newBank, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "bank/bank/edit";
        }
        Bank oldBank = bankRepository.findById(newBank.getId()).get();
        newBank.setCreatedBy(oldBank.getCreatedBy());
        newBank.setCreatedDate(oldBank.getCreatedDate());
        newBank.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        newBank.setModifiedDate(now);
        bankRepository.save(newBank);
        return "redirect:/banks";
    }

    /**
     * Submit data when adding bank.
     *
     * @return
     */
    @RequestMapping(value = "/submit_add_bank", method = RequestMethod.POST)
    public String submitAddBank(@ModelAttribute("accountObject") @Valid Bank bank, BindingResult bindingResult, Authentication authentication) {
        // FIXME: chuyển sang dùng BankForm, có cả upload file logo của bank.
        if (bindingResult.hasErrors()) {
            return "bank/add";
        }
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        bank.setCreatedDate(now);
        bank.setCreatedBy(authentication.getName());
        // Khi tạo mới thì ActiveStatus = TRUE.
        bank.setActiveStatus(Boolean.TRUE);
        bankRepository.save(bank);
        return "redirect:/banks";
    }

}
