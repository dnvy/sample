package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.InventoryItemCategoryDTO;
import com.example.entity.InventoryItemCategory;
import com.example.form.InventoryItemCategoryForm;
import com.example.repository.InventoryItemCategoryRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Nhóm vật tư, hàng hóa, dịch vụ.
 */
@Controller
public class InventoryItemCategoryController {

    @Autowired
    InventoryItemCategoryRepository inventoryItemCategoryRepository;

    /**
     * Hiển thị trang để thêm InventoryItemCategory.
     *
     * @return
     */
    @RequestMapping(value = "/add_inventory_item_category", method = RequestMethod.GET)
    public ModelAndView showPageForAddInventoryItemCategory() {
        ModelAndView modelAndView = new ModelAndView("inventory_item_category/add");
        InventoryItemCategoryForm inventoryItemCategoryForm = new InventoryItemCategoryForm();
        modelAndView.addObject("inventoryItemCategoryForm", inventoryItemCategoryForm);
        return modelAndView;
    }

    /**
     * Submit dữ liệu thêm InventoryItemCategory.
     *
     * @return
     */
    @RequestMapping(value = "/add_inventory_item_category", method = RequestMethod.POST)
    public String submitDataAddInventoryItemCategory(InventoryItemCategoryForm inventoryItemCategoryForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "inventory_item_category/add";
        }
        InventoryItemCategory inventoryItemCategory = new InventoryItemCategory();
        inventoryItemCategory.setInventoryCategoryCode(inventoryItemCategoryForm.getInventoryCategoryCode());
        inventoryItemCategory.setInventoryCategoryName(inventoryItemCategoryForm.getInventoryCategoryName());
        inventoryItemCategory.setVyCodeId(inventoryItemCategoryForm.getVyCodeId());
        inventoryItemCategory.setGrade(2); // Chưa rõ tác dụng.
        inventoryItemCategory.setDescription(inventoryItemCategoryForm.getDescription());
        inventoryItemCategory.setIsParent(Boolean.FALSE);
        // Khi mới thêm, thì ActiveStatus = TRUE.
        inventoryItemCategory.setActiveStatus(Boolean.TRUE);
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        inventoryItemCategory.setCreatedDate(now);
        inventoryItemCategory.setCreatedBy(authentication.getName());
        inventoryItemCategory.setIsSystem(Boolean.FALSE); // Do người dùng thêm vào.
        inventoryItemCategory.setIsParent(Boolean.FALSE); // Chưa rõ tác dụng. NOT NULL.
        if (inventoryItemCategoryForm.getParentId() == null) {
            inventoryItemCategory.setParentId(0);
        } else {
            inventoryItemCategory.setParentId(inventoryItemCategoryForm.getParentId());
            InventoryItemCategory inventoryItemCategoryRealted = inventoryItemCategoryRepository.findById(inventoryItemCategoryForm.getParentId()).get();
            inventoryItemCategoryRealted.setIsParent(Boolean.TRUE);
            inventoryItemCategoryRepository.save(inventoryItemCategoryRealted);
        }
        inventoryItemCategoryRepository.save(inventoryItemCategory);
        return "redirect:/inventory_item_categories";
    }

    /**
     * Show list all inventory_item_categories (activeStatus = 0 and 1).
     *
     * @return
     */
    @RequestMapping(value = "/inventory_item_categories", method = RequestMethod.GET)
    public String showListInventoryItemCategories() {
        return "inventory_item_category/list";
    }

    /**
     * JSON multi-columns. Trả về tất cả các item, có ActiveStatus 0 và 1.
     * Dùng cho TreeList ở màn hình danh sách.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/inventory_item_categories_dto_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String inventoryItemCategoriesJSON() throws JsonProcessingException {
        List<InventoryItemCategory> inventoryItemCategoryList = inventoryItemCategoryRepository.findAll();
        List<InventoryItemCategoryDTO> inventoryItemCategoryDTOList = new ArrayList<>();
        for (InventoryItemCategory inventoryItemCategory : inventoryItemCategoryList) {
            InventoryItemCategoryDTO inventoryItemCategoryDTO = new InventoryItemCategoryDTO();
            BeanUtils.copyProperties(inventoryItemCategory, inventoryItemCategoryDTO);
            if (inventoryItemCategory.getActiveStatus() == Boolean.TRUE) {
                inventoryItemCategoryDTO.setActiveStatusString("Có");
            }
            if (inventoryItemCategory.getActiveStatus() == Boolean.FALSE) {
                inventoryItemCategoryDTO.setActiveStatusString("Không");
            }
            inventoryItemCategoryDTOList.add(inventoryItemCategoryDTO);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(inventoryItemCategoryDTOList);
        return jsonDataString;
    }

    /**
     * JSON multi-columns. Trả về tất cả các item, có ActiveStatus 0 và 1.
     * Dùng cho TreeList ở màn hình danh sách.
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/active_inventory_item_categories_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String activeInventoryItemCategoriesJSON() throws JsonProcessingException {
        List<InventoryItemCategory> inventoryItemCategoryList = inventoryItemCategoryRepository.getAllActiveInventoryItemCategories();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(inventoryItemCategoryList);
        return jsonDataString;
    }

    /**
     * Delete an inventory_item_category.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/inventory_item_category/delete/{id}", method = RequestMethod.POST)
    public String deleteInventoryItemCategory(@PathVariable("id") Integer id, HttpServletRequest request) {
        InventoryItemCategory inventoryItemCategory = inventoryItemCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid job id: " + id));
        inventoryItemCategoryRepository.delete(inventoryItemCategory);
        return "redirect:/inventory_item_categories";
    }

    /**
     * Hiển thị trang để thêm InventoryItemCategory.
     *
     * @return
     */
    @RequestMapping(value = "/view_inventory_item_category/{id}", method = RequestMethod.GET)
    public ModelAndView viewInventoryItemCategory(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("inventory_item_category/view");
        Optional<InventoryItemCategory> inventoryItemCategoryOptional = inventoryItemCategoryRepository.findById(id);
        InventoryItemCategory inventoryItemCategory = inventoryItemCategoryOptional.get();
        InventoryItemCategoryForm inventoryItemCategoryForm = new InventoryItemCategoryForm();
        BeanUtils.copyProperties(inventoryItemCategory, inventoryItemCategoryForm);
        modelAndView.addObject("inventoryItemCategoryForm", inventoryItemCategoryForm);
        return modelAndView;
    }

    /**
     * Hiển thị trang để edit InventoryItemCategory.
     *
     * @return
     */
    @RequestMapping(value = "/inventory_item_category/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageForEditInventoryItemCategory(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("inventory_item_category/edit");
        Optional<InventoryItemCategory> inventoryItemCategoryOptional = inventoryItemCategoryRepository.findById(id);
        InventoryItemCategory inventoryItemCategory = inventoryItemCategoryOptional.get();
        InventoryItemCategoryForm inventoryItemCategoryForm = new InventoryItemCategoryForm();
        BeanUtils.copyProperties(inventoryItemCategory, inventoryItemCategoryForm);
        modelAndView.addObject("inventoryItemCategoryForm", inventoryItemCategoryForm);
        return modelAndView;
    }

    /**
     * Submit dữ liệu edit InventoryItemCategory.
     *
     * @return
     */
    @RequestMapping(value = "/edit_inventory_item_category", method = RequestMethod.POST)
    public String submitDataEditInventoryItemCategory(@Valid @ModelAttribute("inventoryItemCategoryForm") InventoryItemCategoryForm inventoryItemCategoryForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "inventory_item_category/edit";
        }
        Optional<InventoryItemCategory> inventoryItemCategoryOptional = inventoryItemCategoryRepository.findById(inventoryItemCategoryForm.getId());
        InventoryItemCategory inventoryItemCategoryOld = inventoryItemCategoryOptional.get();
        InventoryItemCategory inventoryItemCategory = new InventoryItemCategory();
        inventoryItemCategory.setId(inventoryItemCategoryForm.getId());
        inventoryItemCategory.setCreatedBy(inventoryItemCategoryOld.getCreatedBy());
        inventoryItemCategory.setCreatedDate(inventoryItemCategoryOld.getCreatedDate());
        inventoryItemCategory.setInventoryCategoryCode(inventoryItemCategoryForm.getInventoryCategoryCode());
        inventoryItemCategory.setInventoryCategoryName(inventoryItemCategoryForm.getInventoryCategoryName());
        inventoryItemCategory.setVyCodeId(inventoryItemCategoryForm.getVyCodeId());
        inventoryItemCategory.setGrade(2); // Chưa rõ tác dụng.
        inventoryItemCategory.setDescription(inventoryItemCategoryForm.getDescription());
        if (inventoryItemCategoryForm.getActiveStatus() == null) {
            inventoryItemCategory.setActiveStatus(Boolean.FALSE);
        } else {
            inventoryItemCategory.setActiveStatus(Boolean.TRUE);
        }
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        inventoryItemCategory.setCreatedDate(now);
        inventoryItemCategory.setCreatedBy(authentication.getName());
        inventoryItemCategory.setIsSystem(Boolean.FALSE); // Do người dùng thêm vào.
        inventoryItemCategory.setIsParent(Boolean.FALSE); // Chưa rõ tác dụng. NOT NULL.
        //FIXME: Khi con của nó đang ActiveStatus = 1 mà cho phép cha của nó ActiveStatus = 0 là sai.
        // Kiểm tra xem có con không?
        // Phải tìm cả bản ghi có ActiveStatus = 0.
        // Đối tượng edit là cha ở trạng thái đang theo dõi.. Có con mang AS = 1, báo lỗi. Không cho cha chuyển sang trạng thái không theo dõi.
        if (inventoryItemCategoryForm.getActiveStatus() == null && inventoryItemCategoryOld.getActiveStatus() == Boolean.TRUE) {
            List<InventoryItemCategory> children = inventoryItemCategoryRepository.findByParentId(inventoryItemCategoryOld.getId());
            if (children.size() > 0) {
                for (InventoryItemCategory child : children) {
                    if (child.getActiveStatus() == Boolean.TRUE) {
                        //ObjectError objectError = new ObjectError("activeStatus", "Hiện đang có Nhóm con phụ thuộc vào Nhóm bạn đang sửa, nên không thể thay đổi trạng thái theo dõi được. Cần bỏ trạng thái theo dõi hoặc xóa bỏ nhóm con trước đã.");
                        //bindingResult.addError(objectError);
                        //bindingResult.reject("activeStatus", "Hiện đang có Nhóm con phụ thuộc vào Nhóm bạn đang sửa, nên không thể thay đổi trạng thái theo dõi được. Cần bỏ trạng thái theo dõi hoặc xóa bỏ nhóm con trước đã.");
                        bindingResult.rejectValue("activeStatus", null, "Hiện đang có Nhóm con phụ thuộc vào Nhóm bạn đang sửa, nên không thể thay đổi trạng thái theo dõi được. Cần bỏ trạng thái theo dõi hoặc xóa bỏ nhóm con trước đã.");
                        return "inventory_item_category/edit";
                    }
                }
            }
        }

        // Đối tượng edit là con, có AS = 0. Cha có AS = 0. Không cho con đổi AS sang 1.
        // Đi tìm cha của nó
        Optional<InventoryItemCategory> fatherOptional = inventoryItemCategoryRepository.findById(inventoryItemCategoryOld.getParentId());
        if(fatherOptional.isPresent()){ // Nếu có 1 cha.
            InventoryItemCategory father = fatherOptional.get();
            if(inventoryItemCategoryForm.getActiveStatus() != null && father.getActiveStatus() == Boolean.FALSE){
                bindingResult.rejectValue("activeStatus", null, "Nhóm cha KHÔNG theo dõi, việc sửa Nhóm con (đối tượng này đang sửa) từ chế độ KHÔNG sang CÓ theo dõi là vô lý.");
                return "inventory_item_category/edit";
            }
        }

        // 3. Đối tượng đang sửa là cha, có AS = 1. Muốn đổi AS sang 0. Nhưng con của nó đang có AS = 1
        // 3.1. Kiểm tra xem nó có phải là cha không
        List<InventoryItemCategory> children3 = inventoryItemCategoryRepository.findByParentId(inventoryItemCategoryOld.getId());
        if(children3.size() > 0 && inventoryItemCategoryOld.getActiveStatus() == Boolean.TRUE && inventoryItemCategoryForm.getActiveStatus() == null){
            // Kiểm tra xem con của nó có AS = 1 không
            for (InventoryItemCategory child : children3){
                if(child.getActiveStatus() == Boolean.TRUE){
                    bindingResult.rejectValue("activeStatus", null, "Bản ghi bạn đang sửa là nhóm cha, muốn chuyển trạng thái theo dõi từ CÓ --> KHÔNG, trong khi Nhóm con của nó đang theo dõi. Nên bạn phải hủy theo dõi nhóm con của nó trước.");
                    return "inventory_item_category/edit";
                }
            }
        }

        if (inventoryItemCategoryForm.getParentId() == null) {
            inventoryItemCategory.setParentId(0);
        } else {
            inventoryItemCategory.setParentId(inventoryItemCategoryForm.getParentId());
        }
        inventoryItemCategoryRepository.save(inventoryItemCategory);
        return "redirect:/inventory_item_categories";
    }

}
