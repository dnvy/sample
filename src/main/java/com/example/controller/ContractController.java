package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.ContractDropDownItem;
import com.example.entity.Contract;
import com.example.entity.ContractDetailExpense;
import com.example.entity.ContractDetailInventoryItem;
import com.example.form.ContractForm;
import com.example.repository.ContractDetailExpenseRepository;
import com.example.repository.ContractDetailInventoryItemRepository;
import com.example.repository.ContractRepository;
import com.example.repository.SysAutoIdRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Hợp đồng bán.
 */
@Controller
public class ContractController {

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    ContractDetailExpenseRepository contractDetailExpenseRepository;

    @Autowired
    ContractDetailInventoryItemRepository contractDetailInventoryItemRepository;

    @Autowired
    SysAutoIdRepository sysAutoIdRepository;

    /**
     * Khi thêm hợp đồng từ màn hình này, mã tham chiếu là 9041.
     */
    private static final Integer REF_TYPE_ADD_CONTRACT = 9041;

    /**
     * Show page for adding sale contract.
     *
     * @return
     */
    @RequestMapping(value = "/add_contract", method = RequestMethod.GET)
    public ModelAndView showPageAddContract() {
        ModelAndView modelAndView = new ModelAndView("contract/add");
        ContractForm contractForm = new ContractForm();
        SysAutoIdController sysAutoIdController = new SysAutoIdController();
        String hd = sysAutoIdController.generateVoucherNumber(REF_TYPE_ADD_CONTRACT, sysAutoIdRepository);
        contractForm.setContractCode(hd);
        List<ContractDetailExpense> contractDetailExpenseList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ContractDetailExpense contractDetailExpense = new ContractDetailExpense();
            contractDetailExpenseList.add(contractDetailExpense);
        }
        contractForm.setContractDetailExpenseList(contractDetailExpenseList);
        List<ContractDetailInventoryItem> contractDetailInventoryItemList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ContractDetailInventoryItem contractDetailInventoryItem = new ContractDetailInventoryItem();
            contractDetailInventoryItemList.add(contractDetailInventoryItem);
        }
        contractForm.setContractDetailInventoryItemList(contractDetailInventoryItemList);
        modelAndView.addObject("contractForm", contractForm);
        modelAndView.getModel().put("page_title", "Thêm hợp đồng");
        return modelAndView;
    }

    /**
     * Để hiển thị drop-down list có các cột
     * 1. Mã dự án
     * 2. Ngày ký
     * 3. Trích yếu
     * 4. Khách hàng
     * <p>
     * Dùng trong form Add Contract.
     *
     * @return
     * @throws JsonProcessingException
     * @see #showPageAddContract()
     */
    @RequestMapping(value = "/all_active_contracts_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String allActiveContractsJSON() throws JsonProcessingException {
        // Không có field ActiveStatus, nên sẽ lấy tất cả.
        List<Contract> contractList = contractRepository.findAll();
        List<ContractDropDownItem> contractDropDownItemList = new ArrayList<>();
        if (contractDropDownItemList.size() > 0) {
            for (Contract contract : contractList) {
                ContractDropDownItem contractDropDownItem = new ContractDropDownItem();
                contractDropDownItem.setContractCode(contract.getContractCode());
                contractDropDownItem.setContractSubject(contract.getContractSubject());
                contractDropDownItem.setSignDate(contract.getSignDate());
                contractDropDownItem.setAccountObjectName(contract.getAccountObjectName());
                contractDropDownItemList.add(contractDropDownItem);
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(contractDropDownItemList);
        return jsonDataString;
    }

    /**
     * Submit data add contract.
     *
     * @param contractForm
     * @param bindingResult
     * @param authentication
     * @return
     */
    @RequestMapping(value = "/add_contract", method = RequestMethod.POST)
    public String submitDataAddContract(@ModelAttribute("contractForm") @Valid ContractForm contractForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "contract/add";
        }
        Contract contract = new Contract();
        contract.setBranchId(1);
        contract.setIsProject(contractForm.getIsProject());
        contract.setProjectId(contractForm.getProjectId());
        contract.setContractCode(contractForm.getContractCode());
        contract.setSignDate(contractForm.getSignDate());
        contract.setContractSubject(contractForm.getContractSubject());
        contract.setCurrencyId(contractForm.getCurrencyId());
        contract.setExchangeRate(contractForm.getExchangeRate());
        contract.setContractAmountOc(contractForm.getContractAmountOc());
        contract.setContractAmount(contractForm.getContractAmount());
        contract.setCloseAmountOc(contractForm.getCloseAmountOc());
        contract.setCloseAmount(contractForm.getCloseAmount());
        contract.setPaymentDate(contractForm.getPaymentDate());
        contract.setDeliveryDate(contractForm.getDeliveryDate());
        if (contractForm.getIsArisedBeforeUseSoftware() == null){
            contract.setIsArisedBeforeUseSoftware(Boolean.FALSE);
        }else {
            contract.setIsArisedBeforeUseSoftware(Boolean.TRUE);
        }
        contract.setExpenseAmountFinance(contractForm.getExpenseAmountFinance());
        contract.setReceiptAmountFinance(contractForm.getReceiptAmountFinance());
        contract.setReceiptAmountOcFinance(contractForm.getReceiptAmountOcFinance());
        contract.setExpenseAmountFinance(contractForm.getExpenseAmountFinance());
        contract.setInvoiceAmountOcFinance(contractForm.getInvoiceAmountOcFinance());
        contract.setInvoiceAmountFinance(contractForm.getInvoiceAmountFinance());
        contract.setAccountObjectId(contractForm.getAccountObjectId());
        contract.setAccountObjectAddress(contractForm.getAccountObjectAddress());
        contract.setAccountObjectContactName(contractForm.getAccountObjectContactName());
        contract.setOrganizationUnitId(contractForm.getOrganizationUnitId());
        contract.setEmployeeId(contractForm.getEmployeeId());
        contract.setContractStatusId(contractForm.getContractStatusId());
        contract.setCloseDate(contractForm.getCloseDate());
        contract.setCloseReason(contractForm.getCloseReason());
        contract.setRevenueStatus(contractForm.getRevenueStatus());
        contract.setOtherTerms(contractForm.getOtherTerms());
        if (contractForm.getIsCalculatedCost() == null){
            contract.setIsCalculatedCost(Boolean.FALSE);
        }else {
            contract.setIsCalculatedCost(Boolean.TRUE);
        }
        if (contractForm.getIsInvoiced() == null){
            contract.setIsInvoiced(Boolean.FALSE);
        }else {
            contract.setIsInvoiced(Boolean.TRUE);
        }
        contract.setRevenueStatus(contractForm.getRevenueStatus());
        contract.setTotalExpenseExpectAmount(contractForm.getTotalExpenseExpectAmount());
        contract.setTotalExpensedAmount(contractForm.getTotalExpensedAmount());
        contract.setBalanceExpenseAmountFinance(contractForm.getBalanceExpenseAmountFinance());
        contract.setTotalReceiptedAmount(contractForm.getTotalReceiptedAmount());
        contract.setBalanceReceiptAmountFinance(contractForm.getBalanceReceiptAmountFinance());
        contract.setProfitAndLossExpectAmountFinance(contractForm.getProfitAndLossExpectAmountFinance());
        contract.setCreatedBy(authentication.getName());
        contract.setAccumCostAmountManagement(contractForm.getAccumCostAmountManagement()); // NOT NULL.
        contract.setAccumCostAmountFinance(contractForm.getAccumCostAmountFinance()); // NOT NULL.
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        contract.setCreatedDate(now);
        contract.setRefType(REF_TYPE_ADD_CONTRACT);
        contract.setAccountObjectName(contractForm.getAccountObjectName());
        contractRepository.save(contract);
        Integer contractId = contract.getId();

        // Save vào bảng ContractDetailExpense.
        for (int i = 0; i < 20; i++) {
            if (!contractForm.getContractDetailExpenseList().get(i).getDescription().isBlank() &&
                    !contractForm.getContractDetailExpenseList().get(i).getDescription().isEmpty()) {
                ContractDetailExpense contractDetailExpense = contractForm.getContractDetailExpenseList().get(i);
                contractDetailExpense.setContractId(contractId);
                contractDetailExpenseRepository.save(contractDetailExpense);
            }
        }

        // Save vào bảng ContractDetailInventoryItem
        for (int i = 0; i < 20; i++) {
            if (!contractForm.getContractDetailInventoryItemList().get(i).getDescription().isBlank() &&
                    !contractForm.getContractDetailInventoryItemList().get(i).getDescription().isEmpty()) {
                ContractDetailInventoryItem contractDetailInventoryItem = contractForm.getContractDetailInventoryItemList().get(i);
                contractDetailInventoryItem.setContractId(contractId);
                contractDetailInventoryItemRepository.save(contractDetailInventoryItem);
            }
        }
        return "redirect:/contracts";
    }


}
