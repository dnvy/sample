package com.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.example.dto.ExpenseItemDTO;
import com.example.dropdownlist.ExpenseItemDropDownItem;
import com.example.entity.ExpenseItem;
import com.example.form.ExpenseItemForm;
import com.example.repository.ExpenseItemRepository;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ExpenseItemController {

    @Autowired
    ExpenseItemRepository expenseItemRepository;

    ///**
    // * Show list all expense items in Flat fashion.
    // *
    // * @return
    // * @see #showExpenseItemsTreeList
    // */
    //@Deprecated
    //@RequestMapping(value = "/expense_items_flat", method = RequestMethod.GET)
    //public ModelAndView listAllExpenseItems() {
    //    ModelAndView modelAndView = new ModelAndView("expense_item/list_flat");
    //    UtilityList.setActiveStatusList(modelAndView);
    //    List<ExpenseItem> expenseItemList = expenseItemRepository.findAll();
    //    modelAndView.addObject("expenseItemList", expenseItemList);
    //    modelAndView.getModel().put("page_title", "Danh sách Khoản mục chi phí");
    //    return modelAndView;
    //}

    /**
     * Show all Expense items in TreeList.
     *
     * @return
     */
    @RequestMapping(value = "/expense_items", method = RequestMethod.GET)
    public ModelAndView showExpenseItemsTreeList() {
        ModelAndView modelAndView = new ModelAndView("expense_item/list");
        modelAndView.getModel().put("page_title", "Danh sách Khoản mục chi phí");
        return modelAndView;
    }

    /**
     * JSON for show list all expense_items in TreeList (List screen).
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/expense_item_dto_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String allExpenseItemJSON() throws JsonProcessingException {
        // Tìm cả các mục có ActiveStatus = 0 và 1.
        List<ExpenseItem> expenseItemList = expenseItemRepository.findAll();
        List<ExpenseItemDTO> expenseItemDTOList = new ArrayList<>();
        for (ExpenseItem expenseItem : expenseItemList) {
            ExpenseItemDTO expenseItemDTO = new ExpenseItemDTO();
            expenseItemDTO.setId(expenseItem.getId());
            expenseItemDTO.setParentId(expenseItem.getParentId());
            expenseItemDTO.setActiveStatus(expenseItem.getActiveStatus());
            if (expenseItem.getActiveStatus() == Boolean.TRUE) {
                expenseItemDTO.setActiveStatusString("Có");
            } else {
                expenseItemDTO.setActiveStatusString("Không");
            }
            expenseItemDTO.setExpenseItemCode(expenseItem.getExpenseItemCode());
            expenseItemDTO.setExpenseItemName(expenseItem.getExpenseItemName());
            expenseItemDTO.setDescription(expenseItem.getDescription());
            expenseItemDTOList.add(expenseItemDTO);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(expenseItemDTOList);
        return jsonDataString;
    }

    /**
     * Delete an expense item.
     *
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/expense_item/delete/{id}", method = RequestMethod.POST)
    public String deleteExpenseItem(@PathVariable("id") Integer id, HttpServletRequest request) {
        ExpenseItem expenseItem = expenseItemRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exepense_item id: " + id));
        //TODO: Check related FK.
        expenseItemRepository.delete(expenseItem);
        // return "redirect:" + request.getHeader("Referer");
        return "redirect:/expense_items";
    }

    /**
     * List all expense_item has IsActive = 1, in JSON. (For multi-column drop-down TreeList)
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/expense_items_json", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String expenseItemForDropDownListJSON() throws JsonProcessingException {
        List<ExpenseItem> expenseItemList = expenseItemRepository.findAllActiveExpenseItem();
        List<ExpenseItemDropDownItem> expenseItemDropDownItemList = new ArrayList<>();
        for (ExpenseItem expenseItem : expenseItemList) {
            ExpenseItemDropDownItem expenseItemDropDownItem = new ExpenseItemDropDownItem();
            expenseItemDropDownItem.setId(expenseItem.getId());
            expenseItemDropDownItem.setExpenseItemCode(expenseItem.getExpenseItemCode());
            expenseItemDropDownItem.setExpenseItemName(expenseItem.getExpenseItemName());
            expenseItemDropDownItem.setParentId(expenseItem.getParentId());
            expenseItemDropDownItemList.add(expenseItemDropDownItem);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonDataString = objectMapper.writeValueAsString(expenseItemDropDownItemList);
        return jsonDataString;
    }

    /**
     * Show page for adding expense_item.
     *
     * @return
     */
    @RequestMapping(value = "/add_expense_item", method = RequestMethod.GET)
    public ModelAndView showPageAddExpenseItem() {
        ModelAndView modelAndView = new ModelAndView("expense_item/add");
        ExpenseItemForm expenseItemForm = new ExpenseItemForm();
        modelAndView.addObject("expenseItemForm", expenseItemForm);
        return modelAndView;
    }

    /**
     * Submit data for adding expense_item.
     *
     * @return
     */
    @RequestMapping(value = "/add_expense_item", method = RequestMethod.POST)
    public String submitDataForAddExpenseItem(@ModelAttribute("expenseItemForm") ExpenseItemForm expenseItemForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "expense_item/add";
        }
        ExpenseItem expenseItem = new ExpenseItem();
        // Khi thêm mới thì ActiveStatus = TRUE.
        expenseItem.setActiveStatus(Boolean.TRUE);
        expenseItem.setCreatedBy(authentication.getName());
        expenseItem.setDescription(expenseItemForm.getDescription());
        expenseItem.setExpenseItemCode(expenseItemForm.getExpenseItemCode());
        expenseItem.setExpenseItemName(expenseItemForm.getExpenseItemName());
        if (expenseItemForm.getParentId() == null) {
            expenseItem.setParentId(0);
        } else {
            expenseItem.setParentId(expenseItemForm.getParentId());
        }
        expenseItem.setGrade(2);
        expenseItem.setIsParent(Boolean.FALSE); // NOT NULL. Không rõ trường này để làm gì.
        expenseItem.setIsSystem(Boolean.FALSE); // Trường có sẵn khi khởi tạo công ty trên phần mềm kế toán.
        expenseItemRepository.save(expenseItem);
        return "redirect:/expense_items";
    }

    /**
     * View detail an expense_item.
     *
     * @return
     */
    @RequestMapping(value = "/expense_item/view/{id}", method = RequestMethod.GET)
    public ModelAndView viewAnExpenseItem(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("expense_item/view");
        ExpenseItemForm expenseItemForm = new ExpenseItemForm();
        Optional<ExpenseItem> expenseItemOptional = expenseItemRepository.findById(id);
        ExpenseItem expenseItem = expenseItemOptional.get();
        expenseItemForm.setExpenseItemCode(expenseItem.getExpenseItemCode());
        expenseItemForm.setExpenseItemName(expenseItem.getExpenseItemName());
        expenseItemForm.setDescription(expenseItem.getDescription());
        expenseItemForm.setParentId(expenseItem.getParentId());
        expenseItemForm.setActiveStatus(expenseItem.getActiveStatus());
        modelAndView.addObject("expenseItemForm", expenseItemForm);
        return modelAndView;
    }

    /**
     * Show page edit an expense_item.
     *
     * @return
     */
    @RequestMapping(value = "/expense_item/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showPageEditExpenseItem(@PathVariable("id") Integer id) {
        //FIXME: Có lỗi khi đổ ra drop-down list cho phép chọn cha là chính nó.
        // Khi lỗi cấu trúc Tree thì không show ra dữ liệu. Cần các câu lệnh IF để
        // chặn ở phía back-end các case gây ra lỗi
        ModelAndView modelAndView = new ModelAndView("expense_item/edit");
        ExpenseItemForm expenseItemForm = new ExpenseItemForm();
        Optional<ExpenseItem> expenseItemOptional = expenseItemRepository.findById(id);
        ExpenseItem expenseItem = expenseItemOptional.get();
        expenseItemForm.setId(expenseItem.getId());
        expenseItemForm.setExpenseItemCode(expenseItem.getExpenseItemCode());
        expenseItemForm.setActiveStatus(expenseItem.getActiveStatus());
        expenseItemForm.setExpenseItemName(expenseItem.getExpenseItemName());
        expenseItemForm.setDescription(expenseItem.getDescription());
        expenseItemForm.setParentId(expenseItem.getParentId());
        modelAndView.addObject("expenseItemForm", expenseItemForm);
        return modelAndView;
    }

    /**
     * Update, submit data an expense_item.
     *
     * @return
     */
    @RequestMapping(value = "/update_expense_item", method = RequestMethod.POST)
    public String updateAnExpenseItem(@ModelAttribute("expenseItemForm") @Valid ExpenseItemForm expenseItemForm, BindingResult bindingResult, Authentication authentication) {
        Optional<ExpenseItem> expenseItemOptional = expenseItemRepository.findById(expenseItemForm.getId());
        ExpenseItem expenseItemOld = expenseItemOptional.get();
        if (expenseItemForm.getParentId() == expenseItemForm.getId()) {
            bindingResult.rejectValue("parentId", "error.expenseItemForm", "Không thể chọn chính khoản mục chi phí này là cha của nó.");
        }
        if (bindingResult.hasErrors()) {
            return "expense_item/edit";
        }
        ExpenseItem expenseItem = new ExpenseItem();
        expenseItem.setId(expenseItemForm.getId());
        if (expenseItemForm.getActiveStatus() == null) {
            expenseItem.setActiveStatus(Boolean.FALSE);
        }
        if (expenseItemForm.getActiveStatus() != null) {
            expenseItem.setActiveStatus(Boolean.TRUE);
        }
        expenseItem.setExpenseItemCode(expenseItemForm.getExpenseItemCode());
        expenseItem.setExpenseItemName(expenseItemForm.getExpenseItemName());
        expenseItem.setParentId(expenseItemForm.getParentId());
        expenseItem.setDescription(expenseItemForm.getDescription());
        expenseItem.setIsParent(Boolean.FALSE); // NOT NULL. Không rõ trường này để làm gì.
        expenseItem.setIsSystem(Boolean.FALSE); // Khởi tạo khi bắt đầu hệ thống.
        expenseItem.setGrade(2); // Không cần thiết.
        expenseItem.setCreatedBy(expenseItemOld.getCreatedBy());
        expenseItem.setCreatedDate(expenseItemOld.getCreatedDate());
        expenseItem.setModifiedBy(authentication.getName());
        java.sql.Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());
        expenseItem.setModifiedDate(now);
        expenseItemRepository.save(expenseItem);
        return "redirect:/expense_items";
    }

}
























