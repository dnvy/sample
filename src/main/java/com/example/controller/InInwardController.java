package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.InInwardDetail;
import com.example.form.InInwardForm;
import com.example.repository.GeneralLedgerRepository;
import com.example.repository.InInwardOutwardListRepository;
import com.example.repository.InInwardRepository;
import com.example.repository.InventoryLedgerRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Phiếu nhập kho.
 */
@Controller
public class InInwardController {

    @Autowired
    InInwardRepository inInwardRepository; // Ghi vào bảng chính của nghiệp vụ nhập kho.

    @Autowired
    GeneralLedgerRepository generalLedgerRepository; // Ghi vào bảng chứng từ.

    @Autowired
    InventoryLedgerRepository inventoryLedgerRepository; // Ghi vào bảng Thẻ kho.

    @Autowired
    InInwardOutwardListRepository inInwardOutwardListRepository; // Ghi vào cả bảng này. đã trace misa thấy như thế.

    /**
     * Mã tham chiếu của màn hình Phiếu nhâp kho.
     */
    private static final Integer REF_TYPE_INVENTORY_INWARD = 2010;

    // Ngoài ra còn có ,2010,2013,2014,2017, Xem table SYSAddNewDefaultValue .

    /**
     * Show page for add new inventory_item_inward .
     *
     * @return
     */
    @RequestMapping(value = "/add_inventory_item_inward", method = RequestMethod.GET)
    public ModelAndView showPageAddInventoryItemInward(){
        ModelAndView modelAndView = new ModelAndView("in_inward/add");
        InInwardForm inInwardForm = new InInwardForm();
        List<InInwardDetail> inInwardDetailList = new ArrayList<>();
        for (int i = 0; i< 20; i++){
            InInwardDetail inInwardDetail = new InInwardDetail();
            inInwardDetailList.add(inInwardDetail);
        }
        inInwardForm.setInInwardDetailList(inInwardDetailList);
        modelAndView.addObject("inInwardForm", inInwardForm);
        modelAndView.getModel().put("page_title", "Thêm phiếu nhập kho");
        return modelAndView;
    }



}
