package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.entity.SysdbInfo;
import com.example.repository.SYSDBInfoRepository;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Locale;

@Controller
public class SystemController {

    /**
     * For getting value from application.properties
     */
    @Autowired
    private Environment environment;

    /**
     * For multi-language.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Get system information.
     */
    @Autowired
    SYSDBInfoRepository sysdbInfoRepository;

    /**
     * Setup page.
     *
     * @return
     */
    @RequestMapping(value = "/setup", method = RequestMethod.GET)
    public ModelAndView setup() {
        ModelAndView modelAndView = new ModelAndView("system/create_new_accounting_data");
        modelAndView.getModel().put("page_title", "Cài đặt Hệ thống Kế toán");
        return modelAndView;
    }

    /**
     * Go to homepage.
     *
     * @return
     */
    //@RequestMapping(value = "/", method = RequestMethod.GET)
    //public ModelAndView index() {
    //    ModelAndView modelAndView = new ModelAndView("index");
    //    modelAndView.getModel().put("page_title", "Trang chủ");
    //    return modelAndView;
    //}

    /**
     * Introduction page.
     *
     * @return
     */
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public ModelAndView about() {
        Locale locale = LocaleContextHolder.getLocale();
        String title = messageSource.getMessage("system.about.intro", new Object[]{"messages"}, locale);
        ModelAndView modelAndView = new ModelAndView("system/about");
        modelAndView.getModel().put("page_title", title);
        return modelAndView;
    }

    /**
     * Show desktop.
     *
     * @return
     */
    @RequestMapping(value = "/desktop", method = RequestMethod.GET)
    public ModelAndView desktop() {
        ModelAndView modelAndView = new ModelAndView("system/desktop");
        modelAndView.getModel().put("page_title", "Bàn làm việc");
        return modelAndView;
    }

    /**
     * Accounting information.
     *
     * @return
     */
    @RequestMapping(value = "/accounting_info", method = RequestMethod.GET)
    public ModelAndView accountingInfo(HttpServletRequest request, Authentication authentication) throws MalformedURLException {
        // Get size of database schema.
        String datasource_url = environment.getProperty("spring.datasource.url");
        String username = environment.getProperty("spring.datasource.username");
        String password = environment.getProperty("spring.datasource.password");
        // Size of database schema for this webapp.
        String databaseSize = "";
        String schemaName = "";
        try {
            Connection connection = DriverManager.getConnection(datasource_url, username, password);
            schemaName = connection.getCatalog();
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            String query = "" +
                    "select a.dbdisk from(\n" +
                    "SELECT \n" +
                    "    sys.databases.name as dbname,  \n" +
                    "    CONVERT(VARCHAR,SUM(size)*8/1024)+' MB' AS dbdisk\n" +
                    "FROM        sys.databases\n" +
                    "JOIN        sys.master_files  \n" +
                    "ON          sys.databases.database_id=sys.master_files.database_id  \n" +
                    "GROUP BY    sys.databases.name) a\n" +
                    //"where a.dbname = 'accounting';";
                    "where a.dbname = '" + schemaName + "';";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                databaseSize = resultSet.getString("dbdisk");
            }
            connection.commit();
            // Close connection, but other function of JPA still ok.
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Get version of the software.
        SysdbInfo sysdbInfo = sysdbInfoRepository.getLatestRecord();
        String mvcVersion = sysdbInfo.getMvc();

        // Get server.
        String baseURL = getURLBase(request);

        // Set model and view.
        ModelAndView modelAndView = new ModelAndView("system/accounting_info");
        modelAndView.addObject("databaseSize", databaseSize);
        modelAndView.addObject("mvcVersion", mvcVersion);
        modelAndView.addObject("baseURL", baseURL);
        modelAndView.addObject("schemaName", schemaName);
        modelAndView.addObject("currentUser", authentication.getName());
        modelAndView.getModel().put("page_title", "Thuộc tính của Hệ thống Kế toán");
        return modelAndView;
    }

    /**
     * Get base URL. For Accounting system information page.
     *
     * @param request
     * @return
     * @throws MalformedURLException
     */
    public String getURLBase(HttpServletRequest request) throws MalformedURLException {
        URL requestURL = new URL(request.getRequestURL().toString());
        String port = requestURL.getPort() == -1 ? "" : ":" + requestURL.getPort();
        return requestURL.getProtocol() + "://" + requestURL.getHost() + port;
    }

    @RequestMapping(value = "/cash_subsystem", method = RequestMethod.GET)
    public ModelAndView cashSubSystem() {
        ModelAndView modelAndView = new ModelAndView("cash/list");
        modelAndView.getModel().put("page_title", "Phân hệ Quỹ");
        return modelAndView;
    }

    @RequestMapping(value = "/bank_subsystem", method = RequestMethod.GET)
    public ModelAndView bankSubSystem() {
        ModelAndView modelAndView = new ModelAndView("bank_subsystem/process");
        modelAndView.getModel().put("page_title", "Phân hệ Ngân hàng");
        return modelAndView;
    }

    @RequestMapping(value = "/salary_subsystem", method = RequestMethod.GET)
    public ModelAndView salarySubSystem() {
        ModelAndView modelAndView = new ModelAndView("salary/list");
        modelAndView.getModel().put("page_title", "Phân hệ Tiền lương");
        return modelAndView;
    }

    @RequestMapping(value = "/inventory_subsystem", method = RequestMethod.GET)
    public ModelAndView inventorySubSystem() {
        ModelAndView modelAndView = new ModelAndView("inventory_item/process");
        modelAndView.getModel().put("page_title", "Phân hệ Kho");
        return modelAndView;
    }

    @RequestMapping(value = "/budget_subsystem", method = RequestMethod.GET)
    public ModelAndView budgetSubSystem() {
        ModelAndView modelAndView = new ModelAndView("budget/list");
        modelAndView.getModel().put("page_title", "Phân hệ Ngân sách, dự toán");
        return modelAndView;
    }

    @RequestMapping(value = "/costing_subsystem", method = RequestMethod.GET)
    public ModelAndView costingSubSystem() {
        ModelAndView modelAndView = new ModelAndView("costing/list");
        modelAndView.getModel().put("page_title", "Phân hệ Tính giá thành");
        return modelAndView;
    }

    @RequestMapping(value = "/einvoice_subsystem", method = RequestMethod.GET)
    public ModelAndView einvoiceSubSystem() {
        ModelAndView modelAndView = new ModelAndView("e_invoice/list");
        modelAndView.getModel().put("page_title", "Phân hệ Hóa đơn điện tử");
        return modelAndView;
    }

    @RequestMapping(value = "/fixed_asset_subsystem", method = RequestMethod.GET)
    public ModelAndView fixedAssetSubSystem() {
        ModelAndView modelAndView = new ModelAndView("fixed_asset/list");
        modelAndView.getModel().put("page_title", "Phân hệ Tài sản cố định");
        return modelAndView;
    }

    @RequestMapping(value = "/invoice_subsystem", method = RequestMethod.GET)
    public ModelAndView invoiceSubSystem() {
        ModelAndView modelAndView = new ModelAndView("invoice/list");
        modelAndView.getModel().put("page_title", "Phân hệ Hóa đơn");
        return modelAndView;
    }

    @RequestMapping(value = "/purchase_subsystem", method = RequestMethod.GET)
    public ModelAndView purchaseSubSystem() {
        ModelAndView modelAndView = new ModelAndView("purchase/list");
        modelAndView.getModel().put("page_title", "Phân hệ Mua sắm");
        return modelAndView;
    }

    @RequestMapping(value = "/sale_subsystem", method = RequestMethod.GET)
    public ModelAndView saleSubSystem() {
        ModelAndView modelAndView = new ModelAndView("sale/list");
        modelAndView.getModel().put("page_title", "Phân hệ Bán hàng");
        return modelAndView;
    }

    @RequestMapping(value = "/tool_subsystem", method = RequestMethod.GET)
    public ModelAndView toolSubSystem() {
        ModelAndView modelAndView = new ModelAndView("tools/list");
        modelAndView.getModel().put("page_title", "Phân hệ Công cụ, dụng cụ");
        return modelAndView;
    }

    @RequestMapping(value = "/tax_subsystem", method = RequestMethod.GET)
    public ModelAndView taxSubSystem() {
        ModelAndView modelAndView = new ModelAndView("tax/list");
        modelAndView.getModel().put("page_title", "Phân hệ Thuế");
        return modelAndView;
    }

    @RequestMapping(value = "/contract_subsystem", method = RequestMethod.GET)
    public ModelAndView contractSubSystem() {
        ModelAndView modelAndView = new ModelAndView("tax/list");
        modelAndView.getModel().put("page_title", "Phân hệ Hợp đồng");
        return modelAndView;
    }

    @RequestMapping(value = "/synthetic_subsystem", method = RequestMethod.GET)
    public ModelAndView syntheticSubSystem() {
        ModelAndView modelAndView = new ModelAndView("synthetic/list");
        modelAndView.getModel().put("page_title", "Phân hệ Tổng hợp");
        return modelAndView;
    }

    ///**
    // * Change language to English.
    // *
    // * @return
    // */
    //@RequestMapping(value = "/english", method = RequestMethod.GET)
    //public String chooseEnglish(HttpServletRequest request){
    //    request.getSession().setAttribute("lang", "en");
    //    return "redirect:/desktop";
    //}


    ///**
    // * Change language to English.
    // *
    // * @return
    // */
    //@RequestMapping(value = "/vietnamese", method = RequestMethod.GET)
    //public String chooseVietnamese(HttpServletRequest request){
    //    request.getSession().setAttribute("lang", "vi");
    //    return "redirect:/desktop";
    //}

}
