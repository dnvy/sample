package com.example.dto;

public class SimpleAccount {

    private Integer id;
    private String accountNumber;
    private String accountName;

    public SimpleAccount() {

    }

    public SimpleAccount(Object[] objects) {
        setId((Integer) objects[0]);
        setAccountNumber((String) objects[1]);
        setAccountName((String) objects[2]);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

}
