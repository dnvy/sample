package com.example.dto;

import com.example.controller.AccountController;

/**
 * Dùng cho màn hình danh sách tài khoản.
 *
 * @see AccountController#accountDtoJSON()
 */
public class AccountDTO {

    private Integer id; // NOT NULL. PK tài khoản
    private String accountNumber; // NOT NULL. Số hiệu Tài khoản
    private String accountName; // NOT NULL. Tên Tài khoản
    private String accountNameEnglish; // Tên Tài khoản bằng Tiếng Anh
    private String description; // Diễn giải
    private Integer parentId; // Tài khoản tổng hợp
    private Integer accountCategoryKind; // NOT NULL. Tính chất tài khoản: 0: Dư nợ; 1: Dư có; 2: Lưỡng tính
    private Boolean activeStatus; // NOT NULL. Trạng thái Theo dõi

    private String activeStatusString;
    private String accountCategoryKindString;

    public AccountDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNameEnglish() {
        return accountNameEnglish;
    }

    public void setAccountNameEnglish(String accountNameEnglish) {
        this.accountNameEnglish = accountNameEnglish;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getAccountCategoryKind() {
        return accountCategoryKind;
    }

    public void setAccountCategoryKind(Integer accountCategoryKind) {
        this.accountCategoryKind = accountCategoryKind;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getActiveStatusString() {
        return activeStatusString;
    }

    public void setActiveStatusString(String activeStatusString) {
        this.activeStatusString = activeStatusString;
    }

    public String getAccountCategoryKindString() {
        return accountCategoryKindString;
    }

    public void setAccountCategoryKindString(String accountCategoryKindString) {
        this.accountCategoryKindString = accountCategoryKindString;
    }

}
