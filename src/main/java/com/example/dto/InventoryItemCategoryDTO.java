package com.example.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Danh mục Nhóm vật tư, hàng hóa, dịch vụ.
 */
public class InventoryItemCategoryDTO implements Serializable {

    private static final long serialVersionUID = -3390523606229144726L;

    private Integer id; // PK Loại vật tư
    private Integer parentId; // Mã cha
    private String vyCodeId;
    private Boolean isParent; // NOT NULL. Là cha
    private Integer grade; // Cấp bậc
    private String inventoryCategoryCode; // NOT NULL. Mã loại vật tư
    private String inventoryCategoryName; // NOT NULL. Tên loại vật tư
    private String description; // Mô tả loại hàng hóa
    private Boolean isSystem; // Thuộc hệ thống
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    private String activeStatusString;

    public InventoryItemCategoryDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getInventoryCategoryCode() {
        return inventoryCategoryCode;
    }

    public void setInventoryCategoryCode(String inventoryCategoryCode) {
        this.inventoryCategoryCode = inventoryCategoryCode;
    }

    public String getInventoryCategoryName() {
        return inventoryCategoryName;
    }

    public void setInventoryCategoryName(String inventoryCategoryName) {
        this.inventoryCategoryName = inventoryCategoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    public String getActiveStatusString() {
        return activeStatusString;
    }

    public void setActiveStatusString(String activeStatusString) {
        this.activeStatusString = activeStatusString;
    }
}
