package com.example.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Màn hình Thêm mới Hợp đồng/Dự án có một drop-down list để chọn [Thuộc dự án]
 * đổ ra một multi-column có 4 cột
 * 1. Mã dự án
 * 2. Ngày ký
 * 3. Trích yếu
 * 4. Khách hàng
 *
 * @see com.example.controller.ContractController#showPageAddContract()
 */
public class ContractDropDownItem implements Serializable {

    private static final long serialVersionUID = -3017272746933311586L;

    private Integer id;
    private String contractCode; // Mã dự án.

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date signDate; // Ngày ký

    private String contractSubject; // Trích yếu, tóm tắt
    private String accountObjectName; // Tên khách hàng

    public ContractDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getContractSubject() {
        return contractSubject;
    }

    public void setContractSubject(String contractSubject) {
        this.contractSubject = contractSubject;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

}
