package com.example.dto;

import java.math.BigDecimal;

/**
 * Dùng cho data_grid currencies, trả về JSON, để giảm dung lượng.
 *
 * @see com.example.controller.CcyController#currencyMultiColumnJSON()
 */
public class SimpleCurrency{

    private Integer id;
    private String currencyCode;
    private String currencyName;
    private BigDecimal exchangeRate;

    public SimpleCurrency() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

}
