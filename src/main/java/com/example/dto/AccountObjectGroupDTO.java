package com.example.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Nhóm Đối tượng kế toán (Nhóm khách hàng, Nhóm nhà cung cấp, Nhóm nhân viên)
 */
public class AccountObjectGroupDTO implements Serializable {

    private static final long serialVersionUID = 3873285181340300713L;

    private Integer id; // PK
    private String accountObjectGroupCode; // Mã nhóm khách hàng, nhà cung cấp
    private String accountObjectGroupName; // Tên nhóm khách hàng, nhà cung cấp

    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent;
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạng thái theo dõi
    private Boolean isSystem; // Thuộc hệ thống
    private Integer sortOrder;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    private String isParentString;
    private String activeStatusString;
    private String isSystemString;

    public AccountObjectGroupDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountObjectGroupCode() {
        return accountObjectGroupCode;
    }

    public void setAccountObjectGroupCode(String accountObjectGroupCode) {
        this.accountObjectGroupCode = accountObjectGroupCode;
    }

    public String getAccountObjectGroupName() {
        return accountObjectGroupName;
    }

    public void setAccountObjectGroupName(String accountObjectGroupName) {
        this.accountObjectGroupName = accountObjectGroupName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    public String getIsParentString() {
        return isParentString;
    }

    public void setIsParentString(String isParentString) {
        this.isParentString = isParentString;
    }

    public String getActiveStatusString() {
        return activeStatusString;
    }

    public void setActiveStatusString(String activeStatusString) {
        this.activeStatusString = activeStatusString;
    }

    public String getIsSystemString() {
        return isSystemString;
    }

    public void setIsSystemString(String isSystemString) {
        this.isSystemString = isSystemString;
    }

}
