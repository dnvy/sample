package com.example.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class AccountReceivableVoucher {

    private Boolean choosen;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date voucherDate;


    private String voucherNumber;
    private String invoiceNumber;
    private String description;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate;

    private BigDecimal accountReceiableAmount;
    private BigDecimal accountReceiableRemainAmount;
    private BigDecimal collectedAmount;
    private String accountReceiableAccount;
    private Integer paymentTermId;
    private Double discountPercent;
    private Double discountAmount;
    private String discountAccount;

    public AccountReceivableVoucher() {

    }

    public Boolean getChoosen() {
        return choosen;
    }

    public void setChoosen(Boolean choosen) {
        this.choosen = choosen;
    }

    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getAccountReceiableAmount() {
        return accountReceiableAmount;
    }

    public void setAccountReceiableAmount(BigDecimal accountReceiableAmount) {
        this.accountReceiableAmount = accountReceiableAmount;
    }

    public BigDecimal getAccountReceiableRemainAmount() {
        return accountReceiableRemainAmount;
    }

    public void setAccountReceiableRemainAmount(BigDecimal accountReceiableRemainAmount) {
        this.accountReceiableRemainAmount = accountReceiableRemainAmount;
    }

    public BigDecimal getCollectedAmount() {
        return collectedAmount;
    }

    public void setCollectedAmount(BigDecimal collectedAmount) {
        this.collectedAmount = collectedAmount;
    }

    public String getAccountReceiableAccount() {
        return accountReceiableAccount;
    }

    public void setAccountReceiableAccount(String accountReceiableAccount) {
        this.accountReceiableAccount = accountReceiableAccount;
    }

    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAccount() {
        return discountAccount;
    }

    public void setDiscountAccount(String discountAccount) {
        this.discountAccount = discountAccount;
    }

}
