package com.example.dto;

/**
 * Cho màn hình Thu tiền khách hàng. Form tìm kiếm chứng từ dựa theo Mã Đối tượng kế toán (khách hàng).
 */
public class CollectMoneySearchForm {

    private Integer accountObjectId;

    // Account Object, is customer.
    private String accountObjectCode;

    private String accountObjectName;

    public CollectMoneySearchForm() {

    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }
}
