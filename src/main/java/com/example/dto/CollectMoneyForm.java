package com.example.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Dùng cho form Thu tiền khách hàng.
 */
public class CollectMoneyForm implements Serializable {

    private static final long serialVersionUID = -2698348154373887888L;

    private Integer paymentMethod;
    private String currencyCode;
    private BigDecimal exchangeRate;
    private Integer accountObjectCustomerId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date collectMoneyDate;

    private String accountObjectEmployeeCode;
    private BigDecimal moneyAmount;

    private List<AccountReceivableVoucher> accountReceivableVoucherList;

    public CollectMoneyForm() {

    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Integer getAccountObjectCustomerId() {
        return accountObjectCustomerId;
    }

    public void setAccountObjectCustomerId(Integer accountObjectCustomerId) {
        this.accountObjectCustomerId = accountObjectCustomerId;
    }

    public Date getCollectMoneyDate() {
        return collectMoneyDate;
    }

    public void setCollectMoneyDate(Date collectMoneyDate) {
        this.collectMoneyDate = collectMoneyDate;
    }

    public String getAccountObjectEmployeeCode() {
        return accountObjectEmployeeCode;
    }

    public void setAccountObjectEmployeeCode(String accountObjectEmployeeCode) {
        this.accountObjectEmployeeCode = accountObjectEmployeeCode;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public List<AccountReceivableVoucher> getAccountReceivableVoucherList() {
        return accountReceivableVoucherList;
    }

    public void setAccountReceivableVoucherList(List<AccountReceivableVoucher> accountReceivableVoucherList) {
        this.accountReceivableVoucherList = accountReceivableVoucherList;
    }

}
