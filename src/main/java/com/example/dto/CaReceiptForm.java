package com.example.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * For add_receipt form.
 */
public class CaReceiptForm implements Serializable {

    private static final long serialVersionUID = 4655784453629791627L;

    private Integer id; // PK

    /**
     * Loại chứng từ (lấy từ bảng RefType)
     * Từ màn hình Phiếu thu, refType = 1010.
     */
    private Integer refType; //

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày phiếu thu

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày hạch toán

    private String refNoFinance; // Số chứng từ sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // Trạng thái ghi sổ Sổ tài chính
    private Boolean isPostedManagement; // Trạng thái ghi sổ Sổ quản trị
    private Integer accountObjectId; // Mã đối tượng (Người nộp, khách hàng)
    private String accountObjectName; // Tên đối tượng
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectContactName; // Người nộp
    private Integer reasonTypeId; // Lý do thu
    private String journalMemo; // Diễn giải Lý do nộp
    private String documentIncluded; // Kèm theo
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal totalAmountOc; // Tổng tiền
    private BigDecimal totalAmount; // Tổng tiền quy đổi
    private Integer branchId; // ID của chi nhánh.
    private Integer employeeId; // Nhân viên (đại diện mua, bán hàng)
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Boolean isPostedCashBookFinance; // Trạng thái ghi sổ quỹ (Sổ tài chính)
    private Boolean isPostedCashBookManagement; // Trạng thái ghi sổ quỹ (Sổ quản trị)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate; // Ngày ghi sổ quỹ

    private Integer editVersion; // Phiên bản sửa chứng từ
    private Integer refOrder; // Số thứ tự chứng từ nhập vào database
    private Timestamp createdDate; // Ngày lập chứng từ
    private String createdBy; // Người lập chứng từ
    private Timestamp modifiedDate; // Ngày sửa chứng từ lần cuối
    private String modifiedBy; // Người sửa chứng từ lần cuối
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer glVoucherRefId;

    private String voucherNumber; // Số chứng từ. Tự ý bổ sung thêm trường này.
    //List<CaReceiptD>

    private String accountObjectCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    public Boolean getIsPostedCashBookFinance() {
        return isPostedCashBookFinance;
    }

    public void setIsPostedCashBookFinance(Boolean postedCashBookFinance) {
        isPostedCashBookFinance = postedCashBookFinance;
    }

    public Boolean getIsPostedCashBookManagement() {
        return isPostedCashBookManagement;
    }

    public void setIsPostedCashBookManagement(Boolean postedCashBookManagement) {
        isPostedCashBookManagement = postedCashBookManagement;
    }

    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public Integer getGlVoucherRefId() {
        return glVoucherRefId;
    }

    public void setGlVoucherRefId(Integer glVoucherRefId) {
        this.glVoucherRefId = glVoucherRefId;
    }

    // bo sung

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

}
