package com.example.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Dùng cho Màn hình Danh sách Tài khoản Ngân hàng
 * (của công ty - chủ sở hữu bản quyển của bản sao phần mềm này).
 * http://localhost:8080/bank_accounts
 */
public class BankAccountDTO implements Serializable {

    private static final long serialVersionUID = 2018938296610685373L;

    private Integer id; // PK Tài khoản ngân hàng
    private String bankAccountNumber; // Số tài khoản ngân hàng
    private Integer bankId; // Ngân hàng
    private String bankName; // Nơi mở: ví dụ Ngân hàng VCB chi nhánh 01
    private String address; // Địa chỉ nơi mở
    private String description; // Diễn giải
    private Boolean activeStatus; // Ngừng theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer branchId; // Chi nhánh
    private String accountHolder; // Chủ tài khoản
    private String provinceOrCity;

    // thêm vào
    private String bankMainName; // Tên ngân hàng. Không phải chi nhánh.

    public BankAccountDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getProvinceOrCity() {
        return provinceOrCity;
    }

    public void setProvinceOrCity(String provinceOrCity) {
        this.provinceOrCity = provinceOrCity;
    }

    public String getBankMainName() {
        return bankMainName;
    }

    public void setBankMainName(String bankMainName) {
        this.bankMainName = bankMainName;
    }

}
