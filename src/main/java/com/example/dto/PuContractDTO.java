package com.example.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class PuContractDTO {

    private Integer id; // NOT NULL. PK
    private Integer branchId; // Chi nhánh
    private String contractCode; // Số hợp đồng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date signDate; // NOT NULL. Ngày ký

    private String subject; // Trích yếu
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá
    private BigDecimal amountOc; // NOT NULL. Giá trị hợp đồng
    private BigDecimal amount; // NOT NULL. Giá trị hợp đồng Quy đổi
    private Integer accountObjectId; // ID Nhà cung cấp
    private String accountObjectName; // Tên nhà cung cấp
    private String accountObjectAddress; // Địa chỉ nhà cung cấp
    private String accountObjectTaxCode; // Mã số thuế NCC
    private String accountObjectContactName; // Người liên hệ
    private Integer status; // NOT NULL. Tình trạng hợp đồng. 0: Chưa thực hiện; 1: Đang thực hiện; 2: Hoàn thành; 3: Đã thanh lý; 4: Đã hủy bỏ
    private BigDecimal closeAmountOc; // NOT NULL. Giá trị thanh lý. OC: Original currency
    private BigDecimal closeAmount; // NOT NULL. Giá trị thanh lý Quy đổi
    private BigDecimal paidAmountOc; // NOT NULL. Số đã trả
    private BigDecimal paidAmount; // NOT NULL. Số đã trả Quy đổi
    private BigDecimal payableAmountOc; // NOT NULL. Số còn phải trả
    private BigDecimal payableAmount; // NOT NULL. Số còn phải trả Quy đổi

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date closeDate; // Ngày thanh lý/Hủy bỏ

    private String closeReason; // Lý do

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliverDueDate; // Hạn giao hàng

    private String deliverAddress;  // Địa chỉ giao hàng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dueDate; // Hạn thanh toán

    private Integer employeeId; // ID nhân viên
    private String paymentTerm; // Điều khoản khác
    private Integer refType; // Loai chung tu
    private Boolean isArisedBeforeUseSoftware; // Là hợp đồng phát sinh trước khi sử dụng phần mềm
    private BigDecimal executedAmountFinance;
    private BigDecimal executedAmountManagement;
    private BigDecimal paidAmountFinance;
    private BigDecimal paidAmountManagement;
    private Integer puOrderId; // Mã đơn mua hàng
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;

    public PuContractDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getAmountOc() {
        return amountOc;
    }

    public void setAmountOc(BigDecimal amountOc) {
        this.amountOc = amountOc;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getCloseAmountOc() {
        return closeAmountOc;
    }

    public void setCloseAmountOc(BigDecimal closeAmountOc) {
        this.closeAmountOc = closeAmountOc;
    }

    public BigDecimal getCloseAmount() {
        return closeAmount;
    }

    public void setCloseAmount(BigDecimal closeAmount) {
        this.closeAmount = closeAmount;
    }

    public BigDecimal getPaidAmountOc() {
        return paidAmountOc;
    }

    public void setPaidAmountOc(BigDecimal paidAmountOc) {
        this.paidAmountOc = paidAmountOc;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public BigDecimal getPayableAmountOc() {
        return payableAmountOc;
    }

    public void setPayableAmountOc(BigDecimal payableAmountOc) {
        this.payableAmountOc = payableAmountOc;
    }

    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public Date getDeliverDueDate() {
        return deliverDueDate;
    }

    public void setDeliverDueDate(Date deliverDueDate) {
        this.deliverDueDate = deliverDueDate;
    }

    public String getDeliverAddress() {
        return deliverAddress;
    }

    public void setDeliverAddress(String deliverAddress) {
        this.deliverAddress = deliverAddress;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Boolean getIsArisedBeforeUseSoftware() {
        return isArisedBeforeUseSoftware;
    }

    public void setIsArisedBeforeUseSoftware(Boolean arisedBeforeUseSoftware) {
        isArisedBeforeUseSoftware = arisedBeforeUseSoftware;
    }

    public BigDecimal getExecutedAmountFinance() {
        return executedAmountFinance;
    }

    public void setExecutedAmountFinance(BigDecimal executedAmountFinance) {
        this.executedAmountFinance = executedAmountFinance;
    }

    public BigDecimal getExecutedAmountManagement() {
        return executedAmountManagement;
    }

    public void setExecutedAmountManagement(BigDecimal executedAmountManagement) {
        this.executedAmountManagement = executedAmountManagement;
    }

    public BigDecimal getPaidAmountFinance() {
        return paidAmountFinance;
    }

    public void setPaidAmountFinance(BigDecimal paidAmountFinance) {
        this.paidAmountFinance = paidAmountFinance;
    }

    public BigDecimal getPaidAmountManagement() {
        return paidAmountManagement;
    }

    public void setPaidAmountManagement(BigDecimal paidAmountManagement) {
        this.paidAmountManagement = paidAmountManagement;
    }

    public Integer getPuOrderId() {
        return puOrderId;
    }

    public void setPuOrderId(Integer puOrderId) {
        this.puOrderId = puOrderId;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

}
