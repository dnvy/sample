package com.example.common;

import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Support displaying field ActiveStatus of entity to view layer.
 */
public class UtilityList {

    /**
     * For display active_status of objects in list view.
     *
     * @param modelAndView
     */
    public static void setActiveStatusList(ModelAndView modelAndView) {
        Map<Boolean, String> activeStatusList = new HashMap<>();
        activeStatusList.put(true, "Có");
        activeStatusList.put(false, "Không");
        modelAndView.addObject("activeStatusList", activeStatusList);
    }

    /**
     * For screen view detail an account. For example <code>/account/view/1</code>
     *
     * @param modelAndView
     */
    public static void setTrueOrFalseList(ModelAndView modelAndView) {
        Map<Boolean, String> trueOrFalseList = new HashMap<>();
        trueOrFalseList.put(true, "Có");
        trueOrFalseList.put(false, "Không");
        modelAndView.addObject("trueOrFalseList", trueOrFalseList);
    }

    /**
     * For screen view detail an account. For example <code>/account/view/1</code>
     *
     * @param modelAndView
     */
    public static void setAlertOrRequireList(ModelAndView modelAndView) {
        Map<Integer, String> alertOrRequireList = new HashMap<>();
        alertOrRequireList.put(0, "Chỉ cảnh báo");
        alertOrRequireList.put(1, "Bắt buộc nhập");
        modelAndView.addObject("alertOrRequireList", alertOrRequireList);
    }

    /**
     * For screen view detail an account. For example <code>/account/view/1</code>
     *
     * @param modelAndView
     */
    public static void setAccountCategoryKindList(ModelAndView modelAndView) {
        Map<Integer, String> accountCategoryKindList = new HashMap<>();
        accountCategoryKindList.put(0, "Dư nợ");
        accountCategoryKindList.put(1, "Dư có");
        accountCategoryKindList.put(2, "Lưỡng tính");
        modelAndView.addObject("accountCategoryKindList", accountCategoryKindList);
    }

    /**
     * For display gender Male, Female at view detail screens.
     *
     * @param modelAndView
     */
    public static void setGenderList(ModelAndView modelAndView) {
        Map<Integer, String> genderList = new HashMap<>();
        genderList.put(0, "Nam");
        genderList.put(1, "Nữ");
        modelAndView.addObject("genderList", genderList);
    }

}
