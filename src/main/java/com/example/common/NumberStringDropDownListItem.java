package com.example.common;

public class NumberStringDropDownListItem {

    private Integer num;
    private String text;

    public NumberStringDropDownListItem() {

    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
