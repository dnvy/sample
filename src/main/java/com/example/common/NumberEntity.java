package com.example.common;

/**
 * Chữ số nằm trong số tiền.
 */
class NumberEntity {

    int number;
    String numberString;
    String numberSuffix;
    int numberPositionLTR;
    int numberPositionRTL;
    boolean isFirst;
    boolean isLast;

    public NumberEntity() {

    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getNumberString() {
        return numberString;
    }

    public void setNumberString(String numberString) {
        this.numberString = numberString;
    }

    public String getNumberSuffix() {
        return numberSuffix;
    }

    public void setNumberSuffix(String numberSuffix) {
        this.numberSuffix = numberSuffix;
    }

    public int getNumberPositionLTR() {
        return numberPositionLTR;
    }

    public void setNumberPositionLTR(int numberPositionLTR) {
        this.numberPositionLTR = numberPositionLTR;
    }

    public int getNumberPositionRTL() {
        return numberPositionRTL;
    }

    public void setNumberPositionRTL(int numberPositionRTL) {
        this.numberPositionRTL = numberPositionRTL;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean first) {
        isFirst = first;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean last) {
        isLast = last;
    }

}