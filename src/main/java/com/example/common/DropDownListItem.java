package com.example.common;

/**
 * Use in drop-down list.
 */
public class DropDownListItem {

    private String value;
    private String text;

    public DropDownListItem() {

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

//TODO: We will compile these common classes, then package into a JAR file, for secure source code.
