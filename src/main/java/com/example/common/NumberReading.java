package com.example.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Đọc số tiền thành chữ.
 */
public class NumberReading {

    public static String readMoneyNumber(int moneyNumber) {
        String end = " đồng chẵn.";
        List<NumberEntity> numberEntityArrayListRaw = new ArrayList<NumberEntity>();
        String resultRaw = "";
        String moneyNumberString = Integer.toString(moneyNumber);
        int sizeOfString = moneyNumberString.length();
        int[] moneyNumberArr = new int[sizeOfString];
        for (int i = 0; i < sizeOfString; i++) {
            moneyNumberArr[i] = Integer.valueOf(moneyNumberString.substring(i, i + 1));
        }
        // Set thuộc tính cho numberEntity.
        for (int i = 0; i < sizeOfString; i++) {
            NumberEntity numberEntity = new NumberEntity();
            numberEntity.setNumber(moneyNumberArr[i]);
            numberEntity.setNumberString(convertNumberToString(moneyNumberArr[i]));
            numberEntity.setNumberPositionLTR(i);
            numberEntity.setNumberPositionRTL(sizeOfString - 1 - i);
            if (i == 0) {
                numberEntity.setIsFirst(Boolean.TRUE);
            } else {
                numberEntity.setIsFirst(Boolean.FALSE);
            }
            if (i == (sizeOfString - 1)) {
                numberEntity.setIsLast(Boolean.TRUE);
            } else {
                numberEntity.setIsLast(Boolean.FALSE);
            }
            numberEntityArrayListRaw.add(numberEntity);
        }
        List<NumberEntity> numberEntityArrayList = new ArrayList<NumberEntity>();
        for (int i = 0; i < numberEntityArrayListRaw.size(); i++) {
            NumberEntity numberEntity = numberEntityArrayListRaw.get(i);
            if (numberEntity.getNumber() == 1 && (numberEntity.getNumberPositionRTL() == 2 || numberEntity.getNumberPositionRTL() == 4 || numberEntity.getNumberPositionRTL() == 7) || numberEntity.getNumberPositionRTL() == 10) {
                numberEntity.setNumberString("mười");
            }
            if (numberEntity.getNumber() != 1 && (numberEntity.getNumberPositionRTL() == 1 || numberEntity.getNumberPositionRTL() == 4 || numberEntity.getNumberPositionRTL() == 7) || numberEntity.getNumberPositionRTL() == 10) {
                numberEntity.setNumberString(numberEntityArrayListRaw.get(i).getNumberString() + " mươi");
            }
            if (numberEntity.getNumber() == 5 && (numberEntity.getNumberPositionRTL() == 0 || numberEntity.getNumberPositionRTL() == 3 || numberEntity.getNumberPositionRTL() == 6 || numberEntity.getNumberPositionRTL() == 9)) {
                numberEntity.setNumberString("lăm");
            }
            // Chữ số cuối cùng của dãy số.
            if (i == numberEntityArrayListRaw.size() - 1) {
                numberEntity.setIsLast(Boolean.TRUE);
            }
            numberEntityArrayList.add(numberEntity);
        }
        for (int i = 0; i < sizeOfString; i++) {
            // Nếu là chữ số cuối cùng.
            if (i != sizeOfString - 1) {
                // Nếu là chữ số không phải cuối cùng. và có RTL = 3
                if (numberEntityArrayList.get(i).getNumberPositionRTL() == 3) {
                    numberEntityArrayList.get(i).setNumberString(numberEntityArrayList.get(i).getNumberString() + " nghìn, ");
                } else if (numberEntityArrayList.get(i).getNumberPositionRTL() == 6) {
                    numberEntityArrayList.get(i).setNumberString(numberEntityArrayList.get(i).getNumberString() + " triệu, ");
                } else if (numberEntityArrayList.get(i).getNumberPositionRTL() == 9) {
                    numberEntityArrayList.get(i).setNumberString(numberEntityArrayList.get(i).getNumberString() + " tỷ, ");
                } else if (numberEntityArrayList.get(i).getNumberPositionRTL() == 2 || numberEntityArrayList.get(i).getNumberPositionRTL() == 5 || numberEntityArrayList.get(i).getNumberPositionRTL() == 8 || numberEntityArrayList.get(i).getNumberPositionRTL() == 12) {
                    numberEntityArrayList.get(i).setNumberString(numberEntityArrayList.get(i).getNumberString() + " trăm ");
                } else {
                    numberEntityArrayList.get(i).setNumberString(numberEntityArrayList.get(i).getNumberString() + " ");
                }
            }
        }
        //FIXME: Nếu số 1 đứng ở các vị trí RTL = 2, 5, 7, 10, thì nó sẽ là "mốt", chứ không phải là "một".


        if (numberEntityArrayList.get(sizeOfString - 1).getNumber() == 0 && numberEntityArrayList.get(sizeOfString - 2).getNumber() == 0 && numberEntityArrayList.get(sizeOfString - 3).getNumber() == 0) {
            numberEntityArrayList.get(sizeOfString - 1).setNumberString("");
            numberEntityArrayList.get(sizeOfString - 2).setNumberString("");
            numberEntityArrayList.get(sizeOfString - 3).setNumberString("");
            String temp = numberEntityArrayList.get(sizeOfString - 4).getNumberString();
            String temp2 = temp.replace(",", "");
            numberEntityArrayList.get(sizeOfString - 4).setNumberString(temp2);
            end = "đồng chẵn.";
        }

        if (numberEntityArrayList.get(sizeOfString - 1).getNumber() == 0 && numberEntityArrayList.get(sizeOfString - 2).getNumber() == 0) {
            numberEntityArrayList.get(sizeOfString - 1).setNumberString("");
            numberEntityArrayList.get(sizeOfString - 2).setNumberString("");
            end = "đồng chẵn.";
        }

        for (int i = 0; i < sizeOfString; i++) {
            resultRaw += numberEntityArrayList.get(i).getNumberString();
        }
        resultRaw += end;
        String result = resultRaw.substring(0, 1).toUpperCase() + resultRaw.substring(1);
        return result;
    }

    public static String convertNumberToString(int number) {
        String result = "";
        if (number == 0) {
            result = "không";
        }
        if (number == 1) {
            result = "một";
        }
        if (number == 2) {
            result = "hai";
        }
        if (number == 3) {
            result = "ba";
        }
        if (number == 4) {
            result = "bốn";
        }
        if (number == 5) {
            result = "năm";
        }
        if (number == 6) {
            result = "sáu";
        }
        if (number == 7) {
            result = "bảy";
        }
        if (number == 8) {
            result = "tám";
        }
        if (number == 9) {
            result = "chín";
        }
        return result;
    }

}
