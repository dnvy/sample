////package com.donhuvy;
////
////import Account;
////import AccountObject;
////import AccountObjectRepository;
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.data.domain.Page;
////import org.springframework.data.domain.PageRequest;
////import org.springframework.stereotype.Controller;
////import org.springframework.web.bind.annotation.RequestMapping;
////import org.springframework.web.bind.annotation.RequestMethod;
////import org.springframework.web.bind.annotation.RequestParam;
////import org.springframework.web.servlet.ModelAndView;
////
////import java.util.Optional;
////
////@Controller
////public class AccountObjectController {
////
////    private static final int INITIAL_PAGE = 0;
////    private static final int INITIAL_PAGE_SIZE = 10;
////
////    @Autowired
////    AccountObjectRepository accountObjectRepository;
////
////    @RequestMapping(value = "/all", method = RequestMethod.GET)
////    public ModelAndView getAllPaginatingCcy(@RequestParam("pageSize") Optional<Integer> pageSize,
////                                            @RequestParam("page") Optional<Integer> page) {
////        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
////        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
////        Page<AccountObject> accountObjects = accountObjectRepository.findAll(PageRequest.of(evalPage, evalPageSize));
////        ModelAndView modelAndView = new ModelAndView("system/account_object");
////        modelAndView.addObject("accountObjects", accountObjects);
////        return modelAndView;
////    }
////
////}
//
//
package com.example;

import com.example.entity.AccountObject;
import com.example.repository.AccountObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@RestController
public class AccountObjectControllerRest {

    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;

    @Autowired
    AccountObjectRepository accountObjectRepository;

    @RequestMapping(value = "/api/all", method = RequestMethod.GET)
    public ModelAndView getAllPaginatingCcy(@RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<AccountObject> accountObjects = accountObjectRepository.findAll(PageRequest.of(evalPage, evalPageSize));
        ModelAndView modelAndView = new ModelAndView("system/account_object");
        modelAndView.addObject("accountObjects", accountObjects);
        return modelAndView;
    }
//
//    /**
//     * Get all account objects.
//     *
//     * @return
//     */
//    //@GetMapping("/account_object")
//    //public List<AccountObject> retrieveAllAccountObjects() {
//    //    return accountObjectRepository.findAll();
//    //}
//
//    @GetMapping("/add_account_object")
//    public ModelAndView addAccountObject() {
//        ModelAndView modelAndView = new ModelAndView("system/add_account_object");
//        return modelAndView;
//    }
//
//    /**
//     * Get account object by a specific identity numbersArray.
//     * For example: http://localhost:8080/accounting/account_object/1
//     *
//     * @param id
//     * @return
//     */
//    @GetMapping("/account_object/{id}")
//    public AccountObject retrieveAccountObject(@PathVariable int id) {
//        Optional<AccountObject> accountObject = accountObjectRepository.findById(id);
//        if (!accountObject.isPresent()) {
//            throw new AccountObjectNotFoundException("Not found Account object what has id = " + id + ".");
//        }
//        return accountObject.get();
//    }
//
//    /**
//     * Delete an Account object by a specific identity numbersArray.
//     * For example: DELETE METHOD with http://localhost:8080/accounting/account_object/2
//     *
//     * @param id
//     */
//    @DeleteMapping("/account_object/{id}")
//    public void deleteAccountObject(@PathVariable int id) {
//        accountObjectRepository.deleteById(id);
//    }
//
//    /**
//     * Create a new Account object.
//     * For example: Method POST to http://localhost:8080/accounting/account_object .
//     *
//     * @param accountObject
//     * @return
//     */
//    //@PostMapping("/account_object")
//    //public ResponseEntity<Object> createAccountObject(@RequestBody AccountObject accountObject) {
//    //    accountObject.setCreatedDate(new Timestamp(System.currentTimeMillis()));
//    //    AccountObject savedAccountObject = accountObjectRepository.save(accountObject);
//    //    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedAccountObject.getAccountObjectId()).toUri();
//    //    return ResponseEntity.created(location).build();
//    //}
//
//    /**
//     * Update an existing Account object record.
//     * For example: Method PUT to http://localhost:8080/accounting/account_object/1 .
//     *
//     * @param accountObject
//     * @param id
//     * @return
//     */
//    @PutMapping("/account_object/{id}")
//    public ResponseEntity<Object> updateStudent(@RequestBody AccountObject accountObject, @PathVariable int id) {
//        Optional<AccountObject> accountObjectOptional = accountObjectRepository.findById(id);
//        if (!accountObjectOptional.isPresent()) {
//            return ResponseEntity.notFound().build();
//        }
//        accountObject.setAccountObjectId(id);
//        accountObject.setModifiedDate(new Timestamp(System.currentTimeMillis()));
//        accountObjectRepository.save(accountObject);
//        return ResponseEntity.noContent().build();
//    }
//
//    /**
//     * Get all employees (account objects).
//     * GET: http://localhost:8080/accounting/account_object/employee .
//     *
//     * @return
//     */
//    @GetMapping("/account_object/employee")
//    public List<AccountObject> getEmployeeList() {
//        return accountObjectRepository.listEmployee();
//    }
//
//    /**
//     * Get all customers (account objects).
//     * GET: http://localhost:8080/accounting/account_object/customer .
//     *
//     * @return
//     */
//    @GetMapping("/account_object/customer")
//    public List<AccountObject> getCustomerList() {
//        return accountObjectRepository.listCustomer();
//    }
//
//    /**
//     * Get all vendors (account objects).
//     * GET: http://localhost:8080/accounting/account_object/vendor .
//     *
//     * @return
//     */
//    @GetMapping("/account_object/vendor")
//    public List<AccountObject> getVendorList() {
//        return accountObjectRepository.listVendor();
//    }
//
}
