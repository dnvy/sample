package com.example.view;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpeningAccountEntryViewRepository extends JpaRepository<OpeningAccountEntryView, Integer> {

}
