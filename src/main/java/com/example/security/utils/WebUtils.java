package com.example.security.utils;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class WebUtils {

    public static String toString(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UserName:").append(user.getUsername());
        Collection<GrantedAuthority> grantedAuthorityCollection = user.getAuthorities();
        if (grantedAuthorityCollection != null && !grantedAuthorityCollection.isEmpty()) {
            stringBuilder.append(" (");
            boolean first = true;
            for (GrantedAuthority grantedAuthority : grantedAuthorityCollection) {
                if (first) {
                    stringBuilder.append(grantedAuthority.getAuthority());
                    first = false;
                } else {
                    stringBuilder.append(", ").append(grantedAuthority.getAuthority());
                }
            }
            stringBuilder.append(")");
        }
        return stringBuilder.toString();
    }

}