package com.example.security.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncrytedPasswordUtils {

    /**
     * Encrypt password with BCryptPasswordEncoder.
     *
     * @param password
     * @return
     */
    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    //public static void main(String[] args) {
    //    String password = "123456a@";
    //    String encrytedPassword = encrytePassword(password);
    //    System.out.println("Encryted Password: " + encrytedPassword);
    //}

}