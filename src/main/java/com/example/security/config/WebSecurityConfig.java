package com.example.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import com.example.entity.FunctionPath;
import com.example.repository.FunctionPathRepository;
import com.example.security.services.UserDetailsServiceImpl;

import javax.sql.DataSource;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    FunctionPathRepository functionPathRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private DataSource dataSource;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // Set service for searching user in database. And set password_encoder.
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        // Pages no need login request.
        http.authorizeRequests().antMatchers("/", "/login", "/logout", "/images", "/css", "/js").permitAll();

        // Trang /userInfo yêu cầu phải login với vai trò ROLE_USER hoặc ROLE_ADMIN.
        // Nếu chưa login, nó sẽ redirect tới trang /login.
        http.authorizeRequests().antMatchers("/userInfo").access("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')");

        // Page for admin only.
        http.authorizeRequests().antMatchers("/admin").access("hasRole('ROLE_ADMIN')");

        // For accounting system.
        //http.authorizeRequests().antMatchers(
        //        "/accounts",
        //        "/account_transfers",
        //        "/account_default",
        //        "/jobs",
        //        "/banks",
        //        "/currencies",
        //        "/employees",
        //        "/employees",
        //        "/add_personal_customer",
        //        "/add_org_customer",
        //        "/add_cash_receipt"
        //).access("hasRole('ROLE_ADMIN')");

        List<FunctionPath> functionPathList = functionPathRepository.findAllByRole("CHIEF_ACCOUNTANT");
        int size = functionPathList.size();
        String[] functionPathArray = new String[size];
        for (int i = 0; i < size; i++) {
            functionPathArray[i] = functionPathList.get(i).getFunctionPath();
        }
        http.authorizeRequests().antMatchers(functionPathArray).access("hasRole('ROLE_ADMIN')");

        // Khi người dùng đã login, với vai trò XX.
        // Nhưng truy cập vào trang yêu cầu vai trò YY,
        // Ngoại lệ AccessDeniedException sẽ ném ra.
        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

        // Configuration for login form.
        http.authorizeRequests().and().formLogin()
                .loginProcessingUrl("/j_spring_security_check") // Submit URL of Login page.
                .loginPage("/login")
                //.defaultSuccessUrl("/userAccountInfo")
                .defaultSuccessUrl("/desktop")

                .failureUrl("/login?error=true")
                .usernameParameter("username")
                .passwordParameter("password")
                // Configuration for logout page.
                .and().logout().logoutUrl("/logout")
                // .logoutSuccessUrl("/logoutSuccessful")
                .logoutSuccessUrl("/login")

        ;
        // Configuration for Remember me function (remember 24h).
        http
                .authorizeRequests()
                .and()
                .rememberMe()
                .tokenRepository(this.persistentTokenRepository())
                .tokenValiditySeconds(1 * 24 * 60 * 60);
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }

}