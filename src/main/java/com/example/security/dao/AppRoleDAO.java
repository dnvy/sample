package com.example.security.dao;

import com.example.security.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

//@Repository
//@Transactional
//@Controller
public class AppRoleDAO {

    @Autowired
    //@Qualifier(value = "entityManager")
    private EntityManager entityManager;

    public List<String> getRoleNames(Long userId) {
        String sql = "SELECT ur.appRole.roleName from " + UserRole.class.getName() + " ur where ur.appUser.userId = :userId ";
        Query query = this.entityManager.createQuery(sql, String.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }





}