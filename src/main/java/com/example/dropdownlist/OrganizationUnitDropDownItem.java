package com.example.dropdownlist;

/**
 * Use for drop-down list show two columns:
 * - OrganizationUnitCode
 * - OrganizationUnitName
 *
 * @see com.example.controller.OrganizationUnitController
 */
public class OrganizationUnitDropDownItem {

    private Integer id;
    private String organizationUnitCode; // Mã đơn vị
    private String organizationUnitName; // Tên đơn vị

    public OrganizationUnitDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrganizationUnitCode() {
        return organizationUnitCode;
    }

    public void setOrganizationUnitCode(String organizationUnitCode) {
        this.organizationUnitCode = organizationUnitCode;
    }

    public String getOrganizationUnitName() {
        return organizationUnitName;
    }

    public void setOrganizationUnitName(String organizationUnitName) {
        this.organizationUnitName = organizationUnitName;
    }

}
