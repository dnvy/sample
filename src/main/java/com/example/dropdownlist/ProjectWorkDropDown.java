package com.example.dropdownlist;

import java.io.Serializable;

/**
 * Công trình, vụ việc.
 */
public class ProjectWorkDropDown implements Serializable {

    private static final long serialVersionUID = -8382219501819306592L;

    private Integer id;
    private String projectWorkCode; // Mã công trình, vụ việc
    private String projectWorkName; // Tên công trình, vụ việc

    public ProjectWorkDropDown() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectWorkCode() {
        return projectWorkCode;
    }

    public void setProjectWorkCode(String projectWorkCode) {
        this.projectWorkCode = projectWorkCode;
    }

    public String getProjectWorkName() {
        return projectWorkName;
    }

    public void setProjectWorkName(String projectWorkName) {
        this.projectWorkName = projectWorkName;
    }

}
