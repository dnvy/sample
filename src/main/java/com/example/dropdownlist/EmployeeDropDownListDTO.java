package com.example.dropdownlist;

import java.io.Serializable;

/**
 * Dùng cho multi-columns drop-down list chọn Nhân viên.
 */
public class EmployeeDropDownListDTO implements Serializable {

    private static final long serialVersionUID = -4035031297801051962L;

    private Integer id;
    private String accountObjectCode;
    private String accountObjectName;
    private String organizationUnitName;
    private String mobile;

    public EmployeeDropDownListDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getOrganizationUnitName() {
        return organizationUnitName;
    }

    public void setOrganizationUnitName(String organizationUnitName) {
        this.organizationUnitName = organizationUnitName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
