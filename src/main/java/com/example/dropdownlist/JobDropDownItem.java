package com.example.dropdownlist;

/**
 * Dùng cho Multi-drop down để chọn Đối tượng tập hợp chi phí (job).
 */
public class JobDropDownItem {

    private Integer id;
    private String jobCode;
    private String jobName;

    public JobDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

}
