package com.example.dropdownlist;

/**
 * Chỉ lấy các khoản mục chi phí có ActiveStatus = 1.
 * Khi thêm 1 khoản mục chi phí mới, thì lại gọi ra chính Danh sách khoản mục
 * chi phí, để chỉ ra cha của nó (nếu có).
 *
 * @see com.example.controller.ExpenseItemController#showPageAddExpenseItem()
 */
public class ExpenseItemDropDownItem {

    private Integer id; // PK
    private String expenseItemCode; // NOT NULL. Mã khoản mục chi phí
    private String expenseItemName; // NOT NULL. Tên khoản mục chi phí
    private Integer parentId; // ID khoản mục chi phí cha

    public ExpenseItemDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExpenseItemCode() {
        return expenseItemCode;
    }

    public void setExpenseItemCode(String expenseItemCode) {
        this.expenseItemCode = expenseItemCode;
    }

    public String getExpenseItemName() {
        return expenseItemName;
    }

    public void setExpenseItemName(String expenseItemName) {
        this.expenseItemName = expenseItemName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

}
