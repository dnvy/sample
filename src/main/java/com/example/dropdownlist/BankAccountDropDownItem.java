package com.example.dropdownlist;

/**
 * Drop down list Tài khoản ngân hàng.
 */
public class BankAccountDropDownItem {

    private Integer id; // PK Tài khoản ngân hàng
    private String bankAccountNumber; // Số tài khoản ngân hàng
    private Integer bankId; // Ngân hàng
    private String bankIdString; // Tên ngân hàng
    private String bankName; // Chi nhánh

    public BankAccountDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBankIdString() {
        return bankIdString;
    }

    public void setBankIdString(String bankIdString) {
        this.bankIdString = bankIdString;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

}
