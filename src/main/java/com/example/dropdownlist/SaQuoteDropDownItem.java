package com.example.dropdownlist;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 3 cột:
 * 1. Số báo giá
 * 2. Ngày báo giá
 * 3. Khách hàng
 */
public class SaQuoteDropDownItem implements Serializable {

    private static final long serialVersionUID = -2712562656683087782L;

    private Integer id; // NOT NULL. PK
    private String refNo; // NOT NULL. Số báo giá

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày báo giá

    private Integer accountObjectId; // ID khách hàng là tổ chức hoặc cá nhân

    public SaQuoteDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

}
