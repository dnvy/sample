package com.example.dropdownlist;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Dùng cho multi-column drop-down list, chọn vật tư, hàng hóa. Ở màn hình Thêm hợp đồng.
 *
 * @see com.example.controller.InventoryItemController#submitDataAddInventoryItem
 */
public class InventoryItemDropDownItem implements Serializable {

    private static final long serialVersionUID = -6079775139373586747L;

    private Integer id;
    private String inventoryItemCode; // Mã hàng.
    private String inventoryItemName; // Tên hàng.
    private Integer availableQuantity; // Số lượng tồn.
    private BigDecimal salePrice1; // Giá bán 1
    private BigDecimal salePrice2; // Giá bán 2
    private BigDecimal salePrice3; // Giá bán 3
    private BigDecimal fixedSalePrice; // Giá bán cố định.

    public InventoryItemDropDownItem() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public BigDecimal getSalePrice1() {
        return salePrice1;
    }

    public void setSalePrice1(BigDecimal salePrice1) {
        this.salePrice1 = salePrice1;
    }

    public BigDecimal getSalePrice2() {
        return salePrice2;
    }

    public void setSalePrice2(BigDecimal salePrice2) {
        this.salePrice2 = salePrice2;
    }

    public BigDecimal getSalePrice3() {
        return salePrice3;
    }

    public void setSalePrice3(BigDecimal salePrice3) {
        this.salePrice3 = salePrice3;
    }

    public BigDecimal getFixedSalePrice() {
        return fixedSalePrice;
    }

    public void setFixedSalePrice(BigDecimal fixedSalePrice) {
        this.fixedSalePrice = fixedSalePrice;
    }

}
