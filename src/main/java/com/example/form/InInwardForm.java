package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.InInwardDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Form thêm Phiếu nhập kho.
 */
public class InInwardForm implements Serializable {

    private static final long serialVersionUID = 7046165492529346938L;

    private Integer id; // PK
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer refType; // Loại chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày phiếu nhập

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày hạch toán

    private String refNoFinance; // Số chứng từ Sổ tài chính
    private String refNoManagement; // Số chứng từ Sổ quản trị
    private Boolean isPostedFinance; // Trạng thái ghi sổ Sổ tài chính
    private Boolean isPostedManagement; // Trạng thái ghi sổ Sổ quản trị
    private Integer accountObjectId; // Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private String contactName; // Người giao hàng
    private String journalMemo; // Diễn giải
    private String documentIncluded; // Tài liệu kèm theo trên phiếu thu
    private Integer employeeId; // Nhân viên
    private BigDecimal totalAmountFinance; // Tổng thành tiền Sổ tài chính
    private Integer branchId; // ID của chi nhánh.
    private Integer saReturnRefId; // RefID của chứng từ Bán trả lại
    private Integer unitPriceMethod; // Phương thức nhập/tính đơn giá nhập. 0 = Lấy từ giá xuất bán; 1 = Nhập đơn giá bằng tay
    private Boolean isPostedInventoryBookFinance; // Trạng thái ghi sổ thủ kho (Sổ tài chính)
    private Boolean isPostedInventoryBookManagement; // Trạng thái ghi sổ thủ kho (Sổ quản trị)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inventoryPostedDate; // Ngày ghi sổ kho

    private Integer editVersion; // Phiên bản sửa chứng từ
    private Integer refOrder; // Số thứ tự nhập chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private BigDecimal totalAmountManagement; // Tổng thành tiền Sổ quản trị
    private Integer outwardDependentRefId;
    private Integer assemblyRefId;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date inRefOrder;

    private Boolean isReturnWithInward;
    private Integer currencyId;
    private BigDecimal exchangeRate;
    private Boolean isCreatedSaReturnLastYear; // Đã lập bán trả lại năm trước

    List<InInwardDetail> inInwardDetailList;

    public InInwardForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public String getDocumentIncluded() {
        return documentIncluded;
    }

    public void setDocumentIncluded(String documentIncluded) {
        this.documentIncluded = documentIncluded;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public BigDecimal getTotalAmountFinance() {
        return totalAmountFinance;
    }

    public void setTotalAmountFinance(BigDecimal totalAmountFinance) {
        this.totalAmountFinance = totalAmountFinance;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getSaReturnRefId() {
        return saReturnRefId;
    }

    public void setSaReturnRefId(Integer saReturnRefId) {
        this.saReturnRefId = saReturnRefId;
    }

    public Integer getUnitPriceMethod() {
        return unitPriceMethod;
    }

    public void setUnitPriceMethod(Integer unitPriceMethod) {
        this.unitPriceMethod = unitPriceMethod;
    }

    public Boolean getIsPostedInventoryBookFinance() {
        return isPostedInventoryBookFinance;
    }

    public void setIsPostedInventoryBookFinance(Boolean postedInventoryBookFinance) {
        isPostedInventoryBookFinance = postedInventoryBookFinance;
    }

    public Boolean getIsPostedInventoryBookManagement() {
        return isPostedInventoryBookManagement;
    }

    public void setIsPostedInventoryBookManagement(Boolean postedInventoryBookManagement) {
        isPostedInventoryBookManagement = postedInventoryBookManagement;
    }

    public Date getInventoryPostedDate() {
        return inventoryPostedDate;
    }

    public void setInventoryPostedDate(Date inventoryPostedDate) {
        this.inventoryPostedDate = inventoryPostedDate;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public BigDecimal getTotalAmountManagement() {
        return totalAmountManagement;
    }

    public void setTotalAmountManagement(BigDecimal totalAmountManagement) {
        this.totalAmountManagement = totalAmountManagement;
    }

    public Integer getOutwardDependentRefId() {
        return outwardDependentRefId;
    }

    public void setOutwardDependentRefId(Integer outwardDependentRefId) {
        this.outwardDependentRefId = outwardDependentRefId;
    }

    public Integer getAssemblyRefId() {
        return assemblyRefId;
    }

    public void setAssemblyRefId(Integer assemblyRefId) {
        this.assemblyRefId = assemblyRefId;
    }

    public Date getInRefOrder() {
        return inRefOrder;
    }

    public void setInRefOrder(Date inRefOrder) {
        this.inRefOrder = inRefOrder;
    }

    public Boolean getIsReturnWithInward() {
        return isReturnWithInward;
    }

    public void setIsReturnWithInward(Boolean returnWithInward) {
        isReturnWithInward = returnWithInward;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Boolean getCreatedSaReturnLastYear() {
        return isCreatedSaReturnLastYear;
    }

    public void setCreatedSaReturnLastYear(Boolean createdSaReturnLastYear) {
        isCreatedSaReturnLastYear = createdSaReturnLastYear;
    }

    public List<InInwardDetail> getInInwardDetailList() {
        return inInwardDetailList;
    }

    public void setInInwardDetailList(List<InInwardDetail> inInwardDetailList) {
        this.inInwardDetailList = inInwardDetailList;
    }
}
