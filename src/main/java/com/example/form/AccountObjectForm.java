package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.AccountObjectBankAccount;
import com.example.entity.AccountObjectShippingAddress;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Dùng cho form
 * 1. Thêm khách hàng Tổ chức.
 */
public class AccountObjectForm implements Serializable {

    private static final long serialVersionUID = 58866391194608428L;

    private Integer id; // PK Đối tượng
    private String accountObjectCode; // not_null Mã đối tượng
    private String accountObjectName; // Tên đối tượng
    private Integer gender; // Giới tính 0 = Nam; 1 = Nữ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthDate; // Ngày sinh

    private String birthPlace; // Nơi sinh
    private BigDecimal agreementSalary; //Lương thỏa thuận
    private BigDecimal salaryCoefficient; // Hệ số lương
    private Integer numberOfDependent; // Số người phụ thuộc
    private BigDecimal insuranceSalary; // Lương đóng bảo hiểm
    private String bankAccount; // Tài khoản ngân hàng (Là số TK cá nhân nếu là nhân viên)
    private String bankName; // Tên ngân hàng
    private String address; // Địa chỉ của đối tượng
    private String accountObjectGroupList; // Lưu VyCodeID của nhóm KH, NCC được chọn, cách nhau bởi dấu ;
    private String accountObjectGroupListCode; // Lưu AccountObjectGroupCode của các nhóm KH, NCC được chọn, cách nhau bởi dấu ;
    private String companyTaxCode; // Mã số thuế
    private String tel; // Số điện thoại cố định
    private String mobile; // Số điện thoại di động
    private String fax; // Fax

    @Email
    private String emailAddress; // Địa chỉ Email của tổ chức

    private String website; // Website
    private Integer paymentTermId; // Điều khoản thanh toán
    private BigDecimal maxDebtAmount; // Số nợ tối đa
    private Integer dueTime; // Hạn nợ ( Số ngày)
    private String identificationNumber; // Số CMTND của người liên hệ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date issueDate; // Ngày cấp CMTND người liên hệ

    private String issueBy; // Nơi cấp CMTND người liên hệ
    private String country; // Quốc gia
    private String provinceOrCity; // Tỉnh/ Thành phố
    private String district; // Quận/ Huyện
    private String wardOrCommune; // Phường/Xã
    private String prefix; // Xưng hô
    private String contactName; // Tên người liên hệ
    private String contactTitle; // Chức vụ người liên hệ (Nếu đối tượng là nhân viên thì đây chính là chức vụ mặc định của nhân viên đó)
    private String contactMobile; // Số điện thoại di động của người liên hệ
    private String otherContactMobile; // Số điện thoại di động khác của người liên hệ
    private String contactFixedTel; // Điện thoại cố định người liên hệ
    private String contactEmail; // Email người liên hệ
    private String contactAddress; // Địa chỉ người liên hệ
    private Boolean isVendor; // Là nhà cung cấp
    private Boolean isCustomer; // Là khách hàng
    private Boolean isEmployee; // Là cán bộ nhân viên
    private Integer accountObjectType; // 0 = là tổ chức; 1 = Là cá nhân
    private Boolean activeStatus; // Trạng thái theo dõi
    private Integer organizationUnitId; // Đơn vị của nhân viên
    private Integer branchId; // Chi nhánh
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private BigDecimal receiptableDebtAmount;
    private String shippingAddress;
    private String accountObjectGroupListName;
    private Integer employeeId;
    private String description;
    private String bankBranchName; // Chi nhánh tài khoản ngân hàng
    private String bankProvinceOrCity; // Tỉnh/Thành phố nơi mở tài khoản ngân hàng
    private String legalRepresentative;
    private String eInvoiceContactName;
    private String eInvoiceContactEmail;
    private String eInvoiceContactAddress;
    private String eInvoiceContactMobile;

    List<AccountObjectBankAccount> accountObjectBankAccountList;
    List<AccountObjectShippingAddress> accountObjectShippingAddressList;

    public AccountObjectForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountObjectCode() {
        return accountObjectCode;
    }

    public void setAccountObjectCode(String accountObjectCode) {
        this.accountObjectCode = accountObjectCode;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public BigDecimal getAgreementSalary() {
        return agreementSalary;
    }

    public void setAgreementSalary(BigDecimal agreementSalary) {
        this.agreementSalary = agreementSalary;
    }

    public BigDecimal getSalaryCoefficient() {
        return salaryCoefficient;
    }

    public void setSalaryCoefficient(BigDecimal salaryCoefficient) {
        this.salaryCoefficient = salaryCoefficient;
    }

    public Integer getNumberOfDependent() {
        return numberOfDependent;
    }

    public void setNumberOfDependent(Integer numberOfDependent) {
        this.numberOfDependent = numberOfDependent;
    }

    public BigDecimal getInsuranceSalary() {
        return insuranceSalary;
    }

    public void setInsuranceSalary(BigDecimal insuranceSalary) {
        this.insuranceSalary = insuranceSalary;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountObjectGroupList() {
        return accountObjectGroupList;
    }

    public void setAccountObjectGroupList(String accountObjectGroupList) {
        this.accountObjectGroupList = accountObjectGroupList;
    }

    public String getAccountObjectGroupListCode() {
        return accountObjectGroupListCode;
    }

    public void setAccountObjectGroupListCode(String accountObjectGroupListCode) {
        this.accountObjectGroupListCode = accountObjectGroupListCode;
    }

    public String getCompanyTaxCode() {
        return companyTaxCode;
    }

    public void setCompanyTaxCode(String companyTaxCode) {
        this.companyTaxCode = companyTaxCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public BigDecimal getMaxDebtAmount() {
        return maxDebtAmount;
    }

    public void setMaxDebtAmount(BigDecimal maxDebtAmount) {
        this.maxDebtAmount = maxDebtAmount;
    }

    public Integer getDueTime() {
        return dueTime;
    }

    public void setDueTime(Integer dueTime) {
        this.dueTime = dueTime;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvinceOrCity() {
        return provinceOrCity;
    }

    public void setProvinceOrCity(String provinceOrCity) {
        this.provinceOrCity = provinceOrCity;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWardOrCommune() {
        return wardOrCommune;
    }

    public void setWardOrCommune(String wardOrCommune) {
        this.wardOrCommune = wardOrCommune;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getOtherContactMobile() {
        return otherContactMobile;
    }

    public void setOtherContactMobile(String otherContactMobile) {
        this.otherContactMobile = otherContactMobile;
    }

    public String getContactFixedTel() {
        return contactFixedTel;
    }

    public void setContactFixedTel(String contactFixedTel) {
        this.contactFixedTel = contactFixedTel;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public Boolean getIsVendor() {
        return isVendor;
    }

    public void setIsVendor(Boolean vendor) {
        isVendor = vendor;
    }

    public Boolean getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(Boolean customer) {
        isCustomer = customer;
    }

    public Boolean getIsEmployee() {
        return isEmployee;
    }

    public void setIsEmployee(Boolean employee) {
        isEmployee = employee;
    }

    public Integer getAccountObjectType() {
        return accountObjectType;
    }

    public void setAccountObjectType(Integer accountObjectType) {
        this.accountObjectType = accountObjectType;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public BigDecimal getReceiptableDebtAmount() {
        return receiptableDebtAmount;
    }

    public void setReceiptableDebtAmount(BigDecimal receiptableDebtAmount) {
        this.receiptableDebtAmount = receiptableDebtAmount;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getAccountObjectGroupListName() {
        return accountObjectGroupListName;
    }

    public void setAccountObjectGroupListName(String accountObjectGroupListName) {
        this.accountObjectGroupListName = accountObjectGroupListName;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankProvinceOrCity() {
        return bankProvinceOrCity;
    }

    public void setBankProvinceOrCity(String bankProvinceOrCity) {
        this.bankProvinceOrCity = bankProvinceOrCity;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    public String geteInvoiceContactName() {
        return eInvoiceContactName;
    }

    public void seteInvoiceContactName(String eInvoiceContactName) {
        this.eInvoiceContactName = eInvoiceContactName;
    }

    public String geteInvoiceContactEmail() {
        return eInvoiceContactEmail;
    }

    public void seteInvoiceContactEmail(String eInvoiceContactEmail) {
        this.eInvoiceContactEmail = eInvoiceContactEmail;
    }

    public String geteInvoiceContactAddress() {
        return eInvoiceContactAddress;
    }

    public void seteInvoiceContactAddress(String eInvoiceContactAddress) {
        this.eInvoiceContactAddress = eInvoiceContactAddress;
    }

    public String geteInvoiceContactMobile() {
        return eInvoiceContactMobile;
    }

    public void seteInvoiceContactMobile(String eInvoiceContactMobile) {
        this.eInvoiceContactMobile = eInvoiceContactMobile;
    }

    public List<AccountObjectBankAccount> getAccountObjectBankAccountList() {
        return accountObjectBankAccountList;
    }

    public void setAccountObjectBankAccountList(List<AccountObjectBankAccount> accountObjectBankAccountList) {
        this.accountObjectBankAccountList = accountObjectBankAccountList;
    }

    public List<AccountObjectShippingAddress> getAccountObjectShippingAddressList() {
        return accountObjectShippingAddressList;
    }

    public void setAccountObjectShippingAddressList(List<AccountObjectShippingAddress> accountObjectShippingAddressList) {
        this.accountObjectShippingAddressList = accountObjectShippingAddressList;
    }
}
