package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.SAOrderDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Bảng master đơn đặt hàng.
 */
public class SAOrderForm implements Serializable {

    private static final long serialVersionUID = 5945711503894431341L;

    private Integer id; // PK

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày đơn hàng

    private String refNo; // Số đơn hàng
    private Integer status; // Tình trạng đơn hàng, 0: Chưa thực hiện; 1: Đang thực hiện; 2: Tạm dừng; 3: Hoàn thành; 4: Đã hủy bỏ
    private Integer refType; // Loại chứng từ
    private Integer branchId; // Chi nhánh
    private Integer accountObjectId; // FK: ID khách hàng là tổ chức hoặc cá nhân
    private String accountObjectName; // Tên khách hàng là tổ chức hoặc cá nhân
    private String accountObjectAddress; // Địa chỉ
    private String accountObjectTaxCode; // Mã số thuế
    private String receiver; // Người nhận hàng
    private Integer employeeId; // Nhân viên bán hàng

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliveryDate; // Ngày giao hàng

    private Boolean isCalculatedCost; // Check tính giá thành
    private Integer paymentTermId; // Mã điều kiện chiết khấu
    private Integer dueDay; // Số ngày được nợ
    private String journalMemo; // Ghi chú
    private String shippingAddress; // Địa chỉ giao hàng
    private String otherTerm; // Điều khoản khác
    private Integer quoteRefId; // FK với bảng báo giá
    private String currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỉ giá hối đoái
    private BigDecimal totalAmountOc; // Tổng tiền hàng
    private BigDecimal totalAmount; // Tổng tiền hàng quy đổi
    private BigDecimal totalDiscountAmountOc; // Tổng tiền chiết khấu
    private BigDecimal totalDiscountAmount; // Tổng tiền chiết khấu quy đổi
    private BigDecimal totalVatAmountOc; // Tổng tiền thuế
    private BigDecimal totalVatAmount; // Tổng tiền thuế quy đổi
    private Integer editVersion;
    private String modifiedBy;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private BigDecimal lastYearInvoiceAmountOc; // Giá trị đã xuất hóa đơn năm trước nguyên tệ
    private BigDecimal lastYearInvoiceAmount; // Giá trị đã xuất hóa đơn năm trước

    List<SAOrderDetail> saOrderDetailList;

    public SAOrderForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Boolean getIsCalculatedCost() {
        return isCalculatedCost;
    }

    public void setIsCalculatedCost(Boolean calculatedCost) {
        isCalculatedCost = calculatedCost;
    }

    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public Integer getDueDay() {
        return dueDay;
    }

    public void setDueDay(Integer dueDay) {
        this.dueDay = dueDay;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getOtherTerm() {
        return otherTerm;
    }

    public void setOtherTerm(String otherTerm) {
        this.otherTerm = otherTerm;
    }

    public Integer getQuoteRefId() {
        return quoteRefId;
    }

    public void setQuoteRefId(Integer quoteRefId) {
        this.quoteRefId = quoteRefId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public BigDecimal getLastYearInvoiceAmountOc() {
        return lastYearInvoiceAmountOc;
    }

    public void setLastYearInvoiceAmountOc(BigDecimal lastYearInvoiceAmountOc) {
        this.lastYearInvoiceAmountOc = lastYearInvoiceAmountOc;
    }

    public BigDecimal getLastYearInvoiceAmount() {
        return lastYearInvoiceAmount;
    }

    public void setLastYearInvoiceAmount(BigDecimal lastYearInvoiceAmount) {
        this.lastYearInvoiceAmount = lastYearInvoiceAmount;
    }

    public List<SAOrderDetail> getSaOrderDetailList() {
        return saOrderDetailList;
    }

    public void setSaOrderDetailList(List<SAOrderDetail> saOrderDetailList) {
        this.saOrderDetailList = saOrderDetailList;
    }

}
