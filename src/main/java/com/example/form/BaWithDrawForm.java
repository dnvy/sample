package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.BaWithDrawDetail;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class BaWithDrawForm {

    private Integer id; // NOT NULL. PK
    private Integer refType; // NOT NULL. Loại chứng từ (Lấy từ bảng RefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày chứng từ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // NOT NULL. Ngày hạch toán

    private String refNoFinance; // Số chứng từ Sổ tài chính
    private String refNoManagement; // Số chứng từ sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái (Ghi sổ/Chưa ghi sổ)
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi vào sổ quản trị
    private Integer bankAccountId; // Tài khoản ngân hàng
    private String bankName; // Tên ngân hàng đơn vị trả tiền
    private Integer reasonTypeId; // Nội dung thanh toán
    private String journalMemo; // Diễn giải Nội dung thanh toán
    private Integer accountObjectId; // Mã đơn vị nhận tiền
    private String accountObjectName; // Têm đơn vị nhận tiền (Tên đối tượng/Kho bạc nhà nước)
    private String accountObjectAddress; // Địa chỉ đơn vị nhận tiền
    private String accountObjectBankAccount; // Tài khoản đơn vị nhận tiền
    private String accountObjectBankName; // Tên ngân hàng đơn vị nhận tiền
    private String accountObjectContactName; // Người lĩnh tiền
    private String accountObjectContactIdNumber; // Số CMTND của Người lĩnh tiền

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date accountObjectContactIssueDate; // Ngày cấp CMTND Người lĩnh tiền

    private String accountObjectContactIssueBy; // Nơi cấp CMTND Người lĩnh tiền
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Số tiền
    private BigDecimal totalAmount; // NOT NULL. Số tiền= Tổng tiền từ Detail tương ứng với ID (Sum(Amount)) (Quy đổi)
    private Integer branchId; // Mã chi nhánh
    private Integer displayOnBook; // Xác định chứng từ được ghi vào sổ nào (0=Sổ tài chinh;1=Sổ quản trị;2=Cả hai)
    private Integer editVersion; // Ghi nhận phiên bản sửa chứng từ
    private Integer refOrder; // NOT NULL. Số thứ tự chứng từ nhập vào database
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer employeeId;
    private Boolean isImportVat; // NOT NULL.
    private Boolean isSpecialVat; // NOT NULL.
    private Boolean isEnvironmentVat; // NOT NULL.
    private Boolean isVat; // NOT NULL.
    private Boolean isCreateFromEbHistory;

    private String refNoCommon; // Số chứng từ = refNoFinance and/or refNoManagement.

    List<BaWithDrawDetail> baWithDrawDetailList;

    public BaWithDrawForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getRefNoFinance() {
        return refNoFinance;
    }

    public void setRefNoFinance(String refNoFinance) {
        this.refNoFinance = refNoFinance;
    }

    public String getRefNoManagement() {
        return refNoManagement;
    }

    public void setRefNoManagement(String refNoManagement) {
        this.refNoManagement = refNoManagement;
    }

    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getReasonTypeId() {
        return reasonTypeId;
    }

    public void setReasonTypeId(Integer reasonTypeId) {
        this.reasonTypeId = reasonTypeId;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectBankAccount() {
        return accountObjectBankAccount;
    }

    public void setAccountObjectBankAccount(String accountObjectBankAccount) {
        this.accountObjectBankAccount = accountObjectBankAccount;
    }

    public String getAccountObjectBankName() {
        return accountObjectBankName;
    }

    public void setAccountObjectBankName(String accountObjectBankName) {
        this.accountObjectBankName = accountObjectBankName;
    }

    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    public String getAccountObjectContactIdNumber() {
        return accountObjectContactIdNumber;
    }

    public void setAccountObjectContactIdNumber(String accountObjectContactIdNumber) {
        this.accountObjectContactIdNumber = accountObjectContactIdNumber;
    }

    public Date getAccountObjectContactIssueDate() {
        return accountObjectContactIssueDate;
    }

    public void setAccountObjectContactIssueDate(Date accountObjectContactIssueDate) {
        this.accountObjectContactIssueDate = accountObjectContactIssueDate;
    }

    public String getAccountObjectContactIssueBy() {
        return accountObjectContactIssueBy;
    }

    public void setAccountObjectContactIssueBy(String accountObjectContactIssueBy) {
        this.accountObjectContactIssueBy = accountObjectContactIssueBy;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Integer getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(Integer refOrder) {
        this.refOrder = refOrder;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Boolean getIsImportVat() {
        return isImportVat;
    }

    public void setIsImportVat(Boolean importVat) {
        isImportVat = importVat;
    }

    public Boolean getIsSpecialVat() {
        return isSpecialVat;
    }

    public void setIsSpecialVat(Boolean specialVat) {
        isSpecialVat = specialVat;
    }

    public Boolean getIsEnvironmentVat() {
        return isEnvironmentVat;
    }

    public void setIsEnvironmentVat(Boolean environmentVat) {
        isEnvironmentVat = environmentVat;
    }

    public Boolean getIsVat() {
        return isVat;
    }

    public void setIsVat(Boolean vat) {
        isVat = vat;
    }

    public Boolean getIsCreateFromEbHistory() {
        return isCreateFromEbHistory;
    }

    public void setIsCreateFromEbHistory(Boolean createFromEbHistory) {
        isCreateFromEbHistory = createFromEbHistory;
    }

    public List<BaWithDrawDetail> getBaWithDrawDetailList() {
        return baWithDrawDetailList;
    }

    public void setBaWithDrawDetailList(List<BaWithDrawDetail> baWithDrawDetailList) {
        this.baWithDrawDetailList = baWithDrawDetailList;
    }

    public String getRefNoCommon() {
        return refNoCommon;
    }

    public void setRefNoCommon(String refNoCommon) {
        this.refNoCommon = refNoCommon;
    }

}
