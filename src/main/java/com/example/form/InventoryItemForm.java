package com.example.form;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Vật tư hàng hóa
 */
public class InventoryItemForm implements Serializable {

    private static final long serialVersionUID = -8235267025716640863L;

    private Integer id; // NOT NULL
    private Integer branchId;
    private String barCode;

    @NotNull
    private String inventoryItemCode; // NOT NULL. Mã

    private String inventoryItemName; // Tên
    private Integer inventoryItemType; // Tính chất: 0 = Là Vật tư hàng hóa; 1 = Là thành phẩm; 2 = Là dịch vụ; 3 = Chỉ là diễn giải
    private String inventoryItemCategoryList; // Nhóm vật tư hàng hóa. Lưu VyCodeID của các nhóm VTHH, phân tách nhau bởi dấu ;
    private String inventoryItemCategoryCode; // Mã nhóm vật tư hàng hóa. Lưu Mã của các nhóm VTHH, phân tách nhau bởi dấu ;
    private String description; // Mô tả. Mô tả thêm thông tin hàng hóa, quy cách
    private Integer unitId; // Đơn vị tính chính
    private String guarantyPeriod; // Thời hạn bảo hành
    private BigDecimal minimumStock; // NOT NULL. Số lượng tồn tối thiểu
    private String inventoryItemSource; // Nguồn gốc. Nguồn gốc hàng hóa.
    private Integer defaultStockId;     // Kho ngầm định.
    private String inventoryAccount; // Tài khoản kho
    private String cogsAccount;         // Tài khoản chi phí (giá vốn)
    private String saleAccount; // Tài khoản doanh thu
    private BigDecimal purchaseDiscountRate; // NOT NULL.
    private BigDecimal unitPrice; // NOT NULL. Đơn giá mua gần nhất
    private BigDecimal salePrice1; // NOT NULL. Đơn giá bán 1
    private BigDecimal salePrice2; // NOT NULL. Đơn giá bán 2
    private BigDecimal salePrice3; // NOT NULL. Đơn giá bán 3
    private BigDecimal fixedSalePrice; // NOT NULL. Đơn giá bán cố định
    private boolean isUnitPriceAfterTax; // NOT NULL.
    private BigDecimal taxRate;
    private BigDecimal importTaxRate;
    private BigDecimal exportTaxRate;
    private Integer inventoryCategorySpecialTaxId; // Nhóm hàng hóa chịu thuế Tiêu thụ đặc biệt
    private boolean isSystem;
    private boolean activeStatus;
    private Boolean isSaleDiscount;
    private Integer discountType;
    private Boolean isFollowSerialNumber;
    private Integer costMethod;
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String inventoryItemCategoryName;
    private String specificity;
    private String purchaseDescription; // Diễn giải khi mua
    private String saleDescription; // Diễn giải khi bán
    private Integer taCareerGroupId; // ID nhóm ngành nghề.
    private String image;   // Hình ảnh sản phẩm
    private BigDecimal fixedUnitPrice; // NOT NULL. Đơn giá mua cố định
    private String frontEndFormula; // Công thức tính số lượng trên chứng từ bán hàng do NSD nhập.
    private String backEndFormula; // Công thức tính số lượng trên chứng từ bán hàng chuyển từ công thức NSD ra để dùng tính toán trên chứng từ bán hàng.
    private Integer baseOnFormula; // Lưu Value của Enum Dựa trên cách tính khi thiết lập công thức tính số lượng. Cho phép NSD không nhập liệu.
    private String discountAccount;
    private String saleOffAccount; // Tài khoản giảm giá.
    private String returnAccount; // Tài khoản trả lại

    public InventoryItemForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    public void setInventoryItemCode(String inventoryItemCode) {
        this.inventoryItemCode = inventoryItemCode;
    }

    public String getInventoryItemName() {
        return inventoryItemName;
    }

    public void setInventoryItemName(String inventoryItemName) {
        this.inventoryItemName = inventoryItemName;
    }

    public Integer getInventoryItemType() {
        return inventoryItemType;
    }

    public void setInventoryItemType(Integer inventoryItemType) {
        this.inventoryItemType = inventoryItemType;
    }

    public String getInventoryItemCategoryList() {
        return inventoryItemCategoryList;
    }

    public void setInventoryItemCategoryList(String inventoryItemCategoryList) {
        this.inventoryItemCategoryList = inventoryItemCategoryList;
    }

    public String getInventoryItemCategoryCode() {
        return inventoryItemCategoryCode;
    }

    public void setInventoryItemCategoryCode(String inventoryItemCategoryCode) {
        this.inventoryItemCategoryCode = inventoryItemCategoryCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getGuarantyPeriod() {
        return guarantyPeriod;
    }

    public void setGuarantyPeriod(String guarantyPeriod) {
        this.guarantyPeriod = guarantyPeriod;
    }

    public BigDecimal getMinimumStock() {
        return minimumStock;
    }

    public void setMinimumStock(BigDecimal minimumStock) {
        this.minimumStock = minimumStock;
    }

    public String getInventoryItemSource() {
        return inventoryItemSource;
    }

    public void setInventoryItemSource(String inventoryItemSource) {
        this.inventoryItemSource = inventoryItemSource;
    }

    public Integer getDefaultStockId() {
        return defaultStockId;
    }

    public void setDefaultStockId(Integer defaultStockId) {
        this.defaultStockId = defaultStockId;
    }

    public String getInventoryAccount() {
        return inventoryAccount;
    }

    public void setInventoryAccount(String inventoryAccount) {
        this.inventoryAccount = inventoryAccount;
    }

    public String getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(String cogsAccount) {
        this.cogsAccount = cogsAccount;
    }

    public String getSaleAccount() {
        return saleAccount;
    }

    public void setSaleAccount(String saleAccount) {
        this.saleAccount = saleAccount;
    }

    public BigDecimal getPurchaseDiscountRate() {
        return purchaseDiscountRate;
    }

    public void setPurchaseDiscountRate(BigDecimal purchaseDiscountRate) {
        this.purchaseDiscountRate = purchaseDiscountRate;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getSalePrice1() {
        return salePrice1;
    }

    public void setSalePrice1(BigDecimal salePrice1) {
        this.salePrice1 = salePrice1;
    }

    public BigDecimal getSalePrice2() {
        return salePrice2;
    }

    public void setSalePrice2(BigDecimal salePrice2) {
        this.salePrice2 = salePrice2;
    }

    public BigDecimal getSalePrice3() {
        return salePrice3;
    }

    public void setSalePrice3(BigDecimal salePrice3) {
        this.salePrice3 = salePrice3;
    }

    public BigDecimal getFixedSalePrice() {
        return fixedSalePrice;
    }

    public void setFixedSalePrice(BigDecimal fixedSalePrice) {
        this.fixedSalePrice = fixedSalePrice;
    }

    public boolean isUnitPriceAfterTax() {
        return isUnitPriceAfterTax;
    }

    public void setUnitPriceAfterTax(boolean unitPriceAfterTax) {
        isUnitPriceAfterTax = unitPriceAfterTax;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getImportTaxRate() {
        return importTaxRate;
    }

    public void setImportTaxRate(BigDecimal importTaxRate) {
        this.importTaxRate = importTaxRate;
    }

    public BigDecimal getExportTaxRate() {
        return exportTaxRate;
    }

    public void setExportTaxRate(BigDecimal exportTaxRate) {
        this.exportTaxRate = exportTaxRate;
    }

    public Integer getInventoryCategorySpecialTaxId() {
        return inventoryCategorySpecialTaxId;
    }

    public void setInventoryCategorySpecialTaxId(Integer inventoryCategorySpecialTaxId) {
        this.inventoryCategorySpecialTaxId = inventoryCategorySpecialTaxId;
    }

    public boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(boolean system) {
        this.isSystem = system;
    }

    public boolean getIsActiveStatus() {
        return activeStatus;
    }

    public void setIsActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Boolean getIsSaleDiscount() {
        return isSaleDiscount;
    }

    public void setIsSaleDiscount(Boolean saleDiscount) {
        this.isSaleDiscount = saleDiscount;
    }

    public Integer getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    public Boolean getIsFollowSerialNumber() {
        return isFollowSerialNumber;
    }

    public void setIsFollowSerialNumber(Boolean followSerialNumber) {
        this.isFollowSerialNumber = followSerialNumber;
    }

    public Integer getCostMethod() {
        return costMethod;
    }

    public void setCostMethod(Integer costMethod) {
        this.costMethod = costMethod;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getInventoryItemCategoryName() {
        return inventoryItemCategoryName;
    }

    public void setInventoryItemCategoryName(String inventoryItemCategoryName) {
        this.inventoryItemCategoryName = inventoryItemCategoryName;
    }

    public String getSpecificity() {
        return specificity;
    }

    public void setSpecificity(String specificity) {
        this.specificity = specificity;
    }

    public String getPurchaseDescription() {
        return purchaseDescription;
    }

    public void setPurchaseDescription(String purchaseDescription) {
        this.purchaseDescription = purchaseDescription;
    }

    public String getSaleDescription() {
        return saleDescription;
    }

    public void setSaleDescription(String saleDescription) {
        this.saleDescription = saleDescription;
    }

    public Integer getTaCareerGroupId() {
        return taCareerGroupId;
    }

    public void setTaCareerGroupId(Integer taCareerGroupId) {
        this.taCareerGroupId = taCareerGroupId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BigDecimal getFixedUnitPrice() {
        return fixedUnitPrice;
    }

    public void setFixedUnitPrice(BigDecimal fixedUnitPrice) {
        this.fixedUnitPrice = fixedUnitPrice;
    }

    public String getFrontEndFormula() {
        return frontEndFormula;
    }

    public void setFrontEndFormula(String frontEndFormula) {
        this.frontEndFormula = frontEndFormula;
    }

    public String getBackEndFormula() {
        return backEndFormula;
    }

    public void setBackEndFormula(String backEndFormula) {
        this.backEndFormula = backEndFormula;
    }

    public Integer getBaseOnFormula() {
        return baseOnFormula;
    }

    public void setBaseOnFormula(Integer baseOnFormula) {
        this.baseOnFormula = baseOnFormula;
    }

    public String getDiscountAccount() {
        return discountAccount;
    }

    public void setDiscountAccount(String discountAccount) {
        this.discountAccount = discountAccount;
    }

    public String getSaleOffAccount() {
        return saleOffAccount;
    }

    public void setSaleOffAccount(String saleOffAccount) {
        this.saleOffAccount = saleOffAccount;
    }

    public String getReturnAccount() {
        return returnAccount;
    }

    public void setReturnAccount(String returnAccount) {
        this.returnAccount = returnAccount;
    }

}
