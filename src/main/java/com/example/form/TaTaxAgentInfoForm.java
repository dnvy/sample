package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Thiết lập thông tin đại lý thuế.
 */
public class TaTaxAgentInfoForm implements Serializable {

    private static final long serialVersionUID = -4931613896610538317L;

    private Integer id; // PK
    private String taxAgentName; // Tên đại lý thuế
    private String taxCode; // Mã số thuế
    private String address; // Địa chỉ
    private String district; // Quận/huyện
    private String provinceOrCity; // Tỉnh/ Thành phố
    private String tel; // Điện thoại
    private String fax; // Fax

    @Email
    private String email; // Email

    private String contractCode; // Hợp đồng số

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date contractDate; // Ngày hợp đồng

    private String employeeName; // Nhân viên đại lý thuế
    private String certificateNo; // Chứng chỉ hành nghề số
    private Boolean isDisplayOnDeclaration; // Có hiển thị thông tin đại lý thuế lên tờ khai hay không
    private Timestamp createdDate; // Ngày lập chứng từ
    private String createdBy; // Người lập chứng từ
    private Timestamp modifiedDate; // Ngày sửa chứng từ lần cuối
    private String modifiedBy; // Người sửa chứng từ lần cuối
    private String taxationBureauCode; // Mã chi cục thuế
    private String taxOrganManagementCode; // mã đơn vị quản lý chi cục thuế
    private Integer branchId;
    private String provideUnit; // Đơn vị cung cấp dịch vụ kế toán
    private String certificateNoUnit; // Số chứng chỉ hành nghề của đơn vị cung cấp DV kế toán

    public TaTaxAgentInfoForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaxAgentName() {
        return taxAgentName;
    }

    public void setTaxAgentName(String taxAgentName) {
        this.taxAgentName = taxAgentName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvinceOrCity() {
        return provinceOrCity;
    }

    public void setProvinceOrCity(String provinceOrCity) {
        this.provinceOrCity = provinceOrCity;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getCertificateNo() {
        return certificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        this.certificateNo = certificateNo;
    }

    public Boolean getIsDisplayOnDeclaration() {
        return isDisplayOnDeclaration;
    }

    public void setIsDisplayOnDeclaration(Boolean displayOnDeclaration) {
        isDisplayOnDeclaration = displayOnDeclaration;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTaxationBureauCode() {
        return taxationBureauCode;
    }

    public void setTaxationBureauCode(String taxationBureauCode) {
        this.taxationBureauCode = taxationBureauCode;
    }

    public String getTaxOrganManagementCode() {
        return taxOrganManagementCode;
    }

    public void setTaxOrganManagementCode(String taxOrganManagementCode) {
        this.taxOrganManagementCode = taxOrganManagementCode;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getProvideUnit() {
        return provideUnit;
    }

    public void setProvideUnit(String provideUnit) {
        this.provideUnit = provideUnit;
    }

    public String getCertificateNoUnit() {
        return certificateNoUnit;
    }

    public void setCertificateNoUnit(String certificateNoUnit) {
        this.certificateNoUnit = certificateNoUnit;
    }
}
