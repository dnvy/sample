package com.example.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UnitForm implements Serializable {

    private static final long serialVersionUID = -8213261069505249115L;

    private Integer id; // PK

    @NotNull
    @Size(min = 1, max = 128)
    private String unitName; //Đơn vị tính

    @Size(max = 512)
    private String description; // Mô tả

    private Boolean activeStatus; // Trạng thái theo dõi

    public UnitForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }
}
