package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.SaQuoteDetail;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Bảng báo giá.
 */
public class SaQuoteForm {

    private Integer id; // NOT NULL. PK

    @NotNull
    private String refNo; // NOT NULL. Số báo giá

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // NOT NULL. Ngày báo giá

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date effectiveDate; // Ngày hiệu lực

    private Integer refType; // NOT NULL. Loại chứng từ
    private Integer branchId; // Chi nhánh
    private Integer accountObjectId; // ID khách hàng là tổ chức hoặc cá nhân
    private String accountObjectName; // Tên khách hàng
    private String accountObjectAddress; // Địa chỉ khách hàng
    private String accountObjectTaxCode; // Mã số thuế
    private String accountObjectContactName; // Người liên hệ
    private String journalMemo; // Ghi chú
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // NOT NULL. Tỉ giá hối đoái
    private BigDecimal totalAmountOc; // NOT NULL. Tổng tiền hàng
    private BigDecimal totalAmount; // NOT NULL. Tổng tiền hàng quy đổi
    private BigDecimal totalDiscountAmountOc; // NOT NULL. Tổng tiền chiết khấu
    private BigDecimal totalDiscountAmount; // NOT NULL. Tổng tiền chiết khấu quy đổi
    private BigDecimal totalVatAmountOc; // NOT NULL. Tổng tiền thuế GTGT
    private BigDecimal totalVatAmount; // NOT NULL. Tổng tiền thuế GTGT quy đổi
    private Integer editVersion;
    private Timestamp modifiedDate;
    private String createdBy;
    private Timestamp createdDate;
    private String modifiedBy;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private Integer employeeId;

    List<SaQuoteDetail> saQuoteDetailList;

    public SaQuoteForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectTaxCode() {
        return accountObjectTaxCode;
    }

    public void setAccountObjectTaxCode(String accountObjectTaxCode) {
        this.accountObjectTaxCode = accountObjectTaxCode;
    }

    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getTotalAmountOc() {
        return totalAmountOc;
    }

    public void setTotalAmountOc(BigDecimal totalAmountOc) {
        this.totalAmountOc = totalAmountOc;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalDiscountAmountOc() {
        return totalDiscountAmountOc;
    }

    public void setTotalDiscountAmountOc(BigDecimal totalDiscountAmountOc) {
        this.totalDiscountAmountOc = totalDiscountAmountOc;
    }

    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public BigDecimal getTotalVatAmountOc() {
        return totalVatAmountOc;
    }

    public void setTotalVatAmountOc(BigDecimal totalVatAmountOc) {
        this.totalVatAmountOc = totalVatAmountOc;
    }

    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public List<SaQuoteDetail> getSaQuoteDetailList() {
        return saQuoteDetailList;
    }

    public void setSaQuoteDetailList(List<SaQuoteDetail> saQuoteDetailList) {
        this.saQuoteDetailList = saQuoteDetailList;
    }

}
