package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Date;

public class OrganizationUnitForm {

    private Integer id; // PK của bảng
    private Integer branchId; // Chi nhánh
    private String organizationUnitCode; // NOT NULL. Mã đơn vị
    private String organizationUnitName; // NOT NULL. Tên đơn vị
    private Boolean isSystem; // NOT NULL. Thuộc hệ thống
    private String vyCodeId; //
    private Integer grade; // Cấp tổ chức
    private Integer parentId; // ID đơn vị cha
    private Boolean isParent; // NOT NULL. Là đơn vị cha
    private String address; // Địa chỉ

    /**
     * Cấp tổ chức:
     * 1 - Tổng công ty;
     * 2 - Chi nhánh;
     * 3 - VP/TT;
     * 4 - Phòng ban;
     * 5- Phân xưởng;
     * 6 - Nhóm/Tổ,hội
     */
    private Integer organizationUnitTypeId; // NOT NULL.

    private String businessRegistrationNumber; // Số đăng ký kinh doanh

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date businessRegistrationNumberIssuedDate; // Ngày cấp

    private String businessRegistrationNumberIssuedPlace; // Nơi cấp
    private Boolean isDependent; // 0 = Hạch toán độc lập; 1 = Hạch toán  phụ thuộc
    private Boolean isPrivateVatDeclaration; // NOT NULL. Kê khai thuế giá trị gia tăng riêng
    private String costAccount; // Tài khoản chi phí lương
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi.
    private String companyTaxCode; // Mã số thuế
    private String companyTel; // Số điện thoại
    private String companyFax; // Fax
    private String companyEmail; // Email
    private String companyWebsite; // Website
    private Integer companyBankAccountId; // Tài khoản ngân hàng
    private String companyOwnerName; // Tên đơn vị chủ quản
    private String companyOwnerTaxCode; // Mã số thuế đơn vị chủ quản
    private String directorTitle; // Tiêu đề người ký là giám đốc
    private String directorName; // Tên giám đốc
    private String chiefOfAccountingTitle; // Tiêu đề người ký là Kế toán trưởng
    private String chiefOfAccountingName; // Tên Kế toán trưởng
    private String storeKeeperTitle; // Tiêu đề người ký là Thủ kho
    private String storeKeeperName; // Tên thủ kho
    private String cashierTitle; // Tiêu đề người ký là Thủ quỹ
    private String cashierName; // Tên Thủ quỹ
    private String reporterTitle; // Tiêu đề người ký là Người lập biểu
    private String reporterName; // Tên Người lập biểu
    private Boolean isPrintSigner; // NOT NULL. In tên người ký lên chứng từ, báo cáo
    private Boolean isGetReporterNameByUserLogIn; // NOT NULL. Lấy tên người lập biểu theo tên người đăng nhập. not null
    private String createdBy;
    private Timestamp createdDate;
    private String modifiedBy;
    private Timestamp modifiedDate;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.
    private String companyDistrict; // Quận/Huyện
    private String companyCity; // Tỉnh/TP

    // 3. Đặc điểm họa động, chế độ kế toán.
    private String i;
    private String i1;
    private String i2;
    private String i3;
    private String i4;
    private String i5;
    private String i6;
    private String ii;
    private String iii;
    private String ii1;
    private String ii2;
    private String iii1;
    private String iii2;
    private String iv;
    private String iv1;
    private String iv2;
    private String iv3;
    private String iv4;
    private String iv41;
    private String iv42;
    private String iv5;
    private String iv6;
    private String iv7;
    private String iv71;
    private String iv72;
    private String iv73;
    private String iv8;
    private String iv9;
    private String iv10;
    private String iv11;
    private String iv111;
    private String iv112;
    private String iv113;
    private String iv114;
    private String iv12;
    private String v15;
    private String v16;
    private String vii;
    private String vii1;
    private String viii;
    private String viii1;
    private String viii2;
    private String viii3;
    private String viii4;
    private String viii5;

    public OrganizationUnitForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getOrganizationUnitCode() {
        return organizationUnitCode;
    }

    public void setOrganizationUnitCode(String organizationUnitCode) {
        this.organizationUnitCode = organizationUnitCode;
    }

    public String getOrganizationUnitName() {
        return organizationUnitName;
    }

    public void setOrganizationUnitName(String organizationUnitName) {
        this.organizationUnitName = organizationUnitName;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getOrganizationUnitTypeId() {
        return organizationUnitTypeId;
    }

    public void setOrganizationUnitTypeId(Integer organizationUnitTypeId) {
        this.organizationUnitTypeId = organizationUnitTypeId;
    }

    public String getBusinessRegistrationNumber() {
        return businessRegistrationNumber;
    }

    public void setBusinessRegistrationNumber(String businessRegistrationNumber) {
        this.businessRegistrationNumber = businessRegistrationNumber;
    }

    public Date getBusinessRegistrationNumberIssuedDate() {
        return businessRegistrationNumberIssuedDate;
    }

    public void setBusinessRegistrationNumberIssuedDate(Date businessRegistrationNumberIssuedDate) {
        this.businessRegistrationNumberIssuedDate = businessRegistrationNumberIssuedDate;
    }

    public String getBusinessRegistrationNumberIssuedPlace() {
        return businessRegistrationNumberIssuedPlace;
    }

    public void setBusinessRegistrationNumberIssuedPlace(String businessRegistrationNumberIssuedPlace) {
        this.businessRegistrationNumberIssuedPlace = businessRegistrationNumberIssuedPlace;
    }

    public Boolean getIsDependent() {
        return isDependent;
    }

    public void setIsDependent(Boolean dependent) {
        isDependent = dependent;
    }

    public Boolean getIsPrivateVatDeclaration() {
        return isPrivateVatDeclaration;
    }

    public void setIsPrivateVatDeclaration(Boolean privateVatDeclaration) {
        isPrivateVatDeclaration = privateVatDeclaration;
    }

    public String getCostAccount() {
        return costAccount;
    }

    public void setCostAccount(String costAccount) {
        this.costAccount = costAccount;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCompanyTaxCode() {
        return companyTaxCode;
    }

    public void setCompanyTaxCode(String companyTaxCode) {
        this.companyTaxCode = companyTaxCode;
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    public String getCompanyFax() {
        return companyFax;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public Integer getCompanyBankAccountId() {
        return companyBankAccountId;
    }

    public void setCompanyBankAccountId(Integer companyBankAccountId) {
        this.companyBankAccountId = companyBankAccountId;
    }

    public String getCompanyOwnerName() {
        return companyOwnerName;
    }

    public void setCompanyOwnerName(String companyOwnerName) {
        this.companyOwnerName = companyOwnerName;
    }

    public String getCompanyOwnerTaxCode() {
        return companyOwnerTaxCode;
    }

    public void setCompanyOwnerTaxCode(String companyOwnerTaxCode) {
        this.companyOwnerTaxCode = companyOwnerTaxCode;
    }

    public String getDirectorTitle() {
        return directorTitle;
    }

    public void setDirectorTitle(String directorTitle) {
        this.directorTitle = directorTitle;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getChiefOfAccountingTitle() {
        return chiefOfAccountingTitle;
    }

    public void setChiefOfAccountingTitle(String chiefOfAccountingTitle) {
        this.chiefOfAccountingTitle = chiefOfAccountingTitle;
    }

    public String getChiefOfAccountingName() {
        return chiefOfAccountingName;
    }

    public void setChiefOfAccountingName(String chiefOfAccountingName) {
        this.chiefOfAccountingName = chiefOfAccountingName;
    }

    public String getStoreKeeperTitle() {
        return storeKeeperTitle;
    }

    public void setStoreKeeperTitle(String storeKeeperTitle) {
        this.storeKeeperTitle = storeKeeperTitle;
    }

    public String getStoreKeeperName() {
        return storeKeeperName;
    }

    public void setStoreKeeperName(String storeKeeperName) {
        this.storeKeeperName = storeKeeperName;
    }

    public String getCashierTitle() {
        return cashierTitle;
    }

    public void setCashierTitle(String cashierTitle) {
        this.cashierTitle = cashierTitle;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public String getReporterTitle() {
        return reporterTitle;
    }

    public void setReporterTitle(String reporterTitle) {
        this.reporterTitle = reporterTitle;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public Boolean getIsPrintSigner() {
        return isPrintSigner;
    }

    public void setIsPrintSigner(Boolean printSigner) {
        isPrintSigner = printSigner;
    }

    public Boolean getIsGetReporterNameByUserLogIn() {
        return isGetReporterNameByUserLogIn;
    }

    public void setIsGetReporterNameByUserLogIn(Boolean getReporterNameByUserLogIn) {
        isGetReporterNameByUserLogIn = getReporterNameByUserLogIn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    public String getCompanyDistrict() {
        return companyDistrict;
    }

    public void setCompanyDistrict(String companyDistrict) {
        this.companyDistrict = companyDistrict;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    // 3. Đặc điểm họa động, chế độ kế toán.

    public String getI() {
        return i;
    }

    public void setI(String i) {
        this.i = i;
    }

    public String getI1() {
        return i1;
    }

    public void setI1(String i1) {
        this.i1 = i1;
    }

    public String getI2() {
        return i2;
    }

    public void setI2(String i2) {
        this.i2 = i2;
    }

    public String getI3() {
        return i3;
    }

    public void setI3(String i3) {
        this.i3 = i3;
    }

    public String getI4() {
        return i4;
    }

    public void setI4(String i4) {
        this.i4 = i4;
    }

    public String getI5() {
        return i5;
    }

    public void setI5(String i5) {
        this.i5 = i5;
    }

    public String getI6() {
        return i6;
    }

    public void setI6(String i6) {
        this.i6 = i6;
    }

    public String getIi() {
        return ii;
    }

    public void setIi(String ii) {
        this.ii = ii;
    }

    public String getIii() {
        return iii;
    }

    public void setIii(String iii) {
        this.iii = iii;
    }

    public String getIi1() {
        return ii1;
    }

    public void setIi1(String ii1) {
        this.ii1 = ii1;
    }

    public String getIi2() {
        return ii2;
    }

    public void setIi2(String ii2) {
        this.ii2 = ii2;
    }

    public String getIii1() {
        return iii1;
    }

    public void setIii1(String iii1) {
        this.iii1 = iii1;
    }

    public String getIii2() {
        return iii2;
    }

    public void setIii2(String iii2) {
        this.iii2 = iii2;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getIv1() {
        return iv1;
    }

    public void setIv1(String iv1) {
        this.iv1 = iv1;
    }

    public String getIv2() {
        return iv2;
    }

    public void setIv2(String iv2) {
        this.iv2 = iv2;
    }

    public String getIv3() {
        return iv3;
    }

    public void setIv3(String iv3) {
        this.iv3 = iv3;
    }

    public String getIv4() {
        return iv4;
    }

    public void setIv4(String iv4) {
        this.iv4 = iv4;
    }

    public String getIv41() {
        return iv41;
    }

    public void setIv41(String iv41) {
        this.iv41 = iv41;
    }

    public String getIv42() {
        return iv42;
    }

    public void setIv42(String iv42) {
        this.iv42 = iv42;
    }

    public String getIv5() {
        return iv5;
    }

    public void setIv5(String iv5) {
        this.iv5 = iv5;
    }

    public String getIv6() {
        return iv6;
    }

    public void setIv6(String iv6) {
        this.iv6 = iv6;
    }

    public String getIv7() {
        return iv7;
    }

    public void setIv7(String iv7) {
        this.iv7 = iv7;
    }

    public String getIv71() {
        return iv71;
    }

    public void setIv71(String iv71) {
        this.iv71 = iv71;
    }

    public String getIv72() {
        return iv72;
    }

    public void setIv72(String iv72) {
        this.iv72 = iv72;
    }

    public String getIv73() {
        return iv73;
    }

    public void setIv73(String iv73) {
        this.iv73 = iv73;
    }

    public String getIv8() {
        return iv8;
    }

    public void setIv8(String iv8) {
        this.iv8 = iv8;
    }

    public String getIv9() {
        return iv9;
    }

    public void setIv9(String iv9) {
        this.iv9 = iv9;
    }

    public String getIv10() {
        return iv10;
    }

    public void setIv10(String iv10) {
        this.iv10 = iv10;
    }

    public String getIv11() {
        return iv11;
    }

    public void setIv11(String iv11) {
        this.iv11 = iv11;
    }

    public String getIv111() {
        return iv111;
    }

    public void setIv111(String iv111) {
        this.iv111 = iv111;
    }

    public String getIv112() {
        return iv112;
    }

    public void setIv112(String iv112) {
        this.iv112 = iv112;
    }

    public String getIv113() {
        return iv113;
    }

    public void setIv113(String iv113) {
        this.iv113 = iv113;
    }

    public String getIv114() {
        return iv114;
    }

    public void setIv114(String iv114) {
        this.iv114 = iv114;
    }

    public String getIv12() {
        return iv12;
    }

    public void setIv12(String iv12) {
        this.iv12 = iv12;
    }

    public String getV15() {
        return v15;
    }

    public void setV15(String v15) {
        this.v15 = v15;
    }

    public String getV16() {
        return v16;
    }

    public void setV16(String v16) {
        this.v16 = v16;
    }

    public String getVii() {
        return vii;
    }

    public void setVii(String vii) {
        this.vii = vii;
    }

    public String getVii1() {
        return vii1;
    }

    public void setVii1(String vii1) {
        this.vii1 = vii1;
    }

    public String getViii() {
        return viii;
    }

    public void setViii(String viii) {
        this.viii = viii;
    }

    public String getViii1() {
        return viii1;
    }

    public void setViii1(String viii1) {
        this.viii1 = viii1;
    }

    public String getViii2() {
        return viii2;
    }

    public void setViii2(String viii2) {
        this.viii2 = viii2;
    }

    public String getViii3() {
        return viii3;
    }

    public void setViii3(String viii3) {
        this.viii3 = viii3;
    }

    public String getViii4() {
        return viii4;
    }

    public void setViii4(String viii4) {
        this.viii4 = viii4;
    }

    public String getViii5() {
        return viii5;
    }

    public void setViii5(String viii5) {
        this.viii5 = viii5;
    }
}
