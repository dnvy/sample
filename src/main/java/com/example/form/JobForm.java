package com.example.form;

import com.example.entity.JobProduct;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Đối tượng tập hợp chi phí.
 */
public class JobForm implements Serializable {

    private static final long serialVersionUID = 5006397710453615479L;

    private Integer id; // PK Công việc, công trình dự án
    private String jobCode; // Mã công việc
    private String jobName; // Tên Công việc
    private Integer parentId;
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent;
    private Integer jobType; // Loại có giá trị: 0 = Phân xưởng, 1 = Sản phẩm, 2 = Quy trình sản xuất, 3 = Công đoạn
    private String description; // Diễn giải
    private Boolean activeStatus; // Trạng thái theo dõi
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isSystem; // Là hệ thống
    private Integer inventoryItemId; // Thành phẩm
    private Integer productionProcessType; // Loại quy trình sản xuất (0=Phân bước liên tục;1=phân bước song song)
    private Integer stage; // Công đoạn thứ n
    private Integer stageId; // Công đoạn trước
    private Integer branchId; // Chi nhánh
    private Integer collectCostInStageType; // 0:Tập hợp chi phí đến công đoạn; 1: Tập hợp chi phi đến từng sản phẩm trong công đoạn.
    private Boolean isSemiProduct; // Là bán thành phẩm của công đoạn
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    List<JobProduct> jobProductList; // Lưu ý, lable của một cột của table là "Ghi chú", không phải "Tên thành phẩm".

    public JobForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    public Integer getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(Integer inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public Integer getProductionProcessType() {
        return productionProcessType;
    }

    public void setProductionProcessType(Integer productionProcessType) {
        this.productionProcessType = productionProcessType;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getCollectCostInStageType() {
        return collectCostInStageType;
    }

    public void setCollectCostInStageType(Integer collectCostInStageType) {
        this.collectCostInStageType = collectCostInStageType;
    }

    public Boolean getIsSemiProduct() {
        return isSemiProduct;
    }

    public void setIsSemiProduct(Boolean semiProduct) {
        isSemiProduct = semiProduct;
    }

    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

    public List<JobProduct> getJobProductList() {
        return jobProductList;
    }

    public void setJobProductList(List<JobProduct> jobProductList) {
        this.jobProductList = jobProductList;
    }

}
