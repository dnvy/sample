package com.example.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Dùng cho form Thêm mới Khoản mục chi phí và Validation.
 */
public class ExpenseItemForm implements Serializable {

    private static final long serialVersionUID = 1130125165082089951L;

    private Integer id; // PK

    @NotNull
    @Size(min = 1, max = 256)
    private String expenseItemCode; // NOT NULL. Mã khoản mục chi phí

    @NotNull
    @Size(min = 1, max = 512)
    private String expenseItemName; // NOT NULL. Tên khoản mục chi phí

    @Size(min = 0, max = 512)
    private String description; // Diễn giải

    private Integer parentId; // ID khoản mục chi phí cha
    private String vyCodeId;
    private Integer grade;
    private Boolean isParent; // NOT NULL.
    private Boolean activeStatus; // NOT NULL. Trạng thái theo dõi
    private Boolean isSystem; // NOT NULL. Thuộc hệ thống
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private String sortVyCodeId; // Cột dùng để sort trên báo cáo. Không sử dụng trên giao diện.

    public ExpenseItemForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExpenseItemCode() {
        return expenseItemCode;
    }

    public void setExpenseItemCode(String expenseItemCode) {
        this.expenseItemCode = expenseItemCode;
    }

    public String getExpenseItemName() {
        return expenseItemName;
    }

    public void setExpenseItemName(String expenseItemName) {
        this.expenseItemName = expenseItemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getVyCodeId() {
        return vyCodeId;
    }

    public void setVyCodeId(String vyCodeId) {
        this.vyCodeId = vyCodeId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean system) {
        isSystem = system;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getSortVyCodeId() {
        return sortVyCodeId;
    }

    public void setSortVyCodeId(String sortVyCodeId) {
        this.sortVyCodeId = sortVyCodeId;
    }

}
