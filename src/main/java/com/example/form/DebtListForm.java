package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

/**
 * Danh sách đợt thu nợ.
 */
public class DebtListForm implements Serializable {

    private static final long serialVersionUID = -3934782420635447375L;

    private Integer id; // NOT NULL. PK
    private Integer branchId; // NOT NULL. Chi nhánh
    private String debtListName; // NOT NULL. Tên đợt thu nợ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date fromDate; // NOT NULL. Từ ngày

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date toDate; // NOT NULL. Đến ngày

    private BigDecimal totalReceiptableAmount; // NOT NULL. Tổng công nợ
    private BigDecimal targetPercent; // NOT NULL. Mục tiêu thu được (%)
    private BigDecimal targetAmount; // NOT NULL. Mục tiêu thu được (Số tiền)
    private String description; // Diễn giải
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;

    public DebtListForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getDebtListName() {
        return debtListName;
    }

    public void setDebtListName(String debtListName) {
        this.debtListName = debtListName;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getTotalReceiptableAmount() {
        return totalReceiptableAmount;
    }

    public void setTotalReceiptableAmount(BigDecimal totalReceiptableAmount) {
        this.totalReceiptableAmount = totalReceiptableAmount;
    }

    public BigDecimal getTargetPercent() {
        return targetPercent;
    }

    public void setTargetPercent(BigDecimal targetPercent) {
        this.targetPercent = targetPercent;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

}
