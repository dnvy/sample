package com.example.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Bảng lưu cấu hình tự tăng của Số chứng từ.
 */
public class SysAutoIdForm implements Serializable {

    private static final long serialVersionUID = -6889751984958156309L;

    private Integer id; // PK.
    private Integer refTypeCategory; // FK
    private String refTypeCategoryName; // Tên loại chứng từ

    @NotNull
    private String prefixString; // Tiền tố

    @NotNull
    @Min(0)
    private Integer maxExistValue; // Giá trị

    @NotNull
    @Min(1)
    private Integer lengthOfValue; // Độ dài


    private String suffix; // Hậu tố
    private Integer branchId; // Chi nhánh. Các thiết lập mặc định mang đi thì BranchID = NULL
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer displayOnBook; // 0=Sổ tài chính; 1=Sổ quản trị; 2=Tự tăng trên cả 2 sổ

    public SysAutoIdForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefTypeCategory() {
        return refTypeCategory;
    }

    public void setRefTypeCategory(Integer refTypeCategory) {
        this.refTypeCategory = refTypeCategory;
    }

    public String getRefTypeCategoryName() {
        return refTypeCategoryName;
    }

    public void setRefTypeCategoryName(String refTypeCategoryName) {
        this.refTypeCategoryName = refTypeCategoryName;
    }

    public String getPrefixString() {
        return prefixString;
    }

    public void setPrefixString(String prefixString) {
        this.prefixString = prefixString;
    }

    public Integer getMaxExistValue() {
        return maxExistValue;
    }

    public void setMaxExistValue(Integer maxExistValue) {
        this.maxExistValue = maxExistValue;
    }

    public Integer getLengthOfValue() {
        return lengthOfValue;
    }

    public void setLengthOfValue(Integer lengthOfValue) {
        this.lengthOfValue = lengthOfValue;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }
}
