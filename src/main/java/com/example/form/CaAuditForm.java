package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.CaAuditDetail;
import com.example.entity.CaAuditMemberDetail;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Kiểm kê quỹ.
 */
public class CaAuditForm implements Serializable {

    private static final long serialVersionUID = 7874009724575672614L;

    private Integer id; // NOT NULL. PK
    private Integer branchId; // Mã chi nhánh.

    @NotNull
    private String refNo; // Số kiểm kê

    private Integer refType; // NOT NULL. Loại chứng từ (Lấy từ bảng RefType)

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date refDate; // Ngày kiểm kê

    private Timestamp refTime; // Giờ kiểm kê

    private String journalMemo; // Diễn giải

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date auditDate; // NOT NULL. Kiểm kê đến ngày

    private Integer currencyId; // Loại tiền: Mặc định đồng tiền hạch toán

    @NotNull
    private BigDecimal totalAuditAmount; // NOT NULL. Tổng số tiền kiểm kê

    @NotNull
    private BigDecimal totalBalanceAmount; // NOT NULL. Tổng dư nợ tài khoản theo sổ kế toán tiền mặt

    private String reason; // Lý do
    private String conclusion; // Kết luận
    private Boolean isExecuted; // NOT NULL. Đã xử lý chênh lệch
    private Integer displayOnBook; // NOT NULL. 0 = Kiểm kê trên Sổ tài chinh;1 = Kiểm kê trên Sổ quản trị
    private Integer editVersion; // Phiên bản sửa chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Integer caReceiptRefId; // Số chứng từ xử lý thừa, thiếu --> phiếu thu, phiếu chi tiền.
    private Integer caPaymentRefId;

    @NotNull
    private String refTimeString;

    List<CaAuditDetail> caAuditDetailList;
    List<CaAuditMemberDetail> caAuditMemberDetailList;

    public CaAuditForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Date getRefDate() {
        return refDate;
    }

    public void setRefDate(Date refDate) {
        this.refDate = refDate;
    }

    public Timestamp getRefTime() {
        return refTime;
    }

    public void setRefTime(Timestamp refTime) {
        this.refTime = refTime;
    }

    public String getJournalMemo() {
        return journalMemo;
    }

    public void setJournalMemo(String journalMemo) {
        this.journalMemo = journalMemo;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getTotalAuditAmount() {
        return totalAuditAmount;
    }

    public void setTotalAuditAmount(BigDecimal totalAuditAmount) {
        this.totalAuditAmount = totalAuditAmount;
    }

    public BigDecimal getTotalBalanceAmount() {
        return totalBalanceAmount;
    }

    public void setTotalBalanceAmount(BigDecimal totalBalanceAmount) {
        this.totalBalanceAmount = totalBalanceAmount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public Boolean getIsExecuted() {
        return isExecuted;
    }

    public void setIsExecuted(Boolean executed) {
        isExecuted = executed;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getCaReceiptRefId() {
        return caReceiptRefId;
    }

    public void setCaReceiptRefId(Integer caReceiptRefId) {
        this.caReceiptRefId = caReceiptRefId;
    }

    public Integer getCaPaymentRefId() {
        return caPaymentRefId;
    }

    public void setCaPaymentRefId(Integer caPaymentRefId) {
        this.caPaymentRefId = caPaymentRefId;
    }

    public List<CaAuditDetail> getCaAuditDetailList() {
        return caAuditDetailList;
    }

    public void setCaAuditDetailList(List<CaAuditDetail> caAuditDetailList) {
        this.caAuditDetailList = caAuditDetailList;
    }

    public List<CaAuditMemberDetail> getCaAuditMemberDetailList() {
        return caAuditMemberDetailList;
    }

    public void setCaAuditMemberDetailList(List<CaAuditMemberDetail> caAuditMemberDetailList) {
        this.caAuditMemberDetailList = caAuditMemberDetailList;
    }

    public String getRefTimeString() {
        return refTimeString;
    }

    public void setRefTimeString(String refTimeString) {
        this.refTimeString = refTimeString;
    }
}
