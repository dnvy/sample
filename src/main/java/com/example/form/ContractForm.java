package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.ContractDetailExpense;
import com.example.entity.ContractDetailInventoryItem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Form add Hợp đồng bán.
 */
public class ContractForm implements Serializable {

    private static final long serialVersionUID = 9139702728049804511L;

    private Integer id; // PK
    private Integer branchId; // Chi nhánh
    private Boolean isProject; // True: Là dự án; Flase: Là hợp đồng
    private Integer projectId; // Cho biết Hợp đồng thuộc dự án nào
    private String contractCode; // Số hợp đồng bán/mua

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date signDate; // Ngày ký (*)

    private String contractSubject; // Trích yếu hợp đồng bán/mua
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá
    private BigDecimal contractAmountOc; // Giá trị hợp đồng/dự án
    private BigDecimal contractAmount; // Giá trị hợp đồng/dự án Quy đổi
    private BigDecimal closeAmountOc; // Giá trị thanh lý
    private BigDecimal closeAmount; // Giá trị thanh lý Quy đổi

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date paymentDate; // Hạn thanh toán

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date deliveryDate; // Hạn giao hàng


    private Boolean isArisedBeforeUseSoftware; // Là hợp đồng phát sinh trước khi sử dụng phần mềm
    private BigDecimal expenseAmountFinance; // Số đã chi
    private BigDecimal receiptAmountOcFinance; // Số đã thu
    private BigDecimal receiptAmountFinance; // Số đã thu Quy đổi
    private BigDecimal invoiceAmountOcFinance; // Giá trị đã xuất hóa đơn
    private BigDecimal invoiceAmountFinance; // Giá trị đã xuất hóa đơn Quy đổi
    private Integer accountObjectId; // ID  là khách hàng (cá nhân hay tổ chức) trong hợp đồng bán
    private String accountObjectAddress; // Địa chỉ khách hàng
    private String accountObjectContactName; // Người liên hệ của khách hàng
    private Integer organizationUnitId; // Đơn vị thực hiện
    private Integer employeeId; // Người thực hiện
    private Integer contractStatusId; // Tình trạng hợp đồng bán/mua

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date closeDate; // Ngày thanh lý/Hủy bỏ

    private String closeReason; // Lý do thanh lý, hủy bỏ
    private Integer revenueStatus; // Tình trạng ghi nhận doanh số. 0=Chưa ghi doanh số: 1=Đã ghi doanh số
    private String otherTerms; // Điều khoản khác
    private Boolean isCalculatedCost; // Check tính giá thành
    private Boolean isInvoiced; // Đã xuất hóa đơn

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date revenueDate; // Ngày ghi nhận doanh số

    private BigDecimal totalExpenseExpectAmount; // Dự kiến chi
    private BigDecimal totalExpensedAmount; // Thực chi
    private BigDecimal balanceExpenseAmountFinance; // Số còn phải chi=Dự kiến chi-thực chi
    private BigDecimal totalReceiptedAmount; // Thực thu
    private BigDecimal balanceReceiptAmountFinance; // Số còn phải thu
    private BigDecimal profitAndLossExpectAmountFinance; // Dự kiến lãi lỗ
    private String createdBy; // Người tạo
    private Timestamp createdDate; // Ngày tạo
    private String modifiedBy; // Người sửa cuối
    private Timestamp modifiedDate; // Ngày sửa cuối
    private Integer refType; // Loai chung tu
    private Integer saOrderId;
    private Boolean isParent;
    private BigDecimal totalInvoiceAmountFinance; // Tổng giá trị đã xuất hóa đơn Sổ Tài Chính
    private BigDecimal totalInvoiceAmountManagement; // Tổng giá trị đã xuất hóa đơn Sổ Quản Trị
    private BigDecimal accumSaleAmountFinance;
    private BigDecimal accumCostAmountFinance;
    private BigDecimal accumOtherAmountFinance;
    private BigDecimal expenseAmountManagement;
    private BigDecimal receiptAmountOcManagement;
    private BigDecimal receiptAmountManagement;
    private BigDecimal invoiceAmountOcManagement;
    private BigDecimal invoiceAmountManagement;
    private BigDecimal accumSaleAmountManagement;
    private BigDecimal accumCostAmountManagement;
    private BigDecimal accumOtherAmountManagement;
    private BigDecimal totalInvoiceAmountOcFinance;
    private BigDecimal totalInvoiceAmountOcManagement;
    private BigDecimal balanceReceiptAmountManagement;
    private BigDecimal balanceExpenseAmountManagement;
    private BigDecimal profitAndLossExpectAmountManagement;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private String customField6;
    private String customField7;
    private String customField8;
    private String customField9;
    private String customField10;
    private String accountObjectName; // Tên khách hàng

    List<ContractDetailExpense> contractDetailExpenseList;
    List<ContractDetailInventoryItem> contractDetailInventoryItemList;


    public ContractForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Boolean getIsProject() {
        return isProject;
    }

    public void setIsProject(Boolean project) {
        isProject = project;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getContractSubject() {
        return contractSubject;
    }

    public void setContractSubject(String contractSubject) {
        this.contractSubject = contractSubject;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getContractAmountOc() {
        return contractAmountOc;
    }

    public void setContractAmountOc(BigDecimal contractAmountOc) {
        this.contractAmountOc = contractAmountOc;
    }

    public BigDecimal getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }

    public BigDecimal getCloseAmountOc() {
        return closeAmountOc;
    }

    public void setCloseAmountOc(BigDecimal closeAmountOc) {
        this.closeAmountOc = closeAmountOc;
    }

    public BigDecimal getCloseAmount() {
        return closeAmount;
    }

    public void setCloseAmount(BigDecimal closeAmount) {
        this.closeAmount = closeAmount;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Boolean getIsArisedBeforeUseSoftware() {
        return isArisedBeforeUseSoftware;
    }

    public void setIsArisedBeforeUseSoftware(Boolean arisedBeforeUseSoftware) {
        isArisedBeforeUseSoftware = arisedBeforeUseSoftware;
    }

    public BigDecimal getExpenseAmountFinance() {
        return expenseAmountFinance;
    }

    public void setExpenseAmountFinance(BigDecimal expenseAmountFinance) {
        this.expenseAmountFinance = expenseAmountFinance;
    }

    public BigDecimal getReceiptAmountOcFinance() {
        return receiptAmountOcFinance;
    }

    public void setReceiptAmountOcFinance(BigDecimal receiptAmountOcFinance) {
        this.receiptAmountOcFinance = receiptAmountOcFinance;
    }

    public BigDecimal getReceiptAmountFinance() {
        return receiptAmountFinance;
    }

    public void setReceiptAmountFinance(BigDecimal receiptAmountFinance) {
        this.receiptAmountFinance = receiptAmountFinance;
    }

    public BigDecimal getInvoiceAmountOcFinance() {
        return invoiceAmountOcFinance;
    }

    public void setInvoiceAmountOcFinance(BigDecimal invoiceAmountOcFinance) {
        this.invoiceAmountOcFinance = invoiceAmountOcFinance;
    }

    public BigDecimal getInvoiceAmountFinance() {
        return invoiceAmountFinance;
    }

    public void setInvoiceAmountFinance(BigDecimal invoiceAmountFinance) {
        this.invoiceAmountFinance = invoiceAmountFinance;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public String getAccountObjectAddress() {
        return accountObjectAddress;
    }

    public void setAccountObjectAddress(String accountObjectAddress) {
        this.accountObjectAddress = accountObjectAddress;
    }

    public String getAccountObjectContactName() {
        return accountObjectContactName;
    }

    public void setAccountObjectContactName(String accountObjectContactName) {
        this.accountObjectContactName = accountObjectContactName;
    }

    public Integer getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Integer organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getContractStatusId() {
        return contractStatusId;
    }

    public void setContractStatusId(Integer contractStatusId) {
        this.contractStatusId = contractStatusId;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public Integer getRevenueStatus() {
        return revenueStatus;
    }

    public void setRevenueStatus(Integer revenueStatus) {
        this.revenueStatus = revenueStatus;
    }

    public String getOtherTerms() {
        return otherTerms;
    }

    public void setOtherTerms(String otherTerms) {
        this.otherTerms = otherTerms;
    }

    public Boolean getIsCalculatedCost() {
        return isCalculatedCost;
    }

    public void setIsCalculatedCost(Boolean calculatedCost) {
        isCalculatedCost = calculatedCost;
    }

    public Boolean getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Boolean invoiced) {
        isInvoiced = invoiced;
    }

    public Date getRevenueDate() {
        return revenueDate;
    }

    public void setRevenueDate(Date revenueDate) {
        this.revenueDate = revenueDate;
    }

    public BigDecimal getTotalExpenseExpectAmount() {
        return totalExpenseExpectAmount;
    }

    public void setTotalExpenseExpectAmount(BigDecimal totalExpenseExpectAmount) {
        this.totalExpenseExpectAmount = totalExpenseExpectAmount;
    }

    public BigDecimal getTotalExpensedAmount() {
        return totalExpensedAmount;
    }

    public void setTotalExpensedAmount(BigDecimal totalExpensedAmount) {
        this.totalExpensedAmount = totalExpensedAmount;
    }

    public BigDecimal getBalanceExpenseAmountFinance() {
        return balanceExpenseAmountFinance;
    }

    public void setBalanceExpenseAmountFinance(BigDecimal balanceExpenseAmountFinance) {
        this.balanceExpenseAmountFinance = balanceExpenseAmountFinance;
    }

    public BigDecimal getTotalReceiptedAmount() {
        return totalReceiptedAmount;
    }

    public void setTotalReceiptedAmount(BigDecimal totalReceiptedAmount) {
        this.totalReceiptedAmount = totalReceiptedAmount;
    }

    public BigDecimal getBalanceReceiptAmountFinance() {
        return balanceReceiptAmountFinance;
    }

    public void setBalanceReceiptAmountFinance(BigDecimal balanceReceiptAmountFinance) {
        this.balanceReceiptAmountFinance = balanceReceiptAmountFinance;
    }

    public BigDecimal getProfitAndLossExpectAmountFinance() {
        return profitAndLossExpectAmountFinance;
    }

    public void setProfitAndLossExpectAmountFinance(BigDecimal profitAndLossExpectAmountFinance) {
        this.profitAndLossExpectAmountFinance = profitAndLossExpectAmountFinance;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Integer getSaOrderId() {
        return saOrderId;
    }

    public void setSaOrderId(Integer saOrderId) {
        this.saOrderId = saOrderId;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean parent) {
        isParent = parent;
    }

    public BigDecimal getTotalInvoiceAmountFinance() {
        return totalInvoiceAmountFinance;
    }

    public void setTotalInvoiceAmountFinance(BigDecimal totalInvoiceAmountFinance) {
        this.totalInvoiceAmountFinance = totalInvoiceAmountFinance;
    }

    public BigDecimal getTotalInvoiceAmountManagement() {
        return totalInvoiceAmountManagement;
    }

    public void setTotalInvoiceAmountManagement(BigDecimal totalInvoiceAmountManagement) {
        this.totalInvoiceAmountManagement = totalInvoiceAmountManagement;
    }

    public BigDecimal getAccumSaleAmountFinance() {
        return accumSaleAmountFinance;
    }

    public void setAccumSaleAmountFinance(BigDecimal accumSaleAmountFinance) {
        this.accumSaleAmountFinance = accumSaleAmountFinance;
    }

    public BigDecimal getAccumCostAmountFinance() {
        return accumCostAmountFinance;
    }

    public void setAccumCostAmountFinance(BigDecimal accumCostAmountFinance) {
        this.accumCostAmountFinance = accumCostAmountFinance;
    }

    public BigDecimal getAccumOtherAmountFinance() {
        return accumOtherAmountFinance;
    }

    public void setAccumOtherAmountFinance(BigDecimal accumOtherAmountFinance) {
        this.accumOtherAmountFinance = accumOtherAmountFinance;
    }

    public BigDecimal getExpenseAmountManagement() {
        return expenseAmountManagement;
    }

    public void setExpenseAmountManagement(BigDecimal expenseAmountManagement) {
        this.expenseAmountManagement = expenseAmountManagement;
    }

    public BigDecimal getReceiptAmountOcManagement() {
        return receiptAmountOcManagement;
    }

    public void setReceiptAmountOcManagement(BigDecimal receiptAmountOcManagement) {
        this.receiptAmountOcManagement = receiptAmountOcManagement;
    }

    public BigDecimal getReceiptAmountManagement() {
        return receiptAmountManagement;
    }

    public void setReceiptAmountManagement(BigDecimal receiptAmountManagement) {
        this.receiptAmountManagement = receiptAmountManagement;
    }

    public BigDecimal getInvoiceAmountOcManagement() {
        return invoiceAmountOcManagement;
    }

    public void setInvoiceAmountOcManagement(BigDecimal invoiceAmountOcManagement) {
        this.invoiceAmountOcManagement = invoiceAmountOcManagement;
    }

    public BigDecimal getInvoiceAmountManagement() {
        return invoiceAmountManagement;
    }

    public void setInvoiceAmountManagement(BigDecimal invoiceAmountManagement) {
        this.invoiceAmountManagement = invoiceAmountManagement;
    }

    public BigDecimal getAccumSaleAmountManagement() {
        return accumSaleAmountManagement;
    }

    public void setAccumSaleAmountManagement(BigDecimal accumSaleAmountManagement) {
        this.accumSaleAmountManagement = accumSaleAmountManagement;
    }

    public BigDecimal getAccumCostAmountManagement() {
        return accumCostAmountManagement;
    }

    public void setAccumCostAmountManagement(BigDecimal accumCostAmountManagement) {
        this.accumCostAmountManagement = accumCostAmountManagement;
    }

    public BigDecimal getAccumOtherAmountManagement() {
        return accumOtherAmountManagement;
    }

    public void setAccumOtherAmountManagement(BigDecimal accumOtherAmountManagement) {
        this.accumOtherAmountManagement = accumOtherAmountManagement;
    }

    public BigDecimal getTotalInvoiceAmountOcFinance() {
        return totalInvoiceAmountOcFinance;
    }

    public void setTotalInvoiceAmountOcFinance(BigDecimal totalInvoiceAmountOcFinance) {
        this.totalInvoiceAmountOcFinance = totalInvoiceAmountOcFinance;
    }

    public BigDecimal getTotalInvoiceAmountOcManagement() {
        return totalInvoiceAmountOcManagement;
    }

    public void setTotalInvoiceAmountOcManagement(BigDecimal totalInvoiceAmountOcManagement) {
        this.totalInvoiceAmountOcManagement = totalInvoiceAmountOcManagement;
    }

    public BigDecimal getBalanceReceiptAmountManagement() {
        return balanceReceiptAmountManagement;
    }

    public void setBalanceReceiptAmountManagement(BigDecimal balanceReceiptAmountManagement) {
        this.balanceReceiptAmountManagement = balanceReceiptAmountManagement;
    }

    public BigDecimal getBalanceExpenseAmountManagement() {
        return balanceExpenseAmountManagement;
    }

    public void setBalanceExpenseAmountManagement(BigDecimal balanceExpenseAmountManagement) {
        this.balanceExpenseAmountManagement = balanceExpenseAmountManagement;
    }

    public BigDecimal getProfitAndLossExpectAmountManagement() {
        return profitAndLossExpectAmountManagement;
    }

    public void setProfitAndLossExpectAmountManagement(BigDecimal profitAndLossExpectAmountManagement) {
        this.profitAndLossExpectAmountManagement = profitAndLossExpectAmountManagement;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public String getCustomField6() {
        return customField6;
    }

    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }

    public String getCustomField7() {
        return customField7;
    }

    public void setCustomField7(String customField7) {
        this.customField7 = customField7;
    }

    public String getCustomField8() {
        return customField8;
    }

    public void setCustomField8(String customField8) {
        this.customField8 = customField8;
    }

    public String getCustomField9() {
        return customField9;
    }

    public void setCustomField9(String customField9) {
        this.customField9 = customField9;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public String getAccountObjectName() {
        return accountObjectName;
    }

    public void setAccountObjectName(String accountObjectName) {
        this.accountObjectName = accountObjectName;
    }


    public List<ContractDetailExpense> getContractDetailExpenseList() {
        return contractDetailExpenseList;
    }

    public void setContractDetailExpenseList(List<ContractDetailExpense> contractDetailExpenseList) {
        this.contractDetailExpenseList = contractDetailExpenseList;
    }

    public List<ContractDetailInventoryItem> getContractDetailInventoryItemList() {
        return contractDetailInventoryItemList;
    }

    public void setContractDetailInventoryItemList(List<ContractDetailInventoryItem> contractDetailInventoryItemList) {
        this.contractDetailInventoryItemList = contractDetailInventoryItemList;
    }

}
