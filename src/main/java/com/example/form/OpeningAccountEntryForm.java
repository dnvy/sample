package com.example.form;

import org.springframework.format.annotation.DateTimeFormat;
import com.example.entity.OpeningAccountEntryDetail;
import com.example.entity.OpeningAccountEntryDetailInvoice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Màn hình khai báo Số dư ban đầu, chi tiết cho từng tài khoản.
 */
public class OpeningAccountEntryForm implements Serializable {

    private static final long serialVersionUID = -8132472483834939967L;

    private Integer id; // PK - Số dư đầu Tài khoản

    private Integer refType; // NOT NULL. Loại chứng từ: số dư ban đầu tài khoản

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date postedDate; // Ngày ghi sổ (fix = StartDate-1)

    private String accountNumber; // Tài khoản
    private Integer accountObjectId; // Mã đối tượng
    private Integer currencyId; // Loại tiền
    private BigDecimal exchangeRate; // Tỷ giá hối đoái
    private BigDecimal debitAmountOc; // NOT NULL. Dự nợ
    private BigDecimal debitAmount; // NOT NULL. Dư nợ quy đổi
    private BigDecimal creditAmountOc; // NOT NULL. Dư có
    private BigDecimal creditAmount; // NOT NULL. Dư có quy đổi
    private Integer bankAccountId; // Tài khoản ngân hàng
    private Integer branchId; // ID của chi nhánh.
    private Integer displayOnBook; // NOT NULL. 0=Sổ tài chinh;1=Sổ quản trị
    private Boolean isPostedCashBook; // Trạng thái ghi sổ quỹ

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date cashBookPostedDate; // Ngày ghi sổ quỹ

    private Integer editVersion; // NOT NULL. Phiên bản sửa chứng từ
    private Timestamp createdDate;
    private String createdBy;
    private Timestamp modifiedDate;
    private String modifiedBy;
    private Boolean isPostedManagement; // NOT NULL. Trạng thái ghi sổ Sổ quản trị
    private Boolean isPostedFinance; // NOT NULL. Trạng thái ghi sổ Sổ tài chính
    private Boolean isAutoGenerate;

    List<OpeningAccountEntryDetail> openingAccountEntryDetailList;
    List<OpeningAccountEntryDetailInvoice> openingAccountEntryDetailInvoiceList;

    public OpeningAccountEntryForm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefType() {
        return refType;
    }

    public void setRefType(Integer refType) {
        this.refType = refType;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Integer accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getDebitAmountOc() {
        return debitAmountOc;
    }

    public void setDebitAmountOc(BigDecimal debitAmountOc) {
        this.debitAmountOc = debitAmountOc;
    }

    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    public BigDecimal getCreditAmountOc() {
        return creditAmountOc;
    }

    public void setCreditAmountOc(BigDecimal creditAmountOc) {
        this.creditAmountOc = creditAmountOc;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getDisplayOnBook() {
        return displayOnBook;
    }

    public void setDisplayOnBook(Integer displayOnBook) {
        this.displayOnBook = displayOnBook;
    }

    public Boolean getIsPostedCashBook() {
        return isPostedCashBook;
    }

    public void setIsPostedCashBook(Boolean postedCashBook) {
        isPostedCashBook = postedCashBook;
    }

    public Date getCashBookPostedDate() {
        return cashBookPostedDate;
    }

    public void setCashBookPostedDate(Date cashBookPostedDate) {
        this.cashBookPostedDate = cashBookPostedDate;
    }

    public Integer getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(Integer editVersion) {
        this.editVersion = editVersion;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getIsPostedManagement() {
        return isPostedManagement;
    }

    public void setIsPostedManagement(Boolean postedManagement) {
        isPostedManagement = postedManagement;
    }

    public Boolean getIsPostedFinance() {
        return isPostedFinance;
    }

    public void setIsPostedFinance(Boolean postedFinance) {
        isPostedFinance = postedFinance;
    }

    public Boolean getIsAutoGenerate() {
        return isAutoGenerate;
    }

    public void setIsAutoGenerate(Boolean autoGenerate) {
        isAutoGenerate = autoGenerate;
    }

    public List<OpeningAccountEntryDetail> getOpeningAccountEntryDetailList() {
        return openingAccountEntryDetailList;
    }

    public void setOpeningAccountEntryDetailList(List<OpeningAccountEntryDetail> openingAccountEntryDetailList) {
        this.openingAccountEntryDetailList = openingAccountEntryDetailList;
    }

    public List<OpeningAccountEntryDetailInvoice> getOpeningAccountEntryDetailInvoiceList() {
        return openingAccountEntryDetailInvoiceList;
    }

    public void setOpeningAccountEntryDetailInvoiceList(List<OpeningAccountEntryDetailInvoice> openingAccountEntryDetailInvoiceList) {
        this.openingAccountEntryDetailInvoiceList = openingAccountEntryDetailInvoiceList;
    }

}
