USE accounting133;

DROP VIEW IF EXISTS OpeningAccountEntryView;
GO

CREATE VIEW OpeningAccountEntryView
AS

SELECT
	Acc.ID,
	Acc.ParentID AS ParentID,
	Acc.AccountNumber,
	Acc.AccountName,
	ISNULL(Opn.TotalDebit, 0) AS DebitAmount,
	ISNULL(Opn.TotalCredit, 0) AS CreditAmount,
	Acc.AccountCategoryKind AS AccountCategoryKind
FROM
	(SELECT 
		ID, 
		AccountNumber, 
		AccountName,
		ParentID,
		AccountCategoryKind
	FROM 
		Account 
		-- ORDER BY AccountNumber;
	WHERE ActiveStatus = 1
		) AS Acc LEFT JOIN

	(SELECT
		AccountNumber, 
		SUM(DebitAmount) AS TotalDebit, 
		SUM(CreditAmount) AS TotalCredit
	FROM 
		OpeningAccountEntry
	WHERE 
		AccountNumber IS NOT NULL
		GROUP BY AccountNumber
	) AS Opn

	ON Acc.AccountNumber = Opn.AccountNumber
	--ORDER BY AccountNumber;
GO
