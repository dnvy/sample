SET IDENTITY_INSERT SYSDBInfo ON;
GO

INSERT INTO SYSDBInfo(ID, Application, Version, MVC, CreatedBy, CreatedDate, ClosedDate, Description,  DemoData, DemoDate, Particularity, MobileDatabaseID, IsFirstTimeSyncLedger) VALUES('1', N'PMKT_Vy_2019.06',  N'2019', '2019.06.1', 'ADMIN', '2019-06-09 00:00:00.000', '', '', '0', '', '0', '1', '0');
GO

SET IDENTITY_INSERT SYSDBInfo OFF;
GO
