SET IDENTITY_INSERT CABAReasonType ON;
GO

INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (10, 1010, N'Rút tiền gửi về nộp quỹ', N'Rút tiền gửi về nộp quỹ');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (11, 1010, N'Thu hoàn thuế GTGT', N'Thu hoàn thuế GTGT');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (12, 1010, N'Thu hoàn ứng', N'Thu hoàn ứng sau khi quyết toán tạm ứng nhân viên...');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (13, 1010, N'Thu khác', N'Thu khác');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (21, 1020, N'Tạm ứng cho nhân viên', N'Tạm ứng cho nhân viên');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (22, 1020, N'Gửi tiền vào ngân hàng', N'Gửi tiền vào ngân hàng');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (23, 1020, N'Chi khác', N'Chi khác');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (30, 1500, N'Vay nợ', N'Vay tiền của...');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (31, 1500, N'Thu lãi đầu tư tài chính', N'Thu lãi đầu tư tài chính ...');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (32, 1500, N'Thu hoàn ứng', N'Thu hoàn tạm ứng của nhân viên ...');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (33, 1500, N'Thu hoàn thuế GTGT', N'Thu hoàn thuế GTGT');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (34, 1500, N'Thu khác', N'Thu từ ...');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (40, 1530, N'Trả các khoản vay', N'Trả các khoản vay');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (41, 1530, N'Trả lương nhân viên', N'Trả lương nhân viên');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (42, 1530, N'Tạm ứng cho nhân viên', N'Tạm ứng cho nhân viên');
INSERT INTO CABAReasonType(ID, RefType, ReasonTypeName, Description) VALUES (43, 1530, N'Chi khác', N'Chi khác');
GO

SET IDENTITY_INSERT CABAReasonType OFF;
GO
