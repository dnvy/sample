SET IDENTITY_INSERT ProjectWork ON;
GO

INSERT INTO ProjectWork (ID, ProjectWorkType, ProjectWorkCategoryID, ProjectWorkCode, ProjectWorkName, StartDate, FinishDate, EstimateAmount, Stakeholder, StakeholderAddress, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, SortVyCodeID, ProjectWorkStatus) VALUES(1,  0, 1, N'XD_TRU_SO', N'Xây dựng trụ sở', '', '', '50000000000', N'', N'', '', 0, '/2/', 1, 0, 1, '', '', '', '', 1, '/00002/', 0);
INSERT INTO ProjectWork (ID, ProjectWorkType, ProjectWorkCategoryID, ProjectWorkCode, ProjectWorkName, StartDate, FinishDate, EstimateAmount, Stakeholder, StakeholderAddress, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, SortVyCodeID, ProjectWorkStatus) VALUES(2,  0, 1, N'CC_BALA', N'Chung cư Bala - Hà Đông', '', '', '500000000000', N'Công ty xây dựng Việt An', N'1250 Nguyễn Trãi, Thanh Xuân, Hà Nội', '', 0, '/1/', 1, 1, 1, '', '', '', '', 1, '/00001/', 0);
INSERT INTO ProjectWork (ID, ProjectWorkType, ProjectWorkCategoryID, ProjectWorkCode, ProjectWorkName, StartDate, FinishDate, EstimateAmount, Stakeholder, StakeholderAddress, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, SortVyCodeID, ProjectWorkStatus) VALUES(3,  1, 1, N'NEN_MONG', N'Nền móng', '', '', '1000000000', N'Công ty xây dựng Việt An', N'1250 Nguyễn Trãi, Thanh Xuân, Hà Nội', '', 2, '/1/1/', 2, 0, 1, '', '', '', '', 1, '/00001/00001/', 0);
GO

SET IDENTITY_INSERT ProjectWork OFF;
GO
