SET IDENTITY_INSERT PersonalIncomeTaxRate ON;
GO

INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(1, 6, 30, 52000000, 80000000, 624000000, 960000000, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(2, 3, 15, 10000000, 18000000, 120000000, 216000000, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(3, 5, 25, 32000000, 52000000, 384000000, 624000000, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(4, 2, 10, 5000000, 10000000, 60000000, 120000000, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(5, 7, 35, 80000000, NULL, 960000000, NULL, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(6, 1, 5, 0, 5000000, 0, 60000000, 0);
INSERT INTO PersonalIncomeTaxRate(ID, TaxGrade, TaxRate, FromIncomeByMonth, ToIncomeByMonth, FromIncomeByYear, ToIncomeByYear, ActiveStatus) VALUES(7, 4, 20, 18000000, 32000000, 216000000, 384000000, 0);
GO

SET IDENTITY_INSERT PersonalIncomeTaxRate OFF;
GO
