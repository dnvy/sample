SET IDENTITY_INSERT ExpenseItem ON;
GO

INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (1 , N'CP_NHAN CONG' , N'Chi phí nhân công' , N'' , 6 , '/2/1/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00002/00001/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (2 , N'CP_VAN PHONG' , N'Chi phí văn phòng' , N'' , 6 , '/2/2/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00002/00002/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (3 , N'CPQLK' , N'Chi phí quản lý khác' , N'' , 6 , '/2/3/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00002/00003/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (4 , N'CP_BAO BI' , N'Chi phí bao bì' , N'' , 7 , '/1/1/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00001/00001/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (5 , N'CP_VAN CHUYEN' , N'Chi phí vận chuyển' , N'' , 7 , '/1/2/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00001/00002/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (6 , N'CP_QL' , N'Chi phí quản lý' , N'' , 0 , '/2/' , 1 , 1 , 1 , 0 , '' , '' , '',  '' , '/00002/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (7 , N'CP_BAN HANG' , N'Chi phí bán hàng' , N'' , 0 , '/1/' , 1 , 1 , 1 , 0 , '' , '' , '',  '' , '/00001/');
INSERT INTO ExpenseItem(ID, ExpenseItemCode, ExpenseItemName, Description, ParentID, VyCodeID, Grade, IsParent, ActiveStatus, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID ) VALUES (8 , N'CPBHK' , N'Chi phí bán hàng khác' , N'' , 7 , '/1/3/' , 2 , 0 , 1 , 0 , '' , '' , '',  '' , '/00001/00003/');
GO

SET IDENTITY_INSERT ExpenseItem OFF;
GO
