SET IDENTITY_INSERT PaymentTerm ON;
GO

INSERT INTO PaymentTerm (ID, PaymentTermCode, PaymentTermName, DueTime, DiscountTime, DiscountPercent, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy) VALUES (1, N'ĐKTT01', N'Điều khoản thanh toán 01', 10, 1, 5, 1, '', '', '', '');
INSERT INTO PaymentTerm (ID, PaymentTermCode, PaymentTermName, DueTime, DiscountTime, DiscountPercent, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy) VALUES (2, N'ĐKTT02', N'Điều khoản thanh toán 02', 20, 3, 4, 1, '', '', '', '');
GO

SET IDENTITY_INSERT PaymentTerm OFF;
GO
