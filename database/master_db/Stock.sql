SET IDENTITY_INSERT Stock ON;
GO

INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(1, N'KDL', N'Kho đại lý', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(2, N'KTP', N'Kho thành phẩm', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(3, N'KHH', N'Kho Hàng hoá', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(4, N'KCN', N'Kho chi nhánh', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(5, N'KHMDD', N'Kho hàng mua đang đi đường', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(6, N'KCCDC', N'Kho Công cụ dụng cụ', N'' , 1, '' , '', '', '' , N'', '');
INSERT INTO Stock(ID, StockCode, StockName, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, InventoryAccount, BranchID)VALUES(7, N'KNVL', N'Kho Nguyên vật liệu', N'' , 1, '' , '', '', '' , N'', '');
GO

SET IDENTITY_INSERT Stock OFF;
GO
