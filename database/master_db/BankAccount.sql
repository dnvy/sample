SET IDENTITY_INSERT BankAccount ON;
GO

INSERT INTO BankAccount(ID, BankAccountNumber, BankID, BankName, Address, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, AccountHolder, ProvinceOrCity) VALUES(1, 124010015487, '12', N'Ngân hàng Đầu tư và Phát triển Việt Nam - Chi nhánh Thanh Xuân', N'1254 Nguyễn Trãi, Thanh Xuân, Hà Nội', N'', '1', '', '', '', '', 1, '' , N'');
INSERT INTO BankAccount(ID, BankAccountNumber, BankID, BankName, Address, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, AccountHolder, ProvinceOrCity) VALUES(2, 1254878210, '6', N'Ngân hàng TMCP Quân đội - Chi nhánh Thăng Long', N'2150 Phạm Văn Đồng, Cầu Giấy, Hà Nội', N'', '1', '', '', '', '', 1, '' , N'');
INSERT INTO BankAccount(ID, BankAccountNumber, BankID, BankName, Address, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, BranchID, AccountHolder, ProvinceOrCity) VALUES(3, 12555487201, '8', N'Ngân hàng TMCP  Sài Gòn – Hà Nội - Chi nhánh Hà Đông', N'2154 Lê Trọng Tấn, Hà Đông, Hà Nội', N'', '1', '', '', '', '', 1, '' , N'');
GO

SET IDENTITY_INSERT BankAccount OFF;
GO
