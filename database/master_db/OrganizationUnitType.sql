SET IDENTITY_INSERT OrganizationUnitType ON;
GO

INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(1, N'Tổng công ty/Công ty', N'Cấp tổ chức cao nhất của Doanh nghiệp');
INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(2, N'Chi nhánh', N'Đơn vị trực thuộc của Tổng công ty, hạch toán độc lập hoặc phụ thuộc');
INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(3, N'Văn phòng/Trung tâm', N'Đơn vị trực thuộc của Chi nhánh/Công ty, không hạch toán kế toán riêng');
INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(4, N'Phòng ban', N'Đơn vị trực thuộc của Văn phòng/Trung tâm/Chi nhánh/Công ty');
INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(5, N'Phân xưởng', N'Đơn vị trực thuộc của Văn phòng/Trung tâm/Chi nhánh/Công ty');
INSERT INTO OrganizationUnitType(ID, OrganizationUnitTypeName, Description) VALUES(6, N'Nhóm/Tổ/Đội', N'Đơn vị trực thuộc của Phòng ban/Phân xưởng');
GO

SET IDENTITY_INSERT OrganizationUnitType OFF;
GO
