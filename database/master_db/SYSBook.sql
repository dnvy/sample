SET IDENTITY_INSERT SYSBook ON;
GO

INSERT INTO SYSBook(ID, DisplayOnBook, DisplayOnBookName) VALUES (1, 0, N'Tài chính');
INSERT INTO SYSBook(ID, DisplayOnBook, DisplayOnBookName) VALUES (2, 1, N'Quản trị');
INSERT INTO SYSBook(ID, DisplayOnBook, DisplayOnBookName) VALUES (3, 2, N'Tài chính và Quản trị');
GO

SET IDENTITY_INSERT SYSBook OFF;
GO
