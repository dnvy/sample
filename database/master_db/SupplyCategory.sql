SET IDENTITY_INSERT SupplyCategory ON;
GO

INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (1, 3, 0, 2, N'GIAY_GANGTAY', N'Giày và găng tay', '', 0 , 1 , '', '', '', '', '/1/2/', '/00001/00001/');
INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (2, 0, 0, 1, N'VPP', N'Văn phòng phẩm', '', 0 , 1 , '', '', '', '', '/3/', '/00004/');
INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (3, 0, 1, 1, N'BHLĐ', N'Đồ bảo hộ lao động', '', 0 , 1 , '', '', '', '', '/1/', '/00001/');
INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (4, 0, 0, 1, N'MMCC', N'Máy móc công cụ', '', 0 , 1 , '', '', '', '', '/2/', '/00002/');
INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (5, 0, 0, 1, N'TTB', N'Trang thiết bị', '', 0 , 1 , '', '', '', '', '/4/', '/00003/');
INSERT INTO SupplyCategory(ID, ParentID, IsParent, Grade, SupplyCategoryCode, SupplyCategoryName, Description, IsSystem, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, VyCodeID, SortVyCodeID) VALUES (6, 3, 0, 2, N'QA', N'Quẩn áo', '', 0 , 1 , '', '', '', '', '/1/1/', '/00001/00002/');
GO

SET IDENTITY_INSERT SupplyCategory OFF;
GO
