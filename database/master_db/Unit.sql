SET IDENTITY_INSERT Unit ON;
GO

INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(1, N'Hộp', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(2, N'm', N'Mét', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(3, N'Bình', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(4, N'Gói', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(5, N'Cuộn', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(6, N'Túi', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(7, N'mm', N'Milimet', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(8, N'Cái', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(9, N'Yến', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(10, N'Cuốn', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(11, N'cm', N'Xentimét', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(12, N'dm', N'Đêximét', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(13, N'Quyển', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(14, N'Khối', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(15, N'Lọ', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(16, N'Lần', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(17, N'Bao', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(18, N'ml', N'Mililít', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(19, N'Điếu', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(20, N'Két', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(21, N'Viên', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(22, N'kg', N'Kilôgam', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(23, N'Chai', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(24, N'Ngày', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(25, N'Phút', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(26, N'm3', N'Mét khối', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(27, N'Cây', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(28, N'Giờ', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(29, N'Tuýp', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(30, N'g', N'Gam', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(31, N'Bộ', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(32, N'Lít', N'Lít', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(33, N'Kwh', N'Kilôoát/giờ', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(34, N'Chiếc', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(35, N'Vỉ', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(36, N'Thùng', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(37, N'Tấn', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(38, N'Tạ', N'', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(39, N'm2', N'Mét vuông', 1);
INSERT INTO Unit(ID, UnitName, Description, ActiveStatus) VALUES(40, N'w', N'Oát', 1);
GO

SET IDENTITY_INSERT Unit OFF;
GO
