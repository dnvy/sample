SET IDENTITY_INSERT AccountObjectGroup ON;
GO

INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(1, 'KH_LE', N'Khách hàng lẻ', '', '/3/', 1, 0, '', 1, 0, '', '', '' , '', '', '/00003/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(2, 'DL', N'Đại lý', '', '/1/', 1, 1, '', 1, 0, '', '', '' , '', '', '/00001/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(3, 'DLC1', N'Đại lý cấp 1', '2', '/1/1/', 2, 0, '', 1, 0, '', '', '' , '', '', '/00001/00001/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(4, 'KH_DA', N'Khách hàng dự án', '', '/2/', 1, 0, '', 1, 0, '', '', '' , '', '', '/00002/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(5, 'DLC2', N'Đại lý cấp 2', '2', '/1/2/', 2, 0, '', 1, 0, '', '', '' , '', '', '/00001/00002/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(6, 'NCC_HH', N'Nhà cung cấp hàng hoá', '', '/5/', 1, 0, '', 1, 0, '', '', '' , '', '', '/00005/')
INSERT INTO AccountObjectGroup(ID, AccountObjectGroupCode, AccountObjectGroupName, ParentID, VyCodeID, Grade, IsParent , Description, ActiveStatus, IsSystem, SortOrder, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID)VALUES(7, 'NCC_DV', N'Nhà cung cấp dịch vụ', '', '/4/', 1, 0, '', 1, 0, '', '', '' , '', '', '/00004/')
GO

SET IDENTITY_INSERT AccountObjectGroup OFF;
GO
