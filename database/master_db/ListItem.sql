SET IDENTITY_INSERT ListItem ON;
GO

INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (1, N'MTK 02.02', N'Mã thống kê 02.02' , 2, '/2/2/', 2, 0, '', 1, '', '', '', '', '/00002/00001/');
INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (2, N'MTK02', N'Mã thống kê 02' , 0, '/2/', 1, 1, '', 1, '', '', '', '', '/00002/');
INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (3, N'MTK02.01', N'Mã thống kê 02.01' , 2, '/2/1/', 2, 0, '', 1, '', '', '', '', '/00002/00002/');
INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (4, N'MTK01.01', N'Mã thống kê 01.01' , 5, '/1/1/', 2, 0, '', 1, '', '', '', '', '/00001/00001/');
INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (5, N'MTK01', N'Mã thống kê 01' , 0, '/1/', 1, 1, '', 1, '', '', '', '', '/00001/');
INSERT INTO ListItem(ID, ListItemCode, ListItemName, ParentID, VyCodeID, Grade, IsParent, Description, ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, SortVyCodeID) VALUES (6, N'MTK01.02', N'Mã thống kê 01.02' , 5, '/1/2/', 2, 0, '', 1, '', '', '', '', '/00001/00002/');
GO

SET IDENTITY_INSERT ListItem OFF;
GO
