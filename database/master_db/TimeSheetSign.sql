SET IDENTITY_INSERT TimeSheetSign ON;
GO

INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(1, N'LT(N)', N'Làm thêm ngày thường ban ngày', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(2, N'Ô1/2', N'Ốm, điều dưỡng (nửa ngày)', 75, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(3, N'-', N'Lương thời gian (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(4, N'TS1/2', N'Thai sản (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(5, N'N', N'Ngừng việc', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(6, N'H1/2', N'Hội nghị, học tập (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(7, N'LTN(N)', N'Làm thêm ngày thứ 7, chủ nhật ban ngày', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(8, N'LTL(Đ)', N'Làm thêm nghỉ lễ, tết ban đêm', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(9, N'KL', N'Nghỉ không lương', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(10, N'+ ', N'Lương thời gian (cả ngày)', 100, 1, 1, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(11, N'NB', N'Nghỉ bù', 100, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(12, N'Ô', N'Ốm, điều dưỡng', 75, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(13, N'H', N'Hội nghị, học tập', 100, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(14, N'T', N'Tai nạn', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(15, N'LTN(Đ)', N'Làm thêm ngày thứ 7, chủ nhật ban đêm', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(16, N'P1/2', N'Nghỉ phép (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(17, N'P', N'Nghỉ phép', 100, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(18, N'LĐ1/2', N'Lao động nghĩa vụ (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(19, N'LTL(N)', N'Làm thêm nghỉ lễ, tết ban ngày', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(20, N'LĐ', N'Lao động nghĩa vụ', 100, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(21, N'Cô1/2', N'Con ốm (nửa ngày)', 75, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(22, N'N1/2', N'Ngừng việc (nửa ngày)', 0, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(23, N'NB1/2', N'Nghỉ bù (nửa ngày)', 100, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(24, N'T1/2', N'Tai nạn (nửa ngày)', 0, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(25, N'LT(Đ)', N'Làm thêm ngày thường ban đêm', 0, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(26, N'KL1/2', N'Nghỉ không lương (nửa ngày)', 0, 1, 0, 0, 1);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(27, N'Cô', N'Con ốm', 75, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(28, N'TS', N'Thai sản', 100, 1, 0, 0, 0);
INSERT INTO TimeSheetSign(ID, TimeSheetSignCode, TimeSheetSignName, SalaryRate, ActiveStatus, IsDefault, IsSystem, IsHalfDay)VALUES(29, N'SP', N'Lương sản phẩm', 0, 1, 0, 0, 0);
GO

SET IDENTITY_INSERT TimeSheetSign OFF;
GO
