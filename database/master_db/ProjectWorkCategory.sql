SET IDENTITY_INSERT ProjectWorkCategory ON;
GO

INSERT INTO ProjectWorkCategory (ID, ProjectWorkCategoryCode, ProjectWorkCategoryName,  ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Description) VALUES (1, N'NCC', N'Nhà chung cư', 0, '', '', '', '', '');
INSERT INTO ProjectWorkCategory (ID, ProjectWorkCategoryCode, ProjectWorkCategoryName,  ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Description) VALUES (2, N'DQL', N'Đường quốc lộ', 0, '', '', '', '', '');
INSERT INTO ProjectWorkCategory (ID, ProjectWorkCategoryCode, ProjectWorkCategoryName,  ActiveStatus, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Description) VALUES (3, N'VPCT', N'Văn phòng cho thuê', 0, '', '', '', '', '');
GO

SET IDENTITY_INSERT ProjectWorkCategory OFF;
GO
